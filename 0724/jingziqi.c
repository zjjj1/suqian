#include <stdio.h>
#define size 3
#define false 0
#define true 1


void IB(char (*Board)[size])
{
    for(int i=0;i<size;i++)
    {
        for(int j=0;j<size;j++)
        {
            Board[i][j]=' ';
        }
        
    }
}

void PB(char (*Board)[size])
{
    printf("\n");
    for(int i=0;i<size;i++)
    {
        for(int j=0;j<size;j++)
        {
        printf("%c ",Board[i][j]);
        if (j != size-1)
        printf("|");
        }
        printf("\n");
        if( i !=size-1)
        printf("--|--|--\n");
    }
}

int MMove(char(*Board)[size],int row,int column,int player)  
{
    if(row<0||row>=size||column<0||column>=size)
    {
        printf("invalid place!please try again!\n");
        return false;
    }
    if(Board[row][column]!=' ')
    {
        printf("this place has been placed!try again");
        return false;
    }
    Board[row][column]=player;
    return true;
}     


int CIW(char(*Board)[size],char player)
{
    for(int i=0;i<size;i++)
    {
        if(Board[i][0]==player&&Board[i][1]==player&&Board[i][2]==player)
        return true;
    }
    for(int i=0;i<size;i++)
    {
        if(Board[0][i]==player&&Board[1][i]==player&&Board[2][i]==player)
        return true;
    }
    if(Board[0][0]==player&&Board[1][1]==player&&Board[2][2]==player)
        return true;
    if(Board[0][2]==player&&Board[1][1]==player&&Board[2][0]==player)
        return true;
}

int CID(char(*Board)[size])
{
    for(int i=0;i<size;i++)
    {
        for(int j=0;j<size;j++)
        {
            if(Board[i][j]==' ')
            return false;
        }
    }
    return true;
}

int main()
{   
    char Board[size][size]={0};
    IB(Board);

    char player='X';
    while(1)
    {
        PB(Board);
        printf("player %c please enter:",player);
        int row=0,column=0;
        scanf("%d %d",&row,&column);
        if(MMove(Board,row-1,column-1,player)==false)
        {
            continue;
        }

        if(CIW(Board,player)==true)
        {
            PB(Board);
            printf("you win\n");
            break;
        }

        if(CID(Board)==true)
        {
            PB(Board);
            printf("you not win!\n");
            break;
        }
        player=(player=='X')?'O':'X';

    }
    return 0;
    
}