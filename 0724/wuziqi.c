#include <stdio.h>
#define size 15
#define false 0
#define true 1

void IB(char (*Board)[size])
{
    for(int i=0;i<size;i++)
    {
        for(int j=0;j<size;j++)
        {
            Board[i][j]='-';
        }
        
    }
}

void PB(char (*Board)[size])
{
    printf("      ");
    for(int i=0;i<size;i++)
    {
      printf(" %4d ",i+1);  
    }
    printf("\n");
    
    for(int i=0;i<size;i++)
    {
        printf(" %4d ",i+1); 
        for(int j=0;j<size;j++)
        {
        printf(" %4c ",Board[i][j]);
        }
         printf("\n");
       
    }
}

int MMove(char(*Board)[size],int row,int column,int player)  
{
    if(row<0||row>=size||column<0||column>=size)
    {
        printf("invalid place!please try again!\n");
        return false;
    }
    if(Board[row][column]!='-')
    {
        printf("this place has been placed!try again\n");
        return false;
    }
    Board[row][column]=player;
    return true;
}     

int CIW(char(*Board)[size],int row,int column,char player)
{
    int direction[4][2]=
    {
        {1,0},
        {0,1},
        {1,1},
        {-1,1}
    };

    for(int i=0;i<4;i++)
    {
        int count=1;
        int dx= row + direction[i][0];
        int dy= column+ direction[i][1];
 
        while(dx>=0&&dx<size&&dy>=0&&dy<size&&Board[dx][dy]==player)
        {
            count++;
            dx=dx+direction[i][0];
            dy=dy+direction[i][1];

        }
        dx=row-direction[i][0];
        dy=column-direction[i][1];
        while(dx>=0&&dx<size&&dy>=0&&dy<size&&Board[dx][dy]==player)
        {
            count++;
            dx=dx-direction[i][0];
            dy=dy-direction[i][1];


        }
        if(count>=5)
        {
            return true;
        }

    }
    return false;
}

int CID(char(*Board)[size])
{
    for(int i=0;i<size;i++)
    {
        for(int j=0;j<size;j++)
        {
            if(Board[i][j]=='-')
            return false;
        }
    }
    return true;
}

int main()
{
    char Board[size][size]={0};
    IB(Board);

    char player='X';
    while(1)
    {
        PB(Board);
        printf("player %c please enter:",player);
        int row=0,column=0;
        scanf("%d %d",&row,&column);
        if(MMove(Board,row-1,column-1,player)==false)
        {
            continue;
        }

        if(CIW(Board,row-1,column-1,player)==true)
        {
            PB(Board);
            printf("        %c win!\n",player);
            break;
        }

        if(CID(Board)==true)
        {
            PB(Board);
            printf("you not win!\n");
            break;
        }
        player=(player=='X')?'O':'X';
    }
    return 0;
    
}