#include<stdio.h>

typedef int (*MFunc)(int,int);


int add(int a,int b)
{
    return a+b;
}

int sub(int a,int b)
{
    return a-b;
}

MFunc GMFunc(char symbol)
{
    switch(symbol)

    {
        case'+':return add;
        case'-':return sub;
        default:
        return NULL;
    }
}

int main ()
{
    char symbol='+';
    MFunc func=GMFunc(symbol);
    if(func!=NULL)
    printf("%d\n",func(1,2));
    return 0;
}