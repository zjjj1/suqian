#include<stdio.h>
#define size 10
int main()
{
    int a[size][size]={0};
    // a[0][0]=1;
    // a[1][0]=1;
    // a[2][1]=1;
    for(int i=0;i<size;i++)
    {
        a[i][0]=1;
        a[i][i]=1;
    }

    for (int i=2;i<size;i++)
    {
        for(int j=1;j<=i-1;j++)
        {
            a[i][j]=a[i-1][j]+a[i-1][j-1];
        }
    }

    for (int i=0;i<size;i++)
    {
        for(int j=0;j<=size-i;j++)
        {
            printf("  ");
        }
        for(int j=0;j<=i;j++)
        {
            printf("%3d ",a[i][j]);
        }
        printf("\n");
    }
    printf("\n");

    return 0;
}