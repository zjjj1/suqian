#ifndef STACK_H
#define STACK_H
#define STACK_MAX_SIZE 100
#define ElementStack int
struct StackPrivate;
class Stack
{
public:
    Stack();
    int IsStackEmpty();
    int IsStackFull();
    void PushStack(int element);
    ElementStack* PopStack();
    ElementStack* GetTopStack();
    ~Stack();
private:
    StackPrivate*stack;
};
#endif
