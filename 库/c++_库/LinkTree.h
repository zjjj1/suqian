#ifndef LINKTREE_H
#define LINKTREE_H
#define elementLT int

struct LTNode;
//不能封装，封装后类内函数不能自由调用参数
struct LinkTreePrivate
{
    LTNode* root;
};
class LinkTree
{
public:
    LinkTree();
    LTNode* CreateLTNode(elementLT ele);
    void ConnectBrach(LTNode* parent, LTNode* child);
    void DisconnectBrach(LTNode* parent, LTNode* child);
    void TravelLTree(LTNode* root, int deepth);
    LTNode* FindLTreeNode(LTNode* root, elementLT ele);
    int GetTreeHeight(LTNode* root);
    void FreeLTree(LTNode* root);
    ~LinkTree();
    LinkTreePrivate*tree;
};
#endif
