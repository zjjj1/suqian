#ifndef __THREADPOOL_H__
#define __THREADPOOL_H__

struct StdThreadPoolPrivate;
class StdThreadPool
{
public:
    StdThreadPool(int max_task_queue_num, int min_thrd_num, int max_thrd_num);
    ~StdThreadPool();
    void ThreadP_AddTask(void *(*func)(void *), void *arg);
private:
    StdThreadPoolPrivate *p;

};

#endif
