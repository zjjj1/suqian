#include"DynamicArray.h"
#include<stdlib.h>
#include<stdio.h>
struct DyPrivate
{ 
    DyPrivate(int size)
    {
        this->size = size;
        len = 0;
        dp=new elementDy[size *sizeof(elementDy)];
        if (dp == NULL)
        {
            printf("InitDyArray new error\n");
            //qDebug() << "InitDyArray new error";
        }
    }
    ~DyPrivate()
    {
        delete[] dp; 
dp=NULL;
    }
    elementDy* dp;
    int size;
    int len;
};
//初始化动态数组
Dy::Dy(int size):a(new DyPrivate(size))
{      
}
void Dy::TravelDyArray()
{
    for (int i = 0; i < a->len; i++)
    {
        printf("%d ", a->dp[i]);
        //qDebug() << a->dp[i];
    }
}

//动态数组重新分配空间
int Dy::ReallocDyArray()
{
    elementDy* temp = a->dp;
    a->dp = new elementDy[sizeof(elementDy) * a->size * 2];
    if (a->dp == NULL)
    {
        printf("ReallocDyArray error\n");
      //  qDebug() <<"ReallocDyArray error" ;
        return 0;
    }
    for (int i = 0; i < a->len; i++)
    {
        a->dp[i] = temp[i];
    }
    a->size *= 2;
    delete[] temp;
    return 1;
}
//动态数组尾插元素
int Dy::InsertDyArray(elementDy ele)
{
    if (a->len == a->size)
        if (!ReallocDyArray())
        {
            printf("不能包含更多元素了\n");
         //   qDebug() << "不能包含更多元素了";
            return 0;
        }
    a->dp[a->len] = ele;
    a->len++;
    return 1;
}
//释放动态数组
Dy::~Dy()
{
delete a;
a=NULL;
}
