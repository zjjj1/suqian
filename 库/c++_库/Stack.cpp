#include<stdio.h>
#include"Stack.h"

struct StackPrivate
{
    ElementStack data[STACK_MAX_SIZE];
    int top;
};
//初始化
Stack::Stack():stack(new StackPrivate) 
{
    stack->top = -1;
}
//栈是否为空
int Stack::IsStackEmpty() 
{
    return stack->top == -1;
}
//栈是否已满
int Stack::IsStackFull() 
{
    return stack->top == STACK_MAX_SIZE - 1;
}
//入栈
void Stack::PushStack(int element) 
{
    if (IsStackFull())
    {
        printf("栈已满，不能再添加元素\n");
        //qDebug() <<"栈已满，不能再添加元素" ;
    }
    else 
    {
        stack->top++;
        stack->data[stack->top] = element;
    }
}
//出栈
ElementStack* Stack::PopStack()
{
    if (IsStackEmpty())
    {
        printf("栈为空，不能再出栈了\n");
        //qDebug() << "栈为空，不能再出栈了";
        return NULL; // 返回一个特定值表示出错
    }
    else 
    {
        return &stack->data[stack->top--];
    }
}
//获取栈顶元素指针
ElementStack* Stack::GetTopStack()
{
    if (IsStackEmpty())
    {
        printf("栈为空,无法获取栈顶元素\n");
        //qDebug() << "栈为空,无法获取栈顶元素";
        return NULL; // 返回一个特定值表示出错
    }
    else 
    {
        return &stack->data[stack->top];
    }
}
Stack::~Stack()
{
    delete stack;
    stack=NULL;
}
