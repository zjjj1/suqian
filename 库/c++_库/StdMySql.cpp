#include "StdMySql.h"


struct StdMySqlPrivate
{
    MYSQL mysql;
    bool IsConnected;
};

StdMySql::StdMySql():p(std::make_shared<StdMySqlPrivate>())
{
    mysql_init(&p->mysql);
    p->IsConnected = false;
}

StdMySql::~StdMySql()
{
    if (p->IsConnected == true)
    {
        mysql_close(&p->mysql);
    }
}

bool StdMySql::Connect(const char *host, const char *userName, const char *passWd, const char *db, unsigned int port)
{
    if (p->IsConnected == true)
    {
        mysql_close(&p->mysql);
        mysql_init(&p->mysql);
        p->IsConnected = false;
    }
    if (mysql_real_connect(&p->mysql, host, userName, passWd, db, port, NULL, 0) == NULL)
    {
        return false;
    }
    else
    {
        p->IsConnected = true;
        return true;
    }
}

bool StdMySql::Execute(const std::string sql)
{
    if (p->IsConnected == false)
    {
        std::cout << "Execute: 数据库未连接！" << std::endl;
        return false;
    }
    if (mysql_real_query(&p->mysql, sql.c_str(), sql.size()) == 0)
    {
        return true;
    }
    else
    {
        std::cout << mysql_error(&p->mysql) << std::endl;
        return false;
    }
}

std::vector<MySqlStringArray> StdMySql::GetSelectResult(const std::string sql)
{
    if (p->IsConnected == false)
    {
        std::cout << "GetSelectResult: 数据库未连接！" << std::endl;
        return std::vector<MySqlStringArray>();
    }
    if (Execute(sql) == false)
    {
        std::cout << "GetSelectResult: 查询语句执行失败！" << std::endl;
        return std::vector<MySqlStringArray>();
    }
    else
    {
        if (MYSQL_RES *result = mysql_store_result(&p->mysql))
        {
            std::vector<MySqlStringArray> Array;
            int count = mysql_field_count(&p->mysql);
            while (MYSQL_ROW sql_row = mysql_fetch_row(result))
            {
                MySqlStringArray rowData;
                for (int i = 0; i < count; i++)
                {
                    rowData.push_back(sql_row[i]);
                }
                Array.push_back(rowData);
            }
            return Array;
        }
        else
        {
            std::cout << "GetSelectResult: 获取数据失败！" << std::endl;
            return std::vector<MySqlStringArray>();
        }
    }
}

bool StdMySql::ExcuteInTransaction(const MySqlStringArray &arr)
{
    // 开启手动事物模式
    if (mysql_autocommit(&p->mysql, false) != 0)
    {
        std::cout << "启动手动事物失败!" << std::endl;
        return false;
    }
    for (auto &cmd : arr)
    {
        // 依次执行命令
        if (Execute(cmd) == false)
        {
            std::cout << "执行失败！" << std::endl;
            // 执行失败,回滚
            if (mysql_rollback(&p->mysql) != 0)
            {
                std::cout << "回滚操作失败！" << std::endl;
            }
            mysql_autocommit(&p->mysql, true);
            return false;
        }
    }
    // 执行完成，提交
    if (mysql_commit(&p->mysql) != 0)
    {
        // 提交失败
        mysql_autocommit(&p->mysql, true);
        std::cout << "提交失败！" << std::endl;
        return false;
    }
    mysql_autocommit(&p->mysql, true);
    return true;
}

bool StdMySql::IsConnected()
{
    if (p->IsConnected == false)
    {
        return false;
    }
    else
    {
        if (mysql_ping(&p->mysql) == 0)
        {
            std::cout << "ping服务器成功" << std::endl;
            return true;
        }
        else
        {
            std::cout << "ping服务器失败" << std::endl;
            p->IsConnected = false;
            return false;
        }
    }
}

bool StdMySql::IsNULL()
{
    return !p->IsConnected;
}
  

struct StdMySqlPoolPrivate
{
    StdMySqlPoolPrivate()
    {
        host = "";
        userName = "";
        passwd = "";
        databaseName = "";
        port = 0;
        MaxCount = 0;
        ConnectCount = 0;
    };
    std::string host;
    std::string userName;
    std::string passwd;
    std::string databaseName;
    unsigned int port;
    int MaxCount;                  // 初始连接数量
    int ConnectCount;              // 正真的连接数量
    std::queue<std::shared_ptr<StdMySql>> SqlQueue; // 对象队列
    std::mutex poolMutex;          // 连接池锁
};

StdMySqlPool::StdMySqlPool() : p(new StdMySqlPoolPrivate)
{
}

StdMySqlPool *StdMySqlPool::GlobalInstance()
{
    static StdMySqlPool stdmysqlpool;
    return &stdmysqlpool;
}

void StdMySqlPool::SetParams(int num, const char *host, const char *userName, const char *passWd, const char *db, unsigned int port)
{
    p->host = host;
    p->userName = userName;
    p->passwd = passWd;
    p->port = port;
    p->databaseName = db;
    p->MaxCount = num;

    for (int i = 0; i < num; i++)
    {
        auto mysql = std::make_shared<StdMySql>();
        // 如果连接失败
        if (mysql->Connect(host, userName, passWd, db, port) == false)
        {
            std::cout<<"连接失败"<<std::endl;
            continue;
        }
        p->ConnectCount++;
        p->SqlQueue.push(mysql);
    }
}

// mysql 会自动关闭连接
// 使用mysql对象之前。要判断连接池中的对象是否有效
std::shared_ptr<StdMySql> StdMySqlPool::GetMysqlObj()
{
    std::lock_guard<std::mutex> lock(p->poolMutex);

    // 队列的第一个连接已经断开了
    while (p->SqlQueue.empty() == false && p->SqlQueue.front()->IsConnected() == false)
    {
        p->SqlQueue.pop();
        p->ConnectCount--;
    }

    // 已经达到了mysql连接数量最大上限
    if (p->SqlQueue.empty() == true)
    {
        if (p->ConnectCount == p->MaxCount)
        {
            std::cout << "已经达到了mysql连接数量最大上限" << std::endl;
            return nullptr;
        }
        else
        {
            std::shared_ptr<StdMySql> mysql;
            // 如果连接失败
            if (mysql->Connect(p->host.c_str(), p->userName.c_str(), p->passwd.c_str(), p->databaseName.c_str(), p->port) == false)
            {
                std::cout << "连接失败" << std::endl;
                return nullptr;
            }
            p->ConnectCount++;
            return mysql;
        }
    }
    else
    {
        auto mysql = p->SqlQueue.front();
        p->SqlQueue.pop();
        return mysql;
    }
}

void StdMySqlPool::CloseMysql(std::shared_ptr<StdMySql> &mysql)
{
    std::lock_guard<std::mutex> lock(p->poolMutex);
    if(mysql->IsNULL() == false)
    {
        p->SqlQueue.push(mysql);
    }
}

StdMySqlPool::~StdMySqlPool()
{
    while(p->SqlQueue.empty() == false)
    {
        p->SqlQueue.pop();
    }
    delete p;
}



