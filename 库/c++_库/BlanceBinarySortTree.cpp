#include <stdio.h>
#include <stdlib.h>
#include"BlanceBinarySortTree.h"

struct BBSTreeNode
{
    elementBBSTree data;
    struct BBSTreeNode* left;
    struct BBSTreeNode* right;
    struct BBSTreeNode* parent;
};
BBSTree::BBSTree():tree(new BBSTreePrivate)
{
    tree->root = NULL;
}
// 右旋
BBSTreeNode* BBSTree::RotateRight(BBSTreeNode* node)
{
    BBSTreeNode* t = node->left;
    node->left = t->right;
    if (t->right != NULL)
        t->right->parent = node;
    t->right = node;
    t->parent = node->parent;
    node->parent = t;
    return t;
}
// 左旋
BBSTreeNode* BBSTree::RotateLeft(BBSTreeNode* node)
{
    BBSTreeNode* t = node->right;
    node->right = t->left;
    if (t->left != NULL)
        t->left->parent = node;
    t->left = node;
    t->parent = node->parent;
    node->parent = t;
    return t;
}
// 左右旋
BBSTreeNode* BBSTree::RotateLeftRight(BBSTreeNode* node)
{
    node->left = RotateLeft(node->left);
    return RotateRight(node);
}
// 右左旋
BBSTreeNode* BBSTree::RotateRightLeft(BBSTreeNode* node)
{
    node->right = RotateRight(node->right);
    return RotateLeft(node);
}
int BBSTree::GetBBSTreeHeight(BBSTreeNode* root) 
{
    if (root == NULL)
        return 0;
    int leftheight = GetBBSTreeHeight(root->left);
    int rightheight = GetBBSTreeHeight(root->right);
    return (leftheight > rightheight ? leftheight : rightheight) + 1;
}
BBSTreeNode* BBSTree::CreateBBSTreeNode(elementBBSTree ele)
{
    BBSTreeNode* newnode = new BBSTreeNode;
    if (newnode == NULL)
    {
        printf("CreateBBSTreeNode new error\n");
        // qDebug()<<"CreateBBSTreeNode new error";
        return NULL;
    }
    newnode->data = ele;
    newnode->left = NULL;
    newnode->parent = NULL;
    newnode->right = NULL;
    return newnode;
}

//插入int类型时，比较的回调函数
int BBSTree::FuncInsertIntBBSTreeNode(int ele1, int ele2)
{
    if (ele1 < ele2)
        return 1;
    else
        return 0;
}
// 二叉平衡树的插入
BBSTreeNode* BBSTree::InsertDataBBSTree(BBSTreeNode* root, elementBBSTree data, int (*func)(elementBBSTree, elementBBSTree))
{
    if (root == NULL)
    {
        root = CreateBBSTreeNode(data);
        return root;
    }
    if (func(data, root->data)) // 如果当前值比node的值小
    {
        root->left = InsertDataBBSTree(root->left, data, func);
        root->left->parent = root;

        if (GetBBSTreeHeight(root->left) - GetBBSTreeHeight(root->right) > 1) // 如果左右高度差大于1，进行平衡操作
        {
            if (func(data, root->left->data)) // 如果是左左树
            {
                root = RotateRight(root); // 右旋
            }
            else // 或者是左右树
            {
                root = RotateLeftRight(root); // 左右旋
            }
        }
    }
    else // 或者当前值比node的值大
    {
        root->right = InsertDataBBSTree(root->right, data, func);
        root->right->parent = root;
        if (GetBBSTreeHeight(root->right) - GetBBSTreeHeight(root->left) > 1) // 如果左右高度差大于1，进行平衡操作
        {
            if (func(data, root->right->data)) // 如果是右左树
            {
                root = RotateRightLeft(root); // 右左旋
            }
            else // 或者是右右树
            {
                root = RotateLeft(root); // 左旋
            }
        }
    }
    return root;
}
//调用上方的函数插入元素,该函数的意义在于可以让调用者无需传二级指针
void BBSTree::InsertElementBBSTree(elementBBSTree ele, int (*func)(elementBBSTree, elementBBSTree))
{
    tree->root = InsertDataBBSTree(tree->root, ele, func);
}
void BBSTree::FreeBBSTreeNode(BBSTreeNode* root)
{
    if (root == NULL)
        return;
    FreeBBSTreeNode(root->left);
    FreeBBSTreeNode(root->right);
  //  delete node->data;
    delete root;
    root = NULL;
}
//前序遍历回调
void BBSTree::FuncPrevIntBBSTree(BBSTreeNode* node)
{
    if (node != NULL)
    {
        printf("%d ", node->data);
        //qDebug() << node->data;
        FuncPrevIntBBSTree(node->left);
        FuncPrevIntBBSTree(node->right);
    }
}
//中序遍历回调
void BBSTree::FuncMidIntBBSTree(BBSTreeNode* node)
{
    if (node != NULL)
    {
        FuncMidIntBBSTree(node->left);
        printf("%d ", node->data);
        //qDebug() << node->data;
        FuncMidIntBBSTree(node->right);
    }
}
//后序遍历回调
void BBSTree::FuncPostIntBBSTree(BBSTreeNode* node)
{
    if (node != NULL)
    {
        FuncPostIntBBSTree(node->left);
        FuncPostIntBBSTree(node->right);
        printf("%d ", node->data);
        //qDebug() << node->data;
    }
}
//遍历树
void BBSTree::TravelBBSTree(void (*func)(BBSTreeNode*))
{
    func(tree->root);
}
//下方函数比较相等时的回调
int BBSTree::FuncFindIntBBSTree(elementBBSTree ele1, elementBBSTree ele2)
{
    if (ele1 == ele2)
        return 1;
    else if (ele1 < ele2)
        return 2;
    else
        return 0;
}
BBSTreeNode* BBSTree::FindBBSTreeNode(BBSTreeNode* root, elementBBSTree ele, int(*func)(elementBBSTree, elementBBSTree))
{
    if (root == NULL)
        return NULL;
    if (func(root->data, ele) == 1)
    {
        return root;
    }
    else if (func(root->data, ele) == 2)
        return FindBBSTreeNode(root->right, ele, func);
    else
        return FindBBSTreeNode(root->left, ele, func);
}
void BBSTree::DeleteBBSTreeElememt(elementBBSTree ele, int (*func)(elementBBSTree, elementBBSTree))
{
    BBSTreeNode* targetNode = FindBBSTreeNode(tree->root, ele, func);
    if (targetNode == NULL)
        return;
    BBSTreeNode* parentNode = targetNode->parent;
    if (targetNode->left == NULL && targetNode->right == NULL)
    {
        if (parentNode == NULL)
            tree->root = NULL;
        else if (parentNode->left == targetNode)
            parentNode->left = NULL;
        else
            parentNode->right = NULL;
        delete targetNode;
        targetNode=NULL;
    }
    else if (targetNode->right == NULL)
    {
        if (parentNode == NULL)
            tree->root = targetNode->left;
        else if (parentNode->left == targetNode)
            parentNode->left = targetNode->left;
        else
            parentNode->right = targetNode->left;
        targetNode->left->parent = parentNode;
        delete targetNode;
        targetNode=NULL;
    }
    else if (targetNode->left == NULL)
    {
        if (parentNode == NULL)
            tree->root = targetNode->right;
        else if (parentNode->left == targetNode)
            parentNode->left = targetNode->right;
        else
            parentNode->right = targetNode->right;
        targetNode->right->parent = parentNode;
        delete targetNode;
        targetNode=NULL;
    }
    else
    {
        BBSTreeNode* minnode = targetNode->right;
        while (minnode->left != NULL)
        {
            minnode = minnode->left;
        }
        elementBBSTree temp = targetNode->data;
        targetNode->data = minnode->data;
        minnode->data = temp;

        BBSTreeNode* minnodeParent = minnode->parent;
        if (minnodeParent->left == minnode)
            minnodeParent->left = minnode->right;
        else
            minnodeParent->right = minnode->right;

        if (minnode->right != NULL)
            minnode->right->parent = minnodeParent;
        delete minnode;
        minnode=NULL;
    }
    int balanceFactor = GetBBSTreeHeight(tree->root->left) - GetBBSTreeHeight(tree->root->right);
    if (balanceFactor > 1)
    {
        if (GetBBSTreeHeight(tree->root->left->left) >= GetBBSTreeHeight(tree->root->left->right))
            tree->root=RotateRight(tree->root);
        else
            tree->root = RotateLeftRight(tree->root);
    }
    else if (balanceFactor < -1)
    {
        if (GetBBSTreeHeight(tree->root->right->right) >= GetBBSTreeHeight(tree->root->right->left))
            tree->root = RotateLeft(tree->root);
        else
            tree->root = RotateRightLeft(tree->root);
    }
}
//获取叶子结点数量
int BBSTree::GetLeafNumBBSTree(BBSTreeNode* root)
{
    if (root == NULL)
        return 0;
    if (root->left == NULL && root->right == NULL)
        return 1;
    int leftNum = GetLeafNumBBSTree(root->left);
    int rightNum = GetLeafNumBBSTree(root->right);
    return leftNum + rightNum;
}
BBSTree::~BBSTree()
{

}

