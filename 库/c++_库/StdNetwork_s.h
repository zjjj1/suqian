#ifndef _StdNetwork_h_
#define _StdNetwork_h_

#include <stddef.h>

class NetworkServer
{
public:
    NetworkServer() {}
    ~NetworkServer() {}
    int ServerAccept();
    virtual void ServerSend(void *ptr, size_t size, int ClientSock){}
    virtual void ServerRecv(void *ptr, size_t size, int ClientSock){}
protected:
    int sock;
};


void GetUpp(char* str);

NetworkServer *CreatServer(char *promotion, const char *ip, const int &port);


#endif
