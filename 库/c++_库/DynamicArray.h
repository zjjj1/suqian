#ifndef DYNAMICARRAY_H
#define DYNAMICARRAY_H

#define elementDy int
struct DyPrivate;
class Dy
{
public:
    Dy(int size);
    void TravelDyArray();
    int ReallocDyArray();
    int InsertDyArray(elementDy ele);
    ~Dy();
private:
    DyPrivate*a;
};

#endif
