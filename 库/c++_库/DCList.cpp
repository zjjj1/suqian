#include<stdio.h>
#include<stdlib.h>
#include"DCList.h"
struct DCListPrivate
{
    int len;
    DCNode* head;
    DCNode* tail;
};
//初始化链表
DCList::DCList():list(new DCListPrivate)
{
    list->head = NULL;
    list->tail = NULL;
    list->len = 0;
}
//创建结点
DCNode* DCList::CreateDCNode(elementDCList ele)
{
    DCNode* newnode = new DCNode;
    if (newnode == NULL)
    {
        printf("CreateDCNode new error\n");
        //qDebug() <<"CreateDCNode new error";
        return NULL;
    }
    newnode->next = NULL;
    newnode->prev = NULL;
    newnode->data = ele;
    return newnode;
}
//尾插值
void DCList::InsertTailDCList(elementDCList ele)
{
    DCNode* newnode = CreateDCNode(ele);
    if (newnode == NULL)
    {
        printf("InsertTailDCList CreateDCNode error\n");
        //qDebug() << "InsertTailDCList CreateDCNode error";
        return;
    }
    if (list->len == 0)
    {
        list->head = newnode;
        list->tail = newnode;
    }
    else
    {
        list->tail->next = newnode;
        newnode->prev = list->tail;
        list->tail = newnode;

        list->tail->next = list->head;
        list->head->prev = list->tail;
    }
    list->len++;
}
//头插值
void DCList::InsertHeadDCList(elementDCList ele)
{
    InsertTailDCList(ele);
    if (list->len > 1)
    {
        list->tail = list->tail->prev;
        list->head = list->head->prev;
    }
}

//遍历
void DCList::TravelDCList()
{
    printf("双向循环链表的长度:%d\n", list->len);
    //qDebug() << "双向循环链表的长度 : "<<list->len;
    if (list->len == 0)
        return;
    printf("正向遍历:");
    //qDebug()<<"正向遍历:";
    DCNode* travel = list->head;
    while (travel != list->tail)
    {
        printf("%d ", travel->data);
        travel = travel->next;
    }
    printf("%d ", list->tail->data);
    //qDebug()<<list->tail->data;
    printf("\n");
    printf("逆向遍历:");
    //qDebug() << "逆向遍历";
    travel = list->tail;
    while (travel != list->head)
    {
        printf("%d ", travel->data);
        //qDebug() << travel->data;
        travel = travel->prev;
    }
    printf("%d ", list->head->data);
    //qDebug() << travel->data;
    printf("\n");
}
//根据下标删除元素
void DCList::RemoveByIndexDCList(int index)
{
    if (index < 0 || index >= list->len)
    {
        printf("RemoveByIndexDCList 无效的下标\n");
        //qDebug() <<"RemoveByIndexDCList 无效的下标" ;
        return;
    }
    if (index == 0)
    {
        if (list->len == 1)
        {
            free(list->head);
            list->tail = NULL;
            list->head = NULL;
            list->len = 0;
            return;
        }
        DCNode* freenode = list->head;
        list->head = list->head->next;
        list->head->prev = list->tail;
        list->tail->next = list->head;
        delete freenode;
        freenode = NULL;
        list->len--;
        return;
    }
    if (index == list->len - 1)
    {
        DCNode* freenode = list->tail;
        list->tail = list->tail->prev;
        list->tail->next = list->head;
        list->head->prev = list->tail;
        delete freenode;
        list->len--;
        return;
    }
    DCNode* travel = list->head;
    while (index > 0)
    {
        travel = travel->next;
        index--;
    }
    travel->next->prev = travel->prev;
    travel->prev->next = travel->next;
    list->len--;
    delete travel;
    travel = NULL;
}
//根据值删除元素
void DCList::RemoveByElementDCList(elementDCList ele)
{
    int index = FindFirstByElementDCList(ele);
    while (index != -1)
    {
        RemoveByIndexDCList(index);
        index = FindFirstByElementDCList(ele);
    }
}
//寻找链表中第一个与该值相等的下标
int DCList::FindFirstByElementDCList(elementDCList ele)
{
    int count = 0;
    DCNode* travel = list->head;
    while (travel != list->tail)
    {
        if (travel->data == ele)
        {
            return count;
        }
        count++;
        travel = travel->next;
    }
    if (list->tail != NULL && list->tail->data == ele)
    {
        return count;
    }
    return -1;
}
//释放链表
DCList::~DCList()
{   
    while (list->head != NULL)
    {
        if (list->len == 0)
            return;
        DCNode* freenode = list->head;
        list->head = list->head->next;
        delete freenode;
        freenode=NULL;
        list->len--;
    }
}
