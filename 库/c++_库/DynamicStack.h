#ifndef DYNAMICSTACK_H
#define DYNAMICSTACK_H

#define ElementDyStack int
struct DyStackPrivate;
class DyStack
{
public:
    DyStack(int maxlen);
    void PushDy(ElementDyStack ele);
    ElementDyStack* PopDy();   
    void ReallocDyStack();
    ~DyStack();
private:
    DyStackPrivate*s;
};
#endif
