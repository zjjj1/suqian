#ifndef DCList_H
#define DCList_H

#define elementDCList int
struct DCNode
{
    elementDCList data;
    DCNode* next;
    DCNode* prev;
};
struct DCListPrivate;
class DCList
{
public:
    DCList();
    DCNode* CreateDCNode(elementDCList ele);
    void TravelDCList();
    void InsertTailDCList(elementDCList ele);
    void InsertHeadDCList(elementDCList ele);
    void RemoveByIndexDCList(int index);
    void RemoveByElementDCList(elementDCList ele);
    int FindFirstByElementDCList(elementDCList ele);
    ~DCList();
private:
    DCListPrivate*list;
};
#endif
