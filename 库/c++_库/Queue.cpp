#include<stdio.h>
#include<stdlib.h>
#include"Queue.h"
struct QueuePrivate
{
    QueuePrivate()
    {
        front = rear = -1;
    }
    int data[QUEUE_MAX_SIZE];
    int front, rear;
};
//初始化队列
Queue::Queue():q(new QueuePrivate)
{
   
}
//队列是否为空
int Queue::IsQueueEmpty() 
{
    return (q->front == -1);
}
//队列是否已满
int Queue::IsQueueFull() 
{
    return ((q->rear + 1) % QUEUE_MAX_SIZE == q->front);
}
//入队
void Queue::InsertQueue(int num)
{
    if (IsQueueFull())
    {
        printf("队列已满，插入失败\n");
        //qDebug() <<"队列已满，插入失败" ;
        return;
    }
    if (q->front == -1)
        q->front = 0;
    q->rear = (q->rear + 1) % QUEUE_MAX_SIZE;
    q->data[q->rear] = num;
}
//删除一个队尾元素并返回这个元素的值，当删除的为最后一个元素时，此时队列为空，将front和rear重新置为-1
int Queue::RemoveQueue() 
{
    int num;
    if (IsQueueEmpty())
    {
        printf("队列已空，删除失败\n");
        //qDebug()<<"队列已空，删除失败";
        return -1;
    }
    num = q->data[q->front];
    if (q->front == q->rear)
        q->front = q->rear = -1;
    else
        q->front = (q->front + 1) % QUEUE_MAX_SIZE;
    return num;
}
//遍历队列
void Queue::TravelQueue()
{
    if (IsQueueEmpty()) 
    {
        printf("队列为空，遍历失败\n");
        //qDebug()<<"队列为空,遍历失败";
        return;
    }
    for (int i = q->front; i <= q->rear; i++)
        printf("%d ", q->data[i]);
        //qDebug()<<q->data[i];
    printf("\n");      
}
Queue::~Queue()
{
    delete q;
    q=NULL;
}
