#include"List.h"
#include<stdio.h>
#include<stdlib.h>
struct ListPrivate
{
    ListPrivate()
    {
        head=new Node;
        head->data = 0;
        head->next = NULL;
        len = 0;
    }
    ~ListPrivate()
    {
        delete head;
        head=NULL;
    }
    Node * head;
    int len;
};
List::List():list(new ListPrivate)
{
    if(list->head == NULL)
    {
        printf("ListPrivate new error\n");
        //qDebug() << "ListPrivate new error";
    }    
}
Node* List::CreateNodeList(ElementList ele)
{
    Node* newNode = new Node;
    if (newNode == NULL)
    {
        printf("CreateNodeList new error\n");
        //qDebug() <<"CreateNodeList new error";
        return NULL;
    }
    newNode->next = NULL;
    newNode->data = ele;
    return newNode;
}
//尾插法
void List::InsertTailList(ElementList ele)
{
    //异常判断
    if (list->head == NULL)
        return;
    Node* newNode = CreateNodeList(ele);
    if (newNode == NULL)
    {
        printf("InsertTailList CreateNodeList error\n");
        //qDebug()<< "InsertTailList CreateNodeList error":
        return;
    }
    Node* travelPoint = list->head;
    while (travelPoint->next != NULL)
        travelPoint = travelPoint->next;
    travelPoint->next = newNode;
    list->len++;
}

void List::TravelList()
{
    Node* travelPoint = list->head->next;
    while (travelPoint != NULL)
    {
        printf("%d ", travelPoint->data);
        // qDebug()<<travelPoint->data;
        travelPoint = travelPoint->next;
    }
    printf("len=%d", list->len);
    //qDebug()<<"len="<<list->len;
    printf("\n");
}
void List::InsertHeadList(ElementList ele)
{
    Node* newNode = CreateNodeList(ele);
    if (newNode == NULL)
    {
        printf("InsertHeadList CreateNodeList error\n");
        //qDebug() <<"InsertHeadList CreateNodeList error" ;
        return;
    }
    newNode->next = list->head->next;
    list->head->next = newNode;
    list->len++;
}
void List::RemoveByElementList(ElementList ele)
{
    Node* travelPoint = list->head;
    while (travelPoint->next != NULL)
    {
        if (travelPoint->next->data == ele)
        {
            Node* temp = travelPoint->next;
            travelPoint->next = temp->next;
            delete temp;
            list->len--;
        }
        else
            travelPoint = travelPoint->next;
    }
}
void List::RemoveByIndexList(int index)
{
    if (index < 0 || index >= list->len)
    {
        printf("RemoveByIndexList 无效的下标\n");
        //qDebug <<"RemoveByIndexList 无效的下标" ;
        return;
    }
    Node* travelPoint = list->head;
    while (index != 0)
    {
        travelPoint = travelPoint->next;
        index--;
    }
    Node* temp = travelPoint->next;
    travelPoint->next = temp->next;
    delete temp;
    temp=NULL;
    list->len--;

}
void List::InsertByIndexList(ElementList ele, int index)
{
    if (index<0 || index>list->len)
    {
        printf("InsertByIndexList 无效的下标\n");
        //qDebug() <<"InsertByIndexList 无效的下标" ;
        return;
    }
    Node* newNode = CreateNodeList(ele);
    if (newNode == NULL)
    {
        printf("InsertIndex createNode error\n");
        //qDebug() << "InsertIndex createNode error";
        return;
    }
    Node* travelPoint = list->head;
    while (index != 0)
    {
        travelPoint = travelPoint->next;
        index--;
    }
    newNode->next = travelPoint->next;
    travelPoint->next = newNode;
    list->len++;
}
//根据下标寻找值
ElementList* List::FindByIndexList(int index)
{
    if (index < 0 || index >= list->len)
    {
        printf("FindByIndexList 无效的下标\n");
        //qDebug() << "FindByIndexList 无效的下标";
        return NULL;
    }
    Node* travelPoint = list->head;
    while (index != 0)
    {
        travelPoint = travelPoint->next;
        index--;
    }
    return &travelPoint->next->data;
}
//寻找值，返回等于该值的下标，数组最后一个值设为-1，作为数组结束的标志
int* List::FindByElementList(ElementList ele)
{
    int* findVector = new int[sizeof(int) * (list->len + 1)];
    if (findVector == NULL)
    {
        printf("FindByElementList new error\n");
        //qDebug() <<"FindByElementList new error" ;
        return NULL;
    }
    Node* travelPoint = list->head;
    int count = 0;
    int len = 0;
    while (travelPoint->next != NULL)
    {
        if (travelPoint->next->data == ele)
        {
            findVector[len] = count;
            len++;
        }
        count++;
        travelPoint = travelPoint->next;
    }
    findVector[len] = -1;
    return findVector;
}
//根据下标修改值
void List::SetValueByIndexList(int index, ElementList ele)
{
    if (index < 0 || index >= list->len)
    {
        printf("SetValueByIndexList无效的下标\n");
        //qDebug()<<"无效的下标";
        return;
    }
    Node* travelPoint = list->head;
    while (index != 0)
    {
        travelPoint = travelPoint->next;
        index--;
    }
    travelPoint->next->data = ele;
}
//根据值来修改值
void List::SetValueByElementList(ElementList ele, ElementList newele)
{
    int* find = FindByElementList( ele);
    if (find == NULL)
    {
        printf("SetValueByElementList:找不到该值\n");
      //  qDebug() <<"找不到该值" ;
        return;
    }
    int* temp = find;
    while (*temp != -1)
    {
        SetValueByIndexList(*temp, newele);
        temp++;
    }
    delete find;
    find=NULL;
}
//链表的冒泡排序
void List::BubbleSortList(ListPrivate* list)
{
    for (int i = 0; i < list->len - 1; i++)
    {
        Node* travelPoint = list->head;
        for (int j = 0; j < list->len - 1 - i; j++)
        {
            if (travelPoint->next->data > travelPoint->next->next->data)
            {
                Node* Prev = travelPoint->next;
                Node* Next = travelPoint->next->next;
                Prev->next = Next->next;
                Next->next = Prev;
                travelPoint->next = Next;
            }
            travelPoint = travelPoint->next;
        }
    }
}
//链表逆序
void List::ReserveList1()
{
    Node* Prev = NULL;
    Node* cur = list->head->next;
    Node* Next = cur->next;
    while (Next != NULL)
    {
        cur->next = Prev;
        Prev = cur;
        cur = Next;
        Next = cur->next;
    }
    cur->next = Prev;
    list->head->next = cur;
}
//递归逆序,返回的结点为新的头结点
Node* List::ReserveList2(Node* travelPoint)
{
    Node* tail;
    if (travelPoint->next != NULL)
    {
        tail=ReserveList2(travelPoint->next);
        travelPoint->next->next = travelPoint;
        travelPoint->next = NULL;
        return tail;
    }
    tail = travelPoint;
    return travelPoint;
}
//归并排序
ListPrivate* List::MergeList(ListPrivate&other)
{
    BubbleSortList(list);
    BubbleSortList(&other);
    ListPrivate* list3 = new ListPrivate;
    if (list3 == NULL)
    {
        printf("MergeList new error\n");
      //  qDebug() <<"MergeList new error" ;
        return NULL;
    }
    Node* travelPoint = list3->head;
    while (list->head->next != NULL && other.head->next != NULL)
    {
        if (list->head->next->data < other.head->next->data)
        {
            travelPoint->next = list->head->next;
            list->head->next = list->head->next->next;
            list3->len++;
        }
        else
        {
            travelPoint->next = other.head->next;
            other.head->next = other.head->next->next;
            list3->len++;
        }
        travelPoint = travelPoint->next;
    }
    while (list->head->next != NULL)
    {
        travelPoint->next = list->head->next;
        list3->len++;
        travelPoint = travelPoint->next;
        list->head = list->head->next;
    }
    while (other.head->next != NULL)
    {
        travelPoint->next = other.head->next;
        list3->len++;
        travelPoint = travelPoint->next;
        other.head = other.head->next;
    }
    travelPoint->next = NULL;
    return list3;
}
List::~List()
{
    Node* travelPoint = list->head->next;
    while (travelPoint != NULL)
    {
        delete list->head;
        list->head = travelPoint;
        travelPoint = list->head->next;
    }
    delete list;
    list=NULL;
}
