#include "StdNetwork.h"
#include <iostream>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>



void GetUpp(char *str)
{
    for (int i = 0; i < strlen(str); i++)
    {
        if (str[i] >= 'a' && str[i] <= 'z')
            str[i] -= 32;
    }
}



//TCPServer
class TcpServer : public NetworkServer
{
public:
    TcpServer(const char *ip, short int port);
    ~TcpServer();
    void ServerSend( void *ptr, size_t size,int ClientSock)override;
    void ServerRecv( void *ptr, size_t size,int ClientSock)override;
};

TcpServer::TcpServer(const char *ip, short int port)
{
    this->sock = socket(AF_INET, SOCK_STREAM, 0);
    if (this->sock < 0)
    {
        perror("socket");
    }
    int on = 1;
    if (setsockopt(this->sock, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) < 0) // 端口后可以重复使用
    {
        perror("setsockopt");
    }

    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = inet_addr(ip);
    if (bind(this->sock, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("bind");
    }
    if (listen(this->sock, 10) != 0)
    {
        perror("listen");
    }
}

TcpServer::~TcpServer()
{
    close(this->sock);
}

void TcpServer::ServerSend( void *ptr, size_t size,int ClientSock)
{
    if (send(ClientSock, ptr, size, 0) < 0)
    {
        perror("send");
    }
}

void TcpServer::ServerRecv(void *ptr, size_t size,int ClientSock )
{
    if (recv(ClientSock, ptr, size, 0) < 0)
    {
        perror("recv");
    }
}

int NetworkServer::ServerAccept()
{
    int accept_ClientSock = 0;
    struct sockaddr_in addr;
    socklen_t len;
    if ((accept_ClientSock = accept(this->sock, (struct sockaddr *)&addr, &len)) < 0)
    {
        perror("Accept");
        return -1;
    }
    return accept_ClientSock;
}

//UDPServer
class UdpServer : public NetworkServer
{
public:
    UdpServer(const char *IP, short int port);
    ~UdpServer();
    void ServerSend(void *ptr, size_t size,int = 0)override;
    void ServerRecv(void *ptr, size_t size,int = 0)override;
private:
    char ClientIP[20]={0};
    short int ClientPort;
};

UdpServer::UdpServer(const char *IP, short int port)
{
    this->sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (this->sock < 0)
    {
        perror("socket: ");
    }
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = inet_addr(IP);
    if (bind(this->sock, (struct sockaddr *)&addr, sizeof(addr)) != 0)
    {
        perror("bind:");
    }
}

UdpServer::~UdpServer()
{
    close(this->sock);
}

void UdpServer::ServerSend(void *ptr, size_t size,int = 0)
{
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(this->ClientPort);
    addr.sin_addr.s_addr = inet_addr(this->ClientIP);
    if (sendto(this->sock, ptr, size, 0, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("UpdsServersebd sendto :");
    }
}

void UdpServer::ServerRecv(void *ptr, size_t size,int = 0)
{
    struct sockaddr_in addr;
    socklen_t len;
    if (recvfrom(this->sock, ptr, size, 0, (struct sockaddr *)&addr, &len) < 0)
    {
        perror("UpdsServerRecv recvform:");
    }
}

NetworkServer *CreatServer(char *promotion, const char *ip, const int &port)
{
    GetUpp(promotion);
    if (strcmp(promotion,"TCP") == 0)
    {
        return new TcpServer(ip, port);
    }
    else if (strcmp(promotion,"UDP") == 0)
    {
        return new UdpServer(ip, port);
    }
}


