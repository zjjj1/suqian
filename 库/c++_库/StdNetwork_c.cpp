#include "StdNetwork.h"
#include <iostream>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>



void GetUpp(char *str)
{
    for (int i = 0; i < strlen(str); i++)
    {
        if (str[i] >= 'a' && str[i] <= 'z')
            str[i] -= 32;
    }
}



//TCPClient
class TcpClient : public NetworkClient
{
public:
    TcpClient(const char *Serverip, short int Serverport);
    ~TcpClient();
    void ClientSend(void *ptr, size_t size)override;
    void ClientRecv(void *ptr, size_t size)override;
};

TcpClient::TcpClient(const char *Serverip, short int Serverport)
{
    this->sock = socket(AF_INET, SOCK_STREAM, 0);
    if (this->sock < 0)
    {
        perror("socket");
    }

    struct sockaddr_in Serveraddr;
    Serveraddr.sin_family = AF_INET;
    Serveraddr.sin_port = htons(Serverport);
    Serveraddr.sin_addr.s_addr = inet_addr(Serverip);
    if (connect(this->sock, (struct sockaddr *)&Serveraddr, sizeof(Serveraddr)) < 0)
    {
        perror("connect");
    }
}

TcpClient::~TcpClient()
{
    close(this->sock);
}


void TcpClient::ClientSend(void *ptr, size_t size)
{
    if (send(this->sock, ptr, size, 0) < 0)
    {
        perror("send");
    }
}

void TcpClient::ClientRecv(void *ptr, size_t size)
{
    if (recv(this->sock, ptr, size, 0) < 0)
    {
        perror("recv");
    }
}

//UDPClient
class UdpClient : public NetworkClient
{
public:
    UdpClient(const char *ClientIP, short int Clientport);
    ~UdpClient();
    void ClientSend(void *ptr, size_t size)override;
    void ClientRecv(void *ptr, size_t size)override;
private:
    char ServerIP[20]={0};
    short int ServerPort{0};
};

UdpClient::UdpClient(const char *SreverIP, short int Sreverport)
{
    this->sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (this->sock < 0)
    {
        perror("socket: ");
    }
    memset(this->ServerIP, 0, 20);
    strcpy(this->ServerIP, SreverIP);
    this->ServerPort = Sreverport;
}

UdpClient::~UdpClient()
{
    close(this->sock);
}

void UdpClient::ClientSend(void *ptr, size_t size)
{
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(this->ServerPort);
    addr.sin_addr.s_addr = inet_addr(this->ServerIP);
    if (sendto(this->sock, ptr, size, 0, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("UpdsClientSend sendto :");
    }
}

void UdpClient::ClientRecv(void *ptr, size_t size)
{
    struct sockaddr_in addr;
    socklen_t len;
    if (recvfrom(this->sock, ptr, size, 0, (struct sockaddr *)&addr, &len) < 0)
    {
        perror("UpdsClientRecv recvform:");
    }
}


NetworkClient *CreatClient(char *promotion, const char *ip, const int &port)
{
    GetUpp(promotion);
    if (strcmp(promotion,"TCP") == 0)
    {
        return new TcpClient(ip, port);
    }
    else if (strcmp(promotion,"UDP") == 0)
    {
        return new UdpClient(ip, port);
    }
}



