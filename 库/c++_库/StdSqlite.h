#ifndef __STDSQLITE_H__
#define __STDSQLITE_H__

#include <iostream>
#include <vector>
#define Size_len(s) sizeof(s) / sizeof(s[0])


struct StdSqlitePricate;
class StdSqlite
{
public:
    StdSqlite(const char *filename);
    ~StdSqlite();
    // 执行语句
    void Sql_Exec(const char *sql);
    // 获取数据
    std::vector<std::vector<std::string>> GetFromTable(const char *sql);
    // 获取数据2
    void SelectInfo(const char *sql, char ***result, int *row, int *column);
    // 判断一个表中是否有这个数据
    bool IsHaveData(const char *sql);
    // 释放二级数组
    void FreeInfoResult(char **result);
private:
    StdSqlitePricate *s;
};


#endif