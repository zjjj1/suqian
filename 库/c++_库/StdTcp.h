#ifndef __STDTCP_H__
#define __STDTCP_H__

#include <stddef.h>


struct TcpServerPricate;
class StdTcps
{
public:
    StdTcps(const char *ip, short port);
    ~StdTcps();
    int Accept();
    int Send(int ClientSock, void *ptr, size_t size);
    void Recv(int ClientSock, void *ptr, size_t size);

private:
    struct TcpServerPricate *s;
};


struct TcpClientPricate;
class Tcpc
{
public:
    Tcpc(const char *Serverip, short int Serverport);
    ~Tcpc();
    int Send(void *ptr, size_t size);
    void Recv(void *ptr, size_t size);

private:
    struct TcpClientPricate *c;
};



#endif