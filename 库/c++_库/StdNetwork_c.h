#ifndef _StdNetwork_h_
#define _StdNetwork_h_

#include <stddef.h>

class NetworkClient
{
public:
    NetworkClient() {}
    ~NetworkClient() {}
    virtual void ClientSend(void *ptr, size_t size){}
    virtual void ClientRecv(void *ptr, size_t size){}
protected:
    int sock;
};

void GetUpp(char* str);

NetworkClient *CreatClient(char *promotion, const char *ip, const int &port);


#endif
