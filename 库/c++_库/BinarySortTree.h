#ifndef BINARYSORTTREE_H
#define BINARYSORTTREE_H
#define elementBSTree int

struct BSTreeNode;
struct BSTreePrivate
{
    BSTreeNode* root;
};
class BSTree
{
public:
    BSTree();
    BSTreeNode* CreateBSTreeNode(elementBSTree ele);
    static int FuncInsertIntBSTreeNode(int ele1, int ele2);
    void InsertElementBSTree(elementBSTree ele, int(*func)(elementBSTree, elementBSTree));
    void InsertBSTreeNode(BSTreeNode* node, elementBSTree ele, int(*func)(elementBSTree, elementBSTree));
    void FreeBSTree(BSTreeNode* root);
    static void FuncPrevIntBSTree(BSTreeNode* node);
    static void FuncMidIntBSTree(BSTreeNode* node);
    static void FuncPostIntBSTree(BSTreeNode* node);
    void TravelBSTree(void (*func)(BSTreeNode*));
    int FuncFindIntBSTree(elementBSTree ele1, elementBSTree ele2);
    BSTreeNode* FindBSTreeNodeByElement(BSTreeNode* root, elementBSTree ele, int(*func)(elementBSTree, elementBSTree));
    void DeleteBSTreeElememt(elementBSTree ele, int(*func)(elementBSTree, elementBSTree));
    int GetLeafNumFromBSTree(BSTreeNode* root);
    ~BSTree();
    BSTreePrivate*tree;  
};

#endif
