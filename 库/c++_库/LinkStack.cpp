#include"LinkStack.h"
#include <stdio.h>
#include <stdlib.h>
// 定义栈节点结构
struct LSNode
{
    ElementLStack data;
    LSNode* next;
};
// 定义链式栈结构
struct LStackPrivate
{
    LStackPrivate()
    {
        top=NULL;
    }
    LSNode* top;
};
// 创建空栈
LStack::LStack():stack(new LStackPrivate) 
{

}
// 判断栈是否为空
int LStack::IsLStackEmpty() 
{
    return stack->top == NULL;
}
// 入栈
void LStack::PushLStack(ElementLStack data)
{
    LSNode* newNode = new LSNode;
    if (newNode == NULL) 
    {
        printf("PushLStack new error\n");
        // qDebug()<<"PushLStack new error";
        return;
    }
    newNode->data = data;
    newNode->next = stack->top;
    stack->top= newNode;
}
// 出栈,由于无法做到释放掉结点的同时返回该元素值的指针，除非用户在调用后手动释放，所以返回值设为int型，
//这也就要求调用者一定要注意printf的打印信息，以防止出栈元素刚好为-1时带来的问题。
ElementLStack LStack::PopLStack()
{
    if (IsLStackEmpty())
    {
        printf("栈为空，无法出栈\n");
        //qDebug() << "栈为空，无法出栈";
        return -1;
    }
    LSNode* temp = stack->top;
    ElementLStack data = temp->data;
    stack->top = stack->top->next;
    delete temp;
    temp=NULL;
    return data;
}
// 遍历栈
void LStack::TravelLStack() 
{
    if (IsLStackEmpty())
    {
        printf("栈为空，无法遍历\n");
        //qDebug() << "栈为空，无法遍历";
        return;
    }
    LSNode* current = stack->top;
    while (current != NULL) 
    {
        printf("%d ", current->data);
        //qDebug() << current->data;
        current = current->next;
    }
    printf("\n");
}
//获取栈顶元素指针
ElementLStack* LStack::GetTopLStack()
{
    if (IsLStackEmpty())
    {
        printf("栈为空,无法获取栈顶元素\n");
        //qDebug() << "栈为空,无法获取栈顶元素";
        return NULL; // 返回一个特定值表示出错
    }
    else
    {
        return &stack->top->data;
    }
}
//获取栈中的元素个数
int LStack::GetLenFromLStack()
{
    if (IsLStackEmpty())
    {
        return 0;
    }
    int len = 0;
    LSNode* current = stack->top;
    while (current != NULL)
    {
        current = current->next;
        len++;
    }
    return len;
}
LStack::~LStack() 
{
    while (!IsLStackEmpty()) 
    {
        PopLStack();
    }
    delete stack;
    stack=NULL;
}
