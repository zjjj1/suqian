#ifndef __DOUBLELINKLIST_H__
#define __DOUBLELINKLIST_H__

#define ElementType void *

struct Node
{
    ElementType data;
    struct Node *next;
    struct Node *prev;
};
typedef struct Node node;

struct DoubleLinklistPrivate;
class DoubleLinklist
{
private:
    DoubleLinklist();
    void DLInsertTail( ElementType element);
    void DLInsertHead( ElementType element);
    void DLRemoveByIndex( int index);
    void DLRemoveByElement( ElementType element);
    int FindFirstByElement(ElementType element);
    // void DLTravel(void (*func)(ElementType));
    // void FreeDLlist(void (*func)(ElementType));
    int GetDLlistLen();

public:
    DoubleLinklistPrivate *list;
};



#endif