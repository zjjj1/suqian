#include <stdio.h>
#include <stdlib.h>
#include<string.h>
#include"LinkTree.h"
struct LTNode
{
    elementLT data;
    struct LTNode* firstchild;
    struct LTNode* nextSibling;
    struct LTNode* parent;
};
LinkTree::LinkTree():tree(new LinkTreePrivate)
{
    tree->root = CreateLTNode(0);
}
//创建一个树结点
LTNode* LinkTree::CreateLTNode(elementLT ele)
{
    LTNode* newnode = new LTNode;
    if (newnode == NULL)
    {
        printf("InitLTree new error\n");
        //qDebug() <<"InitLTree new error" ;
        return NULL;
    }
    newnode->data = ele;
    newnode->firstchild = NULL;
    newnode->nextSibling = NULL;
    newnode->parent = NULL;
    return newnode;
}

//连接分支
void LinkTree::ConnectBrach(LTNode* parent, LTNode* child)
{
    if (parent == NULL || child == NULL)
    {
        return;
    }
    child->nextSibling = parent->firstchild;
    parent->firstchild = child;
    child->parent = parent;
}
//断开分支
void LinkTree::DisconnectBrach(LTNode* parent, LTNode* child)
{
    if (parent == NULL || child == NULL || parent->firstchild == NULL)
    {
        return;
    }
    LTNode* travelPoint = parent->firstchild;
    if (travelPoint == child)
    {
        parent->firstchild = child->nextSibling;
        child->parent = NULL;
        child->nextSibling = NULL;
        return;
    }
    while (travelPoint->nextSibling != NULL)
    {
        if (travelPoint->nextSibling == child)
        {
            travelPoint->nextSibling = child->nextSibling;
            child->parent = NULL;
            child->nextSibling = NULL;
            return;
        }
        travelPoint = travelPoint->nextSibling;
    }
}

//遍历树,传参时，deepth设为0，因为要递归所以不能在函数内直接设为0
void LinkTree::TravelLTree(LTNode* root, int deepth)
{
    if (root == NULL)
        return;
    for (int i = 0; i < deepth; i++)
    {
        printf("  ");
    }
    if (root->parent != NULL)
        printf("|__");
        //qDebug()<<"|__";
    printf("%d\n", root->data);
   // qDebug() << root->data;
    TravelLTree(root->firstchild, deepth + 1);
    TravelLTree(root->nextSibling, deepth);
}
//寻找树结点
LTNode* LinkTree::FindLTreeNode(LTNode* root, elementLT ele)
{
    if (root == NULL)
        return NULL;
    if (root->data==ele)
    {
        return root;
    }
    LTNode* childNode = root->firstchild;
    LTNode* targetNode = NULL;
    while (childNode != NULL)
    {
        targetNode = FindLTreeNode(childNode, ele);
        if (targetNode != NULL)
        {
            return targetNode;
        }
        childNode = childNode->nextSibling;
    }
    return targetNode;
}
//获取树的高度
int LinkTree::GetTreeHeight(LTNode* root)
{
    if (root == NULL)
        return 0;
    int height = 0;
    LTNode* travelPoint = root->firstchild;
    while (travelPoint != NULL)
    {
        int childheight = GetTreeHeight(travelPoint);
        height = height > childheight ? height : childheight;
        travelPoint = travelPoint->nextSibling;
    }
    return height + 1;
}
//释放树结点
void LinkTree::FreeLTree(LTNode* root)
{
    if (root == NULL)
        return;
    FreeLTree(root->firstchild);
    FreeLTree(root->nextSibling);
    delete root;
    root = NULL;
}
LinkTree::~LinkTree()
{
    FreeLTree(tree->root);
    delete tree;
    tree=NULL;
}
