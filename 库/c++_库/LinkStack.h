#ifndef LINKSTACK_H
#define LINKSTACK_H
#define ElementLStack int
struct LSNode;
struct LStackPrivate;
class LStack
{
public:
    LStack();
    int IsLStackEmpty();
    void PushLStack(ElementLStack data);
    ElementLStack PopLStack();
    void TravelLStack();
    ElementLStack* GetTopLStack();
    int GetLenFromLStack();
    ~LStack();
private:
    LStackPrivate*stack;
};
#endif
