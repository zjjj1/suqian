#ifndef ARRAY_H
#define ARRAY_H
#define ElementARRAY int
#define ArrayMaxSize 100
struct ArrayPrivate;
class Array
{
public:
    Array();
    void TravelArray();
   // void TravelArrayFunc(void (*func)(void*));
    void InsertTailArray(ArrayPrivate* a,ElementARRAY ele);
    void InsertHeadArray(ElementARRAY ele);
    void InsertByIndexArray(ElementARRAY ele, int index);
    void RemoveByIndexArray(ArrayPrivate*a,int index);
    void RemoveByElementArray(ElementARRAY key);
    ElementARRAY* FindByIndexArray(int index);
    int* FindByKeyArray(ElementARRAY key);
    void TravelFindByKeyArray(int* num);
   // int* FindByKeyArrayFunc(ElementARRAY key, int (*func)(void*, void*));
    void SetValueIndexArray(int index, ElementARRAY ele);
    void SetValueKeyArray(int key, ElementARRAY ele);
    ArrayPrivate* FindIntersectionArray(const ArrayPrivate& other);
    ArrayPrivate* FindUnionArray(const ArrayPrivate& other);
    ArrayPrivate* MergeArray(ArrayPrivate& other);
    ~Array();
    ArrayPrivate* a;
};
#endif
