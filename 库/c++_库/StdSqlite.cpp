#include <sqlite3.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "StdSqlite.h"

struct StdSqlitePricate
{
    sqlite3 *db;
};

StdSqlite::StdSqlite(const char *filename):s(new StdSqlitePricate)
{
    if (sqlite3_open(filename, &s->db) != SQLITE_OK)
    {
        printf("open %s error :%s\n", filename, sqlite3_errmsg(s->db));
        return;
    }
}

StdSqlite::~StdSqlite()
{
    sqlite3_close(s->db);
    delete s;
}

void StdSqlite::Sql_Exec(const char *sql)
{
    if(sqlite3_exec(s->db, sql, NULL, NULL, NULL) != SQLITE_OK)
    {
        printf("creat table error : %s\n", sqlite3_errmsg(s->db));
    }
}

std::vector<std::vector<std::string>> StdSqlite::GetFromTable(const char *sql)
{
    char **result;
    int row, column;
    if (sqlite3_get_table(s->db, sql, &result, &row, &column, NULL) != SQLITE_OK)
    {
        printf("sqlite3_get_table error : %s\n", sqlite3_errmsg(s->db));
        return std::vector<std::vector<std::string>>();
    }
    std::vector<std::vector<std::string>> returnValue;
    for(int i = 1; i <= row; i++)
    {
        std::vector<std::string> rowValue;
        for(int j = 0; j < column; j++)
        {
            rowValue.push_back(result[i*column + j]);
        }
        returnValue.push_back(rowValue);
    }
    sqlite3_free_table(result);
    return returnValue;
}

void StdSqlite::SelectInfo(const char *sql, char ***result, int *row, int *column)
{
    if (sqlite3_get_table(s->db, sql, result, row, column, NULL) != SQLITE_OK)
    {
        printf("SelectInfo error : %s\n", sqlite3_errmsg(s->db));
    }
}


bool StdSqlite::IsHaveData(const char *sql)
{
    char **result_User;
    int row_id, column_id;
    SelectInfo(sql, &result_User, &row_id, &column_id);
    if (row_id != 0)
    {
        FreeInfoResult(result_User);
        return true;
    }
    else
    {
        FreeInfoResult(result_User);
        return false;
    }
}

void StdSqlite::FreeInfoResult(char **result)
{
    sqlite3_free_table(result);
}

