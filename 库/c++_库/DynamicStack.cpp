#include"DynamicStack.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
struct DyStackPrivate
{
    DyStackPrivate(int maxlen)
    {
        this->maxlen = maxlen;
        len = 0;
        pstack=new ElementDyStack[maxlen*sizeof(ElementDyStack)];
        if (pstack == NULL)
        {
            printf("DyStackPrivate new error\n");
            //qDebug() << "DyStackPrivate new error";
        }
    }
    ~DyStackPrivate()
    {
        delete[] pstack; 
pstack=NULL;
    }
    int maxlen;
    int len;
    ElementDyStack* pstack;
};
//初始化动态栈
DyStack::DyStack(int maxlen):s(new DyStackPrivate(maxlen))
{
}
//重新分配栈空间
void DyStack::ReallocDyStack()
{
    ElementDyStack* newStack =new ElementDyStack[2 * s->maxlen * sizeof(ElementDyStack)];
    if (newStack == NULL)
    {
        printf("ReallocStack new error\n");
        //qDebug() <<"ReallocStack new error" ;
        return;
    }
    memcpy(newStack, s->pstack, s->len * sizeof(ElementDyStack)); // 复制原栈中的元素到新栈
    delete s->pstack; // 释放原栈
    s->pstack = newStack; // 将栈指针指向新栈
    s->maxlen *= 2; // 更新栈的最大容量
}
//动态栈入栈
void DyStack::PushDy(ElementDyStack ele)
{
    if (s->len == s->maxlen)
        ReallocDyStack();
    s->pstack[s->len] = ele; // 入栈
    s->len++; // 更新栈的长度
}
//动态栈出栈
ElementDyStack* DyStack::PopDy()
{
    if (s->len == 0)
    {
        printf("栈为空，不能再出栈\n");
    //    qDebug() << "栈为空，不能再出栈";
        return NULL;
    }
    s->len--;
    return &s->pstack[s->len];
}
DyStack::~DyStack()
{
    delete s;
}
