#include <stdio.h>
#include <stdlib.h>
#include"LinkQueue.h"
struct LQNode 
{
    int data;
    struct LQNode* next;
};
struct LQueuePrivate
{
    LQueuePrivate()
    {
        front = NULL;
        rear = NULL;
    }
    LQNode* front;
    LQNode* rear;
};
// 初始化队列
LQueue::LQueue():queue(new LQueuePrivate)
{
 
}
// 判断队列是否为空
int LQueue::IsLQueueEmpty() 
{
    return queue->front == NULL;
}
// 入队
void LQueue::InsertLQueue(int item) 
{
    LQNode* newNode = new LQNode;
    if (newNode == NULL)
    {
        printf("InsertLQueue new error\n");
        //qDebug() << "InsertLQueue new error";
        return;
    }
    newNode->data = item;
    newNode->next = NULL;
    if (IsLQueueEmpty()) 
    {
        queue->front = newNode;
        queue->rear = newNode;
    }
    else 
    {
        queue->rear->next = newNode;
        queue->rear = newNode;
    }
}
// 出队,删除队头元素，注意查看打印信息，以防出队元素刚好为-1时可能混淆
int LQueue::RemoveLQueue() 
{
    if (IsLQueueEmpty()) 
    {
        printf("队列为空，出队失败\n");
        //qDebug() <<"队列为空，出队失败" ;
        return -1;
    }
    int item = queue->front->data;
    LQNode* temp = queue->front;
    queue->front = queue->front->next;
    delete temp;
    temp=NULL;
    return item;
}
// 遍历队列元素
void LQueue::TravelLQueue() 
{
    if (IsLQueueEmpty()) 
    {
        printf("队列为空，遍历失败\n");
        //qDebug() <<"队列为空，遍历失败" ;
        return;
    }
    LQNode* curr = queue->front;
    while (curr != NULL) 
    {
        printf("%d\n", curr->data);
        //qDebug()<<curr->data;
        curr = curr->next;
    }
}
LQueue::~LQueue()
{
    delete queue;
    queue=NULL;
}
