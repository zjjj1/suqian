#ifndef _STDMYSQLPOOL_H_
#define _STDMYSQLPOOL_H_

#include <mysql/mysql.h>
#include <iostream>
#include <string>
#include <queue>
#include <mutex>
#include <vector>
#include <memory>

struct StdMySqlPoolPrivate;
class StdMySqlPool
{
public:
    // 禁止拷贝构造
    StdMySqlPool(const StdMySqlPool& other) = delete;
    // 赋值运算禁用
    void operator=(const StdMySqlPool& other) = delete;

    // 设置全局单例
    static StdMySqlPool * GlobalInstance();
    // 设置mysql连接参数
    void SetParams(int num,const char* host,const char* userName,const char* passWd,const char *db,unsigned int port);
    // 获取一个可以直接使用的mysql连接对象指针
    std::shared_ptr<StdMySql> GetMysqlObj();
    void CloseMysql(std::shared_ptr<StdMySql> &mysql);
    ~StdMySqlPool();
private:
    StdMySqlPool();
    StdMySqlPoolPrivate *p;
};

typedef std::vector<std::string> MySqlStringArray;
struct StdMySqlPrivate;
class StdMySql
{
public:
    StdMySql();
    // 设置友元
    friend class StdMySqlPool;
    // 执行 mysql 语句
    bool Execute(const std::string sql);
    // 获取数据 
    std::vector<MySqlStringArray> GetSelectResult(const std::string sql);
    bool ExcuteInTransaction(const MySqlStringArray &arr);
    // 判断当前对象是否是已连接对象
    bool IsConnected();
    // 判断对象是否能用
    bool IsNULL();
    ~StdMySql();

private:
    // 连接 mysql
    bool Connect(const char* host,const char* userName,const char* passWd,const char *db,unsigned int port);
    std::shared_ptr<StdMySqlPrivate> p;
};


#endif