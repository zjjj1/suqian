#ifndef LINKLIST_H
#define LINKLIST_H
#define ElementList int
struct Node
{
    ElementList data;
    Node* next;
};

struct ListPrivate;
class List
{
public:
    List();
    Node* CreateNodeList(ElementList ele);
    void InsertTailList(ElementList ele);
    void TravelList();
    void InsertHeadList(ElementList ele);
    void RemoveByElementList(ElementList ele);
    void RemoveByIndexList(int index);
    void InsertByIndexList(ElementList ele, int index);
    ElementList* FindByIndexList(int index);
    int* FindByElementList(ElementList ele);
    void SetValueByIndexList(int index, ElementList ele);
    void SetValueByElementList(ElementList ele, ElementList newele);
    void BubbleSortList(ListPrivate* list);
    void ReserveList1();
    Node* ReserveList2(Node* travelPoint);
    ListPrivate* MergeList(ListPrivate&other);
    ~List();

    ListPrivate*list;
};
#endif
