#ifndef DLIST_H
#define DLIST_H

#define elementDList int

struct DNode
{
    elementDList data;
    DNode* next;
    DNode* prev;
};
struct DListPrivate;
class DList
{
public:
    DList();
    DNode* CreateDNode(elementDList ele);
    void InsertTailDList(elementDList ele);
    void InsertHeadDList(elementDList ele);
    void TravelDList();
    void RemoveByIndexDList(int index);
    void RemoveByElementDList(elementDList ele);
    int FindFirstByElementDList(elementDList ele);
    ~DList();
private:
    struct DListPrivate*list;
};
#endif
