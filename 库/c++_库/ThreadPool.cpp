#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <list>
#include <thread>
#include <queue>
#include "ThreadPool.h"

#define Sleep_time 3

void *thread_manager(void *arg);
void *Thread_worker(void *arg);

struct ThreadTask
{
    // 函数指针
    void *(*function)(void *);
    void *arg;
};

struct StdThreadPoolPrivate
{
    std::list<std::thread *> threads;    // 线程链表
    std::queue<ThreadTask *> task_queue; // 任务等待队列

    int max_task_queue_num;
    int min_thrd_num;
    int max_thrd_num;
    int busy_thrd_num;
    int exit_thrd_num;

    pthread_mutex_t pool_mutex;      // 给等待的任务上锁
    pthread_mutex_t busy_thrd_mutex; // 给在工作的线程上锁
    pthread_cond_t queue_not_empty;  // 任务队列不为空，激发条件变量
    pthread_cond_t queue_not_full;   // 队列不满的情况

    std::thread *admin_thread; // 管理者线程
    int shutdown;
};

StdThreadPool::StdThreadPool(int max_task_queue_num, int min_thrd_num, int max_thrd_num)
{
    p->max_task_queue_num = max_task_queue_num; // 最大等待数量
    p->min_thrd_num = min_thrd_num;             // 最小线程数
    p->max_thrd_num = max_thrd_num;             // 最大线程数
    p->busy_thrd_num = 0;                       // 在工作的线程数
    p->exit_thrd_num = 0;                       // 需要删除的线程数

    pthread_mutex_init(&p->pool_mutex, NULL);
    pthread_mutex_init(&p->busy_thrd_mutex, NULL);
    pthread_cond_init(&p->queue_not_empty, NULL);
    pthread_cond_init(&p->queue_not_full, NULL);
    p->shutdown = false;
    for (int i = 0; i < max_thrd_num; i++)
    {
        std::thread *t = new std::thread(Thread_worker, p);
        // DLInsertTail(&p->threads, t);
        p->threads.push_back(t);
    }
    p->admin_thread = new std::thread(thread_manager, p);
}

StdThreadPool::~StdThreadPool()
{
    if (p == NULL)
        return;

    p->shutdown = true;
    // 等待管理者线程退出回收线程
    p->admin_thread->join();
    // 释放管理者线程
    delete p->admin_thread;
    // 获取线程链表的长度
    int len = p->threads.size();
    for (int i = 0; i < len; i++)
    {
        // 获取线程链表的长度
        pthread_cond_broadcast(&p->queue_not_empty);
    }

    // 释放正在忙碌的线程
    for (auto threadp : p->threads)
    {
        // 等待忙碌线程完成任务后退出
        threadp->join();
        delete threadp;
    }
    p->threads.clear();

    while (p->task_queue.empty() != true)
    {
        ThreadTask *t = p->task_queue.front();
        free(t);
        p->task_queue.pop();
    }

    pthread_mutex_destroy(&p->pool_mutex);
    pthread_mutex_destroy(&p->busy_thrd_mutex);

    pthread_cond_destroy(&p->queue_not_empty);
    pthread_cond_destroy(&p->queue_not_full);
}

ThreadTask *CreatTask(void *(*function)(void *), void *arg)
{
    // ThreadTask *t = (ThreadTask *)malloc(sizeof(ThreadTask));
    ThreadTask *t = new ThreadTask;
    if (t == NULL)
    {
        printf("task malloc error!\n");
        return NULL;
    }

    t->function = function;
    t->arg = arg;
    return t;
}

void FreeTsask(ThreadTask *t)
{
    // free(t);
    delete t;
}

void *Thread_worker(void *arg)
{
    struct StdThreadPoolPrivate *p = (struct StdThreadPoolPrivate *)arg;
    // 执行完任务不断的去抢任务
    while (1)
    {
        // 防止多个线程抢夺一个任务，加锁
        pthread_mutex_lock(&p->pool_mutex);
        // 如果任务队列为空并且线程池没有关闭，就让线程等待
        while (p->task_queue.empty() == true && p->shutdown == false)
        {
            // 先释放该线程的锁，持续在这等待
            pthread_cond_wait(&p->queue_not_empty, &p->pool_mutex);
            if (p->exit_thrd_num > 0)
            {
                p->exit_thrd_num--;
                // 返回当前线程的线程号
                for (auto ite = p->threads.begin(); ite != p->threads.end(); ite++)
                {
                    // 如果链表中的线程ID和当前等待的这个线程ID相等，就删除链表中的这个线程
                    if ((*ite)->get_id() == std::this_thread::get_id())
                    {
                        p->threads.remove(*ite);
                        break;
                    }
                }
                pthread_mutex_unlock(&p->pool_mutex);
                // 退出当前这条线程
                pthread_exit(NULL);
            }
        }
        // 如果要关掉线程池，解锁，退线程
        if (p->shutdown == true)
        {
            pthread_mutex_unlock(&p->pool_mutex);
            // 退出当前线程
            pthread_exit(NULL);
        }

        ThreadTask *tk = (ThreadTask *)(p->task_queue.front());
        p->task_queue.pop();
        // 取出任务后解锁
        pthread_mutex_unlock(&p->pool_mutex);
        // 拿走一个任务，就可以发送信号插入一个任务
        pthread_cond_broadcast(&p->queue_not_full);

        // 如果多个线程同时抢到了任务，那么忙的数量会发生错误，所以加锁
        pthread_mutex_lock(&p->busy_thrd_mutex);
        p->busy_thrd_num++;
        pthread_mutex_unlock(&p->busy_thrd_mutex);

        // 执行任务
        tk->function(tk->arg);

        // 如果多个线程同时结束了任务，那么忙的数量会发生错误，所以加锁
        pthread_mutex_lock(&p->busy_thrd_mutex);
        p->busy_thrd_num--;
        pthread_mutex_unlock(&p->busy_thrd_mutex);

        FreeTsask(tk);
    }
}

void *thread_manager(void *arg)
{
    StdThreadPoolPrivate *p = (StdThreadPoolPrivate *)arg;
    while (p->shutdown != true)
    {
        sleep(Sleep_time);
        // 对线程池的互斥锁进行上锁，确保只有当前线程可以操作线程池
        pthread_mutex_lock(&p->pool_mutex);

        // 加线程
        int queueLen = p->task_queue.size();
        int thread_num = p->threads.size();
        // 如果当前任务队列中的任务数量大于忙碌线程数量，并且当前线程数量小于最大线程数，则需要增加工作线程。
        if (p->busy_thrd_num < queueLen && thread_num < p->max_thrd_num)
        {
            // 要确保新增加的线程数加上之前的线程数的总和小于最大线程数
            int add = ((queueLen / 2) > (p->max_thrd_num - thread_num) ? (p->max_thrd_num - thread_num) : (queueLen / 2));
            for (int i = 0; i < add; i++)
            {
                std::thread *t = new std::thread(Thread_worker, p);
                p->threads.push_back(t);
            }
        }
        // 减线程 线程多，任务少
        thread_num = p->threads.size();
        // 如果两倍的忙碌线程数比线程总数还要小，就减少线程
        if (thread_num > p->busy_thrd_num * 2 && thread_num > p->min_thrd_num)
        {
            int minus_thrd_num = (thread_num - p->busy_thrd_num) / 2;
            int minus = (minus_thrd_num > (thread_num - p->min_thrd_num) ? (thread_num - p->min_thrd_num) : minus_thrd_num);
            // 赋值需要退出的线程数量
            p->exit_thrd_num = minus;
            // 解锁线程池，方便其他线程使用
            pthread_mutex_unlock(&p->pool_mutex);
            for (int i = 0; i < minus; i++)
            {
                // 循环给线程发出退出信号
                pthread_cond_broadcast(&p->queue_not_empty);
            }
            continue;
        }
        pthread_mutex_unlock(&p->pool_mutex); // 如果既不加，也不需要减，直接解锁
    }
    // 如果要销毁线程池，退出管理线程
    pthread_exit(NULL);
    return nullptr;
}

void StdThreadPool::ThreadP_AddTask(void *(*func)(void *), void *arg)
{
    // 操作公共变量,需要上锁，如添加任务时，线程正在取任务
    pthread_mutex_lock(&p->pool_mutex);
    // 如果要销毁线程池，退出管理线程
    while (p->task_queue.size() == p->max_task_queue_num)
    {
        pthread_cond_wait(&p->queue_not_full, &p->pool_mutex);
    }
    // 如果关闭线程池，解锁
    if (p->shutdown == true)
    {
        pthread_mutex_unlock(&p->pool_mutex);
        return;
    }
    // 添加任务到任务队列中
    p->task_queue.push(CreatTask(func, arg));
    // 如果要销毁线程池，退出管理线程
    pthread_cond_broadcast(&p->queue_not_empty);
    // 解锁
    pthread_mutex_unlock(&p->pool_mutex);
}

