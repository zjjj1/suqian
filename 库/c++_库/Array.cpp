#include<stdio.h>
#include<stdlib.h>
#include"Array.h"
struct ArrayPrivate
{
    ElementARRAY array[ArrayMaxSize];
    int len;
};
//冒泡排序
void Sort(int arr[], int n)
{
    for (int i = 0; i < n - 1; i++)
        for (int j = 0; j < n - i - 1; j++)
            if (arr[j] > arr[j + 1])
            {
                int temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
}
//初始化
Array::Array():a(new ArrayPrivate)
{
    a->len = 0;
}
//遍历
void Array::TravelArray()
{
    printf("遍历数组：\n");
    //    qDebug() <<"遍历数组：";
    for (int i = 0; i < a->len; i++)
        printf("%d ", a->array[i]);
    printf("\n");
    //for (int i = 0; i < a->len; i++)
    //    qDebug()<< a->array[i];
}
//遍历回调
// void Array::TravelArrayFunc(void (*func)(void*))
// {
//     printf("遍历数组：\n");
//     //    qDebug() <<"遍历数组：";
//     for (int i = 0; i < a->len; i++)
//         func(a->array[i]);
// }
//尾插数组
void Array::InsertTailArray(ArrayPrivate* a,ElementARRAY ele)
{
    if (a->len >= ArrayMaxSize)
    {
        printf("数组溢出\n");
        //qDebug()<<"数组溢出";
        return;
    }
    a->array[a->len] = ele;
    a->len++;
}
//头插数组
void Array::InsertHeadArray(ElementARRAY ele)
{
    if (a->len >= ArrayMaxSize)
    {
        printf("数组溢出\n");
        //qDebug()<<"数组溢出";
        return;
    }
    for (int i = a->len; i > 0; i--)
        a->array[i] = a->array[i - 1];
    a->array[0] = ele;
    a->len++;
}
//插入数组
void Array::InsertByIndexArray(ElementARRAY ele, int index)
{
    if (index<0 || index>a->len)
    {
        printf("插入无效的下标\n");
        //qDebug()<<"插入无效的下标";
        return;
    }
    if (a->len >= ArrayMaxSize)
    {
        printf("数组溢出\n");
        //qDebug()<<"数组溢出";
        return;
    }
    for (int i = a->len; i > index; i--)
        a->array[i] = a->array[i - 1];
    a->array[index] = ele;
    a->len++;
}
//根据下标删除元素
void Array::RemoveByIndexArray(ArrayPrivate*a,int index)
{
    if (index < 0 || index >= a->len)
    {
        printf("删除无效的下标\n");
        //qDebug()<<"删除无效的下标";
        return;
    }
    for (int i = index; i < a->len - 1; i++)
        a->array[i] = a->array[i + 1];
    a->len--;
}
//根据值删除元素，如果删除指针需要调用者自行释放
void Array::RemoveByElementArray(ElementARRAY key)
{
    for (int i = 0; i < a->len; i++)
        if (a->array[i] == key)
        {
            RemoveByIndexArray(a,i);
            i--;
        }
}
//根据下标寻找元素值
ElementARRAY* Array::FindByIndexArray(int index)
{
    if (index < 0 || index >= a->len)
    {
        printf("FindIndexArray 无效的下标\n");
        //qDebug()<<"FindIndexArray 无效的下标";
        return NULL;
    }
    return &a->array[index];
}
//根据值寻找，返回一个数组存储与该值相等的下标数组，数组的第一个值用于标志数组的长度
int* Array::FindByKeyArray(ElementARRAY key)
{
    int* s = new int[sizeof(int) * (a->len + 1)];
    int count = 0;
    if (s == NULL)
    {
        printf("FindByKeyArray new error\n");
        //qDebug()<<"FindByKeyArray new error";
        return NULL;
    }
    for (int i = 0; i < a->len; i++)
    {
        if (a->array[i] == key)
        {
            count++;
            s[count] = i;
        }
    }
    s[0] = count;
    return s;
}//遍历上面这个函数中的下标数组
void Array::TravelFindByKeyArray(int*num)
{
    for (int i = 0; i < num[0]; i++)
    {
        printf("a[%d]=%d\n", i, num[i + 1]);
    }
}
//根据值寻找，返回一个满足回调的下标数组，数组的第一个值用于标志数组的长度
// int* Array::FindByKeyArrayFunc(ElementARRAY key,int (*func)(void*,void*))
// {
//     int* s = new int[sizeof(int) * (a->len + 1)];
//     int count = 0;
//     if (s == NULL)
//     {
//         printf("FindByKeyArray new error\n");
//         //qDebug()<<"FindByKeyArray new error";
//         return NULL;
//     }
//     for (int i = 0; i < a->len; i++)
//     {
//         if (func(a->array[i], key))
//         {
//             count++;
//             s[count] = i;
//         }
//     }
//     s[0] = count;
//     return s;
// }
//根据下标修改值
void Array::SetValueIndexArray(int index, ElementARRAY ele)
{
    if (index < 0 || index >= a->len)
    {
        printf("setValue 无效的下标\n");
        //qDebug() <<"setValue 无效的下标" ;
        return;
    }
    a->array[index] = ele;
}
//根据值修改值
void Array::SetValueKeyArray(int key, ElementARRAY ele)
{
    for (int i = 0; i < a->len; i++)
        if (a->array[i] == key)
            a->array[i] = ele;
}
//返回两个数组的交集,交集元素不重复
ArrayPrivate* Array::FindIntersectionArray(const ArrayPrivate& other)
{
    ArrayPrivate* intersection =new ArrayPrivate;
    if (intersection == NULL)
    {
        printf("FindIntersection malloc error\n");
        //qDebug() << "FindIntersection malloc error";
        return NULL;
    }
    intersection->len = 0;
    for (int i = 0; i < a->len; i++)
        for (int j = 0; j < other.len; j++)
            if (a->array[i] == other.array[j])
            {
                int flag = 0;
                for (int k = 0; k < intersection->len; k++)
                {
                    if (intersection->array[k]==a->array[i])
                        flag = 1;
                }
                if (flag == 0)
                    InsertTailArray(intersection,a->array[i]);
            }
    return intersection;
}
//返回两个数组的并集，并集元素不重复
ArrayPrivate* Array::FindUnionArray(const ArrayPrivate& other)
{
    ArrayPrivate* a3 =new ArrayPrivate;  
    if (a3 == NULL)
    {
        printf("FindUnionArray malloc error\n");
      //  qDebug() <<"FindUnionArray malloc error" ;
        return NULL;
    }
    a3->len = 0;
    for (int i = 0; i < a->len; i++)
        InsertTailArray(a3, a->array[i]);
    for (int i = 0; i < other.len; i++)
        InsertTailArray(a3, other.array[i]);
    Sort(a3->array, a3->len);
    for (int i = 1; i < a3->len; i++)
    {
        if (a3->array[i] == a3->array[i - 1])
        {
            RemoveByIndexArray(a3, i);
            i--;
        }
    }
    return a3;
}
//归并排序,从小到大
ArrayPrivate* Array::MergeArray(ArrayPrivate& other)
{
    Sort(a->array, a->len);
    Sort(other.array, other.len);
    ArrayPrivate* a3 = new ArrayPrivate;
    if (a3 == NULL)
    {
        printf("MergeArray new error\n");
       // qDebug() << "MergeArray new error";
        return NULL;
    }
    a3->len = 0;
    int i = 0, j = 0;
    while (i < a->len && j < other.len)
    {
        if (a->array[i] < other.array[j])
        {
            InsertTailArray(a3, a->array[i]);
            i++;
        }
        else
        {
            InsertTailArray(a3, other.array[j]);
            j++;
        }
    }
    while (i < a->len)
    {
        InsertTailArray(a3, a->array[i]);
        i++;
    }
    while (j <other.len)
    {
        InsertTailArray(a3, other.array[j]);
        j++;
    }
    return a3;
}
Array::~Array()
{
delete a;
 a=NULL;
}
