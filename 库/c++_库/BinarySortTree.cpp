#include <stdio.h>
#include <stdlib.h>
#include "BinarySortTree.h"

struct BSTreeNode
{
    elementBSTree data;
    struct BSTreeNode *left;
    struct BSTreeNode *right;
    struct BSTreeNode *parent;
};

BSTree::BSTree() : tree(new BSTreePrivate)
{
    tree->root = NULL;
}
// 创建树结点
BSTreeNode *BSTree::CreateBSTreeNode(elementBSTree ele)
{
    BSTreeNode *newnode = new BSTreeNode;
    if (newnode == NULL)
    {
        printf("CreateBSTNode new error\n");
        // qDebug() << "CreateBSTNode new error";
        return NULL;
    }
    newnode->data = ele;
    newnode->left = NULL;
    newnode->parent = NULL;
    newnode->right = NULL;
    return newnode;
}

// 插入int类型时，比较的回调函数
int BSTree::FuncInsertIntBSTreeNode(int ele1, int ele2)
{
    if (ele1 < ele2)
        return 1;
    else
        return 0;
}
// 插入树结点
void BSTree::InsertBSTreeNode(BSTreeNode *node, elementBSTree ele, int (*func)(elementBSTree, elementBSTree))
{
    if (node == NULL)
        return;
    if (func(ele, node->data))
    {
        if (node->left != NULL)
            InsertBSTreeNode(node->left, ele, func);
        else
        {
            node->left = CreateBSTreeNode(ele);
            if (node->left == NULL)
            {
                printf("InsertBSTreeNode CreateBSTreeNode error1\n");
                // qDebug()<<"InsertBSTreeNode CreateBSTreeNode error1";
                return;
            }
            node->left->parent = node;
        }
    }
    if (!func(ele, node->data))
    {
        if (node->right != NULL)
            InsertBSTreeNode(node->right, ele, func);
        else
        {
            node->right = CreateBSTreeNode(ele);
            if (node->right == NULL)
            {
                printf("InsertBSTreeNode CreateBSTreeNode error2\n");
                // qDebug()<<"InsertBSTreeNode CreateBSTreeNode error2";
                return;
            }
            node->right->parent = node;
        }
    }
}
// 调用上方的函数插入元素
void BSTree::InsertElementBSTree(elementBSTree ele, int (*func)(elementBSTree, elementBSTree))
{
    if (tree->root == NULL)
    {
        tree->root = CreateBSTreeNode(ele);
        if (tree->root == NULL)
        {
            printf("InsertElementBSTree CreateBSTreeNode error\n");
            // qDebug() << "InsertElementBSTree CreateBSTreeNode error";
            return;
        }
    }
    else
    {
        InsertBSTreeNode(tree->root, ele, func);
    }
}
// 释放树，如果为指针类型记得写回调函数
void BSTree::FreeBSTree(BSTreeNode *root)
{
    if (root == NULL)
        return;
    FreeBSTree(root->left);
    FreeBSTree(root->right);
    //    fuc(root->data);
    delete root;
    root = NULL;
}
// 前序遍历回调
void BSTree::FuncPrevIntBSTree(BSTreeNode *node)
{
    if (node != NULL)
    {
        printf("%d ", node->data);
        // qDebug() << node->data;
        FuncPrevIntBSTree(node->left);
        FuncPrevIntBSTree(node->right);
    }
}
// 中序遍历回调
void BSTree::FuncMidIntBSTree(BSTreeNode *node)
{
    if (node != NULL)
    {
        FuncMidIntBSTree(node->left);
        printf("%d ", node->data);
        // qDebug() << node->data;
        FuncMidIntBSTree(node->right);
    }
}
// 后序遍历回调
void BSTree::FuncPostIntBSTree(BSTreeNode *node)
{
    if (node != NULL)
    {
        FuncPostIntBSTree(node->left);
        FuncPostIntBSTree(node->right);
        printf("%d ", node->data);
        // qDebug() << node->data;
    }
}
// 遍历树
void BSTree::TravelBSTree(void (*func)(BSTreeNode *))
{
    func(tree->root);
}
// 下方函数比较相等时的回调
int BSTree::FuncFindIntBSTree(elementBSTree ele1, elementBSTree ele2)
{
    if (ele1 == ele2)
        return 1;
    else if (ele1 < ele2)
        return 2;
    else
        return 0;
}
// 寻找第一个等于该值的结点，如果插的值都不相等，就是唯一的
BSTreeNode *BSTree::FindBSTreeNodeByElement(BSTreeNode *root, elementBSTree ele, int (*func)(elementBSTree, elementBSTree))
{
    if (root == NULL)
        return NULL;
    if (func(root->data, ele) == 1)
    {
        return root;
    }
    else if (func(root->data, ele) == 2)
        return FindBSTreeNodeByElement(root->right, ele, func);
    else
        return FindBSTreeNodeByElement(root->left, ele, func);
}
// 删除元素
void BSTree::DeleteBSTreeElememt(elementBSTree ele, int (*func)(elementBSTree, elementBSTree))
{
    BSTreeNode *targetNode = FindBSTreeNodeByElement(tree->root, ele, func);
    if (targetNode == NULL)
        return;
    BSTreeNode *parentNode = targetNode->parent;
    if (targetNode->left == NULL && targetNode->right == NULL)
    {
        if (parentNode == NULL)
            tree->root = NULL;
        else if (parentNode->left == targetNode)
            parentNode->left = NULL;
        else
            parentNode->right = NULL;
        delete targetNode;
        targetNode = NULL;
    }
    else if (targetNode->right == NULL)
    {
        if (parentNode == NULL)
            tree->root = targetNode->left;
        else if (parentNode->left == targetNode)
            parentNode->left = targetNode->left;
        else
            parentNode->right = targetNode->left;
        targetNode->left->parent = parentNode;
        delete targetNode;
        targetNode = NULL;
    }
    else if (targetNode->left == NULL)
    {
        if (parentNode == NULL)
            tree->root = targetNode->right;
        else if (parentNode->left == targetNode)
            parentNode->left = targetNode->right;
        else
            parentNode->right = targetNode->right;
        targetNode->right->parent = parentNode;
        delete targetNode;
        targetNode = NULL;
    }
    else
    {
        BSTreeNode *minnode = targetNode->right;
        while (minnode->left != NULL)
        {
            minnode = minnode->left;
        }
        elementBSTree temp = targetNode->data;
        targetNode->data = minnode->data;
        minnode->data = temp;

        BSTreeNode *minnodeParent = minnode->parent;
        if (minnodeParent->left == minnode)
            minnodeParent->left = minnode->right;
        else
            minnodeParent->right = minnode->right;

        if (minnode->right != NULL)
            minnode->right->parent = minnodeParent;
        delete targetNode;
        targetNode = NULL;
    }
}

int BSTree::GetLeafNumFromBSTree(BSTreeNode *root)
{
    if (root == NULL)
        return 0;
    if (root->left == NULL && root->right == NULL)
        return 1;
    int leftNum = GetLeafNumFromBSTree(root->left);
    int rightNum = GetLeafNumFromBSTree(root->right);
    return leftNum + rightNum;
}
BSTree::~BSTree()
{
    delete tree;
    tree = NULL;
}
