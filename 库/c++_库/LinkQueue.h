#ifndef LINKQUEUE_H
#define LINKQUEUE_H

struct LQNode ;
struct LQueuePrivate;
class LQueue
{
public:
    LQueue();
    int IsLQueueEmpty();
    void InsertLQueue(int item);
    int RemoveLQueue();
    void TravelLQueue();
    ~LQueue();
private:
    LQueuePrivate*queue;
};

#endif 
