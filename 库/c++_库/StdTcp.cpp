#include <stdio.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include "StdTcp.h"


struct TcpServerPricate
{
    int sock;
};

StdTcps::StdTcps(const char *ip, short port):s(new TcpServerPricate)
{
    s->sock = socket(AF_INET, SOCK_STREAM, 0);
    if (s->sock < 0)
    {
        perror("socket");
        return;
    }
    int on = 100;
    if (setsockopt(s->sock, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) != 0)//端口后可以重复使用
    {
        perror("setsockopt");
        return;
    }

    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = inet_addr(ip);
    if (bind(s->sock, (struct sockaddr *)&addr, sizeof(addr)) != 0)
    {
        perror("bind");
        return;
    }
    if (listen(s->sock, 10) < 0)
    {
        perror("listen");
        return;
    }
}

StdTcps::~StdTcps()
{
    close(s->sock);
    delete s;
}

int StdTcps::Accept()
{
    int acceptSock = 0;
    struct sockaddr_in addr;
    socklen_t len = 1;
    if ((acceptSock = accept(s->sock, (struct sockaddr *)&addr, &len)) < 0)
    {
        perror("Accept");
        return -1;
    }
    return acceptSock; 
}

int StdTcps::Send(int ClientSock, void *ptr, size_t size)
{
    return send(ClientSock, ptr, size, 0);
}

void StdTcps::Recv(int ClientSock, void *ptr, size_t size)
{
    if (recv(ClientSock, ptr, size, 0) < 0)
    {
        perror("recv");
    }
}



struct TcpClientPricate
{
    int sock;
};

Tcpc::Tcpc(const char *Serverip, short int Serverport):c(new TcpClientPricate)
{

    c->sock = socket(AF_INET, SOCK_STREAM, 0);
    if (c->sock < 0)
    {
        perror("socket");
        return;
    }

    struct sockaddr_in Serveraddr;
    Serveraddr.sin_family = AF_INET;
    Serveraddr.sin_port = htons(Serverport);
    Serveraddr.sin_addr.s_addr = inet_addr(Serverip);
    if (connect(c->sock, (struct sockaddr *)&Serveraddr, sizeof(Serveraddr)) < 0)
    {
        perror("connect");
        return;
    }
}


Tcpc::~Tcpc()
{
    close(c->sock);
    delete c;
}

int Tcpc::Send(void *ptr, size_t size)
{
    return send(c->sock, ptr, size, 0);
}

void Tcpc::Recv(void *ptr, size_t size)
{
    if (recv(c->sock, ptr, size, 0) < 0)
    {
        perror("recv");
    }
}



