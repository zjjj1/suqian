#include<stdio.h>
#include<stdlib.h>
#include"DList.h"
struct DListPrivate
{
    int len;
    DNode* head;
    DNode* tail;
};
//初始化
DList::DList():list(new DListPrivate)
{
    list->head = NULL;
    list->tail = NULL;
    list->len = 0;
}
//创建结点
DNode* DList::CreateDNode(elementDList ele)
{
    DNode* newnode = new DNode;
    if (newnode == NULL)
    {
        printf("DList::CreateDNode error\n");
        //qDebug()<<"DList::CreateDNode error";
        return NULL;
    }
    newnode->next = NULL;
    newnode->prev = NULL;
    newnode->data = ele;
    return newnode;
}
//尾插值
void DList::InsertTailDList(elementDList ele)
{
    DNode* newnode = CreateDNode(ele);
    if (newnode == NULL)
    {
        printf("DList::InsertTailDList create error\n");
        //qDebug()<<"DList::InsertTailDList create error";
        return;
    }
    if (list->len == 0)
    {
        list->head = newnode;
        list->tail = newnode;
    }
    else
    {
        list->tail->next = newnode;
        newnode->prev = list->tail;
        list->tail = newnode;
    }
    list->len++;
}
//头插值
void DList::InsertHeadDList(elementDList ele)
{
    DNode* newnode = CreateDNode(ele);
    if (newnode == NULL)
    {
        printf("DList::InsertHeadDList CreateDNode error\n");
        //qDebug()<<"DList::InsertHeadDList CreateDNode error";
        return;
    }
    if (list->len == 0)
    {
        list->head = newnode;
        list->tail = newnode;
    }
    else
    {
        list->head->prev = newnode;
        newnode->next = list->head;
        list->head = newnode;
    }
    list->len++;
}

//遍历链表
void DList::TravelDList()
{
    printf("len:%d\n", list->len);
    if (list->len == 0)
        return;
    printf("next:");
    DNode* travel = list->head;
    while (travel != NULL)
    {
        printf("%d ", travel->data);
        //qDebug()<<"travel->data";
        travel = travel->next;
    }
    printf("\n");
    printf("prev:");
    travel = list->tail;
    while (travel != NULL)
    {
        printf("%d ", travel->data);
        //qDebug()<<"travel->data";
        travel = travel->prev;
    }
    printf("\n");
}
//根据下标删除元素
void DList::RemoveByIndexDList(int index)
{
    if (index < 0 || index >= list->len)
    {
        printf("DList::RemoveByIndexDList 无效的下标\n");
        //qDebug()<<"DList::RemoveByIndexDList 无效的下标";
        return;
    }
    if (index == 0)
    {
        if (list->len == 1)
        {
            delete list->head;
            list->tail = NULL;
            list->head = NULL;
            list->len = 0;
            return;
        }
        DNode* freenode = list->head;
        list->head = list->head->next;
        list->head->prev = NULL;
        delete freenode;
        list->len--;
        return;
    }
    if (index == list->len - 1)
    {
        DNode* freenode = list->tail;
        list->tail = list->tail->prev;
        list->tail->next = NULL;
        delete freenode;
        list->len--;
        return;
    }
    DNode* travel = list->head;
    while (index > 0)
    {
        travel = travel->next;
        index--;
    }
    travel->next->prev = travel->prev;
    travel->prev->next = travel->next;
    list->len--;
    delete travel;
    travel = NULL;
}
//根据值删除元素
void DList::RemoveByElementDList(elementDList ele)
{
    int index = FindFirstByElementDList(ele);
    while (index != -1)
    {
        RemoveByIndexDList(index);
        index = FindFirstByElementDList(ele);
    }
}
//寻找链表中第一个与该值相等的下标
int DList::FindFirstByElementDList(elementDList ele)
{
    int count = 0;
    DNode* travel = list->head;
    while (travel != NULL)
    {
        if (travel->data == ele)
        {
            return count;
        }
        count++;
        travel = travel->next;
    }
    return -1;
}
//释放链表
DList::~DList()
{
    while (list->head != NULL)
    {
        DNode* freenode = list->head;
        list->head = list->head->next;
        delete freenode;
    }
    delete list->head;
    list->head=NULL;
    list->tail=NULL;
    list->len=0;
delete list;
list=NULL;
}
