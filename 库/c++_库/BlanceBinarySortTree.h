#ifndef BLANCEBINARYSORTTREE_H
#define BLANCEBINARYSORTTREE_H
#define elementBBSTree int

struct BBSTreeNode;
struct BBSTreePrivate
{
    BBSTreeNode* root;
};
class BBSTree
{
public:
    BBSTree();
    BBSTreeNode* RotateRight(BBSTreeNode* node);
    BBSTreeNode* RotateLeft(BBSTreeNode* node);
    BBSTreeNode* RotateLeftRight(BBSTreeNode* node);
    BBSTreeNode* RotateRightLeft(BBSTreeNode* node);
    int GetBBSTreeHeight(BBSTreeNode* root);
    BBSTreeNode* CreateBBSTreeNode(elementBBSTree ele);
    static int FuncInsertIntBBSTreeNode(int ele1, int ele2);
    BBSTreeNode* InsertDataBBSTree(BBSTreeNode* root, elementBBSTree data, int (*func)(elementBBSTree, elementBBSTree));
    void InsertElementBBSTree(elementBBSTree ele, int (*func)(elementBBSTree, elementBBSTree));
    void FreeBBSTreeNode(BBSTreeNode* root);
    static void FuncPrevIntBBSTree(BBSTreeNode* node);
    static void FuncMidIntBBSTree(BBSTreeNode* node);
    static void FuncPostIntBBSTree(BBSTreeNode* node);
    void TravelBBSTree(void (*func)(BBSTreeNode*));
    static int FuncFindIntBBSTree(elementBBSTree ele1, elementBBSTree ele2);
    BBSTreeNode* FindBBSTreeNode(BBSTreeNode* root, elementBBSTree ele, int(*func)(elementBBSTree, elementBBSTree));
    void DeleteBBSTreeElememt(elementBBSTree ele, int(*func)(elementBBSTree, elementBBSTree));
    int GetLeafNumBBSTree(BBSTreeNode* root);
    ~BBSTree();

    BBSTreePrivate*tree;
};
#endif
