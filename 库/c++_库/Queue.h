#ifndef QUEUE_H
#define QUEUE_H
#define QUEUE_MAX_SIZE 10
struct QueuePrivate;
class Queue
{
public:
    Queue();
    int IsQueueEmpty();
    int IsQueueFull();
    void InsertQueue(int num);
    int RemoveQueue();
    void TravelQueue();
    ~Queue();
private:
    QueuePrivate*q;
};
#endif
