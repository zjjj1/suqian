#include "ShardMemory.h"

struct ShardMemory
{
    key_t key;
    int shmID;
    size_t size;
};

ShMemory *InitShardMemory(const char *path, int pro_id, size_t size)
{
    ShMemory *m = (ShMemory *)malloc(sizeof(ShMemory));
    if (m == NULL)
    {
        printf("InitShardMemory malloc error!\n");
        return NULL;
    }
    m->key = ftok(path, pro_id);
    if (m->key < 0)
    {
        perror("InitShardMemory ftok:");
        free(m);
        return NULL;
    }
    m->size = size;
    m->shmID = shmget(m->key, size, IPC_CREAT | 0666);
    if (m->shmID < 0)
    {
        perror("InitShardMemory shmget:");
        free(m);
        return NULL;
    }
    return m;
}

void ClearShardMemory(ShMemory *m)
{
    if (shmctl(m->shmID, IPC_RMID, NULL) != 0)
    {
        perror("ClearShardMemory shmctl:");
        return;
    }
    free(m);
}

void ReadFromClearShardMemory(ShMemory *m, void *ptr, size_t size)
{
    if (m->size < size)
    {
        printf("读取长度超过本身长度！\n");
        return;
    }
    void *text = shmat(m->shmID, NULL, SHM_RDONLY);
    if (text == NULL)
    {
        perror("ReadFromClearShardMemory shmat:");
        return;
    }
    memcpy(ptr, text, size);
    if (shmdt(text) != 0)
    {
        perror("ReadFromClearShardMemory shmat:");
        return;
    }
}

void WriteFromClearShardMemory(ShMemory *m, void *ptr, size_t size)
{
    if (m->size < size)
    {
        printf("写入长度超过本身长度！\n");
        return;
    }
    void *text = shmat(m->shmID, NULL, 0);
    if (text == NULL)
    {
        perror("WriteFromClearShardMemory shmat:");
        return;
    }
    memcpy(text, ptr, size);
    if (shmdt(text) != 0)
    {
        perror("WriteFromClearShardMemory shmat:");
        return;
    }
}
