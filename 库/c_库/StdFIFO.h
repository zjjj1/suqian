#ifndef __STDFIFO_H__
#define __STDFIFO_H__

#define PATH_SIZE 100

#include <stddef.h>

enum STDMode
{
    ReadOnly,
    WriteOnly
};

typedef enum STDMode Mode;

struct StdIFO;

typedef struct StdIFO FIFO;

FIFO *InitFIFO(const char *path);
int OpenFIFO(FIFO *f, Mode mode);
void WriteToFIFO(FIFO *f, void *buff, size_t size);
void ReadFormFIFO(FIFO *f, void *buff, size_t size);
void SetBlock(FIFO *f,int IsBlock);
void ClearFIFO(FIFO *f);

#endif