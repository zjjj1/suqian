#include <stdio.h>
#include <stdlib.h>
#include "myarray.h"

#define Init_ArrayPoint(name,log) dma *name=(dma *)malloc(sizeof(dma));\
                                  if(name == NULL)\
                                  {printf(log);return NULL;}\
                                  name->len = 0;\
                                  name->size = 15;

#define FreeArrayPoint(name) if(name != NULL){free(name);name = NULL;}

#define true 1
#define false 0

void Travel(dma *array)
{
    printf("  len:%d   size:%d\n",array->len,array->size);
    printf("-----------------------------\n");
    for(int i = 0;i < array->len;i++)
    printf("%d ",array->dp[i]);
    printf("\n");
    printf("-----------------------------\n");
}

int ArrayInit(dma *array)
{
    array->dp = (ElementType *)malloc(array->size* sizeof(ElementType));
    if(array->dp == NULL)
    {
        printf("Init malloc error!\n");return false;
    }
    return true;
}

void FreeArray(dma *array)
{
    if(array->dp != NULL)
    {
        free(array->dp);
        array->dp = NULL;
    }
}

int ReallocArray(dma *array)
{
    ElementType *temp=array->dp;
    array->dp=(ElementType *)malloc(sizeof(ElementType) *array->size*2);
    if(array->dp==NULL)
    {
        printf("ReallocArray error!\n");return false;
    }
    for(int i=0;i<array->len;i++)
    {
        array->dp[i] = temp[i];
    }
    array->size *= 2;
    free(temp);
    return true;
}

int InsertArray(dma *array, ElementType element)
{
    if(array->len == array->size)
    {
        if(ReallocArray(array) == false)
        {
            printf("can not contain more elements!\n");
            return false;
        }        
    }
    array->dp[array->len]=element;
    array->len++;
    return true;
}

void InsertTail(dma *array, ElementType element)
{
    if(array->len>=array->size)
        {   printf("inserTail out of range!\n");return; }
    array->dp[array->len]=element;
    array->len++;
}

void InsertHead(dma *array, ElementType element)
{
    if(array->len>=array->size)
        {   printf("InsertHead out of range!\n");return; }
    for(int i=array->len;i>0;i--)
    {
        array->dp[i]=array->dp[i-1];
    }
    array->dp[0]=element;
    array->len++;
}

void InsertByIndex(dma *array, ElementType element, int index)
{
    if(index < 0 || index > array->len)
        {   printf("insertindex invalid place!");return; }
    if(array->len >= array->size)
        {   printf("insertindex out of range!");return; }
    for(int i=array->len;i>index;i--)
    {
        array->dp[i] = array->dp[i-1];
    }
    array->dp[index] = element;
    array->len++;
}

void RemoveByIndex(dma *array, int index)
{
    if(index < 0 || index > array->len)
        {   printf("removebyindex invalid place!");return; }
    for(int i=index; i < array->len-1 ;i++)
    {
        array->dp[i] = array->dp[i+1];
    }
    array->len--;
}

void RemoveByElement(dma *array, ElementType element)
{
    for(int i=0;i<array->len;i++)
    {
        if(array->dp[i] == element)
        {
            RemoveByIndex(array,i);
            i--;
        }
    }
}

ElementType *FindByIndex(dma *array, int index)
{
    if(index < 0 || index > array->len)
        {   printf("findindex invalid place!");return NULL; }
    return &array->dp[index];
}

int *FindByElement(dma *array, ElementType element)
{
    int *FindVector = (int *)malloc(sizeof(int) * (array->len+1));
    if(FindVector == NULL)
        {   printf("malloc error!\n");return NULL; }
    int count = 0;
    for(int i=0;i<array->len;i++)
    {
        if(array->dp[i] == element)
        {
            count++;
            FindVector[count] = i;
        }
    }
    FindVector[0] = count;
    return FindVector;
}

int *FindByElement2(dma *array, ElementType element)
{
    int *FindVector = (int *)malloc(sizeof(int) * (array->len+1));
    if(FindVector == NULL)
        {   printf("malloc error!\n");return NULL;}
    int count = 0;
    for(int i=0;i<array->len;i++)
    {
        if(array->dp[i] == element)
        {
            FindVector[count] = i;
            count++;
        }
    }
    FindVector[count]= -1;
    return FindVector;
}

void SetValueByIndex(dma *array, int index, ElementType element)
{
    if(index < 0 || index > array->len)
        {   printf("SetValueByIndex invalid place!");return; }
    array->dp[index] = element;
}

void SetValueByElement(dma *array, ElementType oldValue, ElementType newValue)
{
    for(int i=0;i<array->len;i++)
    {
        if(array->dp[i] == oldValue)
        {
            array->dp[i] = newValue;
        }
    }
}

dma *findIntersection(dma *array1, dma *array2)
{
    Init_ArrayPoint(intersection,"findIntersection malloc error!\n");
    ArrayInit(intersection);
    for(int i=0;i<array1->len;i++)
    {
        for(int j=0;j<array2->len;j++)
        {
            int flag = 0;
            if(array1->dp[i] == array2->dp[j])
            {
                for(int k=0;k<intersection->len;k++)
                {
                    if(intersection->dp[k] == array1->dp[i])
                    flag=1;
                }
                if(flag == 0)
                InsertArray(intersection,array1->dp[i]);
            }
        }
    }
    return intersection;
}

void QuChong(dma *array)
{
    FastSortSmaller(array->dp,0,array->len-1);
    for(int i=0;i<array->len-1;i++)
    {
        if(array->dp[i] == array->dp[i+1])
        {
            RemoveByIndex(array,i);
            i--;
        }
    }    
}

dma *FindUnionSet(dma *array1, dma *array2)
{
    Init_ArrayPoint(unionset,"findunionset malloc error!\n");
    ArrayInit(unionset);
    for(int i=0;i<array1->len;i++)
    {
        InsertArray(unionset,array1->dp[i]);
    }
    for(int i=0;i<array2->len;i++)
    {
        InsertArray(unionset,array2->dp[i]);
    }
    QuChong(unionset);
    return unionset;
}

dma *MergeArray(dma *array1, dma *array2)
{
    FastSortSmaller(array1->dp,0,array1->len-1);
    FastSortSmaller(array2->dp,0,array2->len-1);
    Init_ArrayPoint(merge,"mergearray malloc error!\n");
    ArrayInit(merge);
    int i=0,j=0;
    while(i<array1->len&&j<array2->len)
    {
        if(array1->dp[i]<array2->dp[j])
        {
            InsertArray(merge,array1->dp[i]);i++;
        }
        else
        {
            InsertArray(merge,array2->dp[j]);j++;
        }
    }
    while(i<array1->len)
    {
        InsertArray(merge,array1->dp[i]);i++;
    }
    while(j<array2->len)
    {
        InsertArray(merge,array2->dp[j]);j++;
    }
    return merge;
}

void FastSortSmaller(int *a,int start,int end)
{
    int temp=a[start];
    int left=start;
    int right=end;
    while(left<right)
    {
        while(left<right && temp<a[right])
        {
            right--;
        }
        if(left < right)
        {
        a[left]=a[right];
        left++;
        }
        while(right>left&&temp>a[left])
        {
            left++;
        }
        if(left < right)
        {
        a[right]=a[left];
        right--;
        }
        a[left]=temp;
    FastSortSmaller(a,start,left-1);
    FastSortSmaller(a,right+1,end);
    } 
}

void FastSortBigger(int *a,int start,int end)
{
    int temp=a[start];
    int left=start;
    int right=end;
    while(left<right)
    {
        while(left<right && temp>a[right])
        {
            right--;
        }
        if(left < right)
        {
        a[left]=a[right];
        left++;
        }
        while(right>left&&temp<a[left])
        {
            left++;
        }
        if(left < right)
        {
        a[right]=a[left];
        right--;
        }
        a[left]=temp;
    FastSortBigger(a,start,left-1);
    FastSortBigger(a,right+1,end);
    } 
}

void StackInit(dma *stack)
{
    stack->dp = (ElementType *)malloc(sizeof(ElementType)*stack->size);
    if(stack->dp == NULL)
    {   printf("init malloc error!\n");return;}
}

void Push(dma *stack, ElementType element)
{
    if(stack->len > stack->size)
    {   printf("stack is full!\n");return;}
    stack->dp[stack->len]=element;
    stack->len++;
}

ElementType *Pop(dma *stack)
{
    if(stack->len == 0)
    {printf("stack is empty!\n");return NULL;}
    stack->len--;
    return &stack->dp[stack->len];
}

int main()
{
    dma array1={NULL,10,0};
    ArrayInit(&array1);
    for(int i=0;i<5;i++)
    {
        InsertArray(&array1,i+1);
    }   
    Travel(&array1);
    FreeArray(&array1);
    dma s;
    StackInit(&s);
    s.len=0;
    s.size=20;
    int num = 0;
    printf("please your num :");
    scanf("%d",&num);
    int n = 0;
    printf("please a system num:");
    scanf("%d",&n);
    if(n==16)printf("0x");
    while(num!=0)
    {
        int temp = num % n;
        if(temp <= 9)
        {   temp=temp+'0';  }
        else
        {   temp=temp-10+'A';}
        Push(&s,temp);
        num=num/n;
    }
    while(s.len!=0)
    {printf("%c",*Pop(&s));}
    printf("\n");
    return 0;
}

