#include "BinarySortTree.h"
#include <stdio.h>
#include <stdlib.h>

#define true 1
#define false 0

// 右旋
BSTNode *RotateRight(BSTNode *node)
{
    BSTNode *t = node->left;     // 定义一个t指向当前节点的左孩子
    node->left = t->right;       // 将t的右孩子接到当前节点的左孩子
    if (t->right != NULL)        // 如果t节点有右孩子
        t->right->parent = node; // 将t的右孩子的父指针指向node
    t->right = node;             // 将当前节点作为t的右孩子
    t->parent = node->parent;    // 将t的父节点置为node的父节点
    node->parent = t;            // 将node的父节点置为t
    return t;                    // 将t作为树新的根节点
}

// 左旋
BSTNode *RotateLeft(BSTNode *node)
{
    BSTNode *t = node->right;
    node->right = t->left;
    if (t->left != NULL)
        t->left->parent = node;
    t->left = node;
    t->parent = node->parent;
    node->parent = t;
    return t;
}

// 左右旋
BSTNode *RotateLeftRight(BSTNode *node)
{
    node->left = RotateLeft(node->left);
    return RotateRight(node);
}

// 右左旋
BSTNode *RotateRightLeft(BSTNode *node)
{
    node->right = RotateRight(node->right);
    return RotateLeft(node);
}

// 获取树的高度
int GetNodeHeight(BSTNode *node)
{
    if (node == NULL)
        return 0;
    int leftHeight = GetNodeHeight(node->left);
    int rightHeight = GetNodeHeight(node->right);
    return ((leftHeight > rightHeight ? leftHeight : rightHeight) + 1);
}

// 对一个要插入的值进行初始化，并封装为一个节点，
BSTNode *CreateTreeNode(ElementType element)
{
    BSTNode *newNode = (BSTNode *)malloc(sizeof(BSTNode)); // 在堆上申请空间
    if (newNode == NULL)                                   // 失败打印信息并返回
    {
        printf("CreateTreeNode malloc error!\n");
        return NULL;
    }
    newNode->data = element; // 将数值赋值到节点里
    newNode->left = NULL;    // 将其余指针都指向空
    newNode->right = NULL;
    newNode->parent = NULL;
    return newNode; // 返回新节点
}

// 树的初始化
int InitBSTree(BSTree *tree)
{
    tree->root = NULL; // 树的根节点指向空
    return true;
}

// 节点插入数据，func为函数指针，比较数据的大小
void InsertNode(BSTNode *node, ElementType element, int (*func)(ElementType, ElementType))
{
    if (node == NULL) // 如果节点为空，直接返回
        return;
    if (func(element, node->data)) // 调用函数指针进行判断，两个数据的大小，如果比node小返回真
    {
        if (node->left != NULL) // 直到左边指向一直为空，进行递归循环
            InsertNode(node->left, element, func);
        else
        {
            node->left = CreateTreeNode(element); // 当node的左孩子指向为空后进行插入数据节点
            if (node->left == NULL)               // 如果左边指向仍然为空，打印错误信息并返回
            {
                printf("creatNode error");
                return;
            }
            node->left->parent = node; // 将差入的数据节点的父节点指向node
        }
    }
    else // 要插入的数据比node的数据大
    {
        if (node->right != NULL) // 直到右边指向一直为空，进行递归循环
            InsertNode(node->right, element, func);
        else
        {
            node->right = CreateTreeNode(element);
            if (node->right == NULL)
            {
                printf("creatNode error");
                return;
            }
            node->right->parent = node;
        }
    }
}

// 二叉平衡树的插入
BSTNode *InsertData(BSTNode *node, ElementType data, int (*func)(ElementType, ElementType))
{
    if (node == NULL) // 如果node为空，直接插值
    {
        node = CreateTreeNode(data); // 直接赋值
    }
    else if (func(data, node->data)) // 如果当前值比node的值小
    {
        node->left = InsertData(node->left, data, func); // 在node的左边插入
        node->left->parent = node;
        if (GetNodeHeight(node->left) - GetNodeHeight(node->right) > 1) // 如果左右高度差大于1，进行平衡操作
        {
            if (func(data, node->left->data)) // 如果是左左树
            {
                node = RotateRight(node); // 右旋
            }
            else // 或者是左右树
            {
                node = RotateLeftRight(node); // 左右旋
            }
        }
    }
    else // 或者当前值比node的值大
    {
        node->right = InsertData(node->right, data, func); // 在node的右边插入
        node->right->parent = node;
        if (GetNodeHeight(node->right) - GetNodeHeight(node->left) > 1) // 如果左右高度差大于1，进行平衡操作
        {
            if (func(data, node->right->data)) // 如果是右左树
            {
                node = RotateRightLeft(node); // 右左旋
            }
            else // 或者是右右树
            {
                node = RotateLeft(node); // 左旋
            }
        }
    }
    return node;
}

// 平衡树插值
void InsertBalanceTree(BSTree *tree, ElementType element, int (*func)(ElementType, ElementType))
{
    tree->root = InsertData(tree->root, element, func);
}

// 给树插值
void InsertElement(BSTree *tree, ElementType element, int (*func)(ElementType, ElementType))
{
    if (tree->root == NULL) // 如果根节点为空，直接插入
    {
        tree->root = CreateTreeNode(element);
        if (tree->root == NULL) // 插入失败，打印错误信息，并返回
        {
            printf("creat root error!\n");
            return;
        }
    }
    else
    {
        InsertNode(tree->root, element, func); // 如果根节点不为空，调用节点插值函数
    }
}

// 释放节点
void FreeNode(BSTNode *node)
{
    if (node == NULL) // 如果节点本身为空，直接返回
    {
        return;
    }
    FreeNode(node->left); // 递归放空左孩子右孩子节点
    FreeNode(node->right);
    free(node->data); // 释放数据
    free(node);       // 释放节点
}

// 树的释放
void FreeTree(BSTree *tree)
{
    FreeNode(tree->root); // 从根节点释放
    tree->root = NULL;    // 根节点置空
}

// 先序遍历
void PrevPrint(BSTNode *node, void (*func)(ElementType))
{
    if (node != NULL)
    {
        func(node->data);             // 先输出自己
        PrevPrint(node->left, func);  // 在递归输出左孩子
        PrevPrint(node->right, func); // 在递归输出右孩子
    }
}

// 输出一个树的值
void PrevTravel(BSTree *tree, void (*func)(ElementType))
{
    printf("prev: \n");
    PrevPrint(tree->root, func);
    printf("\n");
}

// 中序遍历
void MidPrint(BSTNode *node, void (*func)(ElementType))
{
    if (node != NULL)
    {
        MidPrint(node->left, func);  // 先遍历左孩子
        func(node->data);            // 调用函数来输出里面的数据
        MidPrint(node->right, func); // 最后遍历右孩子
    }
}

// 输出一个树的值
void MidTravel(BSTree *tree, void (*func)(ElementType))
{
    printf("Mid: \n");
    MidPrint(tree->root, func);
    printf("\n");
}

// 后序遍历
void PostPrint(BSTNode *node, void (*func)(ElementType))
{
    if (node != NULL)
    {
        PostPrint(node->left, func);  // 先左
        PostPrint(node->right, func); // 在右
        func(node->data);             // 在自己
    }
}

// 输出一个树的值
void PostTravel(BSTree *tree, void (*func)(ElementType))
{
    printf("Post: \n");
    PostPrint(tree->root, func);
    printf("\n");
}

// 查值删除
void DeleteElement(BSTree *tree, ElementType element, int (*func)(ElementType, ElementType))
{
    // 调用找值函数并用新的指针指向他
    BSTNode *targetNode = FindElement(tree, element, func);
    if (targetNode == NULL) // 如果该节点为空直接返回
        return;
    if (targetNode->left == NULL && targetNode->right == NULL) // 如果该节点没有孩子
    {
        if (targetNode->parent == NULL) // 如果该节点为根节点
        {
            tree->root = NULL; // 根节点置为空
        }
        else if (targetNode->parent->left == targetNode) // 如果该节点为父节点的左孩子
        {
            targetNode->parent->left = NULL; // 将父节点的左孩子置为空
        }
        else // 否则为父节点的右孩子，
        {
            targetNode->parent->right = NULL; // 将父节点的右孩子置为空
        }
        free(targetNode->data); // 释放该节点里的数据
        free(targetNode);       // 释放该节点
    }
    else if (targetNode->right == NULL) // 如果该节点有左孩子
    {
        if (targetNode->parent == NULL) // 如果该节点为根节点
        {
            tree->root = targetNode->left; // 将根节点指向他的左孩子
        }
        else if (targetNode->parent->left == targetNode) // 如果该节点为父节点的左孩子
        {
            targetNode->parent->left = targetNode->left; // 将父节点的左孩子指向他的左孩子
        }
        else // 否则为父节点的右孩子，
        {
            targetNode->parent->right = targetNode->left; // 将父节点的右孩子指向他的左孩子
        }
        free(targetNode->data); // 释放该节点里的数据
        free(targetNode);       // 释放该节点
    }
    else if (targetNode->left == NULL) // 如果该节点有左孩子
    {
        if (targetNode->parent == NULL) // 如果该节点为根节点
        {
            tree->root = targetNode->right; // 将根节点指向他的右孩子
        }
        else if (targetNode->parent->left == targetNode) // 如果该节点为父节点的左孩子
        {
            targetNode->parent->left = targetNode->right; // 将父节点的左孩子指向他的右孩子
        }
        else // 否则为父节点的右孩子
        {
            targetNode->parent->right = targetNode->right; // 将父节点的右孩子指向他的右孩子
        }
        free(targetNode->data); // 释放该节点里的数据
        free(targetNode);       // 释放该节点
    }
    else // 如果该节点左右孩子都有
    {
        BSTNode *MinNode = targetNode->right; // 定义一个新节点去找该节点右孩子里的最小值
        while (MinNode->left != NULL)         // 直到该节点右孩子的左孩子指向空，退出循环
        {
            MinNode = MinNode->left; // 新节点一直向该节点的右孩子里的左孩子寻找
        }
        ElementType temp = targetNode->data; // 定义一个temp去接收该节点的值
        targetNode->data = MinNode->data;    // 将找到的节点的值赋值给该节点
        MinNode->data = temp;                // 将该节点的值赋值给找到的节点

        if (MinNode->parent == targetNode) // 如果找到的节点为该节点的右孩子
        {
            targetNode->right = MinNode->right; // 将找到的节点的右孩子赋值给该节点的右孩子
        }
        else // 如果找到的节点不是该节点的右孩子
        {
            MinNode->parent->left = MinNode->right; // 将找到的节点的右孩子给找到的节点的父节点的左孩子（让自己的右孩子顶替自己的位置）
        }
        free(MinNode->data); // 释放找到的节点里的值
        free(MinNode);       // 释放该节点
    }
}

// 查找一个值
BSTNode *findNode(BSTNode *node, ElementType element, int (*func)(ElementType, ElementType))
{
    if (node == NULL) // 如果node本身为空，直接返回空地址
        return NULL;
    if (func(node->data, element) == 0) // 如果node的值就是要找的值，返回node
    {
        return node;
    }
    else if (func(node->data, element) < 0) // 如果要找的值比node的值大，在node的右孩子里递归寻找
    {
        return findNode(node->right, element, func);
    }
    else // 否则在左孩子递归寻找
    {
        return findNode(node->left, element, func);
    }
}

// 在树中找值
BSTNode *FindElement(BSTree *tree, ElementType element, int (*func)(ElementType, ElementType))
{
    return findNode(tree->root, element, func);
}

// 获取叶子数量，没有左右孩子的节点
int GetLeafNode(BSTNode *node)
{
    if (node == NULL) // 如果node为空，返回0
        return 0;
    if (node->left == NULL && node->right == NULL) // 如果是叶子节点，返回1
        return 1;
    int leftNum = GetLeafNode(node->left);   // 递归左孩子叶子节点数量
    int rightNum = GetLeafNode(node->right); // 递归右孩子叶子节点数量
    return leftNum + rightNum;               // 将两数相加
}

// 获取树中叶子数量
int GetLeafNum(BSTree *tree)
{
    return GetLeafNode(tree->root); // 调用函数
}
