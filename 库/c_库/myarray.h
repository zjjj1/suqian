#ifndef __MYARRAY_H__
#define __MYARRAY_H__

#define ElementType int

struct DynamicArray
{
    ElementType *dp;
    int size;
    int len;
};

typedef struct DynamicArray dma;

void Travel(dma *array);
int ArrayInit(dma *array);
void FreeArray(dma *array);
int ReallocArray(dma *array);
int InsertArray(dma *array, ElementType element);

void InsertTail(dma *array, ElementType element);
void InsertHead(dma *array, ElementType element);
void InsertByIndex(dma *array, ElementType element, int index);
void RemoveByIndex(dma *array, int index);
void RemoveByElement(dma *array, ElementType element);
ElementType *FindByIndex(dma *array, int index);
int *FindByElement(dma *array, ElementType element);
int *FindByElement2(dma *array, ElementType element);
void SetValueByIndex(dma *array, int index, ElementType element);
void SetValueByElement(dma *array, ElementType oldValue, ElementType newValue);
dma *findIntersection(dma *array1, dma *array2);
void QuChong(dma *array);
dma *FindUnionSet(dma *array1, dma *array2);
dma *MergeArray(dma *array1, dma *array2);

void FastSortSmaller(int *a, int start, int end);
void FastSortBigger(int *a, int start, int end);

void StackInit(dma *stack);
void Push(dma *stack,ElementType element);
ElementType* Pop(dma *stack);

#endif