#include <stdlib.h>
#include <stdio.h>
#include "LinkList.h"

#define true 1
#define false 0

int LinkListInit(link *list)
{
    list->head = (node *)malloc(sizeof(node));
    if(list->head == NULL)
    {
        printf("initlinklist malloc error!\n");
        return false;
    }
    list->head->data = 0;
    list->head->next = NULL;
    list->len = 0;
    return true;
}

node * CreatNode(ElementType element)
{
    node *NewNode = (node *)malloc(sizeof(node));
    if(NewNode == NULL)
    {
        printf("CreatNode malloc error!\n");
        return NULL;
    }
    NewNode->data = element;
    NewNode->next = NULL;
    return NewNode;
}

void InsertTail(link *list, ElementType element)
{
    node *NewNode = CreatNode(element);
    if(NewNode == NULL)
    {
        printf("insertTail CreatNode malloc error!\n");
        return ;
    }
    
    node *TravelPoint = list->head;
    while(TravelPoint->next != NULL)
    {
        TravelPoint = TravelPoint->next;
    }
    TravelPoint->next= NewNode;
    list->len++;

}

void InsertHead(link *list, ElementType element)
{
    node *NewNode = CreatNode(element);
    if(NewNode == NULL)
    {
        printf("insertTail CreatNode malloc error!\n");
        return ;
    }
    NewNode->next = list->head->next;
    list->head->next=NewNode;
    list->len++;
}

void InsertByIndex(link *list, ElementType element, int index)
{
    if(index<0||index>list->len)
    {
        printf("InsertByIndex invalid please!\n");
        return;
    }
    node *NewNode = CreatNode(element);
    if(NewNode == NULL)
    {
        printf("insertTail CreatNode malloc error!\n");
        return ;
    }
    node *TravelPoint = list->head;//tou jiedian bucanyu jisuan
    while(index !=0)//chazai 'index' de qianyige jiedian
    {
        TravelPoint = TravelPoint->next;
        index--;
    }
    NewNode->next = TravelPoint->next;
    TravelPoint->next=NewNode;
    list->len++;
}

void RemoveByIndex(link *list, int index)
{
    if(index<0||index>=list->len)
    {
        printf("RemoveByIndex invalid please!\n");
        return;
    }

    node *TravelPoint = list->head;
    while(index !=0)
    {
        TravelPoint = TravelPoint->next;
        index--;
    }

    struct Node *Freenode = TravelPoint->next;
    TravelPoint->next = Freenode->next;
    free(Freenode);
    list->len--;
}

void RemoveByElement(link *list, ElementType element)
{
    node *TravelPoint = list->head;
    while(TravelPoint->next != NULL)
    {
        if(TravelPoint->next->data == element)
        {
            struct Node *Freenode = TravelPoint->next;
            TravelPoint->next = Freenode->next;
            free(Freenode);
            list->len--;
        }
        else
        {
        TravelPoint = TravelPoint->next;
        }
    }
}

ElementType *FindByIndex(link *list, int index)
{
    if(index<0||index>=list->len)
    {
        printf("FindByIndex invalid please!\n");
        return NULL;
    }
    node *TravelPoint = list->head;
    while(index != 0)
    {
        TravelPoint = TravelPoint->next;
        index--;
    }
    return &TravelPoint->next->data;
}

int *FindByElement(link *list, ElementType element)
{
    int *findVector = (int *)malloc(sizeof(int) * list->len+1);
    if(findVector == NULL)
     {
        printf("FindByElement malloc error!\n");
        return NULL;
    }
    node *TravelPoint = list->head;
    int count = 0;
    int k = 0;
    while(TravelPoint->next != NULL)
    {
        if(TravelPoint->next->data == element)
        {
        findVector[k] = count;
        k++;
        }
        count++;
        TravelPoint = TravelPoint->next;
    }
    findVector[k] = -1;
    return findVector;
}

void SetValueByIndex(link *list, ElementType element, int index)
{
    int *temp=FindByIndex(list,index);
    *temp= element;
}

void SetValueByElement(link *list, ElementType oldValue, ElementType newValue)
{
    int *findVector = FindByElement(list,oldValue);
    if(findVector == NULL)
     {
        printf("can not find element!\n");
        return;
    }
    int *temp = findVector;
    while(*temp != -1)
    {
        SetValueByIndex(list,newValue,*temp);
        temp++;
    }
    free(findVector);
}

void BubbleSort(link *list)
{   
    for(int i=0;i<list->len-1;i++)
    {
        node *TravelPoint = list->head;
        for(int j=0;j<list->len-i-1;j++)
        {
            if(TravelPoint->next->data>TravelPoint->next->next->data)
            {
                node *Prev = TravelPoint->next;
                node *Next = TravelPoint->next->next;
                Prev->next = Next->next;
                Next->next = Prev;
                TravelPoint->next = Next;
            }
            TravelPoint = TravelPoint->next;
        }
    }
}

void FastSort(link *list)
{
    if ( list->head->next == NULL || list->head->next->next == NULL ) 
    {
        return;
    }
    node *TravelPoint = list->head->next->next;
    node *Temp=list->head->next;   
    list->head->next=NULL;
    Temp->next=NULL;
    link list1;
    LinkListInit(&list1);
    node *Smaller = list1.head;
    link list2;
    LinkListInit(&list2);
    node *Bigger = list2.head;
    while(TravelPoint != NULL)
    {

        while (TravelPoint != NULL) {
        node *Next = TravelPoint->next;
        if (Temp->data >TravelPoint->data) {
           Smaller->next = TravelPoint;
            Smaller = Smaller->next;}
        else {
            
           Bigger->next = TravelPoint;
            Bigger = Bigger->next;
        }
        TravelPoint->next = NULL;
        TravelPoint = Next; }

        if(TravelPoint == NULL)
        {
        Smaller->next=Temp;
        Smaller=Smaller->next;
        FastSort(&list1);
        FastSort(&list2);
        }
    }
    Smaller->next=list2.head->next;
    free(list2.head);
    list2.head->next=NULL;
    list->head->next=list1.head->next;
    free(list1.head);
    list1.head->next=NULL; 
}

void Reserve(link *list)
{
    node *Prev = NULL;
    node *Cur = list->head->next;
    node *Next = Cur->next;
    while(Next != NULL)
    {
        Cur->next = Prev;
        Prev = Cur;
        Cur = Next;
        Next = Cur->next;
    }
    Cur->next = Prev;
    list->head->next = Cur;
}

void Reserve1(link *list)
{
    node *TravelPoint = list->head->next->next;
    list->head->next->next= NULL;
    while(TravelPoint != NULL)
    {
        node *Prev=list->head->next;
        node *temp=TravelPoint;
        node *Next=TravelPoint->next;
        temp->next=Prev;
        list->head->next=temp;
        TravelPoint=Next;
    }
}

node *Reserve2(node *nod)
{
    node *tail;
    if(nod->next !=NULL)
    {
        tail = Reserve2(nod->next);
        nod->next->next = nod;
        nod->next = NULL;
        return tail;
    }
    return nod;
}

link *MergeList(link *list1, link *list2)
{
    BubbleSort(list1);
    BubbleSort(list2);
    link *list3 = (link *)malloc(sizeof(link));
    if(list3 == NULL)
    {
        printf("MergeList malloc error");
        return NULL;
    }
    LinkListInit(list3);
    node *TravelPoint = list3->head;
    while(list1->head->next != NULL && list2->head->next != NULL)
    {
        if(list1->head->next->data<list2->head->next->data)
        {
            TravelPoint->next = list1->head->next;
            list1->head->next = list1->head->next->next;
            list3->len++;
        }
        else
        {
            TravelPoint->next = list2->head->next;
            list2->head->next = list2->head->next->next;
            list3->len++;
        }
        TravelPoint = TravelPoint->next;
    }
    while(list1->head->next != NULL)
    {
        TravelPoint->next = list1->head->next;
        list3->len++;
        TravelPoint=TravelPoint->next;
        list1->head=list1->head->next;
    }
    while(list2->head->next != NULL)
    {
        TravelPoint->next = list2->head->next;
        list3->len++;
        TravelPoint=TravelPoint->next;
        list2->head=list2->head->next;
    }
    TravelPoint->next=NULL;
    return list3;
}

void Travel(link *list)
{
    printf("len: %d\n",list->len);
    node *TravelPoint = list->head->next;
    while(TravelPoint != NULL)
    {
        printf("%d ",TravelPoint->data);
        TravelPoint = TravelPoint->next;
    }
    printf("\n");
}

void FreeLinkList(link *list)
{
    node *TravelPoint = list->head->next;
    while(TravelPoint != NULL)
    {
        free(list->head);
        list->head = TravelPoint;
        TravelPoint = list->head->next;
    }
    free(list->head);
    list->head = NULL;
    list->len = 0;
}
