#ifndef __STDSQLITE_H__
#define __STDSQLITE_H__

#define Size_len(s) sizeof(s) / sizeof(s[0])

struct SsdSqlite;
typedef struct SsdSqlite SQL;

// 数据库的初始化
SQL *InitSqlite(const char *filename);
// 在库中创建表
void CreatTable(SQL *s, const char *tableName, char **prolist, int row);
// 删表
void DeleteTable(SQL *s, const char *tableName);
// 插入数据
void InsertDate(SQL *s, const char *tableName, char **values, int size);
// 删除数据
void DeleteData(SQL *s, const char *tableName, char *where);
// 修改数据
void UpdataData(SQL *s, const char *tableName, const char *SetValue, const char *where);
// 查找数据
void GetTableInfo(SQL *s, const char *tableName, char ***result, int *row, int *column);

void SelectInfo(SQL *s, const char *sql, char ***result, int *row, int *column);
// 获取一张表的长度
int GetTableLen(SQL *s, const char *tableName);
// 判断一个表中有无数据
int IsTableEmpty(SQL *s, const char *tableName);
// 释放数据库
void FreeSqlite(SQL *s);
void FreeInfoResult(char **result);

#endif