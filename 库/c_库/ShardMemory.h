#ifndef __SHARDMEMORY_H__
#define __SHARDMEMORY_H__

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>

#include <sys/types.h>
#include <sys/shm.h>

struct ShardMemory;
typedef struct ShardMemory ShMemory;

//创建共享内存
ShMemory *InitShardMemory(const char *path,int pro_id,size_t size);
//清理共享内存
void ClearShardMemory(ShMemory *m);
//读取共享内存
void ReadFromClearShardMemory(ShMemory *m,void *ptr,size_t size);
//写入共享内存
void WriteFromClearShardMemory(ShMemory *m,void *ptr,size_t size);



#endif 