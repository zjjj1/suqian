#ifndef __LINKTREE_H__
#define __LINKTREE_H__

#define ElementType struct UniversalType

enum Type//定义一个枚举体，用来提示数据类型
{
    charPoint,
    intPoint,
    structPoint
};

struct UniversalType//定义一个通用结构体
{
    void *value;//定义一个万能指针，指向他的数据
    enum Type type;//提示上面指针指向的数据类型
};

struct LinkTreeNode//定义一个树节点
{
    ElementType data;//存放值
    struct LinkTreeNode *firstChild;//指向子节点
    struct LinkTreeNode *nextSibling;//指向兄弟节点
    struct LinkTreeNode *parent;//指向父节点
    int deepth;//高度
};

typedef struct LinkTreeNode LTNode;

LTNode *CreateTreeNode(ElementType element);
void ConnectBranch(LTNode *parent, LTNode *child);
void DisConnectBranch(LTNode *parent, LTNode *child);
int GetNodeHeight(LTNode *treeNode);
void FreeLTNode(LTNode *treeNode);

struct LinkTree//定义一个树
{
    LTNode *root;
};

typedef struct LinkTree LTree;

int InitLTree(LTree *tree);
int GetTreeHeight(LTree *tree);
LTNode *FindTreeNode(LTree *tree, ElementType element, int (*func)(ElementType, ElementType));
void FreeLTree(LTree *tree);
void TravelLTree(LTree *tree, void (*func)(ElementType));

#endif