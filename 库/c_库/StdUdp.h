#ifndef __STDUDP_H__
#define __STDUDP_H__

#include <stddef.h>

struct UdpServer;
typedef struct UdpServer UdpS;

UdpS *InitUpdsServer(const char *IP, short int port);
void UpdsServerSend(UdpS *s,const char*destip,short int port, void*ptr,size_t size);
void UpdsServerRecv(UdpS *s,void*ptr,size_t size);
void ClearUpdsServer(UdpS *s);

struct UdpClient;
typedef struct UdpClient UdpC;

UdpC *InitUdpClient(const char *ClientIP, short int Clientport);
void UpdsClientSend(UdpC *c, void*ptr,size_t size);
void UpdsClientRecv(UdpC *c, void*ptr,size_t size);
void ClearUpdsClient(UdpC *c);

#endif