#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>

#include <fcntl.h>
#include "StdFIFO.h"

#define true 1
#define false 0



struct StdIFO
{
    char Path[PATH_SIZE];
    int fd; // 文件标志符
    Mode mode;
};

FIFO *InitFIFO( const char *path)
{
    FIFO *f= (FIFO *)malloc (sizeof(FIFO));
    if(f==NULL)
    {
        printf("初始化FIFO申请空间失败!\n");
        return NULL;
    }
    int ret = mkfifo(path, S_IRUSR | S_IWUSR);
    if (ret == -1)
        printf("这个文件可能已经存在又或者您没有权限！\n");
    strcpy(f->Path, path);
    return f;
}

int OpenFIFO(FIFO *f, Mode mode)
{
    if(f==NULL)
    {
        printf("f为空\n");
        return false;
    }
    if (mode == ReadOnly)
    {
        f->fd = open(f->Path, O_RDONLY);
        if (f->fd < 0)
        {
            perror("OpenFIFO open: ");
            return false;
        }
    }
    else if (mode == WriteOnly)
    {
        f->fd = open(f->Path, O_WRONLY);
        if (f->fd < 0)
        {
            perror("OpenFIFO open: ");
            return false;
        }
    }
    f->mode = mode;
    return true;
}

void WriteToFIFO(FIFO *f, void *buff, size_t size)
{
    if(f==NULL)
    {
        printf("f为空\n");
        return;
    }
    if (f->mode == ReadOnly)
    {
        printf("您无法写入，您的权限是只读！\n");
        return;
    }
    if (write(f->fd, buff, size) <= 0)
    {
        printf("写入有问题！\n");
        return;
    }
}

void ReadFormFIFO(FIFO *f, void *buff, size_t size)
{
    if(f==NULL)
    {
        printf("f为空\n");
        return;
    }
    if (f->mode == WriteOnly)
    {
        printf("您无法读取，您的权限是只写！\n");
        return;
    }
    if (read(f->fd, buff, size) <= 0)
    {
        printf("读取有问题！\n");
        return;
    }
}

void SetBlock(FIFO *f, int IsBlock)
{
    if(f==NULL)
    {
        printf("f为空\n");
        return;
    }
    if (IsBlock == true)
    {
        fcntl(f->fd, F_SETFL, 0);
    }
    else
    {
        fcntl(f->fd, F_SETFL, O_NONBLOCK);
    }
}

void ClearFIFO(FIFO *f)
{
    close(f->fd);
    char RMStr[PATH_SIZE*2] = {0};
    sprintf(RMStr, "rm -f %s", f->Path);
    system(RMStr);
    free(f);
}
