#ifndef __LINKSTACK_H__
#define __LINKSTACK_H__

#include "DoubleLinklist.h"

struct LinkStack
{
    DLlist stack;
    ElementType TopElement;
};

typedef struct LinkStack lstack;

int InitStack(lstack *s);
void SPush(lstack *s, ElementType element);
ElementType *SPop(lstack *s);
node *GetTop(lstack *s);
int IsEmpty(lstack *s);
void StackTravle(lstack *s, void (*func)(ElementType));
void FreeLstack(lstack *s, void (*func)(ElementType));

#endif