#ifndef __DOUBLECIRCLELINKLIST_H__
#define __DOUBLECIRCLELINKLIST_H__

#define ElementType int

struct Node
{
    ElementType data;
    struct Node *next;
    struct Node *prev;  
};

struct DoubleCircleLinklist
{
    struct Node *head;
    struct Node *tail;
    int len;
};

typedef struct Node node;
typedef struct DoubleCircleLinklist DCLlist;

int InitDCLlist(DCLlist *list);
void InsertTail(DCLlist *list,ElementType element);
void InsertHead(DCLlist *list,ElementType element);
void RemoveByIndex(DCLlist *list,int index);
void RemoveByElement(DCLlist *list,ElementType element);
int FindFirstDCLByElement(DCLlist *list,ElementType element);
void Travel(DCLlist *list);
void FreeDCLlist(DCLlist *list);

#endif