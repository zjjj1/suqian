#include <sqlite3.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "StdSqlite.h"

#define true 1
#define false 0

struct SsdSqlite
{
    sqlite3 *db;
};

SQL *InitSqlite(const char *filename)
{
    SQL *s = (SQL *)malloc(sizeof(SQL));
    if (s == NULL)
    {
        printf("InmitSqlite malloc error!\n");
        return NULL;
    }

    if (sqlite3_open(filename, &s->db) != SQLITE_OK)
    {
        printf("open %s error :%s\n", filename, sqlite3_errmsg(s->db));
        free(s);
        return NULL;
    }
    return s;
}

void CreatTable(SQL *s, const char *tableName, char **prolist, int row)
{
    char propertry[1024] = {0};
    for (int i = 0; i < row; i++)
    {
        strcat(propertry, prolist[2 * i]);
        strcat(propertry, " ");
        strcat(propertry, prolist[2 * i + 1]);
        if (i != row - 1)
            strcat(propertry, ",");
    }
    char sql[4096] = {0};
    sprintf(sql, "create table %s(%s);", tableName, propertry);
    if (sqlite3_exec(s->db, sql, NULL, NULL, NULL) != SQLITE_OK)
    {
        printf("creat table error : %s\n", sqlite3_errmsg(s->db));
    }
}

void DeleteTable(SQL *s, const char *tableName)
{
    char sql[4096] = {0};
    sprintf(sql, "drop table %s;", tableName);
    if (sqlite3_exec(s->db, sql, NULL, NULL, NULL) != SQLITE_OK)
    {
        printf("DeleteTable error : %s\n", sqlite3_errmsg(s->db));
    }
    
}

void InsertDate(SQL *s, const char *tableName, char **values, int size)
{
    char valuesList[1024] = {0};
    for (int i = 0; i < size; i++)
    {
        strcat(valuesList, values[i]);
        if (i != size - 1)
            strcat(valuesList, ",");
    }
    char sql[4096] = {0};
    sprintf(sql, "insert into %s values(%s);", tableName, valuesList);
    if (sqlite3_exec(s->db, sql, NULL, NULL, NULL) != SQLITE_OK)
    {
        printf("creat table error : %s\n", sqlite3_errmsg(s->db));
    }
}

void DeleteData(SQL *s, const char *tableName, char *where)
{
    char sql[4096] = {0};
    if (where == NULL)
        sprintf(sql, "delete from %s;", tableName);
    else
        sprintf(sql, "delete from %s where %s;", tableName, where);
    if (sqlite3_exec(s->db, sql, NULL, NULL, NULL) != SQLITE_OK)
    {
        printf("DeleteData error : %s\n", sqlite3_errmsg(s->db));
    }
}

void UpdataData(SQL *s, const char *tableName, const char *SetValue, const char *where)
{
    char sql[4096] = {0};
    sprintf(sql, "update %s set %s where %s;", tableName, SetValue, where);
    if (sqlite3_exec(s->db, sql, NULL, NULL, NULL) != SQLITE_OK)
    {
        printf("UpdataData error : %s\n", sqlite3_errmsg(s->db));
    }
}

void GetTableInfo(SQL *s, const char *tableName, char ***result, int *row, int *column)
{
    char sql[4096] = {0};
    sprintf(sql, "select *from %s;", tableName);
    if (sqlite3_get_table(s->db, sql, result, row, column, NULL) != SQLITE_OK)
    {
        printf("GetTableInfo error : %s\n", sqlite3_errmsg(s->db));
    }
}

void SelectInfo(SQL *s, const char *sql, char ***result, int *row, int *column)
{
    if (sqlite3_get_table(s->db, sql, result, row, column, NULL) != SQLITE_OK)
    {
        printf("GetTableLen error : %s\n", sqlite3_errmsg(s->db));
    }
}

int GetTableLen(SQL *s, const char *tableName)
{
    char sql[4096] = {0};
    sprintf(sql, "select count(*) from %s;", tableName);
    char **result;
    int row, column;
    if (sqlite3_get_table(s->db, sql, &result, &row, &column, NULL) != SQLITE_OK)
    {
        printf("GetTableLen error : %s\n", sqlite3_errmsg(s->db));
    }
    int len = atoi(result[column]);
    sqlite3_free_table(result);
    return len;
}

int IsTableEmpty(SQL *s, const char *tableName)
{
    if (GetTableLen(s, tableName) > 0)
        return false;
    else
        return true;
}

void FreeSqlite(SQL *s)
{
    if (s == NULL)
        return;
    sqlite3_close(s->db);
    free(s);
}

void FreeInfoResult(char **result)
{
    sqlite3_free_table(result);
}
