#ifndef __BUBLLEA_H__
#define __BUBLLEA_H__

int IsSmaller(int a,int b);
int IsBigger(int a,int b);
void BulleSort(int *a,int len,int (*Rule)(int,int));
void BulleSort2(int *a,int len);
void InsertSort(int *a,int len);
void ChooseSort(int *a,int len);
void ChooseSort2(int *a,int len);
void FastSort(int *a,int start,int end);
#endif