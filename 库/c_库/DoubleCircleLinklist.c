#include <stdio.h>
#include <stdlib.h>
#include "DoubleCircleLinklist.h"

int InitDCLlist(DCLlist *list)
{
    list->head = NULL;
    list->tail = NULL;
    list->len = 0;
    return 0;

}

node *CreatNode(ElementType elemen)
{
    node *newNode = (node *)malloc(sizeof(node));
    if(newNode == NULL)
    {
        printf("CreatNode malloc error!\n");
        return NULL;
    }
    newNode->data = elemen;
    newNode->next = NULL;
    newNode->prev = NULL;
    return newNode;
}

void InsertTail(DCLlist *list, ElementType element)
{
    node *newNode = CreatNode(element);
    if(newNode == NULL)
    {
        printf("InsertTail malloc error!\n");
        return;
    }
    if(list->len == 0)
    {
        list->head = newNode;
        list->tail = newNode;
    }
    else
    {
        list->tail->next = newNode;
        newNode->prev = list->tail;
        list->tail = newNode;
        // list->tail->next = list->head;
        // list->head->prev = list->tail;
    }
    list->tail->next = list->head;
    list->head->prev = list->tail;
    list->len++;
}

void InsertHead(DCLlist *list, ElementType element)
{
    // node *newNode = CreatNode(element);
    // if(newNode == NULL)
    // {
    //     printf("InsertHead malloc error!\n");
    //     return;
    // }
    // if(list->len == 0)
    // {
    //     list->head = newNode;
    //     list->tail = newNode;
    // }
    // else
    // {
    //     list->head->prev = newNode;
    //     newNode->next = list->head;
    //     list->head = newNode;
    //     list->tail->next = list->head;
    //     list->head->prev = list->tail;
    // }
    // list->len++;
    
    InsertTail(list,element);
    // if(list->len>1)
    // {
        list->tail = list->tail->prev;
        list->head = list->head->prev;
    // }

}

void RemoveByIndex(DCLlist *list, int index)
{
    if( index<0 || index>=list->len ) 
    {
        printf("RemoveByIndex invalid please!\n");
        return;
    }
    if(index == 0)
    {
        if(list->len == 1)
        {
            FreeDCLlist(list);
            return;
        }
        node *freeNode = list->head;
        list->head = list->head->next;
        list->head->prev = list->tail;
        list->tail->next = list->head;
        free(freeNode);
        list->len--;
        return;
    }
    if(index == list->len-1)
    {
        node *freeNode = list->tail;
        list->tail = list->tail->prev;
        list->tail->next = list->head;
        list->head->prev = list->tail;
        free(freeNode);
        list->len--;
        return;
    }
    node *TravelPoint = list->head;
    while(index>0)
    {
        TravelPoint=TravelPoint->next;
        index--;
    }
    node *PrevNode = TravelPoint->prev;
    node *NextNode = TravelPoint->next;
    PrevNode->next = TravelPoint->next;
    NextNode->prev = TravelPoint->prev;
    free(TravelPoint);
    list->len--;
}

void RemoveByElement(DCLlist *list, ElementType element)
{
    int index = FindFirstByElement(list,element);
    while(index != -1)
    {
        RemoveByIndex(list,index);
        index = FindFirstByElement(list,element);
    }
}

int FindFirstDCLByElement(DCLlist *list, ElementType element)
{
    int count = 0;
    node *TravelPoint = list->head;
    while(TravelPoint != list->tail)
    {
        if(TravelPoint->data == element)
        {
            return count;
        }
        
        count++;
        TravelPoint = TravelPoint->next;
    }
    if (list->tail != NULL && list->tail->data == element)
    {
        return count;
    }
    return -1;
}

void Travel(DCLlist *list)
{
    if(list->len == 0) 
    {
        printf("len: %d\n",list->len);
        return;
    }
    printf("len:  %d\n",list->len);
    printf("next: ");
    node *TravelPoint = list->head;
    while(TravelPoint != list->tail)
    {
        printf("%2d ",TravelPoint->data);
        TravelPoint = TravelPoint->next;
    }
    printf(" %d ",list->tail->data);

    printf("\nprev: ");
    TravelPoint = list->tail;
    while(TravelPoint != list->head)
    {
        printf("%2d ",TravelPoint->data);
        TravelPoint = TravelPoint->prev;
    }
    printf(" %d ",list->head->data);
    printf("\n");
}

void FreeDCLlist(DCLlist *list)
{
    if(list->len == 0) return;
    
    while(list->head != list->tail)
    {
        node *freeNode = list->head;
        list->head=list ->head->next;
        free(freeNode);
    }
    free(list->head);
    list->head = NULL;
    list->tail = NULL;
    list->len = 0;
}
