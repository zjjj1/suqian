#include "LinkStack.h"
#include <stdio.h>
#define true 1
#define false 0

int InitStack(lstack *s)
{

    return InitDLlist(&s->stack);
}

void SPush(lstack *s, ElementType element)
{
    DLInsertTail(&s->stack,element);
}

ElementType *SPop(lstack *s)
{
    if(s->stack.len == 0)
    {
        printf("the stack is empty");
        return NULL;
    }
    s->TopElement= s->stack.tail->data;
    DLRemoveByIndex(&s->stack,s->stack.len-1);
    return &s->TopElement;
}

node *GetTop(lstack *s)
{
    return s->stack.tail;
}

int IsEmpty(lstack *s)
{
    if(s->stack.len == 0)
    {
        return true;
    }
    return false;
}

void FreeLstack(lstack *s, void (*func)(ElementType))
{
    FreeDLlist(&s->stack,func);
}

void StackTravle(lstack *s, void (*func)(ElementType))
{
    DLTravel(&s->stack,func);
}


