#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include "StdUdp.h"

struct UdpServer
{
    int sock;
};

UdpS *InitUpdsServer(const char *IP, short int port)
{
    UdpS *s = (UdpS *)malloc(sizeof(UdpS));
    if (s == NULL)
    {
        printf("初始化UDPS申请空间失败!\n");
        return NULL;
    }
    s->sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (s->sock < 0)
    {
        perror("socket: ");
        free(s);
        return NULL;
    }
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = inet_addr(IP);
    if (bind(s->sock, (struct sockaddr *)&addr, sizeof(addr)) != 0)
    {
        perror("bind:");
        free(s);
        return NULL;
    }
    return s;
}

void UpdsServerSend(UdpS *s, const char *destip, short int port, void *ptr, size_t size)
{
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = inet_addr(destip);
    if (sendto(s->sock, ptr, size, 0, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("UpdsServersebd sendto :");
    }
}

void UpdsServerRecv(UdpS *s, void *ptr, size_t size)
{
    struct sockaddr_in addr;
    socklen_t len;
    if (recvfrom(s->sock, ptr, size, 0, (struct sockaddr *)&addr, &len) < 0)
    {
        perror("UpdsServerRecv recvform:");
    }
}

void ClearUpdsServer(UdpS *s)
{
    close(s->sock);
    free(s);
}

struct UdpClient
{
    int sock;
    char ServerIP[20];
    short int ServerPort;
};

UdpC *InitUdpClient(const char *SreverIP, short int Sreverport)
{
    UdpC *c = (UdpC *)malloc(sizeof(UdpC));
    if (c == NULL)
    {
        printf("初始化UDPC申请空间失败!\n");
        return NULL;
    }
    c->sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (c->sock < 0)
    {
        perror("socket: ");
        free(c);
        return NULL;
    }
    strcpy(c->ServerIP, SreverIP);
    c->ServerPort = Sreverport;

    return c;
}

void UpdsClientSend(UdpC *c, void *ptr, size_t size)
{
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(c->ServerPort);
    addr.sin_addr.s_addr = inet_addr(c->ServerIP);
    if (sendto(c->sock, ptr, size, 0, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("UpdsClientSend sendto :");
    }
}

void UpdsClientRecv(UdpC *c, void *ptr, size_t size)
{
    struct sockaddr_in addr;
    socklen_t len;
    if (recvfrom(c->sock, ptr, size, 0, (struct sockaddr *)&addr, &len) < 0)
    {
        perror("UpdsClientRecv recvform:");
    }
}

void ClearUpdsClient(UdpC *c)
{
    close(c->sock);
    free(c);
}
