#ifndef __LINKLIST_H__
#define __LINKLIST_H__

#define ElementType int
struct Node
{
    ElementType data;
    struct Node *next;
};

struct LinkList
{
    struct Node *head;
    int len;
};

typedef struct Node node;
typedef struct LinkList link;

int LinkListInit(link *list);
void InsertTail(link *list,ElementType element);
void InsertHead(link *list,ElementType element);
void InsertByIndex(link *list,ElementType element,int index);
void RemoveByIndex(link *list,int index);
void RemoveByElement(link *list,ElementType element);
ElementType *FindByIndex(link *list,int index);
int *FindByElement(link *list,ElementType element);
void SetValueByIndex(link *list,ElementType element,int index);
void SetValueByElement(link *list,ElementType oldValue,ElementType newValue);
void BubbleSort(link *list);
void FastSort(link *list);
void Reserve(link *list);
void Reserve1(link *list);
node *Reserve2(node *nod);
link *MergeList(link *list1,link *list2);
void Travel(link *list);
void FreeLinkList(link *list);

#endif