#include "LinkTree.h"
#include <dirent.h>
#include <stdio.h>
#include <string.h>

void ExploreDirectory(const char *path, LTNode *parentNode)
{
    DIR *dir = opendir(path);
    if (dir == NULL)
        return;

    struct dirent *entry = readdir(dir);
    while ((entry = readdir(dir)) != NULL)
    {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
            continue;
        struct UniversalType Typevalue;
        Typevalue.value = entry->d_name;
        Typevalue.type = charPoint;
        LTNode *fileNode = CreateTreeNode(Typevalue);
        ConnectBranch(parentNode, fileNode);
        if (entry->d_type == 4)
        {
            char filePath[100] = {0};
            strcpy(filePath, path);
            strcat(filePath, "/");
            strcat(filePath, entry->d_name);
            ExploreDirectory(filePath, fileNode);
        }
    }
    closedir(dir);
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        printf("invalid input numbers!\n");
        return 0;
    }

    LTree FileTree;
    InitLTree(&FileTree);
    struct UniversalType Typevalue;
    Typevalue.value = argv[1];
    Typevalue.type = charPoint;
    FileTree.root->data = Typevalue;
    ExploreDirectory(argv[1], FileTree.root);
    TravelLTree(&FileTree,);
    return 0;
}