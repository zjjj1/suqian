#include "LinkTree.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define true 1
#define false 0

LTNode *CreateTreeNode(ElementType element) // 为传入的东西定义一个新的树的节点
{
    LTNode *newNode = (LTNode *)malloc(sizeof(LTNode)); // 为这个节点在堆上申请空间
    if (newNode == NULL)                                // 如果申请失败，打印错误信息，并直接返回
    {
        printf("create malloc error!\n");
        return NULL;
    }
    newNode->data = element;    // 将传入的参数作为新节点的值
    newNode->firstChild = NULL; // 将新节点的子、兄、父节点都指向空
    newNode->nextSibling = NULL;
    newNode->parent = NULL;
    return newNode; // 返回新节点
}

void ConnectBranch(LTNode *parent, LTNode *child) // 头插法插入子节点
{
    if (parent == NULL || child == NULL) // 判断父子节点是否为空
    {
        return;
    }
    child->nextSibling = parent->firstChild; // 将子节点的兄弟指针指向父节点的孩子
    parent->firstChild = child;              // 将父节点的子指针指向要插入的节点
    child->parent = parent;                  // 将要插入的节点的父指针指向父节点
}

void DisConnectBranch(LTNode *parent, LTNode *child) // 删除父节点下的一个子节点
{
    if (parent == NULL || child == NULL || parent->firstChild == NULL) // 判断父、子、要删除的节点是否为空
    {
        return;
    }
    LTNode *TravePoint = parent->firstChild; // 定义一个遍历指针指向父节点的第一个孩子
    if (TravePoint == child)                 // 如果第一个孩子就是要删除的
    {
        parent->firstChild = child->nextSibling; // 将父节点的子指针指向第二个孩子
        child->parent = NULL;                    // 将子节点的父指针指空
        child->nextSibling = NULL;               // 将子节点的兄弟指针指空
        return;
    }
    while (TravePoint->nextSibling != NULL) // 直到遍历指针指向的第二个节点为空
    {
        if (TravePoint->nextSibling == child) // 遍历指针指向的第二个节点为要删除的值
        {
            TravePoint->nextSibling = child->nextSibling; // 将当前指向节点的兄弟指针指向下下一个节点
            child->parent = NULL;                         // 将子节点的父指针指空
            child->nextSibling = NULL;                    // 将子节点的兄弟指针指空
            return;
        }
        TravePoint = TravePoint->nextSibling; // 遍历指针指向下一个节点
    }
}

int GetNodeHeight(LTNode *treeNode) // 获得当前树的高度
{
    if (treeNode == NULL) // 如果当前树为空，返回0
        return 0;
    int height = 0;                            // 定义初始高度为0
    LTNode *TravePoint = treeNode->firstChild; // 定义一个遍历指针指向树节点的第一个孩子
    while (TravePoint != NULL)                 // 直到遍历指针为空
    {
        int childHeight = GetNodeHeight(TravePoint);          // 定义一个新的高度获取递归得到的高度
        height = height > childHeight ? height : childHeight; // 当前高度和递归得到的高度进行比较，输出大的值
        TravePoint = TravePoint->nextSibling;                 // 遍历指针指向当前节点的下一个兄弟
    }
    return height + 1; // 返回高度加一
}

void FreeNode(LTNode *treeNode) // 释放树
{
    if (treeNode == NULL) // 如果树为空，结束
    {
        return;
    }
    FreeNode(treeNode->firstChild);  // 递归当前点节点的子节点
    FreeNode(treeNode->nextSibling); // 递归当前点节点的兄弟节点
    // 方法二
    // LTNode *TravePoint = treeNode->firstChild;
    // while(TravePoint != NULL)
    // {
    //     LTNode*nextNode=TravePoint->nextSibling;
    //     FreeNode(TravePoint);
    //     TravePoint = nextNode;
    // }

    free(treeNode); // 释放当点节点
}

int InitLTree(LTree *tree) // 初始化一个树
{
    struct UniversalType typedata = {NULL, -1}; // 创建一个变量并初始化
    tree->root = CreateTreeNode(typedata);      // 创建根节点
    if (tree->root == NULL)                     // 判断节点是否为空
    {
        return false; // 返回错误
    }
    else
    {
        return true; // 返回正确
    }
}

int GetTreeHeight(LTree *tree) // 获得树的高度
{
    return GetNodeHeight(tree->root); // 返回调用函数获得的高度
}

LTNode *FindNode(LTNode *node, ElementType element, int (*func)(ElementType, ElementType)) // 查找节点
{
    if (node == NULL) // 如果节点为空，直接返回
        return NULL;
    if (func != NULL && func(node->data, element) == true) // 判断该函数指针是否为空，且调用该函数判断当前节点的值是否为要找的节点
        return node;                                       // 返回当前节点
    LTNode *targetNode = NULL;                             // 定义一个新的节点指向空
    if ((targetNode = FindNode(node->firstChild, element, func)) != NULL)
        return targetNode; // 查找该节点的子节点
    if ((targetNode = FindNode(node->nextSibling, element, func)) != NULL)
        return targetNode; // 查找该节点的兄弟节点
    // 方法二
    //  LTNode *childNode = node->firstChild;
    //  while (childNode != NULL)
    //  {
    //      targetNode = FindNode(childNode, element,func);
    //      if (targetNode != NULL)
    //      {
    //          return targetNode;
    //      }
    //      childNode = childNode->nextSibling;
    //  }
    //  return targetNode;
}

LTNode *FindTreeNode(LTree *tree, ElementType element, int (*func)(ElementType, ElementType)) // 寻找节点
{

    return FindNode(tree->root, element, func);
}

void FreeLTree(LTree *tree) // 释放树
{
    FreeNode(tree->root);
    tree->root = NULL;
}

void TravelTreeNode(LTNode *node, int deepth, void (*func)(ElementType)) // 遍历树的节点
{
    if (node == NULL) // 如果该节点为空，直接返回
    {
        return;
    }
    for (int i = 0; i < deepth - 1; i++) // 输出空格
        printf(" ");
    if (node->parent != NULL) // 是否要为根节点输出符号
        printf("|——");
    if (func != NULL) // 如果函数不为空
    {

        func(node->data); // 调用打印函数
    }
    TravelTreeNode(node->firstChild, deepth + 1, func); // 递归当前节点的子节点
    TravelTreeNode(node->nextSibling, deepth, func);    // 递归当前节点的兄弟节点
    // 方法二
    //  LTNode *TravePoint = node->firstChild;
    //  while(TravePoint != NULL)
    //  {
    //      TravelTreeNode(TravePoint,deepth+1,func);
    //      TravePoint= TravePoint->nextSibling;
    //  }
}

void TravelLTree(LTree *tree, void (*func)(ElementType)) // 遍历树
{
    TravelTreeNode(tree->root, 0, func);
}


