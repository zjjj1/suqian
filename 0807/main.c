#include "LinkTree.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define true 1
#define false 0

struct student//定义学生的结构体
{
    int id;//学号
    char *name;//姓名
    int score;//分数
};

struct student *CreateStudent(int id, char *name, int score)//创建学生的信息
{
    struct student *stu = (struct student *)malloc(sizeof(struct student));//在堆上申请新建的学生信息的空间
    if (stu == NULL)//如果申请失败，返回空指针
    {
        return NULL;
    }
    stu->id = id;//学生的信息为传入的参数
    stu->name = (char *)name;
    stu->score = score;
}

void FreeStudent(struct student *stu)//释放堆上申请的学生信息的空间
{
    if (stu == NULL)//如果本身为空，直接返回
        return;
    free(stu);
}

void Print(ElementType data)//判断输出类型是结构体还是字符串
{

    if (data.type == structPoint)//如果data里的类型是学生类型的结构体
    {
        struct student *stu = (struct student *)data.value;//将data里的类型强制转换为学生类型的结构体，并定义新的结构体指针指向他
        if (stu != NULL)
        {
            printf("id:%d ,name:%s, score:%d", stu->id, stu->name, stu->score);//输出结构体里的信息
        }
    }
    else if (data.type == charPoint)//如果data里的类型是char型的
    {
        char *temp = (char *)data.value;//将data里的类型强制转换为char*类型，并定义新的char型指针指向他
        printf("%s", temp);//输出字符串信息
    }
    printf("\n");
}

int IsEqual(ElementType data1, ElementType data2)//匹配两个data里的数据是否相同
{
    if (data1.type == structPoint && data2.type == structPoint)//两个是否都是结构体类型
    {
        struct student *stu1 = (struct student *)data1.value;//将data里的类型强制转换为学生类型的结构体，并定义新的结构体指针指向他们
        struct student *stu2 = (struct student *)data2.value;
        if (stu1->id == stu2->id)//如果里面的两个学号相同
        {
            return true;//返回true
        }
        return false;//不相同返回false
    }
    else if (data1.type == charPoint && data2.type == charPoint)//两个是否都是char类型
    {
        char *temp = (char *)data1.value;//将data里的类型强制转换为char类型，并定义新的char型指针指向他们
        char *temp2 = (char *)data2.value;
        if (strcmp(temp, temp2) == 0)//匹配两个字符串是否相同
        {
            return true;//返回true
        }
        return false;//不相同返回false
    }
}

int main()
{
    LTree tree;//定义一个新的树
    InitLTree(&tree);//对树进行初始化

    struct UniversalType Typevalue;//定义一个通用结构体
    Typevalue.value = "243class";//初始化，存放字符型
    Typevalue.type = charPoint;//提示为字符型
    LTNode *node1 = CreateTreeNode(Typevalue);//创建树节点，存放这个值

    Typevalue.value = CreateStudent(1, "abc", 100);//初始化，存放学生类型结构体
    Typevalue.type = structPoint;//提示为结构体类型
    LTNode *node2 = CreateTreeNode(Typevalue);//创建树节点，存放这个值

    Typevalue.value = CreateStudent(2, "def", 90);
    Typevalue.type = structPoint;
    LTNode *node3 = CreateTreeNode(Typevalue);

    ConnectBranch(node1, node2);//将node2，node3插入到node1
    ConnectBranch(node1, node3);
    ConnectBranch(tree.root, node1);
    TravelLTree(&tree, Print);
    // LTNode *node6 = CreateTreeNode("243class");
    // LTNode *node7 = CreateTreeNode("5");
    // LTNode *node8 = CreateTreeNode("6");
    // LTNode *node9 = CreateTreeNode("7");
    // LTNode *node10 = CreateTreeNode("8");
    // ConnectBranch(node6, node7);
    // ConnectBranch(node6, node8);
    // ConnectBranch(node6, node9);
    // ConnectBranch(node6, node10);

    

    // ConnectBranch(tree.root, node6);

    // LTNode *classNode = FindTreeNode(&tree, "247class");
    // if (classNode != NULL)
    // {
    //     LTNode *newNode = CreateTreeNode("12");
    //     ConnectBranch(classNode, newNode);
    // }

    // int hight = GetTreeHeight(&tree);
    // printf("\nhight: %d \n", hight);
    

    // Typevalue.type = charPoint;
    // Typevalue.value = "243class";
    // LTNode *findnode = FindTreeNode(&tree, Typevalue, IsEqual);
    // if (findnode != NULL)
    // {
    //     char *className = "247class";
    //     findnode->data.value = className;
    // }
    // TravelLTree(&tree, Print);
    
    // Typevalue.type = structPoint;
    // Typevalue.value=CreateStudent(1, "abc", 100);
    // findnode = FindTreeNode(&tree, Typevalue, IsEqual);
    // if (findnode != NULL)
    // {
    //     struct student *stu= (struct student*)findnode->data.value;
    //     stu->score = 60;
    // }
    // free(Typevalue.value);
    // TravelLTree(&tree, Print);


    FreeLTree(&tree);//释放
    return 0;
}