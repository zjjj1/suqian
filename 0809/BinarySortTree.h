#ifndef __BSTREE_H__
#define __BSTREE_H__

#define ElementType void *

// 树节点的一个结构体
struct BinarySortTreeNode
{
    ElementType data;                  // 数据
    struct BinarySortTreeNode *left;   // 左孩子
    struct BinarySortTreeNode *right;  // 右孩子
    struct BinarySortTreeNode *parent; // 父节点
};

typedef struct BinarySortTreeNode BSTNode;

// 树的结构体
struct BinarySortTree
{
    BSTNode *root; // 树的根节点
};

typedef struct BinarySortTree BSTree;

int InitBSTree(BSTree *tree);
void InsertElement(BSTree *tree, ElementType element, int (*func)(ElementType, ElementType));
void InsertBalanceTree(BSTree *tree, ElementType element, int (*func)(ElementType, ElementType));
void FreeTree(BSTree *tree);
void PrevTravel(BSTree *tree, void (*func)(ElementType));
void MidTravel(BSTree *tree, void (*func)(ElementType));
void PostTravel(BSTree *tree, void (*func)(ElementType));
void DeleteElement(BSTree *tree, ElementType element, int (*func)(ElementType, ElementType));
BSTNode *FindElement(BSTree *tree, ElementType element, int (*func)(ElementType, ElementType));
int GetLeafNum(BSTree *tree);

#endif