#include <stdio.h>
#include <stdlib.h>

struct TreeNode
{
    int data;
    struct TreeNode *left;
    struct TreeNode *right;
};

typedef struct TreeNode TNode;

TNode *CreateTreeNode(int element) // 对一个要插入的值进行封装，并初始化，
{
    TNode *newNode = (TNode *)malloc(sizeof(TNode)); // 在堆上申请空间
    if (newNode == NULL)
    {
        printf("CreateTreeNode malloc error!\n");
        return NULL;
    }
    newNode->data = element;
    newNode->left = NULL;
    newNode->right = NULL;
    return newNode;
}

// 右旋
TNode *RotateRight(TNode *node)
{
    TNode *t = node->left; // 定义一个t指向当前节点的左孩子
    node->left = t->right; // 将t的右孩子接到当前节点的左孩子
    t->right = node;       // 将当前节点作为t的右孩子
    return t;              // 将t作为树新的根节点
}

// 左旋
TNode *RotateLeft(TNode *node)
{
    TNode *t = node->right;
    node->right = t->left;
    t->left = node;
    return t;
}

// 左右旋
TNode *RotateLeftRight(TNode *node)
{
    node->left = RotateLeft(node->left);
    return RotateRight(node);
}

// 右左旋
TNode *RotateRightLeft(TNode *node)
{
    node->right = RotateRight(node->right);
    return RotateLeft(node);
}

// 获取树的高度
int GetNodeHeight(TNode *node)
{
    if (node == NULL)
        return 0;
    int leftHeight = GetNodeHeight(node->left);
    int rightHeight = GetNodeHeight(node->right);
    return ((leftHeight > rightHeight ? leftHeight : rightHeight) + 1);
}

TNode *InsertData(TNode *node, int data)
{
    if (node == NULL) // 如果node为空，直接插值
    {
        node = CreateTreeNode(data);
    }
    else if (data < node->data) // 如果当前值比node的值小
    {
        node->left = InsertData(node->left, data);                      // 在node的左边插入
        if (GetNodeHeight(node->left) - GetNodeHeight(node->right) > 1) // 如果左右高度差大于1，进行平衡操作
        {
            if (data < node->left->data) // 如果是左左树
            {
                node = RotateRight(node); // 右旋
            }
            else // 或者是左右树
            {
                node = RotateLeftRight(node); // 左右旋
            }
        }
    }
    else // 或者当前值比node的值大
    {
        node->right = InsertData(node->right, data);                    // 在node的右边插入
        if (GetNodeHeight(node->right) - GetNodeHeight(node->left) > 1) // 如果左右高度差大于1，进行平衡操作
        {
            if (data > node->right->data) // 如果是右右树
            {
                node = RotateLeft(node); // 左旋
            }
            else // 或者是右左树
            {
                node = RotateRightLeft(node); // 右左旋
            }
        }
    }
    return node;
}

void PrevPrint(TNode *node)
{
    if (node != NULL)
    {
        printf("%d ", node->data);
        PrevPrint(node->left);
        PrevPrint(node->right);
    }
}

void PrevTravel(TNode *tree)
{
    printf("prev: ");
    PrevPrint(tree);
    printf("\n");
}

void MidPrint(TNode *node)
{
    if (node != NULL)
    {
        MidPrint(node->left);
        printf("%d ", node->data);
        MidPrint(node->right);
    }
}

void MidTravel(TNode *tree)
{
    printf("Mid: \n");
    MidPrint(tree);
    printf("\n");
}

void PostPrint(TNode *node)
{
    if (node != NULL)
    {
        PostPrint(node->left);
        PostPrint(node->right);
        printf("%d ", node->data);
    }
}

void PostTravel(TNode *tree)
{
    printf("Post: ");
    PostPrint(tree);
    printf("\n");
}

int main()
{
    int a[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    TNode *root = NULL;
    for (int i = 0; i < sizeof(a) / sizeof(a[0]); i++)
    {
        root = InsertData(root, a[i]);
    }

    PrevTravel(root);
    MidTravel(root);
    PostTravel(root);

    return 0;
}