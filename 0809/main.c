#include "BinarySortTree.h"
#include <stdio.h>
#include <stdlib.h>
#define true 1
#define false 0

struct Student
{
    int id;
    char *name;
};

typedef struct Student student;

//定义学生类型，并进行赋值（将参数赋值给学生体类型）
student *CreateStudent(int id, const char *name)
{
    student *stu = (student *)malloc(sizeof(student));
    if (stu == NULL)
    {
        printf("CreateStudent malloc error!\n");
        return NULL;
    }
    stu->id = id;
    stu->name = (char *)name;
}

//输出函数
void Print(ElementType value)
{
    student *stu = (student *)value;
    printf("stu id: %d      stu name: %s\n", stu->id, stu->name);
}

//比较函数
int SortByID(ElementType value1, ElementType value2)
{
    student *stu1 = (student *)value1;
    student *stu2 = (student *)value2;

    if (stu1->id < stu2->id)
    {
        return true;
    }
    else
    {
        return false;
    }
}

//比较ID
int CompareID(ElementType value1, ElementType value2)
{
    student *stu1 = (student *)value1;
    student *stu2 = (student *)value2;
    return stu1->id - stu2->id;
}

int main()
{
    // BSTree tree;
    // InitBSTree(&tree);
    // int a[] = {5, 1, 16, 19, 6, 9, 8, 10};
    // for (int i = 0; i < sizeof(a) / sizeof(a[0]); i++)
    // {
    //     InsertElement(&tree, a[i]);
    // }
    // BSTNode * node = FindElement(&tree,9);
    // if(node != NULL)
    // node->data = 99;
    // PrevTravel(&tree);
    // MidTravel(&tree);
    // PostTravel(&tree);
    // FreeTree(&tree);

    BSTree tree;
    InitBSTree(&tree);
    InsertBalanceTree(&tree, CreateStudent(1, "wang"), SortByID);
    InsertBalanceTree(&tree, CreateStudent(2, "wang"), SortByID);
    InsertBalanceTree(&tree, CreateStudent(3, "wang"), SortByID);
    InsertBalanceTree(&tree, CreateStudent(4, "wang"), SortByID);
    InsertBalanceTree(&tree, CreateStudent(5, "wang"), SortByID);
    InsertBalanceTree(&tree, CreateStudent(6, "wang"), SortByID);
    InsertBalanceTree(&tree, CreateStudent(7, "wang"), SortByID);
    InsertBalanceTree(&tree, CreateStudent(8, "wang"), SortByID);
    InsertBalanceTree(&tree, CreateStudent(9, "wang"), SortByID);
    InsertBalanceTree(&tree, CreateStudent(10, "wang"), SortByID);
    MidTravel(&tree, Print);
    PrevTravel(&tree, Print);

    student stu = {2, ""};
    DeleteElement(&tree, &stu, CompareID);
    PrevTravel(&tree, Print);
    MidTravel(&tree, Print);

    printf("tree number: %d\n", GetLeafNum(&tree));

    return 0;
}