#include <stdio.h>

struct stu
{
    int id;
    int score;
};

int main()
{
    FILE *file = fopen("file.txt", "r");
    if (file == NULL)
    {
        printf("file open fail!\n");
    }
    else
    {
        printf("file open success!\n");
    }

    // char *str = "yutdrtustxdjtydi";
    // while(*str !='\0')
    // {
    //     fputc(*str,file);
    //     str++;
    // }
    
    // char str[100] = "6846468486'\n'844846466";
    // fputs(str,file);
    // printf("read data : %s\n",str);

    // struct stu str;
    // // fwrite(&str,sizeof(struct stu),1,file);
    // fread(&str,sizeof(struct stu),1,file);
    // printf("data : %d  %d \n ",str.id,str.score);

    // int num =0 ;
    // char temp[20]={0};
    // fscanf(file,"%d %s",&num,temp);
    // printf("%d %s\n",num,temp);

    // struct stu str={1,100};
    // fprintf(file,"the student id is %d,score is %d",str.id,str.score);

    fseek(file,0,SEEK_END);
    // char temp[10]={0};
    // fgets(temp,10,file);
    printf("%ld\n",ftell(file));

    fclose(file);
    
    return 0;
}