#include "Stdfile.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define true 1
#define false 0

int IsFileExist(const char *FilePath)
{
    FILE *file = fopen(FilePath, "r");
    if (file == NULL)
    {
        return false;
    }
    else
    {
        fclose(file);
        return true;
    }
}

char *LoadFormFile(const char *FilePath)
{
    FILE *file = fopen(FilePath, "r");
    if (file == NULL)
    {
        printf("file %s open fail!\n", FilePath);
        return NULL;
    }
    fseek(file, 0, SEEK_END);
    size_t size = ftell(file);

    char *content = (char *)malloc(size + 1);
    fseek(file, 0, SEEK_SET);
    fread(content, size, 1, file);
    fclose(file);
    return content;
}

void RemoveCharIndex(char *a, int index)
{
    if (index < 0 || index > strlen(a))
    {
        printf("InsertIndex invalid place!\n");
        return;
    }
    for (int i = index; i < strlen(a); i++)
    {
        a[i] = a[i + 1];
    }
}

void RemoveCharElement(char *a, char element)
{
    int len = strlen(a);
    for (int i = 0; i < len; i++)
    {
        if (a[i] == element)
        {
            RemoveCharIndex(a, i);
            i--;
        }
    }
}

DLlist *GetLineFormFile(const char *FilePath)
{
    FILE *file = fopen(FilePath, "r");
    if (file == NULL)
    {
        printf("file %s open fail!\n", FilePath);
        return NULL;
    }

    DLlist *list = (DLlist *)malloc(sizeof(DLlist));
    InitDLlist(list);

    char ContentTemp[100] = {0};
    while (fgets(ContentTemp, 100, file) != NULL)
    {
        char *Line = (char *)malloc(strlen(ContentTemp) + 1);

        strcpy(Line, ContentTemp);
        RemoveCharElement(Line,'\n');
        DLInsertTail(list, Line);
    }
    fclose(file);
    return list;
}

void WriteToFile(const char *filePath, void *ptr, size_t size)
{
    FILE *file = fopen(filePath, "w");
    if (file == NULL)
    {
        printf("WriteToFile open file error !\n");
        return;
    }
    if (fwrite(ptr, size, 1, file) <= 0)
    {
        printf("WriteToFile error!\n");
    }
    fclose(file);
}

void WriteLineToFile(const char *filePath, DLlist *list)
{
    FILE *file = fopen(filePath, "w");
    if (file == NULL)
    {
        printf("WriteToFile open file error !\n");
        return;
    }
    node *TravePoint = list->head;
    while (TravePoint != NULL)
    {
        fputs((const char*)TravePoint->data, file);
        TravePoint = TravePoint->next;
    }
    fclose(file);
}

void CopyFile(const char *SourcePath, const char *targetPath)
{
    if (IsFileExist(SourcePath) == false) // 如果本身文件打不开，直接返回
    {
        printf("the sourcefile is not exist or has read permission! \n");
        return;
    }
    char *fileContent = LoadFormFile(SourcePath);
    if (IsFileExist(targetPath) == true) // 如果文件可以打开说明有重名文件
    {
        char Target[100] = {0};
        strcpy(Target, targetPath);
        char *fileName = strtok(Target, ".");
        char *backName = strtok(NULL, ".");

        char NewPath[100] = {0};
        strcpy(NewPath, fileName);
        strcat(NewPath, "_new.");
        strcat(NewPath, backName);
        if (IsFileExist(NewPath) == true) // 如果修改后还有重名文件
        {
            CopyFile(SourcePath, NewPath); // 进行递归，再次循环
            return;
        }
        WriteToFile(NewPath, fileContent, strlen(fileContent));
        free(fileContent);
        return; // 结束函数
    }
    WriteToFile(targetPath, fileContent, strlen(fileContent));
    free(fileContent);
}

void AppendToFile(const char *FilePath, void *ptr, size_t size)
{
    FILE *file = fopen(FilePath,"a");
    if (file == NULL)
    {
        printf("AppendToFile open file error !\n");
        return;
    }
    fwrite (ptr,size,1,file);
    fclose(file);
}
