#include "SysTheamHard.h"
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <QThread>
#include <QDebug>
#include <QFile>

// 创建用户消息结构体
UM *creatUserMessage(int flag, const char *FormName, const char *ToName, const char *Message)
{
    UM *u = (UM *)malloc(sizeof(UM));
    if (u == NULL)
    {
        printf("creatClient malloc error!\n");
        return NULL;
    }

    u->flag = flag;
    strcpy(u->FromName, FormName);
    strcpy(u->ToName, ToName);
    strcpy(u->Message, Message);
    return u;
}

// 创建总用户表
void InitTable(SQL *sql_data)
{
    char *Us[] = {"UserName", "text", "PassWd", "text", "online", "integer", "ISVIP", "integer"};
    CreatTable(sql_data, "AllUser", Us, Size_len(Us) / 2);
    char *Us_message[] = {"FromName", "text", "ToName", "text", "Message", "text"};
    CreatTable(sql_data, "User_Message", Us_message, Size_len(Us_message) / 2);
    char *Us_groups[] = {"GroupID", "integer", "GroupName", "text", "UserName", "text", "Permissions", "integer"};
    CreatTable(sql_data, "GroupS", Us_groups, Size_len(Us_groups) / 2);
    char *Us_groups_Message[] = {"GroupID", "integer", "UserName", "text", "Message", "text"};
    CreatTable(sql_data, "GroupS_Message", Us_groups_Message, Size_len(Us_groups_Message) / 2);
}

// 初始化
void InitAll(SQL *sql_data, DLlist *s_UserList, DLlist *s_MessageList)
{
    InitTable(sql_data);
    InitDLlist(s_UserList);
    InitDLlist(s_MessageList);
}

// 服务端接收用户发消息请求
void Server_SendMessage(const char *S_ALL, int sock, SQL *sql, DLlist *UserList, DLlist *MessageList)
{
    char sc[10] = {0};
    char temp[2048] = {0};
    strcat(temp, S_ALL);
    char *flag = strtok(temp, " ");
    char *s_UserName = strtok(NULL, " ");
    char *s_toName = strtok(NULL, " ");
    char *content = strtok(NULL, "\0");

    char sql_new[1024] = {0};
    sprintf(sql_new, "select Friends_Name from Friends_%s where Friends_Name='%s';", s_UserName, s_toName);
    if (IsHaveData(sql, sql_new) == false)
    {
        sprintf(sc, "%c %c %c", 'M', 'C', 'N');
        TcpServerSend(sock, sc, strlen(sc) + 1);
        return;
    }
    memset(sql_new, 0, 1024);
    sprintf(sql_new, "select Friends_Name from Friends_%s where Friends_Name='%s';", s_toName, s_UserName);
    if (IsHaveData(sql, sql_new) == false)
    {
        sprintf(sc, "%c %c %c", 'M', 'C', 'n');
        TcpServerSend(sock, sc, strlen(sc) + 1);
    }
    else
    {
        DLInsertTail(MessageList, creatUserMessage(0, s_UserName, s_toName, content));
        SendOnline(UserList, s_toName, s_UserName, "C");
        char sql_n[2048] = {0};
        sprintf(sql_n, "insert into User_Message values('%s','%s','%s');", s_UserName, s_toName, content);
        Sql_Exec(sql, sql_n);
        sprintf(sc, "%c %c %c", 'M', 'C', 'Y');
        TcpServerSend(sock, sc, strlen(sc) + 1);
    }
    return;
}

// VIP给在线用户发提示消息
int VIPSendOnline(DLlist *list, const char *s_toName, const char *content, int flag)
{
    char temp[2048] = {0};
    strcat(temp, content);
    node *T1 = list->head;
    if (flag == 1)
    {
        while (T1 != NULL)
        {
            Cli *c = (Cli *)T1->data;
            if (strcmp(c->name, s_toName) == 0)
            {
                TcpServerSend(c->sock, temp, strlen(temp) + 1);
                return true;
            }
            T1 = T1->next;
        }
    }
    else
    {
        while (T1 != NULL)
        {
            Cli *c = (Cli *)T1->data;
            if (strcmp(c->name, s_toName) != 0)
            {
                TcpServerSend(c->sock, temp, strlen(temp) + 1);
                sleep(0);
            }
            T1 = T1->next;
        }
    }
    return false;
}

// 服务端接收VIP用户发消息请求
void Server_Any_SendMessage(const char *S_ALL, int sock, SQL *sql, DLlist *UserList)
{
    char s_UserName[30] = {0};
    char s_ToName[30] = {0};
    char s[1024] = {0};
    char sc[10] = {0};
    char flag;
    sscanf(S_ALL, "%c %s %s %s", &flag, s_UserName, s_ToName, s);
    if (strcmp(s_UserName, s_ToName) == 0)
    {
        char temp[2048] = {0};
        strcat(temp, S_ALL);
        TcpServerSend(sock, temp, strlen(temp) + 1);
    }
    else
    {
        if (VIPSendOnline(UserList, s_ToName, S_ALL, 1) == false)
        {
            sprintf(sc, "%c %c %c", 'M', 'c', 'N');
            TcpServerSend(sock, sc, strlen(sc) + 1);
        }
        sprintf(sc, "%c %c %c", 'M', 'c', 'Y');
        TcpServerSend(sock, sc, strlen(sc) + 1);
    }
    return;
}

// 服务端接收VIP用户群发消息请求
void Server_Any_ALLSendMessage(const char *S_ALL, int sock, SQL *sql, DLlist *UserList)
{
    char s_UserName[30] = {0};
    char s[1024] = {0};
    char sc[10] = {0};
    char flag;
    sscanf(S_ALL, "%c %s %s", &flag, s_UserName, s);

    VIPSendOnline(UserList, s_UserName, S_ALL, 0);

    sprintf(sc, "%c %c %c", 'M', 'c', 'Y');
    TcpServerSend(sock, sc, strlen(sc) + 1);
    return;
}

// 给在线用户发提示消息
int SendOnline(DLlist *list, const char *s_toName, const char *FromName, char *flag)
{
    node *T1 = list->head;
    while (T1 != NULL)
    {
        Cli *c = (Cli *)T1->data;
        if (strcmp(c->name, s_toName) == 0)
        {
            char sc[50] = {0};
            sprintf(sc, "%c %c %s", 'O', *flag, FromName);
            TcpServerSend(c->sock, sc, strlen(sc) + 1);
            return true;
        }
        T1 = T1->next;
    }
    return false;
}

// 服务端接收用户查看好友请求
void Server_SeeFriends(const char *S_ALL, int sock, SQL *sql)
{
    char flag;
    char s_UserName[30] = {0};
    sscanf(S_ALL, "%c %s", &flag, s_UserName);

    char sql_id[100] = {0};
    sprintf(sql_id, "select * from Friends_%s;", s_UserName);
    char **result_Name;
    int row, column;
    SelectInfo(sql, sql_id, &result_Name, &row, &column);

    char AllFriendsName[1024] = {0};
    char str[10] = {0};
    sprintf(str, "%c %d", 'F', row);
    strcat(AllFriendsName, str);
    for (int i = 1; i <= row; i++)
    {
        strcat(AllFriendsName, " ");
        strcat(AllFriendsName, result_Name[i]);
    }
    TcpServerSend(sock, AllFriendsName, strlen(AllFriendsName) + 1);
    FreeInfoResult(result_Name);
}

// 服务端判断用户是否在线
void Server_IsOnile(const char *S_ALL, int sock, SQL *sql)
{
    char s_UserName[30] = {0};
    char s_Friends[30] = {0};
    char sc[10] = {0};
    char flag;
    sscanf(S_ALL, "%c %s %s", &flag, s_UserName, s_Friends);

    char sql_new[1024] = {0};
    sprintf(sql_new, "select Friends_Name from Friends_%s where Friends_Name='%s';", s_UserName, s_Friends);
    if (IsHaveData(sql, sql_new) == false)
    {
        sprintf(sc, "%c %c %c", 'M', 'I', 'N');
        TcpServerSend(sock, sc, strlen(sc) + 1);
        return;
    }
    memset(sql_new, 0, 1024);
    sprintf(sql_new, "select online from AllUser where UserName='%s';", s_Friends);
    char **result_FriendIsOnline;
    int row, column;
    SelectInfo(sql, sql_new, &result_FriendIsOnline, &row, &column);
    if (*result_FriendIsOnline[1] == '1')
    {
        sprintf(sc, "%c %c %c", 'M', 'I', 'Y');
        TcpServerSend(sock, sc, strlen(sc) + 1);
    }
    else
    {
        sprintf(sc, "%c %c %c", 'M', 'I', 'y');
        TcpServerSend(sock, sc, strlen(sc) + 1);
    }
    FreeInfoResult(result_FriendIsOnline);
    return;
}

// 查找链表中数据
int IFLiistHaveDate(DLlist *list, const char *s_toName, const char *FromName, int flag)
{
    node *T1 = list->head;
    while (T1 != NULL)
    {
        UM *u = (UM *)T1->data;
        if (u->flag == flag && (strcmp(u->ToName, s_toName) == 0) && (strcmp(u->FromName, FromName) == 0))
        {
            return true;
        }
        T1 = T1->next;
    }
    return false;
}

// 服务端处理用户加好友申请
void Server_AddFriend(const char *S_ALL, int sock, SQL *sql, DLlist *UserList, DLlist *MessageList)
{
    char s_UserName[30] = {0};
    char add_Friends[30] = {0};
    char sc[10] = {0};
    char flag;
    sscanf(S_ALL, "%c %s %s", &flag, s_UserName, add_Friends);
    if (IFLiistHaveDate(MessageList, add_Friends, s_UserName, 1) == true)
    {
        sprintf(sc, "%c %c %c", 'M', '+', 'Y');
        TcpServerSend(sock, sc, strlen(sc) + 1);
    }
    else
    {
        char sql_new[1024] = {0};
        sprintf(sql_new, "select Friends_Name from Friends_%s where Friends_Name='%s';", s_UserName, add_Friends);
        if (IsHaveData(sql, sql_new) == true)
        {
            sprintf(sc, "%c %c %c", 'M', '+', 'n');
            TcpServerSend(sock, sc, strlen(sc) + 1);
            return;
        }
        memset(sql_new, 0, 1024);
        sprintf(sql_new, "select UserName from AllUser where UserName='%s';", add_Friends);
        if (IsHaveData(sql, sql_new) == false)
        {
            sprintf(sc, "%c %c %c", 'M', '+', 'N');
            TcpServerSend(sock, sc, strlen(sc) + 1);
        }
        else
        {
            DLInsertTail(MessageList, creatUserMessage(1, s_UserName, add_Friends, " "));
            SendOnline(UserList, add_Friends, s_UserName, "+");
            sprintf(sc, "%c %c %c", 'M', '+', 'y');
            TcpServerSend(sock, sc, strlen(sc) + 1);
        }
    }
    return;
}

// 服务端处理用户删除好友请求
void Server_DelFriend(const char *S_ALL, int sock, SQL *sql, DLlist *UserList)
{
    char s_UserName[30] = {0};
    char Del_Friends[30] = {0};
    char sc[10] = {0};
    char flag;
    sscanf(S_ALL, "%c %s %s", &flag, s_UserName, Del_Friends);

    char sql_new[1024] = {0};
    sprintf(sql_new, "select Friends_Name from Friends_%s where Friends_Name='%s';", s_UserName, Del_Friends);
    if (IsHaveData(sql, sql_new) == false)
    {
        sprintf(sc, "%c %c %c", 'M', '-', 'N');
        TcpServerSend(sock, sc, strlen(sc) + 1);
    }
    else
    {
        memset(sql_new, 0, 1024);
        sprintf(sql_new, "delete from Friends_%s where Friends_Name='%s';", s_UserName, Del_Friends);
        Sql_Exec(sql, sql_new);
        memset(sql_new, 0, 1024);
        sprintf(sql_new, "delete from Friends_%s where Friends_Name='%s';", Del_Friends, s_UserName);
        Sql_Exec(sql, sql_new);
        SendOnline(UserList, Del_Friends, s_UserName, "-");
        sprintf(sc, "%c %c %c", 'M', '-', 'Y');
        TcpServerSend(sock, sc, strlen(sc) + 1);
    }
    return;
}

// 将链表中离线用户删除
void RemoveUserByList(const char *exit_UserName, DLlist *UserList)
{
    node *T1 = UserList->head;
    while (T1 != NULL)
    {
        Cli *c = (Cli *)T1->data;
        if (strcmp(c->name, exit_UserName) == 0)
        {
            free(c);
            DLRemoveByElement(UserList, T1->data);
            return;
        }
        T1 = T1->next;
    }
}

// 服务器处理用户退出
void Server_ExitUser(const char *S_ALL, int sock, SQL *sql, DLlist *UserList)
{
    char exit_UserName[30] = {0};
    char flag;
    sscanf(S_ALL, "%c %s", &flag, exit_UserName);

    char sql_exit[1024] = {0};
    sprintf(sql_exit, "update AllUser set online = 0 where UserName='%s';", exit_UserName);
    Sql_Exec(sql, sql_exit);
    char sc[5] = {0};
    sprintf(sc, "%c", '*');
    TcpServerSend(sock, sc, strlen(sc) + 1);

    RemoveUserByList(exit_UserName, UserList);
    close(sock);
}

// 查看链表中数据数量
int GetMessageNum(int flag, const char *s_UserName, DLlist *MessageList)
{
    int num = 0;
    node *T1 = MessageList->head;
    while (T1 != NULL)
    {
        UM *u = (UM *)T1->data;
        if (u->flag == flag && (strcmp(u->ToName, s_UserName) == 0))
            num++;
        T1 = T1->next;
    }
    return num;
}

// 服务端处理用户查看新消息
void Server_SeeNewMessage(const char *S_ALL, int sock, SQL *sql, DLlist *MessageList)
{
    char s_UserName[30] = {0};
    char sc[10] = {0};
    char flag;
    sscanf(S_ALL, "%c %s", &flag, s_UserName);
    int num = 0;
    node *T1 = MessageList->head;
    while (T1 != NULL)
    {
        node *T2 = T1->next;
        UM *u = (UM *)T1->data;
        if (u->flag == 0 && (strcmp(u->ToName, s_UserName) == 0))
        {
            num++;
            char temp[2048] = {0};
            sprintf(temp, "%c %s %s %s", 'X', u->FromName, u->ToName, u->Message);
            TcpServerSend(sock, temp, strlen(temp) + 1);
            QThread::msleep(10);
            free(u);
            DLRemoveByElement(MessageList, T1->data);
        }
        T1 = T2;
    }
    sprintf(sc, "%c %d", 'M', num);
    TcpServerSend(sock, sc, strlen(sc) + 1);
    return;
}

// 服务端接收好友查看新加好友
void Server_SeeNewFriend(const char *S_ALL, int sock, SQL *sql, DLlist *MessageList)
{
    char s_UserName[30] = {0};
    char sc[10] = {0};
    char flag;
    sscanf(S_ALL, "%c %s", &flag, s_UserName);
    int num = GetMessageNum(1, s_UserName, MessageList);
    if (num == 0)
    {
        sprintf(sc, "%c %c %d", 'M', 'N', num);
        TcpServerSend(sock, sc, strlen(sc) + 1);
        return;
    }
    else
    {
        node *T1 = MessageList->head;
        while (T1 != NULL)
        {
            UM *u = (UM *)T1->data;
            if (u->flag == 1 && (strcmp(u->ToName, s_UserName) == 0))
            {
                char temp[100] = {0};
                sprintf(temp, "%c %d %s", 'N', num, u->FromName);
                TcpServerSend(sock, temp, strlen(temp) + 1);
                free(u);
                DLRemoveByElement(MessageList, T1->data);
                return;
            }
        }
    }
}

// 服务端处理同意加好友申请
void Server_AgreeAddFriend(const char *S_ALL, int sock, SQL *sql, DLlist *UserList)
{
    char s_UserName[30] = {0};
    char From_Friends[30] = {0};
    char sc[10] = {0};
    char flag;
    sscanf(S_ALL, "%c %s %s", &flag, s_UserName, From_Friends);
    char sql_new[1024] = {0};
    sprintf(sql_new, "select Friends_Name from Friends_%s where Friends_Name='%s';", s_UserName, From_Friends);
    if (IsHaveData(sql, sql_new) == true)
    {
        sprintf(sc, "%c %c %c", 'M', '+', 'n');
        TcpServerSend(sock, sc, strlen(sc) + 1);
        return;
    }
    memset(sql_new, 0, 1024);
    sprintf(sql_new, "select UserName from AllUser where UserName='%s';", From_Friends);
    if (IsHaveData(sql, sql_new) == false)
    {
        sprintf(sc, "%c %c %c", 'M', '+', 'N');
        TcpServerSend(sock, sc, strlen(sc) + 1);
        return;
    }
    else
    {
        memset(sql_new, 0, 1024);
        sprintf(sql_new, "insert into Friends_%s values('%s');", s_UserName, From_Friends);
        Sql_Exec(sql, sql_new);
        memset(sql_new, 0, 1024);
        sprintf(sql_new, "insert into Friends_%s values('%s');", From_Friends, s_UserName);
        Sql_Exec(sql, sql_new);

        SendOnline(UserList, From_Friends, s_UserName, "N");
        SendOnline(UserList, s_UserName, From_Friends, "N");
    }
}

// 服务端接收查看与好友的聊天记录
void Server_LookAllMessage(const char *S_ALL, int sock, SQL *sql)
{
    char s_UserName[30] = {0};
    char Look_Friends[30] = {0};
    char sc[10] = {0};
    char flag;
    sscanf(S_ALL, "%c %s %s", &flag, s_UserName, Look_Friends);

    char sql_id[1024] = {0};
    sprintf(sql_id, "select * from User_Message where ( FromName = '%s' and ToName = '%s' ) or (FromName = '%s' and ToName = '%s');", s_UserName, Look_Friends, Look_Friends, s_UserName);
    char **result_AllMessage;
    int row, column;
    SelectInfo(sql, sql_id, &result_AllMessage, &row, &column);
    sprintf(sc, "%c %c %d", 'M', 'L', row);
    TcpServerSend(sock, sc, strlen(sc) + 1);
    sleep(0);
    for (int i = 1; i <= row; i++)
    {
        char temp[2048] = {0};
        sprintf(temp, "%c %s %s %s", 'L', result_AllMessage[i * 3], result_AllMessage[i * 3 + 1], result_AllMessage[i * 3 + 2]);
        TcpServerSend(sock, temp, strlen(temp) + 1);
        sleep(0);
    }
    FreeInfoResult(result_AllMessage);
}

void Server_LookAllMessage1(const char *S_ALL, int sock, SQL *sql)
{
    char s_UserName[30] = {0};
    char Look_Friends[30] = {0};
    char flag;
    sscanf(S_ALL, "%c %s %s", &flag, s_UserName, Look_Friends);

    char sql_id[1024] = {0};
    sprintf(sql_id, "select * from User_Message where ( FromName = '%s' and ToName = '%s' ) or (FromName = '%s' and ToName = '%s');", s_UserName, Look_Friends, Look_Friends, s_UserName);
    char **result_AllMessage;
    int row = 0, column = 0;
    SelectInfo(sql, sql_id, &result_AllMessage, &row, &column);
    char temp[2048] = {0};
    int num = (row > 20) ? (row - 20) : 1;
    while (num <= row)
    {
        memset(temp, 0, 2048);
        sprintf(temp, "%c %s %s %s", 'Y', result_AllMessage[num * 3], result_AllMessage[num * 3 + 1], result_AllMessage[num * 3 + 2]);
        TcpServerSend(sock, temp, strlen(temp) + 1);
        QThread::msleep(40);
        num++;
    }
    char sc[] = {'N', '\0'};
    TcpServerSend(sock, sc, strlen(sc) + 1);
    FreeInfoResult(result_AllMessage);
}

// 服务端查找群聊列表
void Server_SeeGroups(const char *S_ALL, int sock, SQL *sql)
{
    char flag;
    char s_UserName[30] = {0};
    sscanf(S_ALL, "%c %s", &flag, s_UserName);

    char sql_id[1024] = {0};
    sprintf(sql_id, "select GroupID,GroupName from Groups where UserName = '%s' and Permissions = 3;", s_UserName);
    char **result_Group3;
    int row3, column3;
    SelectInfo(sql, sql_id, &result_Group3, &row3, &column3);

    memset(sql_id, 0, 1024);
    char str[10] = {0};
    sprintf(str, "%c %d %d", 'G', 3, row3);
    strcat(sql_id, str);
    for (int i = 1; i <= row3; i++)
    {
        strcat(sql_id, " ");
        strcat(sql_id, result_Group3[i * 2]);
        strcat(sql_id, " ");
        strcat(sql_id, result_Group3[i * 2 + 1]);
    }
    TcpServerSend(sock, sql_id, strlen(sql_id) + 1);

    sleep(0);
    memset(sql_id, 0, 1024);
    sprintf(sql_id, "select GroupID,GroupName from Groups where UserName = '%s' and Permissions = 2;", s_UserName);
    SelectInfo(sql, sql_id, &result_Group3, &row3, &column3);

    memset(sql_id, 0, 1024);
    memset(str, 0, 10);
    sprintf(str, "%c %d %d", 'G', 2, row3);
    strcat(sql_id, str);
    for (int i = 1; i <= row3; i++)
    {
        strcat(sql_id, " ");
        strcat(sql_id, result_Group3[i * 2]);
        strcat(sql_id, " ");
        strcat(sql_id, result_Group3[i * 2 + 1]);
    }
    TcpServerSend(sock, sql_id, strlen(sql_id) + 1);

    sleep(0);
    memset(sql_id, 0, 1024);
    sprintf(sql_id, "select GroupID,GroupName from Groups where UserName = '%s' and ( Permissions = 1 or Permissions = 0 );", s_UserName);
    SelectInfo(sql, sql_id, &result_Group3, &row3, &column3);

    memset(sql_id, 0, 1024);
    memset(str, 0, 10);
    sprintf(str, "%c %d %d", 'G', 1, row3);
    strcat(sql_id, str);
    for (int i = 1; i <= row3; i++)
    {
        strcat(sql_id, " ");
        strcat(sql_id, result_Group3[i * 2]);
        strcat(sql_id, " ");
        strcat(sql_id, result_Group3[i * 2 + 1]);
    }
    TcpServerSend(sock, sql_id, strlen(sql_id) + 1);

    FreeInfoResult(result_Group3);
}

void Server_SeeGroups1(const char *S_ALL, int sock, SQL *sql)
{
    char flag;
    char s_UserName[30] = {0};
    sscanf(S_ALL, "%c %s", &flag, s_UserName);

    char sql_id[1024] = {0};
    sprintf(sql_id, "select GroupID,GroupName from Groups where UserName = '%s' and Permissions = 3;", s_UserName);
    char **result_Group3;
    int row3, column3;
    SelectInfo(sql, sql_id, &result_Group3, &row3, &column3);

    memset(sql_id, 0, 1024);
    char str[10] = {0};
    sprintf(str, "%c %d %d", 'G', 3, row3);
    strcat(sql_id, str);
    for (int i = 1; i <= row3; i++)
    {
        strcat(sql_id, " ");
        strcat(sql_id, result_Group3[i * 2]);
        strcat(sql_id, " ");
        strcat(sql_id, result_Group3[i * 2 + 1]);
    }
    TcpServerSend(sock, sql_id, strlen(sql_id) + 1);

    QThread::msleep(20);
    memset(sql_id, 0, 1024);
    sprintf(sql_id, "select GroupID,GroupName from Groups where UserName = '%s' and Permissions = 2;", s_UserName);
    SelectInfo(sql, sql_id, &result_Group3, &row3, &column3);

    memset(sql_id, 0, 1024);
    memset(str, 0, 10);
    sprintf(str, "%c %d %d", 'G', 2, row3);
    strcat(sql_id, str);
    for (int i = 1; i <= row3; i++)
    {
        strcat(sql_id, " ");
        strcat(sql_id, result_Group3[i * 2]);
        strcat(sql_id, " ");
        strcat(sql_id, result_Group3[i * 2 + 1]);
    }
    TcpServerSend(sock, sql_id, strlen(sql_id) + 1);

    sleep(0);QThread::msleep(20);
    memset(sql_id, 0, 1024);
    sprintf(sql_id, "select GroupID,GroupName from Groups where UserName = '%s' and ( Permissions = 1 or Permissions = 0 );", s_UserName);
    SelectInfo(sql, sql_id, &result_Group3, &row3, &column3);

    memset(sql_id, 0, 1024);
    memset(str, 0, 10);
    sprintf(str, "%c %d %d", 'G', 1, row3);
    strcat(sql_id, str);
    for (int i = 1; i <= row3; i++)
    {
        strcat(sql_id, " ");
        strcat(sql_id, result_Group3[i * 2]);
        strcat(sql_id, " ");
        strcat(sql_id, result_Group3[i * 2 + 1]);
    }
    TcpServerSend(sock, sql_id, strlen(sql_id) + 1);
    FreeInfoResult(result_Group3);

    QThread::msleep(20);
    memset(sql_id, 0, 1024);
    sprintf(sql_id, "%c", 'g');
    TcpServerSend(sock, sql_id, strlen(sql_id) + 1);
}

// 是否在群内
int isINGroup(char *name, int GroupID, SQL *sql)
{
    char sql_id[1024] = {0};
    sprintf(sql_id, "select UserName from Groups where GroupID = %d and UserName = '%s';", GroupID,name);
    char **result_Group;
    int row, column;
    SelectInfo(sql, sql_id, &result_Group, &row, &column);
    if (row == 0)
        return false;
    return true;
}

// 服务端查看群聊成员
void Server_SeeGroupFriends(const char *S_ALL, int sock, SQL *sql)
{
    char flag;
    int S_Group = 0;
    char sc[10] = {0};
    char name[30] = {0};
    sscanf(S_ALL, "%c %d %s", &flag, &S_Group,&name);

    char GroupName[30] = {0};
    if (GetGroupName(GroupName, S_Group, sql) == false)
    {
        sprintf(sc, "%c %c %c", 'M', '/', ' ');
        TcpServerSend(sock, sc, strlen(sc) + 1);
    }
    else if(GetGroupName(name,S_Group,sql) == false)
    {
        sprintf(sc, "%c %c %c", 'm', '/', ' ');
        TcpServerSend(sock, sc, strlen(sc) + 1);
    }
    else
    {
        char sql_id[1024] = {0};
        sprintf(sql_id, "select UserName from Groups where GroupID = %d and Permissions = 3;", S_Group);
        char **result_Group3;
        int row3, column3;
        SelectInfo(sql, sql_id, &result_Group3, &row3, &column3);

        memset(sql_id, 0, 1024);
        char str[10] = {0};
        sprintf(str, "%c %d %d", 'S', 3, row3);
        strcat(sql_id, str);
        for (int i = 1; i <= row3; i++)
        {
            strcat(sql_id, " ");
            strcat(sql_id, result_Group3[i]);
        }
        TcpServerSend(sock, sql_id, strlen(sql_id) + 1);

        QThread::msleep(40);
        memset(sql_id, 0, 1024);
        sprintf(sql_id, "select UserName from Groups where GroupID = %d and Permissions = 2;", S_Group);
        SelectInfo(sql, sql_id, &result_Group3, &row3, &column3);

        memset(sql_id, 0, 1024);
        memset(str, 0, 10);
        sprintf(str, "%c %d %d", 'S', 2, row3);
        strcat(sql_id, str);
        for (int i = 1; i <= row3; i++)
        {
            strcat(sql_id, " ");
            strcat(sql_id, result_Group3[i]);
        }
        TcpServerSend(sock, sql_id, strlen(sql_id) + 1);

        QThread::msleep(40);
        memset(sql_id, 0, 1024);
        sprintf(sql_id, "select UserName from Groups where GroupID = %d and ( Permissions = 1 or Permissions = 0 );", S_Group);
        SelectInfo(sql, sql_id, &result_Group3, &row3, &column3);

        memset(sql_id, 0, 1024);
        memset(str, 0, 10);
        sprintf(str, "%c %d %d", 'S', 1, row3);
        strcat(sql_id, str);
        for (int i = 1; i <= row3; i++)
        {
            strcat(sql_id, " ");
            strcat(sql_id, result_Group3[i]);
        }
        TcpServerSend(sock, sql_id, strlen(sql_id) + 1);

        QThread::msleep(40);
        memset(sql_id, 0, 1024);
        sprintf(sql_id, "%c", 's');
        TcpServerSend(sock, sql_id, strlen(sql_id) + 1);
        FreeInfoResult(result_Group3);
    }
}

// 获取用户群中权限
int GetUserPermissions(const char *Username, int GroupID, SQL *sql)
{
    int per = 0;
    char sql_id[1024] = {0};
    sprintf(sql_id, "select Permissions from Groups where UserName = '%s' and GroupID = %d;", Username, GroupID);
    char **result_Group;
    int row, column;
    SelectInfo(sql, sql_id, &result_Group, &row, &column);
    if (row == 0)
        per = -1;
    else
        per = atoi(result_Group[1]);
    FreeInfoResult(result_Group);

    return per;
}

// 服务端查看用户群权限
void Server_UserPermissions(const char *S_ALL, int sock, SQL *sql)
{
    char flag;
    char sc[10] = {0};
    char s_UserName[30] = {0};
    int GroupID = 0;
    sscanf(S_ALL, "%c %s %d", &flag, s_UserName, &GroupID);

    int per = GetUserPermissions(s_UserName, GroupID, sql);
    sprintf(sc, "%c %d", 'Q', per);
    TcpServerSend(sock, sc, strlen(sc) + 1);

    return;
}

// 查找群名
int GetGroupName(char *name, int GroupID, SQL *sql)
{
    char GroupName[30] = {0};
    char sql_id[1024] = {0};
    sprintf(sql_id, "select GroupName from Groups where GroupID = %d and Permissions = 3;", GroupID);
    char **result_Group;
    int row, column;
    SelectInfo(sql, sql_id, &result_Group, &row, &column);
    if (row == 0)
        return false;
    memset(name, 0, 30);
    strcat(name, result_Group[1]);
    FreeInfoResult(result_Group);
    return true;
}

// 给在线用户发提示消息2
int SendOnline2(DLlist *list, const char *s_toName, const char *FromName, char *flag, char *GroupName)
{
    node *T1 = list->head;
    while (T1 != NULL)
    {
        Cli *c = (Cli *)T1->data;
        if (strcmp(c->name, s_toName) == 0)
        {
            char sc[100] = {0};
            sprintf(sc, "%c %c %s %s", 'I', *flag, FromName, GroupName);
            TcpServerSend(c->sock, sc, strlen(sc) + 1);
            return true;
        }
        T1 = T1->next;
    }
    return false;
}

// 服务端拉好友入群
void Server_InviteFriend(const char *S_ALL, int sock, SQL *sql, DLlist *UserList, DLlist *MessageList)
{
    char flag;
    char sc[10] = {0};
    char s_UserName[30] = {0};
    char S_Friends[30] = {0};
    int GroupID = 0;
    sscanf(S_ALL, "%c %s %s %d", &flag, s_UserName, S_Friends, &GroupID);

    char GroupName[30] = {0};
    if (GetGroupName(GroupName, GroupID, sql) == false)
    {
        sprintf(sc, "%c %c %c", 'M', '/', ' ');
        TcpServerSend(sock, sc, strlen(sc) + 1);
    }
    else
    {
        char sql_new[1024] = {0};
        sprintf(sql_new, "select Friends_Name from Friends_%s where Friends_Name = '%s';", s_UserName, S_Friends);
        if (IsHaveData(sql, sql_new) == false)
        {
            sprintf(sc, "%c %c %c", 'M', 'Y', 'N');
            TcpServerSend(sock, sc, strlen(sc) + 1);
            return;
        }
        memset(sql_new, 0, 1024);
        sprintf(sql_new, "select UserName from Groups where UserName = '%s' and GroupID = %d;", S_Friends, GroupID);
        if (IsHaveData(sql, sql_new) == true)
        {
            sprintf(sc, "%c %c %c", 'M', 'Y', 'n');
            TcpServerSend(sock, sc, strlen(sc) + 1);
        }
        else
        {
            memset(sql_new, 0, 1024);
            sprintf(sql_new, "insert into Groups values(%d,'%s','%s',1);", GroupID, GroupName, S_Friends);
            Sql_Exec(sql, sql_new);
            if (SendOnline2(UserList, S_Friends, s_UserName, "Y", GroupName) == false)
            {
                memset(sql_new, 0, 1024);
                sprintf(sql_new, "——>(您的好友: %s ,将您拉入[ %s ]群聊内！)<——\n", s_UserName, GroupName);
                DLInsertTail(MessageList, creatUserMessage(3, "0", S_Friends, sql_new));
            }
            sprintf(sc, "%c %c %c", 'M', 'Y', 'Y');
            TcpServerSend(sock, sc, strlen(sc) + 1);
        }
    }
    return;
}

// 服务端接收成员禁言
void Server_MemberSilence(const char *S_ALL, int sock, SQL *sql, DLlist *UserList, DLlist *MessageList)
{
    char flag;
    char sc[10] = {0};
    char s_Name[30] = {0};
    int GroupID = 0;
    int Userper = 0;
    sscanf(S_ALL, "%c %s %d %d", &flag, s_Name, &GroupID, &Userper);

    char GroupName[30] = {0};
    if (GetGroupName(GroupName, GroupID, sql) == false)
    {
        sprintf(sc, "%c %c %c", 'M', '/', ' ');
        TcpServerSend(sock, sc, strlen(sc) + 1);
    }
    else
    {
        char sql_new[1024] = {0};
        sprintf(sql_new, "select UserName from Groups where UserName = '%s' and GroupID = %d;", s_Name, GroupID);
        if (IsHaveData(sql, sql_new) == false)
        {
            sprintf(sc, "%c %c %c", 'M', 'D', 'n');
            TcpServerSend(sock, sc, strlen(sc) + 1);
            return;
        }
        int per = GetUserPermissions(s_Name, GroupID, sql);
        if (Userper > per)
        {
            memset(sql_new, 0, 1024);
            if (per != 0)
            {
                int vip = UserISVIP(s_Name, sql);
                if (vip == 0)
                {
                    sprintf(sql_new, "update Groups set Permissions = 0 where UserName = '%s' and GroupID = %d;", s_Name, GroupID);
                    Sql_Exec(sql, sql_new);
                    if (SendOnline2(UserList, s_Name, "Y", "D", GroupName) == false)
                    {
                        memset(sql_new, 0, 1024);
                        sprintf(sql_new, "——>(群聊[ %s ]更高级管理员已将您禁言！)<——\n", GroupName);
                        DLInsertTail(MessageList, creatUserMessage(3, "0", s_Name, sql_new));
                    }
                    sprintf(sc, "%c %c %c", 'M', 'D', 'Y');
                    TcpServerSend(sock, sc, strlen(sc) + 1);
                }
                else
                {
                    sprintf(sc, "%c %c %c", 'M', 'D', 'V');
                    TcpServerSend(sock, sc, strlen(sc) + 1);
                }
            }
            else
            {
                sprintf(sql_new, "update Groups set Permissions = 1 where UserName = '%s' and GroupID = %d;", s_Name, GroupID);
                Sql_Exec(sql, sql_new);

                if (SendOnline2(UserList, s_Name, "N", "D", GroupName) == false)
                {
                    memset(sql_new, 0, 1024);
                    sprintf(sql_new, "——>(群聊[ %s ]更高级管理员已将您禁言解除！)<——\n", GroupName);
                    DLInsertTail(MessageList, creatUserMessage(3, "0", s_Name, sql_new));
                }
                sprintf(sc, "%c %c %c", 'M', 'D', 'y');
                TcpServerSend(sock, sc, strlen(sc) + 1);
            }
        }
        else
        {
            sprintf(sc, "%c %c %c", 'M', 'D', 'N');
            TcpServerSend(sock, sc, strlen(sc) + 1);
        }
    }
}

// 服务端踢出群成员
void Server_KickPeople(const char *S_ALL, int sock, SQL *sql, DLlist *UserList, DLlist *MessageList)
{
    char flag;
    char sc[10] = {0};
    char s_Name[30] = {0};
    int GroupID = 0;
    int Userper = 0;
    sscanf(S_ALL, "%c %s %d %d", &flag, s_Name, &GroupID, &Userper);

    char GroupName[30] = {0};
    if (GetGroupName(GroupName, GroupID, sql) == false)
    {
        sprintf(sc, "%c %c %c", 'M', '/', ' ');
        TcpServerSend(sock, sc, strlen(sc) + 1);
    }
    else
    {
        char sql_new[1024] = {0};
        sprintf(sql_new, "select UserName from Groups where UserName = '%s' and GroupID = %d;", s_Name, GroupID);
        if (IsHaveData(sql, sql_new) == false)
        {
            sprintf(sc, "%c %c %c", 'M', 'T', 'n');
            TcpServerSend(sock, sc, strlen(sc) + 1);
            return;
        }
        int per = GetUserPermissions(s_Name, GroupID, sql);
        if (Userper > per)
        {
            memset(sql_new, 0, 1024);
            sprintf(sql_new, "delete from Groups where UserName='%s' and GroupID = %d;", s_Name, GroupID);
            Sql_Exec(sql, sql_new);

            if (SendOnline2(UserList, s_Name, "0", "T", GroupName) == false)
            {
                memset(sql_new, 0, 1024);
                sprintf(sql_new, "——>(更高级管理员已将您踢出[ %s ]群聊！)<——\n", GroupName);
                DLInsertTail(MessageList, creatUserMessage(3, "0", s_Name, sql_new));
            }
            sprintf(sc, "%c %c %c", 'M', 'T', 'Y');
            TcpServerSend(sock, sc, strlen(sc) + 1);
        }
        else
        {
            sprintf(sc, "%c %c %c", 'M', 'T', 'N');
            TcpServerSend(sock, sc, strlen(sc) + 1);
        }
    }
}

// 服务端设置群管理
void Server_AssignGroupManagement(const char *S_ALL, int sock, SQL *sql, DLlist *UserList, DLlist *MessageList)
{
    char flag;
    char sc[10] = {0};
    char s_Name[30] = {0};
    int GroupID = 0;
    sscanf(S_ALL, "%c %s %d", &flag, s_Name, &GroupID);

    char GroupName[30] = {0};
    if (GetGroupName(GroupName, GroupID, sql) == false)
    {
        sprintf(sc, "%c %c %c", 'M', '/', ' ');
        TcpServerSend(sock, sc, strlen(sc) + 1);
    }
    else
    {
        char sql_new[1024] = {0};
        sprintf(sql_new, "select UserName from Groups where UserName = '%s' and GroupID = %d;", s_Name, GroupID);
        if (IsHaveData(sql, sql_new) == false)
        {
            sprintf(sc, "%c %c %c", 'M', 'E', 'N');
            TcpServerSend(sock, sc, strlen(sc) + 1);
            return;
        }
        memset(sql_new, 0, 1024);
        int per = GetUserPermissions(s_Name, GroupID, sql);
        if (per != 2)
        {
            sprintf(sql_new, "update Groups set Permissions = 2 where UserName = '%s' and GroupID = %d;", s_Name, GroupID);
            Sql_Exec(sql, sql_new);

            if (SendOnline2(UserList, s_Name, "Y", "E", GroupName) == false)
            {
                memset(sql_new, 0, 1024);
                sprintf(sql_new, "——>(您已被群聊[ %s ]的群主设置为管理员！)<——\n", GroupName);
                DLInsertTail(MessageList, creatUserMessage(3, "0", s_Name, sql_new));
            }
            sprintf(sc, "%c %c %c", 'M', 'E', 'Y');
            TcpServerSend(sock, sc, strlen(sc) + 1);
        }
        else
        {
            printf(sql_new, "update Groups set Permissions = 1 where UserName = '%s' and GroupID = %d;", s_Name, GroupID);
            Sql_Exec(sql, sql_new);

            if (SendOnline2(UserList, s_Name, "N", "E", GroupName) == false)
            {
                memset(sql_new, 0, 1024);
                sprintf(sql_new, "——>(您已被群聊[ %s ]的群主解除管理员！)<——\n", GroupName);
                DLInsertTail(MessageList, creatUserMessage(3, "0", s_Name, sql_new));
            }
            sprintf(sc, "%c %c %c", 'M', 'E', 'y');
            TcpServerSend(sock, sc, strlen(sc) + 1);
        }
    }
}

// 服务端转让群主
void Server_AssignGroupLeader(const char *S_ALL, int sock, SQL *sql, DLlist *UserList, DLlist *MessageList)
{
    char flag;
    char sc[10] = {0};
    char s_UserName[30] = {0};
    char s_Name[30] = {0};
    int GroupID = 0;
    int newPer = 0;
    sscanf(S_ALL, "%c %s %s %d %d", &flag, s_UserName, s_Name, &GroupID, &newPer);

    char sql_new[1024] = {0};
    sprintf(sql_new, "select UserName from Groups where UserName = '%s' and GroupID = %d;", s_Name, GroupID);
    if (IsHaveData(sql, sql_new) == false)
    {
        sprintf(sc, "%c %c %c", 'M', 'R', 'N');
        TcpServerSend(sock, sc, strlen(sc) + 1);
        return;
    }

    memset(sql_new, 0, 1024);
    sprintf(sql_new, "update Groups set Permissions = 3 where UserName = '%s' and GroupID = %d;", s_Name, GroupID);
    Sql_Exec(sql, sql_new);
    memset(sql_new, 0, 1024);
    sprintf(sql_new, "update Groups set Permissions = %d where UserName = '%s' and GroupID = %d;", newPer, s_UserName, GroupID);
    Sql_Exec(sql, sql_new);
    char GroupName[30] = {0};
    GetGroupName(GroupName, GroupID, sql);
    if (SendOnline2(UserList, s_Name, "0", "R", GroupName) == false)
    {
        memset(sql_new, 0, 1024);
        sprintf(sql_new, "——>(您已成为群聊[ %s ]新的群主！)<——\n", GroupName);
        DLInsertTail(MessageList, creatUserMessage(3, "0", s_Name, sql_new));
    }
    sprintf(sc, "%c %c %c", 'M', 'R', 'Y');
    TcpServerSend(sock, sc, strlen(sc) + 1);
}

// 服务端解散群聊
void Server_RemoveGroup(const char *S_ALL, int sock, SQL *sql, DLlist *UserList, DLlist *MessageList)
{
    char flag;
    int GroupID = 0;
    char myname[30] = {0};
    sscanf(S_ALL, "%c %d %s", &flag, &GroupID,myname);

    char sql_new[1024] = {0};
    sprintf(sql_new, "select UserName from Groups where GroupID = %d;", GroupID);
    char **result_Group;
    int row, column;
    SelectInfo(sql, sql_new, &result_Group, &row, &column);

    char GroupName[30] = {0};
    GetGroupName(GroupName, GroupID, sql);

    memset(sql_new, 0, 1024);
    sprintf(sql_new, "delete from Groups where GroupID = %d;", GroupID);
    Sql_Exec(sql, sql_new);

    memset(sql_new, 0, 1024);
    sprintf(sql_new, "delete from GroupS_Message where GroupID = %d;", GroupID);
    Sql_Exec(sql, sql_new);

    for (int i = 1; i <= row; i++)
    {
        if(strcmp(result_Group[i],myname) == 0)
        {
            SendOnline2(UserList, result_Group[i], "0", "J", GroupName);
        }
        else
        {
            memset(sql_new, 0, 1024);
            sprintf(sql_new, "——>(群聊[ %s ]已被解散！)<——\n", GroupName);
            DLInsertTail(MessageList, creatUserMessage(3, "0", result_Group[i], sql_new));
        }
        sleep(0);
    }
    FreeInfoResult(result_Group);
}

// 服务端搜索群聊加入
void Server_SearchGroup(const char *S_ALL, int sock, SQL *sql, DLlist *UserList, DLlist *MessageList)
{
    char flag;
    char sc[50] = {0};
    char s_UserName[30] = {0};
    int GroupID = 0;
    sscanf(S_ALL, "%c %d %s", &flag, &GroupID, s_UserName);

    char GroupName[30] = {0};
    if (GetGroupName(GroupName, GroupID, sql) == false)
    {
        sprintf(sc, "%c %c %c", 'M', 'M', 'N');
        TcpServerSend(sock, sc, strlen(sc) + 1);
        return;
    }
    char sql_new[1024] = {0};
    sprintf(sql_new, "select UserName from Groups where UserName = '%s' and GroupID = %d;", s_UserName, GroupID);
    if (IsHaveData(sql, sql_new) == true)
    {
        sprintf(sc, "%c %c %c", 'M', 'M', 'n');
        TcpServerSend(sock, sc, strlen(sc) + 1);
        return;
    }

    memset(sql_new, 0, 1024);
    sprintf(sql_new, "select UserName from Groups where GroupID = %d;", GroupID);
    char **result_Group;
    int row, column;
    SelectInfo(sql, sql_new, &result_Group, &row, &column);

    memset(sql_new, 0, 1024);
    sprintf(sql_new, "insert into Groups values(%d,'%s','%s',1);", GroupID, GroupName, s_UserName);
    Sql_Exec(sql, sql_new);

    for (int i = 1; i <= row; i++)
    {
        if (SendOnline2(UserList, result_Group[i], s_UserName, "M", GroupName) == false)
        {
            memset(sql_new, 0, 1024);
            sprintf(sql_new, "——>(用户[ %s ]加入群聊[ %s ]! )<——\n", s_UserName, GroupName);
            DLInsertTail(MessageList, creatUserMessage(3, "0", result_Group[i], sql_new));
        }
        sleep(0);
    }
    FreeInfoResult(result_Group);
    sprintf(sc, "%c %c %s", 'O', 'M', GroupName);
    TcpServerSend(sock, sc, strlen(sc) + 1);
}

// 获取新的群号
int GetNewGroupID(SQL *sql)
{
    int num = 10;
    char sql_new[1024] = {0};
    sprintf(sql_new, "select GroupID from Groups order by GroupID desc;");
    char **result_Group;
    int row, column;
    SelectInfo(sql, sql_new, &result_Group, &row, &column);
    if (row != 0)
    {
        num = atoi(result_Group[1]) + 1;
    }
    FreeInfoResult(result_Group);
    return num;
}

// 服务端创建群聊
void Server_CreateGroup(const char *S_ALL, int sock, SQL *sql)
{
    char flag;
    char sc[50] = {0};
    char s_UserName[30] = {0};
    char s_GroupName[30] = {0};
    sscanf(S_ALL, "%c %s %s", &flag, s_GroupName, s_UserName);

    char sql_new[1024] = {0};
    sprintf(sql_new, "select GroupName from Groups where GroupName = '%s';", s_GroupName);
    if (IsHaveData(sql, sql_new) == true)
    {
        sprintf(sc, "%c %c %c", 'M', 'K', 'N');
        TcpServerSend(sock, sc, strlen(sc) + 1);
        return;
    }
    int GroupID = GetNewGroupID(sql);
    memset(sql_new, 0, 1024);
    sprintf(sql_new, "insert into Groups values(%d,'%s','%s',3);", GroupID, s_GroupName, s_UserName);
    Sql_Exec(sql, sql_new);

    sprintf(sc, "%c %c %d %s", 'I', 'K', GroupID, s_GroupName);
    TcpServerSend(sock, sc, strlen(sc) + 1);
}

// 服务端群发消息
void Server_SendGroupMessage(const char *S_ALL, int sock, SQL *sql, DLlist *UserList, DLlist *MessageList)
{
    char sc[10] = {0};
    char temp[2048] = {0};
    strcat(temp, S_ALL);
    char *flag = strtok(temp, " ");
    int GroupID = atoi(strtok(NULL, " "));
    char *s_UserName = strtok(NULL, " ");
    char *content = strtok(NULL, "\0");

    char GroupName[30] = {0};
    if (GetGroupName(GroupName, GroupID, sql) == false)
    {
        sprintf(sc, "%c %c %c", 'M', '/', '.');
        TcpServerSend(sock, sc, strlen(sc) + 1);
    }
    else
    {
        int per = GetUserPermissions(s_UserName, GroupID, sql);
        if (per == 0)
        {
            sprintf(sc, "%c %c %c", 'M', 'U', 'N');
            TcpServerSend(sock, sc, strlen(sc) + 1);
        }
        else
        {
            char sql_new[2048] = {0};
            sprintf(sql_new, "insert into GroupS_Message values(%d,'%s','%s');", GroupID, s_UserName, content);
            Sql_Exec(sql, sql_new);

            memset(sql_new, 0, 2048);
            sprintf(sql_new, "select UserName from Groups where GroupID = %d;", GroupID);
            char **result_Group;
            int row, column;
            SelectInfo(sql, sql_new, &result_Group, &row, &column);

            for (int i = 1; i <= row; i++)
            {
                if (strcmp(result_Group[i], s_UserName) != 0)
                {
                        memset(sql_new, 0, 1024);
                        sprintf(sql_new, "——>(用户[ %s ]在群聊[ %s ]发送了一条消息! )<——\n", s_UserName, GroupName);
                        DLInsertTail(MessageList, creatUserMessage(3, "0", result_Group[i], sql_new));
                    sleep(0);
                }
            }
            FreeInfoResult(result_Group);
            sprintf(sc, "%c %c %c", 'M', 'U', 'Y');
            TcpServerSend(sock, sc, strlen(sc) + 1);
        }
    }
    return;
}

// 服务端退出群聊
void Server_QuitGroup(const char *S_ALL, int sock, SQL *sql, DLlist *UserList, DLlist *MessageList)
{
    char flag;
    char sc[50] = {0};
    char s_Name[30] = {0};
    int GroupID = 0;
    sscanf(S_ALL, "%c %s %d", &flag, s_Name, &GroupID);

    char GroupName[30] = {0};
    if (GetGroupName(GroupName, GroupID, sql) == false)
    {
        sprintf(sc, "%c %c %c", 'M', '/', ' ');
        TcpServerSend(sock, sc, strlen(sc) + 1);
    }
    else
    {
        char sql_new[1024] = {0};
        sprintf(sql_new, "delete from Groups where GroupID = %d and UserName = '%s';", GroupID, s_Name);
        Sql_Exec(sql, sql_new);

        memset(sql_new, 0, 1024);
        sprintf(sql_new, "select UserName from Groups where GroupID = %d and Permissions > 1;", GroupID);
        char **result_Group;
        int row, column;
        SelectInfo(sql, sql_new, &result_Group, &row, &column);

        for (int i = 1; i <= row; i++)
        {          
            memset(sql_new, 0, 1024);
            sprintf(sql_new, "——>(用户[ %s ]已经退出群聊[ %s ]!)<——\n", s_Name, GroupName);
            DLInsertTail(MessageList, creatUserMessage(3, "0", result_Group[i], sql_new));
            sleep(0);
        }
        FreeInfoResult(result_Group);
        sprintf(sc, "%c %c %s", 'O', 'V', GroupName);
        TcpServerSend(sock, sc, strlen(sc) + 1);
    }
}

// 服务端查看群聊天记录
void Server_See_GroupMessage(const char *S_ALL, int sock, SQL *sql)
{
    char flag;
    char sc[10] = {0};
    int GroupID = 0;
    char name[30] = {0};
    sscanf(S_ALL, "%c %d %s", &flag, &GroupID,&name);

    char GroupName[30] = {0};
    if (GetGroupName(GroupName, GroupID, sql) == false)
    {
        sprintf(sc, "%c %c %c", 'M', '/', ' ');
        TcpServerSend(sock, sc, strlen(sc) + 1);
    }
    else if(GetGroupName(name,GroupID,sql) == false)
    {
        sprintf(sc, "%c %c %c", 'm', '/', ' ');
        TcpServerSend(sock, sc, strlen(sc) + 1);
    }
    else
    {
        char sql_new[1024] = {0};
        sprintf(sql_new, "select UserName,Message from GroupS_Message where GroupID = %d;", GroupID);
        char **result_Group;
        int row, column;
        SelectInfo(sql, sql_new, &result_Group, &row, &column);

        char temp[2048] = {0};
        int num = (row > 20) ? (row - 20) : 1;
        for (int i = 1; i <= row; i++)
        {
            memset(temp,0,2048);
            sprintf(temp, "%c %s %s", 'H', result_Group[i * 2], result_Group[i * 2 + 1]);
            TcpServerSend(sock, temp, strlen(temp) + 1);
            QThread::msleep(40);
        }
        FreeInfoResult(result_Group);

        memset(temp, 0, 2048);
        sprintf(temp, "%c", 'h');
        TcpServerSend(sock, temp, strlen(temp) + 1);
    }
}

// 服务端处理用户查看群聊消息
void Server_LookGroupMessage(const char *S_ALL, int sock, SQL *sql, DLlist *MessageList)
{
    char s_UserName[30] = {0};
    char sc[10] = {0};
    char flag;
    sscanf(S_ALL, "%c %s", &flag, s_UserName);
    int num = 0;
    node *T1 = MessageList->head;
    while (T1 != NULL)
    {
        node *T2 = T1->next;
        UM *u = (UM *)T1->data;
        if (u->flag == 3 && (strcmp(u->ToName, s_UserName) == 0))
        {
            num++;
            char temp[2048] = {0};
            sprintf(temp, "%c %s", 'Z', u->Message);
            TcpServerSend(sock, temp, strlen(temp) + 1);
            QThread::msleep(40);
            free(u);
            DLRemoveByElement(MessageList, T1->data);
        }
        T1 = T2;
    }
    sprintf(sc, "%c %d", 'm', num);
    TcpServerSend(sock, sc, strlen(sc) + 1);
    return;
}

// 获取用户的VIP
int UserISVIP(char *name, SQL *sql)
{
    int vip = 0;
    char sql_id[1024] = {0};
    sprintf(sql_id, "select ISVIP from AllUser where UserName = '%s';", name);
    char **result_Group;
    int row, column;
    SelectInfo(sql, sql_id, &result_Group, &row, &column);
    if (*result_Group[1] == '1')
        vip = 1;
    else
        vip = 0;
    FreeInfoResult(result_Group);
    return vip;
}

// 服务端处理用户是否为VIP
void Server_VIPUser(const char *S_ALL, int sock, SQL *sql)
{
    char s_UserName[30] = {0};
    char sc[10] = {0};
    char flag;
    sscanf(S_ALL, "%c %s", &flag, s_UserName);

    int vip = UserISVIP(s_UserName, sql);
    sprintf(sc, "%c %d", 'Q', vip);
    TcpServerSend(sock, sc, strlen(sc) + 1);
}

// 服务端处理用户开通VIP
void Server_OPEN_VIPUser(const char *S_ALL, int sock, SQL *sql)
{
    char s_UserName[30] = {0};
    char sc[10] = {0};
    char flag;
    sscanf(S_ALL, "%c %s", &flag, s_UserName);

    char sql_id[1024] = {0};
    sprintf(sql_id, "update AllUser set ISVIP = 1 where UserName = '%s';", s_UserName);
    Sql_Exec(sql, sql_id);

    sprintf(sc, "%c %c %c", 'M', 'W', 'Y');
    TcpServerSend(sock, sc, strlen(sc) + 1);
}

// 服务端查看群文件
void Server_See_GroupFile(const char *S_ALL, int sock, SQL *sql)
{
    char flag;
    char sc[10] = {0};
    sscanf(S_ALL, "%c", &flag);

    DIR *dir = opendir("/home/lvguanzhong/Chat/Server/File");
    if (dir == NULL)
    {
        sprintf(sc, "%c %c %c", 'M', '?', 'n');
        TcpServerSend(sock, sc, strlen(sc) + 1);
        return;
    }
    struct dirent *entry;
    char temp[2000] = {0};
    int count = 0;
    while ((entry = readdir(dir)) != NULL)
    {
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
        {
            continue;
        }
        else
        {
            count++;
            strcat(temp," ");
            strcat(temp,entry->d_name);
        }
    }
    if (count == 0)
    {
        sprintf(sc, "%c %c %c", 'M', '?', 'N');
        TcpServerSend(sock, sc, strlen(sc) + 1);
    }
    else
    {
        char c_temp[2048] = {0};
        sprintf(c_temp, "%c %d", '?',count);
        strcat(c_temp,temp);
        TcpServerSend(sock, c_temp, strlen(c_temp) + 1);
    }
}

// 服务端上传群文件
void Server_UploadGroupFile(const char *S_ALL, int sock, SQL *sql)
{
    char sc[10] = {0};
    char temp[2048] = {0};
    strcat(temp, S_ALL);
    char *flag = strtok(temp, " ");
    char *Filename = strtok(NULL, " ");
    char *content = strtok(NULL, "\0");

    char temp_Filename[100] = {0};
    sprintf(temp_Filename, "/home/lvguanzhong/桌面/chat/AA-server/File/%s", Filename);
    FILE *file = fopen(temp_Filename, "r");
    if (file != NULL)
    {
        sprintf(sc, "%c %c %c", 'M', '.', 'N');
        TcpServerSend(sock, sc, strlen(sc) + 1);
        fclose(file);
        return;
    }

    file = fopen(temp_Filename, "w");
    fwrite(content, strlen(content), 1, file);
    fclose(file);
    sprintf(sc, "%c %c %c", 'M', '.', 'Y');
    TcpServerSend(sock, sc, strlen(sc) + 1);
}

// 服务端上传群文件1
void Server_UploadGroupFile1(const char *S_ALL, int sock, SQL *sql)
{
    char flag;
    char Filename[100] = {0};
    char sc[10] = {0};
    sscanf(S_ALL, "%c %s", &flag,Filename);

    char temp_Filename[256] = {0};
    sprintf(temp_Filename, "/home/lvguanzhong/Chat/Server/File/%s", Filename);
    FILE *file = fopen(temp_Filename, "r");
    if (file != NULL)
    {
        sprintf(sc, "%c %c %c", '1', '.', 'N');
        TcpServerSend(sock, sc, strlen(sc) + 1);

    }else
    {
        sprintf(sc, "%c %c %c", '1', '.', 'Y');
        TcpServerSend(sock, sc, strlen(sc) + 1);
    }
    fclose(file);
    return;
}


// 服务端下载群文件
void Server_DownGroupFile(const char *S_ALL, int sock, SQL *sql)
{
    char Name[100] = {0};
    char sc[10] = {0};
    char flag;
    sscanf(S_ALL, "%c %s", &flag, Name);
    char temp_filePath[150] = {0};
    sprintf(temp_filePath, "/home/lvguanzhong/Chat/Server/File/%s", Name);
    if (IsFileExist(temp_filePath) == false)
    {
        sprintf(sc, "%c %c %c", 'M', '!', 'N');
        TcpServerSend(sock, sc, strlen(sc) + 1);
        return;
    }
    FILE* fp = fopen(temp_filePath, "rb"); // 以二进制模式打开文件
    fseek(fp, 0, SEEK_END);
    long size = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    long Size = size;
    long Num = 0; // 已经下载大小

    // 存储每次读取到的数据的缓冲区
    char temp[20600] = {0};
    while (size > 0)
    {
        sprintf(temp, "%c %ld %ld ",'!', Size,Num);
        int num = strlen(temp);
        int bytesRead = fread(temp+num, 1, 20480, fp);
        if (bytesRead <= 0)
        {
            perror("Error reading file");
            fclose(fp);
            return;
        }
        TcpServerSend(sock, temp,20480 + num);
//        qDebug()<<Num;

        size -= bytesRead;
        Num += bytesRead;

        QThread::msleep(50);
        // 清空缓冲区
        memset(temp, 0, 20600);
    }
    if(Num >= Size)
    {
        sprintf(temp, "%c %ld %ld",'!', Size,Num);
        TcpServerSend(sock, temp,strlen(temp) + 1);
//        qDebug()<<Num;
    }
    fclose(fp); // 关闭文件
}

// 服务端上传备份
void Server_UploadArchiveFile(const char *S_ALL, int sock, SQL *sql)
{
    char sc[10] = {0};
    char temp[2048] = {0};
    strcat(temp, S_ALL);
    char *flag = strtok(temp, " ");
    char *UserName = strtok(NULL, " ");
    char *Filename = strtok(NULL, " ");
    char *content = strtok(NULL, "\0");

    char temp_Filename[100] = {0};
    sprintf(temp_Filename, "/home/lvguanzhong/桌面/chat/AA-server/data/Save/%s/%s", UserName, Filename);
    FILE *file = fopen(temp_Filename, "w");

    fwrite(content, strlen(content), 1, file);
    fclose(file);
    if (strcmp(Filename, "player.txt") == 0)
    {
        sprintf(sc, "%c %c %c", 'M', ':', 'a');
        TcpServerSend(sock, sc, strlen(sc) + 1);
    }
    else if (strcmp(Filename, "treasure.txt") == 0)
    {
        sprintf(sc, "%c %c %c", 'M', ':', 'b');
        TcpServerSend(sock, sc, strlen(sc) + 1);
    }
    else if (strcmp(Filename, "monsters.txt") == 0)
    {
        sprintf(sc, "%c %c %c", 'M', ':', 'c');
        TcpServerSend(sock, sc, strlen(sc) + 1);
    }
    else
    {
        sprintf(sc, "%c %c %c", 'M', ':', 'd');
        TcpServerSend(sock, sc, strlen(sc) + 1);
    }
}

// 服务端下载备份
void Server_DownloadArchive(const char *S_ALL, int sock, SQL *sql)
{
    char Name[30] = {0};
    char sc[10] = {0};
    char flag;
    sscanf(S_ALL, "%c %s", &flag, Name);

    char temp_filePath[100] = {0};
    sprintf(temp_filePath, "/home/lvguanzhong/桌面/chat/AA-server/data/Save/%s/player.txt", Name);
    if (IsFileExist(temp_filePath) == false)
    {
        sprintf(sc, "%c %c %c", 'M', ';', 'n');
        TcpServerSend(sock, sc, strlen(sc) + 1);
        return;
    }
    else
    {
        sprintf(sc, "%c %d", 'Q', 1);
        TcpServerSend(sock, sc, strlen(sc) + 1);
        sleep(0);
    }
    char temp[1024] = {0};
    FILE *f1 = fopen(temp_filePath, "r");
    fseek(f1, 0, SEEK_END);
    long s1 = ftell(f1);
    fseek(f1, 0, SEEK_SET);
    fread(temp, s1, 1, f1);
    fclose(f1);

    char str[2048] = {0};
    sprintf(str, "%c %s %s %s", ';', "player.txt", Name, temp);
    TcpServerSend(sock, str, strlen(str) + 1);
    sleep(1);

    memset(temp_filePath, 0, 100);
    memset(temp, 0, 1024);
    memset(str, 0, 2048);
    sprintf(temp_filePath, "/home/lvguanzhong/桌面/chat/AA-server/data/Save/%s/treasure.txt", Name);
    if (IsFileExist(temp_filePath) == false)
        return;
    FILE *f2 = fopen(temp_filePath, "r");
    fseek(f2, 0, SEEK_END);
    long s2 = ftell(f2);
    fseek(f2, 0, SEEK_SET);
    fread(temp, s2, 1, f2);
    fclose(f2);

    sprintf(str, "%c %s %s %s", ';', "treasure.txt", Name, temp);
    TcpServerSend(sock, str, strlen(str) + 1);
    sleep(1);

    memset(temp_filePath, 0, 100);
    memset(temp, 0, 1024);
    memset(str, 0, 2048);
    sprintf(temp_filePath, "/home/lvguanzhong/桌面/chat/AA-server/data/Save/%s/monsters.txt", Name);
    if (IsFileExist(temp_filePath) == false)
        return;
    FILE *f3 = fopen(temp_filePath, "r");
    fseek(f3, 0, SEEK_END);
    long s3 = ftell(f3);
    fseek(f3, 0, SEEK_SET);
    fread(temp, s3, 1, f3);
    fclose(f3);

    sprintf(str, "%c %s %s %s", ';', "monsters.txt", Name, temp);
    TcpServerSend(sock, str, strlen(str) + 1);
    sleep(1);

    memset(temp_filePath, 0, 100);
    memset(temp, 0, 1024);
    memset(str, 0, 2048);
    sprintf(temp_filePath, "/home/lvguanzhong/桌面/chat/AA-server/data/Save/%s/bloodpacks.txt", Name);
    if (IsFileExist(temp_filePath) == false)
        return;

    FILE *f4 = fopen(temp_filePath, "r");
    fseek(f4, 0, SEEK_END);
    long s4 = ftell(f4);
    fseek(f4, 0, SEEK_SET);
    fread(temp, s4, 1, f4);
    fclose(f4);

    sprintf(str, "%c %s %s %s", ';', "bloodpacks.txt", Name, temp);
    TcpServerSend(sock, str, strlen(str) + 1);
}

// 服务器处理用户异常退出
void Server_ExitUser1(int sock, SQL *sql, DLlist *UserList)
{
    char exit_UserName[30] = {0};

    node *T1 = UserList->head;
    while (T1 != NULL)
    {
        Cli *c = (Cli *)T1->data;
        if (c->sock == sock)
        {
            strcpy(exit_UserName, c->name);
            char sql_exit[1024] = {0};
            sprintf(sql_exit, "update AllUser set online = 0 where UserName='%s';", exit_UserName);
            Sql_Exec(sql, sql_exit);

            RemoveUserByList(exit_UserName, UserList);
            close(sock);
            return;
        }
        T1 = T1->next;
    }
    close(sock);
}

void Uploadfile_Server(char *S_ALL)
{
    char flag;
    char Filename[70]= {0};
    sscanf(S_ALL, "%c %s ", &flag, Filename);
    char* content = S_ALL + strlen(Filename) + 3;
    char filePath[256] = {0};
    sprintf(filePath, "/home/lvguanzhong/Chat/Server/File/%s", Filename);

    QFile file(filePath);
    if (file.open(QIODevice::Append))
    {
        file.write(content,20480);
        file.close();
    }
}

// 线程函数
void Server_FuncThread(int sock, SQL *sql_data, DLlist *s_MessageList, DLlist *s_UserList)
{
    while (1)
    {
        char S_ALL[20600] = {0};
        // TcpServerRecv(sock, S_ALL, sizeof(S_ALL));

        int abc = recv(sock, S_ALL, sizeof(S_ALL), 0);
        if (abc <= 0)
        {
            Server_ExitUser1(sock, sql_data, s_UserList);
            return;
        }

        switch (*S_ALL)
        {
        case '@':
            Uploadfile_Server(S_ALL);
            break;
        case 'A':
            Login_Server(S_ALL, sock, sql_data, s_UserList);
            break;
        case 'B':
            Register_Server(S_ALL, sock, sql_data);
            break;
        case 'C':
            Server_SendMessage(S_ALL, sock, sql_data, s_UserList, s_MessageList);
            break;
        case 'c':
            Server_Any_SendMessage(S_ALL, sock, sql_data, s_UserList);
            break;
        case 'D':
            Server_MemberSilence(S_ALL, sock, sql_data, s_UserList, s_MessageList);
            break;
        case 'E':
            Server_AssignGroupManagement(S_ALL, sock, sql_data, s_UserList, s_MessageList);
            break;
        case 'F':
            Server_SeeFriends(S_ALL, sock, sql_data);
            break;
        case 'G':
            Server_SeeGroups(S_ALL, sock, sql_data);
            break;
        case 'g':
            Server_SeeGroups1(S_ALL, sock, sql_data);
            break;
        case 'H':
            Server_See_GroupMessage(S_ALL, sock, sql_data);
            break;
        case 'I':
            Server_IsOnile(S_ALL, sock, sql_data);
            break;
        case 'J':
            Server_RemoveGroup(S_ALL, sock, sql_data, s_UserList, s_MessageList);
            break;
        case 'K':
            Server_CreateGroup(S_ALL, sock, sql_data);
            break;
        case 'L':
            Server_LookAllMessage(S_ALL, sock, sql_data);
            break;
        case 'l':
            Server_LookAllMessage1(S_ALL, sock, sql_data);
            break;
        case 'M':
            Server_SearchGroup(S_ALL, sock, sql_data, s_UserList, s_MessageList);
            break;
        case 'n':
            Server_AgreeAddFriend(S_ALL, sock, sql_data, s_UserList);
            break;
        case 'N':
            Server_SeeNewFriend(S_ALL, sock, sql_data, s_MessageList);
            break;
        case 'Q':
            Server_UserPermissions(S_ALL, sock, sql_data);
            break;
        case 'R':
            Server_AssignGroupLeader(S_ALL, sock, sql_data, s_UserList, s_MessageList);
            break;
        case 'S':
            Server_SeeGroupFriends(S_ALL, sock, sql_data);
            break;
        case 's':
            Server_Any_ALLSendMessage(S_ALL, sock, sql_data, s_UserList);
            break;
        case 'T':
            Server_KickPeople(S_ALL, sock, sql_data, s_UserList, s_MessageList);
            break;
        case 'U':
            Server_SendGroupMessage(S_ALL, sock, sql_data, s_UserList, s_MessageList);
            break;
        case 'V':
            Server_QuitGroup(S_ALL, sock, sql_data, s_UserList, s_MessageList);
            break;
        case 'W':
            Server_VIPUser(S_ALL, sock, sql_data);
            break;
        case 'w':
            Server_OPEN_VIPUser(S_ALL, sock, sql_data);
            break;
        case 'X':
            Server_SeeNewMessage(S_ALL, sock, sql_data, s_MessageList);
            break;
        case 'Y':
            Server_InviteFriend(S_ALL, sock, sql_data, s_UserList, s_MessageList);
            break;
        case 'Z':
            Server_LookGroupMessage(S_ALL, sock, sql_data, s_MessageList);
            break;
        case '+':
            Server_AddFriend(S_ALL, sock, sql_data, s_UserList, s_MessageList);
            break;
        case '-':
            Server_DelFriend(S_ALL, sock, sql_data, s_UserList);
            break;
        case '?':
            Server_See_GroupFile(S_ALL, sock, sql_data);
            break;
        case '.':
            Server_UploadGroupFile(S_ALL, sock, sql_data);
            break;            
        case '<':
            Server_UploadGroupFile1(S_ALL, sock, sql_data);
            break;
        case '!':
            Server_DownGroupFile(S_ALL, sock, sql_data);
            break;
        case ':':
            Server_UploadArchiveFile(S_ALL, sock, sql_data);
            break;
        case ';':
            Server_DownloadArchive(S_ALL, sock, sql_data);
            break;
        case '*':
            Server_ExitUser(S_ALL, sock, sql_data, s_UserList);
            return;
        default:
            break;
        }
    }
}
