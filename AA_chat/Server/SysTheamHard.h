#ifndef __SYSTHEAMHARD_H__
#define __SYSTHEAMHARD_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sqlite3.h>
#include <dirent.h>

#include "AA_Server.h"
#include "StdTcp.h"
#include "Login.h"
#include "StdThread.h"
#include "DoubleLinklist.h"
#include "ThreadPool.h"
#include "Stdfile.h"
#include "StdSqlite.h"
#include "System.h"


#endif

#define true 1
#define false 0