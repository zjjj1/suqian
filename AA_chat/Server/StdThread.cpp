#include "StdThread.h"

struct StdThread
{
    pthread_t threadID;
};

Thread *InitThread(void *(func)(void *), void *arg)
{
    Thread *t = (Thread *)malloc(sizeof(Thread));
    if (t == NULL)
    {
        printf("InitThread malloc error!\n");
        return NULL;
    }
    if (pthread_create(&t->threadID, NULL, func, arg) != 0)
    {
        perror("InitThread pthread_create:");
        free(t);
        return NULL;
    }
    return t;
}

void *JoinThread(Thread *t)
{
    void *value = NULL;
    pthread_join(t->threadID, &value);
    return value;
}

void DetachThread(Thread *t)
{
    if (pthread_detach(t->threadID) != 0)
    {
        perror("DetachThread pthread_detach:");
        return;
    }
}

void CancelThread(Thread *t)
{
    if (pthread_cancel(t->threadID) != 0)
    {
        printf("CancelThread pthread_cancel");
        return;
    }
}

unsigned long int GetThreadId(Thread*t)
{
    return t->threadID;
}

void ClearThread(Thread *t)
{
    free(t);
}
