#ifndef __THREADPOOL_H__
#define __THREADPOOL_H__

struct ThreadPool;
typedef struct ThreadPool ThreadP;

ThreadP *InitThreadPool(int max_task_queue_num, int min_thrd_num, int max_thrd_num);
void ThreadP_Addtask(ThreadP *p, void *(*func)(void *), void *arg);
void FreeL(void *arg);
void FreeQ(void *arg);
void DestoryThreadPool(ThreadP *p);


#endif