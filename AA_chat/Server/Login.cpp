#include "SysTheamHard.h"

Cli *creatClient(int sock, const char *name)
{
    Cli *c = (Cli *)malloc(sizeof(Cli));
    if (c == NULL)
    {
        printf("creatClient malloc error!\n");
        return NULL;
    }

    c->sock = sock;
    strcpy(c->name, name);
    return c;
}

void FreeClient(Cli *c)
{
    if (c != NULL)
        free(c);
}

void FreeCli(void *data)
{
    Cli *c = (Cli *)data;
    close(c->sock);
    free(c);
}

void Login_Server(char *S_ALL, int sock, SQL *sql, DLlist *UserList)
{
    char s_UserName[30] = {0};
    char s_PassWd[30] = {0};
    char sc[5] = {0};
    char flag;
    sscanf(S_ALL, "%c %s %s", &flag, s_UserName, s_PassWd);

    char sql_id[1024] = {0};
    sprintf(sql_id, "select UserName,PassWd from AllUser where UserName='%s';", s_UserName);
    char **result_User;
    int row_id, column_id;
    SelectInfo(sql, sql_id, &result_User, &row_id, &column_id);
    if (row_id == 0 || (strcmp(*(result_User + 3), s_PassWd) != 0))
    {

        sprintf(sc, "%c", '0');
        TcpServerSend(sock, sc, strlen(sc) + 1);
    }
    else
    {
        memset(sql_id, 0, 1024);
        sprintf(sql_id, "select online from AllUser where UserName='%s';", s_UserName);
        SelectInfo(sql, sql_id, &result_User, &row_id, &column_id);
        if (*result_User[1] == '1')
        {
            sprintf(sc, "%c", '1');
            TcpServerSend(sock, sc, strlen(sc) + 1);
        }
        else
        {
            DLInsertTail(UserList, creatClient(sock, s_UserName));
            memset(sql_id, 0, 1024);
            sprintf(sql_id, "update AllUser set online = 1 where UserName='%s';", s_UserName);
            Sql_Exec(sql, sql_id);
            sprintf(sc, "%c", 'A');
            TcpServerSend(sock, sc, strlen(sc) + 1);
        }
    }
    FreeInfoResult(result_User);
    return;
}

void Register_Server(char *S_ALL, int sock, SQL *sql)
{
    char new_UserName[30] = {0};
    char new_PassWd[30] = {0};
    char sc[5] = {0};
    char flag;
    sscanf(S_ALL, "%c %s %s", &flag, new_UserName, new_PassWd);

    char sql_new[100] = {0};
    sprintf(sql_new, "select UserName from AllUser where UserName='%s';", new_UserName);
    if (IsHaveData(sql, sql_new) == true)
    {
        sprintf(sc, "%c", '0');
        TcpServerSend(sock, sc, strlen(sc) + 1);
        return;
    }
    else
    {
        char sql_new[1024] = {0};
        sprintf(sql_new, "insert into AllUser values('%s','%s',0,0);", new_UserName, new_PassWd);
        Sql_Exec(sql, sql_new);

        char id_s[50] = {0};
        sprintf(id_s, "Friends_%s", new_UserName);
        char *Us_friend[] = {"Friends_Name", "text"};
        CreatTable(sql, id_s, Us_friend, Size_len(Us_friend) / 2);

        char s_name[60] = "";
        strcat(s_name, "/home/lvguanzhong/桌面/chat/AA-server/data/Save/");
        strcat(s_name, new_UserName);
        CreateDir(s_name);

        sprintf(sc, "%c", 'B');
        TcpServerSend(sock, sc, strlen(sc) + 1);
        return;
    }
}
