TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle


SOURCES += \
        AA_Server.cpp \
        DoubleLinklist.cpp \
        LinkQueue.cpp \
        Login.cpp \
        StdSqlite.cpp \
        StdTcp.cpp \
        StdThread.cpp \
        Stdfile.cpp \
        System.cpp \
        ThreadPool.cpp \
        main.cpp

DISTFILES += \
    Server.pro.user

HEADERS += \
    AA_Server.h \
    DoubleLinklist.h \
    LinkQueue.h \
    Login.h \
    StdSqlite.h \
    StdTcp.h \
    StdThread.h \
    Stdfile.h \
    SysTheamHard.h \
    System.h \
    ThreadPool.h
LIBS +=-lsqlite3

QT += core concurrent
