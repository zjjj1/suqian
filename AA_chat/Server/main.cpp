#include "SysTheamHard.h"

DLlist s_UserList;
DLlist s_MessageList;
SQL *sql_data;



void *Thread_Ser_Handler(void *arg)
{
    int sock = *((int *)arg);
    Server_FuncThread(sock,sql_data,&s_MessageList,&s_UserList);
    return NULL;
}

int main()
{
    // 绑定端口和地址返回套接字
    TcpS *tcps = InitTcpServer("192.168.242.128", 8080);
    if (tcps == NULL)
    {
        printf("tcps is NULL!\n");
        return -1;
    }
    // 创建线程池
    ThreadP *thp = InitThreadPool(20, 5, 20);
    if (thp == NULL)
    {
        printf("thp is NULL\n");
        return -1;
    }
    // 初始化数据库和链表
    sql_data = InitSqlite("/home/lvguanzhong/桌面/chat/AA-server/data/User.db");
    InitAll(sql_data, &s_UserList, &s_MessageList);
    // 接收用户发来消息并返回处理套接字
    int acceptSock = 0;
    while ((acceptSock = TcpServerAccept(tcps)) > 0)
    {
        // 线程池处理用户发来的消息
        ThreadP_Addtask(thp, Thread_Ser_Handler, &acceptSock);
    }
    // 释放链表,线程池,关闭数据库
    FreeDLlist(&s_UserList, FreeCli);
    FreeSqlite(sql_data);
    DestoryThreadPool(thp);
    return 0;
}
