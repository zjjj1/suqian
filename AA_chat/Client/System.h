#ifndef __SYSTEM_H_
#define __SYSTEM_H_

void CreateDir(const char *name);
void CreateDoc(const char *name);
void DeleteDoc(const char *name);
void DeleteDir(const char *name);
void Copy(const char *sourceName, const char *targetName);
void GrantPermissions(const char *name, const char *permissions);

#endif
