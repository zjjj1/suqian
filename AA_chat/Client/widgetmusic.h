#ifndef WIDGETMUSIC_H
#define WIDGETMUSIC_H

#include <QWidget>
#include <QMediaPlayer>
#include <QPushButton>
#include <QSlider>
#include <QMouseEvent>
#include <QVBoxLayout>
#include <QApplication>
#include "SysTheamHard.h"
#include "picturewidget.h"
#include <QString>
#include <map>
#include <QTimer>

namespace Ui {
class Widgetmusic;
}

class Widgetmusic : public QWidget
{
    Q_OBJECT
public:
    explicit Widgetmusic(QWidget *parent = nullptr);
    ~Widgetmusic();
    void QssbuttonLable();
    void SetButtonLable();
    void SetButtonfunc();
    void SetListT1();
    void SetListT2();
    void SetListT3();
    void SetLable();
    void SetTabWidget();
    void SetLrcList();
    void SetLrc();
    void SetSilder();
    void SetVoive();
    void NextPrve();
    void SetPictureWall();
    void SetShowSongList();
    void InitQss(const QString &qsspath);
    void SetMusicName(const QString &songName);
    void SetUserName(const QString &Name);
    void SetUserLabel();
    void deletePictureWidget()const;

public slots:
    void PlayMusic();
    void DoubleClickedList();
    void SetMusicList();

protected:
    bool isMouseNotOverButtons();
    void mousePressEvent(QMouseEvent *event)override;
    void mouseMoveEvent(QMouseEvent *event)override;
    void mouseReleaseEvent(QMouseEvent *event)override;
    bool eventFilter(QObject* obj, QEvent* event) override;

private:
    QPoint m_dragStartPosition;
    QString c_userName;
    Ui::Widgetmusic *ui;   //命名空间
    QMediaPlayer player;
    QString FilePath;      //歌曲文件夹路径
    std::map<int,QString> LrcList;  //歌词列表
    PictureWidget * pictureWidget;    //图片墙
    QTimer timer; // 歌词定时器
    int volume;
    int showSongList;
    QPushButton *pushButton_voice; //音量按钮
    QSlider *volumeSlider;        //音量滑轨
};

#endif // WIDGETMUSIC_H
