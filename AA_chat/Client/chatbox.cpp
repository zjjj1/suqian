#include "chatbox.h"
#include "ui_chatbox.h"
#include <QMessageBox>
#include <QThread>
#include <QtDebug>

extern TcpClient tcpc;

void ChatBox::c_CloseWindow()
{
    timer.stop();
    close();
}

void ChatBox::c_MiniWindow()
{
    showMinimized();
}

void ChatBox::c_historyMseeage()
{
    char str[100] = {0};
    sprintf(str, "%c %s %s", 'l',  fromName.toUtf8().constData(), toName.toUtf8().constData());
    if(tcpc.TcpClientSend( str, strlen(str) + 1) < 0)
    {
        QMessageBox::critical(nullptr, "错误", "与服务器连接失败！");
        return;
    }

    ui->allMessage_listWidget->clear();
    AllMessageList.clear();
    QThread::msleep(1);

    char c_temp[2048] = {0};
    while(1)
    {
        memset(c_temp,0,2048);
        if(tcpc.TcpclientRecv(c_temp, sizeof(c_temp)) < 0)
        {
             qDebug()<<"未收到"<<endl;
             break;
        }
        if(*c_temp == 'N')
        {
            if(AllMessageList.size() == 0)
            {
                QListWidgetItem *item = new QListWidgetItem;
                item->setText(QString("未查询到历史聊天"));
                ui->allMessage_listWidget->insertItem(0,item);
            }
            return;
        }
        else if(*c_temp == 'Y')
        {
            // 解析服务端发来的消息
            QString message(c_temp);
            QStringList messageParts = message.split(' ');
            if (messageParts.size() < 3) {
                // 错误处理，数组长度不足
                continue; // 跳过当前循环，继续下一次循环
            }
            QString messageType = messageParts[0];
            QString fromName = messageParts[1];
            QString toName = messageParts[2];
            QString messageText = messageParts.mid(3).join(' ');
            QListWidgetItem *item = new QListWidgetItem;
            if( fromName == this->fromName)
            {
                item->setText("「 "+messageText+" 」<——"+fromName);
                item->setTextAlignment(Qt::AlignRight);
            }
            else
            {
                item->setText(fromName+"——>「 "+messageText+" 」");
                item->setTextAlignment(Qt::AlignLeft);
            }
            ui->allMessage_listWidget->addItem(item);
            AllMessageList.insert(std::make_pair(fromName,messageText));
            ui->allMessage_listWidget->scrollToBottom();
        }
    }
}

void ChatBox::c_sendMessage()
{
    QString message = ui->SendMessage_Edit->toPlainText();
    if(message.isEmpty())
    {
        QMessageBox::warning(nullptr, "警告", "不能发送空消息！");
        return;
    }
    char str[2048] = {0};
    sprintf(str, "%c %s %s %s", 'C', fromName.toUtf8().constData(), toName.toUtf8().constData(), message.toUtf8().constData());
    if(tcpc.TcpClientSend(str, strlen(str) + 1) < 0)
    {
        QMessageBox::critical(nullptr, "错误", "与服务器连接失败！");
        return;
    }
    QThread::msleep(1);

    char c_temp[10] = {0};
    tcpc.TcpclientRecv(c_temp, sizeof(c_temp));
    char flag1, flag2, flag3;
    sscanf(c_temp, "%c %c %c", &flag1, &flag2, &flag3);
    if (flag3 == 'Y')
    {
        ui->SendMessage_Edit->clear();
        c_historyMseeage();
    }
    else
        QMessageBox::warning(nullptr, "提示", "消息发送失败，您当前已经不是该用户的好友！");
}

ChatBox::ChatBox(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ChatBox)
{
    ui->setupUi(this);
    setWindowIcon(QIcon(":/image/message.png"));
    setFixedSize(630,480);
    this->move(550,200);

    // 隐藏状态栏（标题栏）
    setWindowFlag(Qt::FramelessWindowHint);

    timer.start(1500);

    ui->Close_Button->setIcon(QIcon(":/image/close.png"));
    connect(ui->Close_Button, &QPushButton::clicked, this, &ChatBox::c_CloseWindow);
    ui->Close_Button->setIconSize(ui->Close_Button->size());
    connect(ui->close_Button, &QPushButton::clicked, this, &ChatBox::c_CloseWindow);
    ui->mini_Button->setIcon(QIcon(":/image/small.png"));
    connect(ui->mini_Button, &QPushButton::clicked, this, &ChatBox::c_MiniWindow);
    ui->mini_Button->setIconSize(ui->mini_Button->size());


    // 刷新消息
    connect(&timer,&QTimer::timeout,[this](){
            c_historyMseeage();
        });
    // 发送消息
    connect(ui->sendMessage_Button, &QPushButton::clicked, [this](){
        emit c_sendMessage();
    });

}

ChatBox::~ChatBox()
{
    delete ui;
}

void ChatBox::mousePressEvent(QMouseEvent *event)
{
    // 记录鼠标点击的位置
    if (event->button() == Qt::LeftButton )
    {
        m_dragStartPosition = event->globalPos() - frameGeometry().topLeft();
    }
}

void ChatBox::mouseMoveEvent(QMouseEvent *event)
{
    // 实现窗口的拖动效果
    if (event->buttons() & Qt::LeftButton &&
            !ui->Close_Button->underMouse() &&
            !ui->mini_Button->underMouse())
    {
        // 判断当前鼠标位置是否在指定范围内
        if (event->pos().x() >= 0 && event->pos().x() <= 630 &&
            event->pos().y() >= 0 && event->pos().y() <= 60)
        {
            move(event->globalPos() - m_dragStartPosition);
        }
    }
}

void ChatBox::mouseReleaseEvent(QMouseEvent *event)
{
    // 清空鼠标点击的位置
    if (event->button() == Qt::LeftButton)
    {
        m_dragStartPosition = QPoint();
    }
}

void ChatBox::c_setFromName(QString fromName)
{
    this->fromName = fromName;
}

void ChatBox::c_setToName(QString toName)
{
    this->toName = toName;
}

QString ChatBox::c_getToName()
{
    return this->toName;
}

void ChatBox::c_setMessageLable()
{
    ui->Message_label->setText("与"+this->toName+"的聊天");
}
