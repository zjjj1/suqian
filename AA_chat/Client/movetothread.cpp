#include "movetothread.h"
#include <QListWidgetItem>

extern TcpClient tcpc;

movetothread::movetothread( QObject* parent) : QObject(parent)
{

}

void movetothread::mUploadfile(QString filePath)
{
    QFileInfo fileInfo(filePath);
    QString fileName = fileInfo.fileName();

    FILE* fp = fopen(filePath.toUtf8().constData(), "rb"); // 以二进制模式打开文件
    fseek(fp, 0, SEEK_END);
    long size = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    long Size = size;
    long Num = 0; // 已经下载大小

    // 存储每次读取到的数据的缓冲区
    char temp[20600] = {0};
    int num = strlen(fileName.toUtf8().constData());
    while (size > 0)
    {
        sprintf(temp, "%c %s ", '@', fileName.toUtf8().constData());
        int bytesRead = fread(temp+3+num, 1, 20480, fp);
        if (bytesRead <= 0)
        {
            perror("Error reading file");
            fclose(fp);
            return;
        }
        if(tcpc.TcpClientSend(temp, 20483+num) < 0)
        {
            QMessageBox::critical(nullptr, "错误", "下载：与服务器连接失败！");
            fclose(fp);
            return;
        }
        size -= bytesRead;
        Num += bytesRead;
        emit m_showUploadFileLable(Size,Num);
        QThread::msleep(50);
        // 清空缓冲区
        memset(temp, 0, 20600);
    }
    fclose(fp); // 关闭文件
}

void movetothread::setDownloadFileName(QString FileName)
{
    this->FileName.clear();
    this->FileName = FileName;
}

void movetothread::mDownloadFile()
{
    // 打开文件夹选择对话框，并获取选择的文件夹路径
    QString folderPath = QFileDialog::getExistingDirectory(nullptr, "选择目标文件夹", "");
    if (folderPath.isEmpty())
    {
        return;
    }
    QString FilePath = folderPath+ "/" +this->FileName;
    qDebug()<<FilePath;
    QFile file(FilePath);
    // 查看是否有同名文件
    if (file.open(QIODevice::ReadOnly))
    {
        file.close();
        QMessageBox::information(nullptr, "提示", "当前路径下已有同名文件！");
        return;
    }

    char str[50] = {0};
    sprintf(str, "%c %s", '!',this->FileName.toUtf8().constData());
    if (tcpc.TcpClientSend(str, strlen(str) + 1) < 0)
    {
        QMessageBox::critical(nullptr, "错误", "与服务器连接失败！");
        return;
    }

    if (file.open(QIODevice::Append))
    {
        char temp[20600] = {0};
        while(1)
        {
            if(tcpc.TcpclientRecv(temp, sizeof(temp)) < 0)
            {
                QMessageBox::critical(nullptr, "错误", "上传：与服务器连接失败！");
                file.close();
                return;
            }
            if(*temp == '!')
            {
                char str;
                char size[50] = {0};
                char num[50] = {0};
                sscanf(temp, "%c %s %s ",&str, size, num);
                long Size = atoi(size);
                long Num = atoi(num);
                emit m_showDownloadFileLable(Size,Num);
                if(Num >= Size)
                {
                    file.close();
                    return;
                }
                char* content = temp + strlen(size) + strlen(num) + 4;
                file.write(content,20480);
                memset(temp, 0, 20600);
            }
        }
    }
}
