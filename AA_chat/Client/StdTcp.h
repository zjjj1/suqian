#ifndef __STDTCP_H__
#define __STDTCP_H__

#include <stddef.h>

class TcpClient
{
public:
    TcpClient(const char *Serverip, short int Serverport);
    ~TcpClient();
    //客户端发送消息
    int TcpClientSend(void* ptr, size_t len);
    //客户端接收消息
    int TcpclientRecv(void* ptr, size_t len);
protected:
    int sock;
};

#endif
