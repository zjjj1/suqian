QT       += core gui widgets multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    DoubleLinklist.cpp \
    StdTcp.cpp \
    StdThread.cpp \
    Stdfile.cpp \
    System.cpp \
    chatbox.cpp \
    groupbox.cpp \
    main.cpp \
    client.cpp \
    movetothread.cpp \
    picturebutton.cpp \
    pictureitem.cpp \
    pictureview.cpp \
    picturewidget.cpp \
    widgetmusic.cpp

HEADERS += \
    DoubleLinklist.h \
    StdTcp.h \
    StdThread.h \
    Stdfile.h \
    SysTheamHard.h \
    System.h \
    chatbox.h \
    client.h \
    groupbox.h \
    movetothread.h \
    picturebutton.h \
    pictureitem.h \
    pictureview.h \
    picturewidget.h \
    widgetmusic.h

FORMS += \
    chatbox.ui \
    client.ui \
    groupbox.ui \
    picturewidget.ui \
    widgetmusic.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    rc.qrc

QMAKE_CXXFLAGS += -frtti
