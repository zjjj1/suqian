#include "widgetmusic.h"
#include "ui_widgetmusic.h"
#include <QSlider>
#include <QDir>
#include <QString>
#include <QMovie>
#include <QDebug>

extern TcpClient tcpc;

Widgetmusic::Widgetmusic(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widgetmusic)
{
    ui->setupUi(this);
    setFixedSize(1000,600);
    // 隐藏状态栏（标题栏）
    setWindowFlag(Qt::FramelessWindowHint);
    setWindowTitle("AA_MUSIC！");
    setWindowIcon(QIcon(":/image/pande.png"));
    // 将主界面背景设置为白色
    QPalette pal(palette());
    pal.setColor(QPalette::Background, Qt::white);
    this->setAutoFillBackground(true);
    this->setPalette(pal);

    // 初始化两个控件
    pushButton_voice = new QPushButton(this);
    volumeSlider = new QSlider(Qt::Vertical, this);

    QssbuttonLable();
    // 设置按钮和标签
    SetButtonLable();
    // 设置按钮功能
    SetButtonfunc();
    // 设置样式表
    InitQss("/home/lvguanzhong/Chat/Client/qss/music.qss");
    // 设置界面左边音乐列表
    SetListT1();
    SetListT2();
    SetListT3();
    // 设置主界面列表
    SetTabWidget();
    // 设置播放音乐条
    SetSilder();
    // 设置音量条
    SetVoive();
    // 设置主界面音乐滚动
    SetPictureWall();
    // 设置音乐歌单列表
    SetShowSongList();
//    installEventFilter(this);
    //设置多媒体文件路径
    //url是一种抽象路径
    SetMusicList();
    // 设置上一首下一首
    NextPrve();
    // 设置歌词
    SetLrc();
    // 设置用户标签
    SetLable();
    SetLrcList(); //调用歌词函数

    // 设置初始化音乐
    player.setMedia(QUrl::fromLocalFile("/home/lvguanzhong/Chat/Client/File/music/顾夕若-阿拉斯加海湾.mp3"));

    // 设置列表字体
    QFont font;
    font.setWeight(QFont::Light);
    font.setPointSize(13);
    ui->listT1->setFont(font);
    ui->listT2->setFont(font);
    ui->listT3->setFont(font);
    QFont font1;
    font1.setWeight(QFont::Light);
    font1.setPointSize(10);
    ui->label_mymusic->setFont(font1);
    ui->label_create_music->setFont(font1);
}

Widgetmusic::~Widgetmusic()
{
    delete ui;
}

bool Widgetmusic::isMouseNotOverButtons()
{
    return !ui->pushButton_close->underMouse() &&
            !ui->pushButton_max->underMouse() &&
            !ui->pushButton_mini->underMouse() &&
            !ui->pushButton_small->underMouse() &&
            !ui->pushButton_message->underMouse() &&
            !ui->pushButton_title_left->underMouse() &&
            !ui->pushButton_title_right->underMouse() &&
            !ui->pushButton_set->underMouse() &&
            !ui->pushButton_Qss->underMouse() &&
            !ui->pushButton_name->underMouse() &&
            !ui->pushButton_listen->underMouse() &&
            !ui->pushButton_vip->underMouse() &&
            !ui->pushButton_people->underMouse();
}

void Widgetmusic::mousePressEvent(QMouseEvent *event)
{
    // 记录鼠标点击的位置
    if (event->button() == Qt::LeftButton )
    {
        m_dragStartPosition = event->globalPos() - frameGeometry().topLeft();
    }
}

void Widgetmusic::mouseMoveEvent(QMouseEvent *event)
{
    // 实现窗口的拖动效果
    if (event->buttons() & Qt::LeftButton && isMouseNotOverButtons())
    {
        // 判断当前鼠标位置是否在指定范围内
        if (event->pos().x() >= 0 && event->pos().x() <= 1000 &&
                event->pos().y() >= 0 && event->pos().y() <= 60)
        {
            move(event->globalPos() - m_dragStartPosition);
        }
    }
}

void Widgetmusic::mouseReleaseEvent(QMouseEvent *event)
{
    // 清空鼠标点击的位置
    if (event->button() == Qt::LeftButton)
    {
        m_dragStartPosition = QPoint();
    }
}

void Widgetmusic::QssbuttonLable()
{
    pushButton_voice->move(877,547);
    pushButton_voice->setFixedSize(31,31);
    pushButton_voice->setIcon(QIcon(":/image/music/voice2.png"));
    pushButton_voice->setIconSize(pushButton_voice->size());
    volumeSlider->move(874,440);
    volumeSlider->setObjectName("volumeSlider");
}

void Widgetmusic::SetButtonLable()
{
    QPixmap LogoMap(":/image/music/wyy.png");
    ui->label_Logo->setPixmap(LogoMap);
    QPixmap musicnameMap(":/image/music/musicname.png");
    ui->label_Name->setPixmap(musicnameMap.scaled(ui->label_Name->size(),Qt::KeepAspectRatio, Qt::SmoothTransformation));
    QPixmap xianMap(":/image/music/竖线.png");
    ui->label_xian->setPixmap(xianMap.scaled(ui->label_xian->size(),Qt::KeepAspectRatio, Qt::SmoothTransformation));
    QPixmap searchMap(":/image/music/searchedit.png");
    ui->label_search->setPixmap(searchMap.scaled(ui->label_search->size(),Qt::KeepAspectRatio, Qt::SmoothTransformation));
    QPixmap bfMap(":/image/music/bofang.png");
    ui->label_bofang->setPixmap(bfMap.scaled(ui->label_bofang->size(),Qt::KeepAspectRatio, Qt::SmoothTransformation));
    QPixmap scMap(":/image/music/shoucang.png");
    ui->label_shoucang->setPixmap(scMap.scaled(ui->label_shoucang->size(),Qt::KeepAspectRatio, Qt::SmoothTransformation));
    ui->pushButton->setIcon(QIcon(":/image/music/on.png"));
    ui->pushButton->setIconSize(ui->pushButton->size());
    ui->PrevButton->setIcon(QIcon(":/image/music/上一曲1.png"));
    ui->PrevButton->setIconSize(ui->PrevButton->size());
    ui->NextButton->setIcon(QIcon(":/image/music/下一曲1.png"));
    ui->NextButton->setIconSize(ui->NextButton->size());
    ui->pushButton_Qss->setIcon(QIcon(":/image/music/clcor.png"));
    ui->pushButton_Qss->setIconSize(ui->pushButton_Qss->size());
    ui->pushButton_close->setIcon(QIcon(":/image/music/close.png"));
    ui->pushButton_close->setIconSize(ui->pushButton_close->size());
    ui->pushButton_max->setIcon(QIcon(":/image/music/max.png"));
    ui->pushButton_max->setIconSize(ui->pushButton_max->size());
    ui->pushButton_mini->setIcon(QIcon(":/image/music/mini.png"));
    ui->pushButton_mini->setIconSize(ui->pushButton_mini->size());
    ui->pushButton_small->setIcon(QIcon(":/image/music/small.png"));
    ui->pushButton_small->setIconSize(ui->pushButton_small->size());
    ui->pushButton_message->setIcon(QIcon(":/image/music/email.png"));
    ui->pushButton_message->setIconSize(ui->pushButton_message->size());
    ui->pushButton_title_left->setIcon(QIcon(":/image/music/后退3.png"));
    ui->pushButton_title_left->setIconSize(ui->pushButton_title_left->size());
    ui->pushButton_title_right->setIcon(QIcon(":/image/music/前进2.png"));
    ui->pushButton_title_right->setIconSize(ui->pushButton_title_right->size());
    ui->pushButton_set->setIcon(QIcon(":/image/music/set.png"));
    ui->pushButton_set->setIconSize(ui->pushButton_set->size());
    ui->pushButton_name->setIcon(QIcon(":/image/music/btn_people_h.png"));
    ui->pushButton_name->setIconSize(ui->pushButton_name->size());
    ui->pushButton_listen->setIcon(QIcon(":/image/music/话筒.png"));
    ui->pushButton_listen->setIconSize(ui->pushButton_listen->size());
    ui->pushButton_vip->setIcon(QIcon(":/image/music/vip.png"));
    ui->pushButton_vip->setIconSize(ui->pushButton_vip->size());
    ui->pushButton_people->setIcon(QIcon(":/image/the.png"));
    ui->pushButton_people->setIconSize(ui->pushButton_people->size());
    ui->pushButton_List->setIcon(QIcon(":/image/music/listall.png"));
    ui->pushButton_List->setIconSize(ui->pushButton_List->size());
    ui->pushButton_friend->setIcon(QIcon(":/image/music/all.png"));
    ui->pushButton_friend->setIconSize(ui->pushButton_friend->size());
    ui->pushButton_voice2->setIcon(QIcon(":/image/music/voice.png"));
    ui->pushButton_voice2->setIconSize(ui->pushButton_voice2->size());
    ui->pushButton_timbre->setIcon(QIcon(":/image/music/标准.png"));
    ui->pushButton_timbre->setIconSize(ui->pushButton_timbre->size());
    ui->pushButton_geci->setIcon(QIcon(":/image/music/歌词.png"));
    ui->pushButton_geci->setIconSize(ui->pushButton_geci->size());
    ui->pushButton_random->setIcon(QIcon(":/image/music/circulation.png"));
    ui->pushButton_random->setIconSize(ui->pushButton_random->size());
    ui->pushButton_like->setIcon(QIcon(":/image/music/like.png"));
    ui->pushButton_like->setIconSize(ui->pushButton_like->size());
}

void Widgetmusic::SetButtonfunc()
{
    connect(ui->pushButton_close,&QPushButton::clicked,this,[=]()
    {
        close();
    });
    connect(ui->pushButton_small,&QPushButton::clicked,this,[=]()
    {
        showMinimized();
    });
    connect(ui->pushButton_shuaxin,&QPushButton::clicked,this,&Widgetmusic::SetMusicList);
}

void Widgetmusic::SetListT1()
{
    //首先我们要把滚动条给取消掉 垂直与水平
    ui->listT1->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->listT1->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    QListWidgetItem *findMusic = new QListWidgetItem(ui->listT1);
    findMusic->setText("发现音乐");
    QListWidgetItem *podcast = new QListWidgetItem(ui->listT1);
    podcast->setText("播客");
    QListWidgetItem *video = new QListWidgetItem(ui->listT1);
    video->setText("视频");
    QListWidgetItem *friends = new QListWidgetItem(ui->listT1);
    friends->setText("朋友");
    QListWidgetItem *live = new QListWidgetItem(ui->listT1);
    live->setText("直播");
    QListWidgetItem *pcFM = new QListWidgetItem(ui->listT1);
    pcFM->setText("私人FM");
}

void Widgetmusic::SetListT2()
{
    ui->listT2->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->listT2->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    QListWidgetItem *localDownload = new QListWidgetItem(ui->listT2);
    localDownload->setIcon(QIcon(":/image/music/下载.png"));
    localDownload->setText("本地与下载");

    QListWidgetItem *recentPlay = new QListWidgetItem(ui->listT2);
    recentPlay->setIcon(QIcon(":/image/music/最近播放.png"));
    recentPlay->setText("最近播放");

    QListWidgetItem *myMusic = new QListWidgetItem(ui->listT2);
    myMusic->setIcon(QIcon(":/image/music/云盘.png"));
    myMusic->setText("我的音乐云盘");

    QListWidgetItem *myPodcast = new QListWidgetItem(ui->listT2);
    myPodcast->setIcon(QIcon(":/image/music/播客.png"));
    myPodcast->setText("我的播客");

    QListWidgetItem *myCollect = new QListWidgetItem(ui->listT2);
    myCollect->setIcon(QIcon(":/image/music/收藏.png"));
    myCollect->setText("我的收藏");
}

void Widgetmusic::SetListT3()
{
    ui->listT3->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    ui->listT3->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    QListWidgetItem *myLove = new QListWidgetItem(ui->listT3);
    myLove->setIcon(QIcon(":/image/music/心.png"));
    myLove->setText("我喜欢的音乐");
}

void Widgetmusic::SetLable()
{
    QPixmap Map_1(":/image/music/推荐歌单.png");
    ui->label_1->setPixmap(Map_1.scaled(ui->label_1->size(),Qt::KeepAspectRatio, Qt::SmoothTransformation));
    QPixmap Map_2(":/image/music/专属定制.png");
    ui->label_2->setPixmap(Map_2.scaled(ui->label_2->size(),Qt::KeepAspectRatio, Qt::SmoothTransformation));
    QPixmap Map_3(":/image/music/歌单.png");
    ui->label_3->setPixmap(Map_3.scaled(ui->label_3->size(),Qt::KeepAspectRatio, Qt::SmoothTransformation));
    QPixmap Map_4(":/image/music/排行榜.png");
    ui->label_4->setPixmap(Map_4.scaled(ui->label_4->size(),Qt::KeepAspectRatio, Qt::SmoothTransformation));
    QPixmap Map_5(":/image/music/歌手.png");
    ui->label_5->setPixmap(Map_5.scaled(ui->label_5->size(),Qt::KeepAspectRatio, Qt::SmoothTransformation));
    QPixmap Map_6(":/image/music/最新音乐.png");
    ui->label_6->setPixmap(Map_6.scaled(ui->label_6->size(),Qt::KeepAspectRatio, Qt::SmoothTransformation));

}

void Widgetmusic::SetTabWidget()
{
    ui->tabWidget->setCurrentIndex(0);
    ui->tabWidget->setTabText(0,"个性推荐");
    ui->tabWidget->setTabText(1,"专属定制");
    ui->tabWidget->setTabText(2,"歌单");
    ui->tabWidget->setTabText(3,"排行榜");
    ui->tabWidget->setTabText(4,"歌手");
    ui->tabWidget->setTabText(5,"最新音乐");
}

void Widgetmusic::SetMusicList()
{
    ui->listWidget->clear();
    FilePath = "/home/lvguanzhong/Chat/Client/File/music";
    QDir dir(FilePath);
    if(dir.exists() == false)
    {
        return;
    }

    auto fileList = dir.entryList(QDir::Files);
    for(auto &fileName : fileList)
    {
        if(fileName.contains(".mp3"))
        {
            QListWidgetItem * item = new QListWidgetItem;
            item->setText(fileName);
            ui->listWidget->insertItem(0,item);
        }
    }
    if(ui->listWidget->count() != 0)
        ui->listWidget->setCurrentRow(0);
}

void Widgetmusic::SetLrcList()
{
    QFile file("/home/lvguanzhong/Chat/Client/File/江南.lrc");
    if(file.open(QIODevice::ReadOnly) == false)
    {
        return;
    }
    QTextStream in(&file);
    while(in.atEnd() == false)
    {
        QString line = in.readLine();
        QStringList linelist = line.split("]");
        if(linelist.size() != 2)
            continue;
        auto LrcStr = linelist[1];
        linelist[0].remove(0,1);
        int minute = linelist[0].split(":")[0].toInt();
        int second = linelist[0].split(":")[1].split(".")[0].toInt();
        int msec = linelist[0].split(":")[1].split(".")[1].toInt();
        int TotalMilliseconds = msec + second * 1000 + minute * 60 * 1000;
        LrcList[TotalMilliseconds] = LrcStr;
    }
    file.close();
}

void Widgetmusic::SetSilder()
{
    connect(&player,&QMediaPlayer::durationChanged,[this]()
    {
        QString TotalTime = QString("%1:%2").arg(player.duration()/1000/60).arg(player.duration()/1000%60);
        ui->TotalTimelable->setText(TotalTime);
        ui->MusicSilder->setRange(0,player.duration());
    });

    connect(&player,&QMediaPlayer::positionChanged,[this]()
    {
        QString CurrentTime = QString("%1:%2").arg(player.position()/1000/60).arg(player.position()/1000%60);
        ui->CurrentTimelabel->setText(CurrentTime);
        ui->MusicSilder->setValue(player.position());
    });

    connect(ui->MusicSilder,&QSlider::sliderMoved,[this]()
    {
        auto CurrentTime = QString("%1:%2").arg(player.position()/1000/60).arg(player.position()/1000%60);
        ui->CurrentTimelabel->setText(CurrentTime);
        player.setPosition(ui->MusicSilder->value());
    });
}

void Widgetmusic::SetVoive()
{
    volumeSlider->hide();
    volumeSlider->setRange(0,100);
    volumeSlider->setValue(100);

    connect(volumeSlider, &QSlider::valueChanged, [=](int value)mutable
    {
        player.setVolume(value);
        if (value == 0)
        {
            // 将按钮图标更改为静音图片
            QIcon icon(":/image/music/voice3.png");
            pushButton_voice->setIcon(icon);
        }
        else
        {
            // 将按钮图标更改为其他图片（非静音状态下的图片）
            QIcon icon(":/image/music/voice2.png");
            pushButton_voice->setIcon(icon);
        }
    });
    connect(pushButton_voice,&QPushButton::clicked,[this]()
    {
        if(volumeSlider->value() != 0)
        {
            volume = volumeSlider->value();
            volumeSlider->setValue(0);
        }
        else
        {
            volumeSlider->setValue(this->volume);
        }
    });
    volumeSlider->setMouseTracking(true);
    // 为 QPushButton 安装事件过滤器
    volumeSlider->installEventFilter(this);
    pushButton_voice->installEventFilter(this);
}

void Widgetmusic::SetLrc()
{
    connect(&timer,&QTimer::timeout,[this]()
    {
        int postion = player.position();
        for(auto ite = LrcList.begin();ite != LrcList.end();ite++)
        {
            auto ite_next = ite;
            ite_next++;
            //当前歌词时间小于等于当前播放器的时间，且下一局的歌词大于播放器的时间
            if(ite->first <= postion && ite_next->first > postion)
            {
                ui->Lrclabel->setText(ite->second);
            }
        }
    });
}

void Widgetmusic::NextPrve()
{
    connect(ui->pushButton,&QPushButton::clicked,this,&Widgetmusic::PlayMusic);
    connect(ui->NextButton,&QPushButton::clicked,[this]()
    {
        int row = (ui->listWidget->currentRow() + 1) % ui->listWidget->count();
        ui->listWidget->setCurrentRow(row);
        DoubleClickedList();
    });

    connect(ui->PrevButton,&QPushButton::clicked,[this]()
    {
        int row;
        if(ui->listWidget->currentRow() == 0)
            row = ui->listWidget->count()-1;
        else
            row = (ui->listWidget->currentRow() - 1);
        ui->listWidget->setCurrentRow(row);
        DoubleClickedList();
    });

    connect(ui->listWidget,&QListWidget::doubleClicked,this,&Widgetmusic::DoubleClickedList);
}

void Widgetmusic::SetPictureWall()
{
    pictureWidget = new PictureWidget();
    ui->verticalLayout->addWidget(pictureWidget);
}

void Widgetmusic::SetShowSongList()
{
    ui->widget_songlist->hide();
    this->showSongList = 0;
    connect(ui->pushButton_List,&QPushButton::clicked,[this]()
    {
        if(this->showSongList == 0)
        {
            ui->widget_songlist->show();
            this->showSongList = 1;
        }
        else
        {
            ui->widget_songlist->hide();
            this->showSongList = 0;
        }
    });
//    ui->pushButton_List->installEventFilter(this);
//    ui->widget_songlist->installEventFilter(this);
}

void Widgetmusic::InitQss(const QString &qsspath)
{
    // 设置对象的专属名称
    ui->Lrclabel->setObjectName("LrcLable");
    QFile file(qsspath);
    if(file.open(QIODevice::ReadOnly) == false)
    {
        qDebug()<<"open qss file fail!\n";
        return;
    }
    QString QssContent = file.readAll();
    //qApp 全局应用指针
    // setStyleSheet 设置全局样式表
    qApp->setStyleSheet(QssContent);
}

void Widgetmusic::SetMusicName(const QString &songName)
{
    QStringList music = songName.split("-");
    ui->pushButton_singer->setText(music[0]);
    ui->pushButton_songtitle->setText(music[1].split(".").first());
}

void Widgetmusic::SetUserName(const QString &Name)
{
    this->c_userName = Name;
}

void Widgetmusic::SetUserLabel()
{
    ui->label_username->setText(this->c_userName);
    ui->label_gedan->setText(this->c_userName+"的雷达歌单");
}

void Widgetmusic::deletePictureWidget() const
{
    delete pictureWidget;
}

void Widgetmusic::PlayMusic()
{

    if(player.state() == QMediaPlayer::PlayingState)
    {
        ui->pushButton->setIcon(QIcon(":/image/music/on.png"));
        ui->pushButton->setIconSize(ui->pushButton->size());
        player.pause();
        timer.stop();
    }
    else
    {
        ui->pushButton->setIcon(QIcon(":/image/music/off.png"));
        ui->pushButton->setIconSize(ui->pushButton->size());
        player.play();
        timer.start(500);
    }
}

void Widgetmusic::DoubleClickedList()
{
    QString MusicPath = FilePath + "/"+ui->listWidget->currentItem()->text();
    player.setMedia(QUrl::fromLocalFile(MusicPath));
    SetMusicName(ui->listWidget->currentItem()->text());
    PlayMusic();
}

bool Widgetmusic::eventFilter(QObject *obj, QEvent *event)
{

    if (event->type() == QEvent::Enter)
    {
        if (obj == pushButton_voice)
        {
            volumeSlider->show();
        }
    }
    else if (event->type() == QEvent::Leave)
    {
        if (obj == pushButton_voice)
        {
            if(!volumeSlider->underMouse())
            {
               volumeSlider->hide();
            }
        }
    }

    if (event->type() == QEvent::Enter)
    {
        if (obj == volumeSlider)
        {
            volumeSlider->show();
        }
    }
    else if (event->type() == QEvent::Leave)
    {
        if (obj == volumeSlider)
        {

            volumeSlider->hide();
        }
    }


    if (obj == volumeSlider && event->type() == QEvent::MouseButtonPress)
    {
        QMouseEvent* mouseEvent = static_cast<QMouseEvent*>(event);
        if (mouseEvent->button() == Qt::LeftButton)
        {
            int startValue = volumeSlider->value();
            if(startValue != 0)
            {
                this->volume = startValue;
            }
        }
    }

//    if (event->type() == QEvent::MouseButtonPress)
//    {
//        QMouseEvent* mouseEvent = static_cast<QMouseEvent*>(event);
//        if (mouseEvent->button() == Qt::LeftButton)
//        {
//            ui->widget_songlist->hide();
//        }
//        else
//        {
//            ui->widget_songlist->hide();
//        }
//    }

    return QWidget::eventFilter(obj, event);
}

