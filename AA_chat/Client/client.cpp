#include "client.h"
#include "ui_client.h"
#include <QLineEdit>
#include <QThread>
#include <QAction>
#include <QMessageBox>
#include <QApplication>
#include <QFileDialog>
#include <QMenu>
#include <QDebug>
#include <QLabel>

TcpClient tcpc = TcpClient("192.168.242.128", 8080);


client::client(QWidget *parent)
    : QWidget(parent), ui(new Ui::client)
{
    ui->setupUi(this);

    setWindowIcon(QIcon(":/image/pande.png"));
    // 隐藏状态栏（标题栏）
    setWindowFlag(Qt::FramelessWindowHint);
    this->move(740,170);

    setWindow();
    setButton();
    setEdit();
    setQMap();
    LoginSignup();
    setFriend();
    setGroup();
    setMessage();
    setUpLoad();
    setdownLoad();
    InitQss("/home/lvguanzhong/Chat/Client/qss/my.qss");

    // music
    isMusicWidgetOpen = 0;
    connect(ui->musicButton,&QPushButton::clicked,this,&client::showMusic);
}

void client::setWindow()
{
    // 创建一个QPixmap对象，加载背景图像文件
    QPixmap backgroundImage(":/image/back.png");
    // 将背景图像缩放到与窗口大小相匹配
    QSize windowSize = this->size(); // 或者根据自己的窗口获取大小
    backgroundImage = backgroundImage.scaled(windowSize, Qt::IgnoreAspectRatio);
    // 创建一个QPalette对象，并为背景图像设置颜色组
    QPalette palette;
    palette.setBrush(QPalette::Background, backgroundImage);
    // 应用新的调色板到UI上
    this->setPalette(palette);
}

void client::setButton()
{
    ui->closeButton->setIcon(QIcon(":/image/close.png"));
    ui->closeButton->setIconSize(ui->closeButton->size());
    connect(ui->closeButton, &QPushButton::clicked, this, &client::CloseWindow);
    ui->smallButton->setIcon(QIcon(":/image/small.png"));
    ui->smallButton->setIconSize(ui->smallButton->size());
    connect(ui->smallButton, &QPushButton::clicked, this, &client::MiniWindow);
    ui->colorButton->setIcon(QIcon(":/image/fu.png"));
    ui->colorButton->setIconSize(ui->colorButton->size());
    ui->spaceButton->setIcon(QIcon(":/image/space.png"));
    ui->gameButton->setIcon(QIcon(":/image/game.png"));
    ui->musicButton->setIcon(QIcon(":/image/music.png"));
    ui->setButton->setIcon(QIcon(":/image/setup.png"));
    // 设置按钮图标大小与按钮大小相匹配
    ui->spaceButton->setIconSize(ui->spaceButton->size());
    ui->gameButton->setIconSize(ui->gameButton->size());
    ui->musicButton->setIconSize(ui->musicButton->size());
    ui->setButton->setIconSize(ui->setButton->size());

}

void client::setEdit()
{
    ui->chat_stackedWidget->setCurrentIndex(2);
    // 设置Edit不能输入空格
    QRegExp regExp("[^\\s]+");
    QValidator* validator = new QRegExpValidator(regExp, this);
    ui->UsrName_Edit->setValidator(validator);
    ui->PassWd_Edit->setValidator(validator);
    ui->UsrName_sigupEdit->setValidator(validator);
    ui->PassWd_sigupEdit->setValidator(validator);
    ui->againPassWd_sigupEdit->setValidator(validator);
    ui->addFriendalineEdit->setValidator(validator);
    ui->newGrouplineEdit->setValidator(validator);
    //登录界面
    ui->UsrName_Edit->setPlaceholderText("请输入用户名！");
    ui->PassWd_Edit->setEchoMode(QLineEdit::Password);
    ui->PassWd_Edit->setPlaceholderText("请输入密码！");
    // 注册界面
    ui->UsrName_sigupEdit->setPlaceholderText("请输入用户名！");
    ui->PassWd_sigupEdit->setEchoMode(QLineEdit::Password);
    ui->PassWd_sigupEdit->setPlaceholderText("请输入密码！");
    ui->againPassWd_sigupEdit->setEchoMode(QLineEdit::Password);
    ui->againPassWd_sigupEdit->setPlaceholderText("请再次输入密码！");

    QAction * action =new QAction(this);
    action->setIcon(QIcon(":/image/user.png"));
    ui->UsrName_Edit->addAction(action,QLineEdit::LeadingPosition);
    QAction * actionpasswd =new QAction(this);
    actionpasswd->setIcon(QIcon(":/image/passwd.png"));
    ui->PassWd_Edit->addAction(actionpasswd,QLineEdit::LeadingPosition);
    ui->UsrName_sigupEdit->addAction(action,QLineEdit::LeadingPosition);
    ui->PassWd_sigupEdit->addAction(actionpasswd,QLineEdit::LeadingPosition);
    ui->againPassWd_sigupEdit->addAction(actionpasswd,QLineEdit::LeadingPosition);
    // +好友/群聊界面
    ui->addFriendalineEdit->setPlaceholderText("请输入昵称：");
    ui->addGrouplineEdit->setPlaceholderText("请输入群聊ID：");
    ui->newGrouplineEdit->setPlaceholderText("请输入群聊名称：");
    // 创建 QIntValidator 对象，并将其设置为 addGrouplineEdit 的验证器，限制输入范围在 10 ~ 99 之间
    QIntValidator *validator1 = new QIntValidator(this);
    ui->addGrouplineEdit->setValidator(validator1);
    validator1->setRange(10, 99);
    //设置个性签名
    ui->label_sign->installEventFilter(this);
    ui->lineEdit_sign->hide();
    connect(ui->lineEdit_sign, &QLineEdit::editingFinished, this, &client::stopEditing);

    QAction * actionaddfriend =new QAction(this);
    actionaddfriend->setIcon(QIcon(":/image/addfriend.png"));
    ui->addFriendalineEdit->addAction(actionaddfriend,QLineEdit::LeadingPosition);

    QAction * actionaddgroup =new QAction(this);
    actionaddgroup->setIcon(QIcon(":/image/addgroup.png"));
    ui->addGrouplineEdit->addAction(actionaddgroup,QLineEdit::LeadingPosition);
    ui->newGrouplineEdit->addAction(actionaddgroup,QLineEdit::LeadingPosition);
}

void client::setQMap()
{
    QPixmap addFriendMap(":/image/addfriendtext.png");
    ui->addFriendlabel->setPixmap(addFriendMap.scaled(ui->addFriendlabel->size(),Qt::KeepAspectRatio));

    QPixmap addGroupMap(":/image/addgrouptext.png");
    ui->addgrouplabel->setPixmap(addGroupMap.scaled(ui->addgrouplabel->size(),Qt::KeepAspectRatio));

    QPixmap newGroupMap(":/image/newGroup.png");
    ui->newgrouplabel->setPixmap(newGroupMap.scaled(ui->newgrouplabel->size(),Qt::KeepAspectRatio));

    QPixmap loginMap(":/image/login.png");
    ui->login_label->setPixmap(loginMap.scaled(ui->login_label->size(),Qt::KeepAspectRatio));

    QPixmap signupMap(":/image/signup.png");
    ui->signup_label->setPixmap(signupMap.scaled(ui->signup_label->size(),Qt::KeepAspectRatio));

    QPixmap map(":/image/pande.png");
    ui->photolabel->setPixmap(map.scaled(ui->photolabel->size(),Qt::KeepAspectRatio));

    QPixmap userPhotoMap(":/image/the.png");
    ui->userPhotoLable->setPixmap(userPhotoMap.scaled(ui->userPhotoLable->size(),Qt::KeepAspectRatio));
}

void client::LoginSignup()
{
    // 登录
    connect(ui->loginButton,&QPushButton::clicked,ui->chat_stackedWidget,[=]()
    {
        auto UserName = ui->UsrName_Edit->text();
        auto PassWd = ui->PassWd_Edit->text();
        if(UserName.isEmpty() || PassWd.isEmpty())
        {
            QMessageBox::warning(nullptr, "警告", "用户名或密码不能为空");
            return;
        }
        else
        {
            char s_signin[100] = {0};
            sprintf(s_signin, "%c %s %s", 'A', UserName.toUtf8().constData(), PassWd.toUtf8().constData());
            if(tcpc.TcpClientSend( s_signin, strlen(s_signin) + 1) < 0)
            {
                QMessageBox::critical(nullptr, "错误", "与服务器连接失败！");
                return;
            }
            QThread::msleep(1);

            char temp[10] = {0};
            tcpc.TcpclientRecv( temp, sizeof(temp));
            if (*temp == 'A')
            {
                this->userName = ui->UsrName_Edit->text();
                ui->UsrName_Edit->clear();
                ui->PassWd_Edit->clear();
//                QMessageBox::information(nullptr, "提示", "登录成功！");
                ui->chat_stackedWidget->setCurrentIndex(0);
                ui->userNameLable->setText(UserName);
                ShowFriendList();
            }
            else if (*temp == '1')
                QMessageBox::warning(nullptr,"警告","该账户已经被登录！");
            else
                QMessageBox::warning(nullptr,"警告","用户名或密码错误！请重新输入！");
        }
    });

    // 注册帐号
    connect(ui->signupButton,&QPushButton::clicked,ui->chat_stackedWidget,[=]()
    {
        ui->chat_stackedWidget->setCurrentIndex(1);
    });

    // 返回登录
    connect(ui->go_loginButton,&QPushButton::clicked,ui->chat_stackedWidget,[=]()
    {
        ui->UsrName_sigupEdit->clear();
        ui->PassWd_sigupEdit->clear();
        ui->againPassWd_sigupEdit->clear();
        ui->chat_stackedWidget->setCurrentIndex(2);
    });

    // 确认注册
    connect(ui->OkButton,&QPushButton::clicked,ui->chat_stackedWidget,[=]()
    {
        auto new_UserName = ui->UsrName_sigupEdit->text();
        auto new_PassWd = ui->PassWd_sigupEdit->text();
        auto new_PassWd_again = ui->againPassWd_sigupEdit->text();

        if(new_UserName.isEmpty() || new_PassWd.isEmpty() || new_PassWd_again.isEmpty())
        {
            QMessageBox::warning(nullptr, "警告", "用户名或密码不能为空");
            return;
        }
        else
        {
            if( new_PassWd != new_PassWd_again )
            {
                QMessageBox::warning(nullptr,"警告","两次密码不相同");
                return;
            }
            char s_register[150] = {0};
            sprintf(s_register, "%c %s %s", 'B', new_UserName.toUtf8().constData(), new_PassWd.toUtf8().constData());
            if(tcpc.TcpClientSend( s_register, strlen(s_register) + 1) < 0)
            {
                QMessageBox::critical(nullptr, "错误", "与服务器连接失败！");
                return;
            }
            QThread::msleep(1);

            char temp[10] = {0};
            tcpc.TcpclientRecv( temp, sizeof(temp));
            if (*temp == 'B')
            {
                QMessageBox::information(nullptr, "提示", "注册成功！");
                ui->UsrName_sigupEdit->clear();
                ui->PassWd_sigupEdit->clear();
                ui->againPassWd_sigupEdit->clear();
                char s_name[20] = "";
                strcat(s_name, "/home/lvguanzhong/桌面/chat/AA-client/Save/");
                strcat(s_name, new_UserName.toUtf8().constData());
                CreateDir(s_name);
            }
            else
                QMessageBox::warning(nullptr, "警告", "用户名已被注册！请重新输入！");
        }
    });

}

void client::setFriend()
{
    ui->friends->setText("好友");
    connect(ui->friends,&QPushButton::clicked,this,&client::ShowFriendList);

    ui->friendsListWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->friendsListWidget, &QListWidget::customContextMenuRequested,
            this, &client::showFriendContextMenu);
    connect(ui->friendsListWidget, &QListWidget::itemDoubleClicked, this, &client::sendFriendMessage);
    // 当添加好友按钮被点击时，将当前界面切换到好友列表页面
    connect(ui->addFriendButton, &QPushButton::clicked, ui->friends_stackedWidget, [=]()
    {
        ui->friends_stackedWidget->setCurrentIndex(1);
    });

    // 当添加好友确认按钮被点击时，调用 addFriend 函数
    connect(ui->addFriend_OKButton, &QPushButton::clicked, this, &client::addFriend);

    // 当前往好友列表按钮被点击时，调用 ShowFriendList 函数
    connect(ui->gotofriendsListButton, &QPushButton::clicked, this, &client::ShowFriendList);


}

void client::setGroup()
{
    ui->groups->setText("群聊");

    // 创建三个根节点
    QTreeWidgetItem* myGroupRoot = new QTreeWidgetItem(ui->groupTreeWidget);
    myGroupRoot->setText(0, "我创建的群聊");

    QTreeWidgetItem* manageGroupRoot = new QTreeWidgetItem(ui->groupTreeWidget);
    manageGroupRoot->setText(0, "我管理的群聊");

    QTreeWidgetItem* joinGroupRoot = new QTreeWidgetItem(ui->groupTreeWidget);
    joinGroupRoot->setText(0, "我加入的群聊");

    // 将根节点添加到 groupTreeWidget 中
    ui->groupTreeWidget->addTopLevelItem(myGroupRoot);
    ui->groupTreeWidget->addTopLevelItem(manageGroupRoot);
    ui->groupTreeWidget->addTopLevelItem(joinGroupRoot);

    connect(ui->groups,&QPushButton::clicked,this,&client::ShowGroupList);
    ui->groupTreeWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->groupTreeWidget, &QTreeWidget::customContextMenuRequested, this, &client::showGroupContextMenu);
    connect(ui->groupTreeWidget, &QTreeWidget::itemDoubleClicked, this, &client::sendMessageToCreatedGroup);

    // 当添加群聊按钮被点击时，将当前界面切换到群聊列表页面
    connect(ui->addGroupButton, &QPushButton::clicked, ui->groups_stackedWidget, [=]()
    {
        ui->addNewGrouplabel->clear();
        ui->groups_stackedWidget->setCurrentIndex(1);
    });

    // 当添加群聊确认按钮被点击时，调用 addGroup 函数
    connect(ui->addGroup_OKButton, &QPushButton::clicked, this, &client::addGroup);

    // 当新建群聊按钮被点击时，将当前界面切换到新建群聊页面
    connect(ui->addNEWGroupButton, &QPushButton::clicked, ui->groups_stackedWidget, [=]()
    {
        ui->newNewGrouplabel->clear();
        ui->groups_stackedWidget->setCurrentIndex(2);
    });

    // 当往群聊列表按钮被点击时，调用 ShowGroupList 函数
    connect(ui->gotoGroupListButton, &QPushButton::clicked, this, &client::ShowGroupList);

    // 当新建群聊确认按钮被点击时，调用 CreateNewGroup 函数
    connect(ui->newGroup_OKButton, &QPushButton::clicked, this, &client::CreateNewGroup);

    // 当往群聊列表按钮被点击时，调用 ShowGroupList 函数
    connect(ui->gotoGroupListButton_2, &QPushButton::clicked, this, &client::ShowGroupList);
}

void client::setMessage()
{
    ui->message->setText("消息");
    connect(ui->message,&QPushButton::clicked,this,&client::LookNewMessage);
    connect(ui->message,&QPushButton::clicked,this,&client::LookNewGroupMessage);
    connect(ui->lookNewButton,&QPushButton::clicked,this,&client::LookNewMessage);
    connect(ui->lookNewgroupButton,&QPushButton::clicked,this,&client::LookNewGroupMessage);
}

void client::setUpLoad()
{
    // 上传按钮点击事件
    connect(ui->openFielPushButton, &QPushButton::clicked, this, [=]()
    {
        if (Uploadfile() == true)
            emit startUpload();
    });

    connect(this, &client::startUpload, this, [=]()
    {
        // 创建线程和对象
        QThread* t_1 = new QThread;
        movetothread* m_1 = new movetothread;

        // 把类的整个对象都放到线程
        m_1->moveToThread(t_1);

        // 上传文件信号连接
        connect(t_1, &QThread::started, this, [=]() {
            m_1->mUploadfile(this->filePath);
        });

        // 上传进度信号连接
        connect(m_1, &movetothread::m_showUploadFileLable, this, [=](long size, long Num) {
            if (Num >= size) {
                ui->label_file->setText(QString("上传完成"));
                QTimer::singleShot(3000, [=]() {
                    ui->label_file->clear();
                });
                // 上传完成后释放资源
                t_1->quit();
                t_1->wait();
                t_1->deleteLater();
                m_1->deleteLater();
            } else {
                int num = Num * 100 / size;
                ui->label_file->setText(QString("正在上传:%1%").arg(QString::number(num)));
                QCoreApplication::processEvents();
            }
        });

        // 开启线程上传文件
        t_1->start();
    });
}

void client::setdownLoad()
{
    ui->widget_down->hide();
    isdownListShow = 0;
    // 下载
    connect(ui->downloadPushButton,&QPushButton::clicked,this,[=]()
    {
        if(ui->label_downfile->text().isEmpty())
        {
            if(isdownListShow == 1)
            {
                isdownListShow = 0;
                ui->widget_down->hide();
                ui->listWidget_down->clear();
            }
            else
            {
                isdownListShow = 1;
                char c_Group[10] = {0};
                sprintf(c_Group, "%c", '?');
                if(tcpc.TcpClientSend( c_Group, strlen(c_Group) + 1) < 0)
                {
                    QMessageBox::critical(nullptr, "错误", "与服务器连接失败！");
                    return;
                }
                QThread::msleep(1);

                char c_temp[2048] = {0};
                tcpc.TcpclientRecv(c_temp, sizeof(c_temp));
                if(*c_temp == 'M')
                {
                    ui->label_down->setText("当前未上传任何文件！");
                }
                else if(*c_temp == '?')
                {
                    ui->label_down->setText("云端文件");
                    QString message(c_temp);
                    QStringList infoList = message.split(" ");
                    int num = infoList[1].toInt();
                    // 使用 QList 存储数据项
                    QStringList items;
                    for (int i = 0; i < num; i++) {
                        items.append(infoList[2 + i]);
                    }
                    // 一次性添加多个列表项
                    ui->listWidget_down->addItems(items);
                }
                ui->widget_down->show();
            }
        }
    });

    connect(ui->listWidget_down, &QListWidget::itemDoubleClicked, this, [=]()
    {
        QString fineName;
        QListWidgetItem* selectedItem = ui->listWidget_down->currentItem();
        if (selectedItem)
        {
            fineName = selectedItem->text();
        }
        else
        {
            return;
        }
        // 创建线程和对象
        QThread* t_1 = new QThread;
        movetothread* m_1 = new movetothread;
        // 把类的整个对象都放到线程
        m_1->moveToThread(t_1);
        m_1->setDownloadFileName(fineName);

        // 下载文件信号连接
        connect(t_1, &QThread::started, this, [=]() {
            m_1->mDownloadFile();
        });

        // 下载进度信号连接
        connect(m_1, &movetothread::m_showDownloadFileLable, this, [=](long size, long Num) {
            if (Num >= size) {
                ui->label_downfile->setText(QString("下载完成"));
                QTimer::singleShot(3000, [=]() {
                    ui->label_downfile->clear();
                });
                // 下载完成后释放资源
                t_1->quit();
                t_1->wait();
                t_1->deleteLater();
                m_1->deleteLater();
            } else {
                int num = Num * 100 / size;
                ui->label_downfile->setText(QString("正在下载:%1%").arg(QString::number(num)));
                QCoreApplication::processEvents();
            }
        });

        // 开启线程下载文件
        t_1->start();
    });
}

void client::CloseWindow()
{
    for (int i = 0; i < chatBoxList.count(); ++i)
    {
        ChatBox* box = chatBoxList.at(i);
        if (box != nullptr)
        {
            box->close();
            delete box;
        }
    }
    for (int i = 0; i < groupBoxList.count(); ++i)
    {
        GroupBox* box = groupBoxList.at(i);
        if (box != nullptr)
        {
            box->close();
            delete box;
        }
    }

    QApplication::activeWindow()->close();
}

void client::MiniWindow()
{
    showMinimized();
}

void client::startEditing()
{
    ui->label_sign->hide();
    ui->lineEdit_sign->setText(ui->label_sign->text());
    ui->lineEdit_sign->show();
    ui->lineEdit_sign->setFocus();

}

void client::stopEditing()
{
    QString text = ui->lineEdit_sign->text();
    ui->label_sign->setText(text);
    ui->label_sign->show();
    ui->lineEdit_sign->hide();
}

void client::ShowFriendList()
{
    ui->list_stackedWidget->setCurrentIndex(1);
    ui->friends_stackedWidget->setCurrentIndex(0);
    ui->friendsListWidget->clear();

    QString request = QString("F %1").arg(this->userName);
    if (tcpc.TcpClientSend(request.toUtf8().data(), request.length() + 1) < 0)
    {
        QMessageBox::critical(nullptr, "错误", "与服务器连接失败！");
        return;
    }
    QThread::msleep(1);

    char c_temp[1024] = {0};
    tcpc.TcpclientRecv(c_temp, sizeof(c_temp));
    QString response = QString::fromUtf8(c_temp);
    QStringList friendList = response.split(" ");
    int num = friendList[1].toInt();

    if (num == 0)
    {
        ui->have_Friends->setText("您当前没有好友哦，快去添加好友吧！");
        return;
    }
    else
    {
        ui->have_Friends->clear();
        for (int i = 0; i < num; i++)
        {
            QListWidgetItem* listItem = new QListWidgetItem(friendList[i + 2]);
            ui->friendsListWidget->addItem(listItem);
        }
    }

    ui->friendsListWidget->scrollToTop();
}

void client::ShowGroupList()
{
    ui->list_stackedWidget->setCurrentIndex(2);
    ui->groups_stackedWidget->setCurrentIndex(0);

    // 发送获取群聊列表的命令
    char c_Friends[50] = {0};
    sprintf(c_Friends, "%c %s", 'g', this->userName.toUtf8().constData());
    if(tcpc.TcpClientSend(c_Friends, strlen(c_Friends) + 1) < 0)
    {
        QMessageBox::critical(nullptr, "错误", "与服务器连接失败！");
        return;
    }
    QTreeWidgetItem* myGroupRoot = ui->groupTreeWidget->topLevelItem(0);  // 获取 "我创建的群聊" 节点
    myGroupRoot->takeChildren();
    QTreeWidgetItem* manageGroupRoot = ui->groupTreeWidget->topLevelItem(1);
    manageGroupRoot->takeChildren();
    QTreeWidgetItem* joinGroupRoot = ui->groupTreeWidget->topLevelItem(2);
    joinGroupRoot->takeChildren();
    QThread::msleep(3);
    char c_temp[1024] = {0};
    while(1)
    {
        memset(c_temp,0,1024);
        if(tcpc.TcpclientRecv(c_temp, sizeof(c_temp)) < 0)
        {
            qDebug()<<"未收到"<<endl;
            break;
        }
        if(*c_temp == 'g')
        {
            break;
        }
        else if(*c_temp == 'G')
        {
            // 解析服务端发来的消息
            QString message(c_temp);
            QStringList infoList = message.split(" ");

            QString messageType = infoList[0];
            int perms = infoList[1].toInt();
            int count = infoList[2].toInt();

            for (int i = 0; i < count; i++)
            {
                int gidIndex = 3 + i * 2;
                int gnameIndex = gidIndex + 1;

                int gid = infoList[gidIndex].toInt();
                QString gname = infoList[gnameIndex];

                // 创建新的群聊项
                QTreeWidgetItem* item = new QTreeWidgetItem();

                // 设置群聊名称和ID
                item->setText(0, QString("%1  (ID: %2)").arg(gname).arg(gid));

                // 根据权限级别添加到不同的列表
                switch (perms) {
                case 3:
                    myGroupRoot->addChild(item);  // 我创建的群聊
                    break;
                case 2:
                    manageGroupRoot->addChild(item);  // 我管理的群聊
                    break;
                case 1:
                    joinGroupRoot->addChild(item);  // 我加入的群聊
                    break;
                default:
                    break;
                }
            }
        }
    }

}

void client::showFriendContextMenu(const QPoint &pos)
{
    QListWidgetItem* selectedItem = ui->friendsListWidget->itemAt(pos);
    if (selectedItem)
    {
        QMenu contextMenu(this);

        QAction* viewStatusAction = new QAction("查看是否在线", this);
        QAction* sendMessageAction = new QAction("给好友发消息", this);
        QAction* deleteFriendAction = new QAction("删除好友", this);

        connect(viewStatusAction, &QAction::triggered,
                this, &client::viewFriendStatus);
        connect(sendMessageAction, &QAction::triggered,
                this, &client::sendFriendMessage);
        connect(deleteFriendAction, &QAction::triggered,
                this, &client::deleteFriend);

        contextMenu.addAction(viewStatusAction);
        contextMenu.addAction(sendMessageAction);
        contextMenu.addAction(deleteFriendAction);

        contextMenu.exec(ui->friendsListWidget->mapToGlobal(pos));
    }
}

void client::viewFriendStatus()
{
    QListWidgetItem* selectedItem = ui->friendsListWidget->currentItem();
    if (selectedItem)
    {
        QString friendName = selectedItem->text();
        // 执行查看好友在线状态的逻辑
        char str[100] = {0};
        sprintf(str, "%c %s %s", 'I', this->userName.toUtf8().constData(), friendName.toUtf8().constData());
        if(tcpc.TcpClientSend( str, strlen(str) + 1) < 0)
        {
            QMessageBox::critical(nullptr, "错误", "与服务器连接失败！");
            return;
        }
        QThread::msleep(1);

        char c_temp[10] = {0};
        tcpc.TcpclientRecv( c_temp, sizeof(c_temp));
        char flag1, flag2, flag3;
        sscanf(c_temp, "%c %c %c", &flag1, &flag2, &flag3);
        if (flag3 == 'Y')
            QMessageBox::information(nullptr, "提示", "该用户当前状态：在线！");
        else if (flag3 == 'N')
            QMessageBox::warning(nullptr, "提示", "查看用户失败，该用户不是您的好友！");
        else
            QMessageBox::critical(nullptr, "提示", "该用户当前状态：离线！");
        return;
    }
}

void client::sendFriendMessage()
{
    QListWidgetItem* selectedItem = ui->friendsListWidget->currentItem();
    if (selectedItem)
    {
        QString friendName = selectedItem->text();
        // 检查该好友的聊天框是否已经打开

        foreach (ChatBox* chatBox, chatBoxList)
        {
            if (chatBox->c_getToName() == friendName)
            {
                // 如果已经打开，则将聊天框关闭
                chatBox->close();
                chatBoxList.removeAll(chatBox);
                break;
            }
        }
        // 创建新的聊天框并添加到list中
        ChatBox *c_box = new ChatBox();
        chatBoxList.append(c_box);
        c_box->c_setFromName(this->userName);
        c_box->c_setToName(friendName);
        c_box->c_setMessageLable();
        c_box->c_historyMseeage();
        c_box->show();
    }
}

void client::deleteFriend()
{
    bool confirm = QMessageBox::question(nullptr, "提示", "您确定要执行该操作吗？", QMessageBox::Ok | QMessageBox::Cancel) == QMessageBox::Ok;
    if(confirm)
    {
        QListWidgetItem* selectedItem = ui->friendsListWidget->currentItem();
        if (selectedItem)
        {
            QString friendName = selectedItem->text();
            // 执行删除好友的逻辑
            char str[100] = {0};
            sprintf(str, "%c %s %s", '-', this->userName.toUtf8().constData(), friendName.toUtf8().constData());
            if(tcpc.TcpClientSend( str, strlen(str) + 1) < 0)
            {
                QMessageBox::critical(nullptr, "错误", "与服务器连接失败！");
                return;
            }
            QThread::msleep(1);

            char c_temp[10] = {0};
            tcpc.TcpclientRecv( c_temp, sizeof(c_temp));
            char flag1, flag2, flag3;
            sscanf(c_temp, "%c %c %c", &flag1, &flag2, &flag3);
            if (flag3 == 'N')
                QMessageBox::warning(nullptr, "提示", "删除好友失败，该用户当前已经不是您的好友！");
            else
                QMessageBox::information(nullptr, "提示", "已删除该用户！");
            ShowFriendList();
        }
    }
    else
    {
        return;
    }
}

void client::addFriend()
{
    auto c_Friends = ui->addFriendalineEdit->text();
    if(c_Friends.isEmpty())
    {
        QMessageBox::warning(nullptr, "警告", "对方昵称不能为空！");
        return;
    }
    else if(c_Friends == this->userName )
    {
        QMessageBox::warning(nullptr, "警告", "不能添加自己为好友！！！");
        return;
    }
    else
    {
        char str[100] = {0};
        sprintf(str, "%c %s %s", 'n', this->userName.toUtf8().constData(), c_Friends.toUtf8().constData());
        if(tcpc.TcpClientSend( str, strlen(str) + 1) < 0)
        {
            QMessageBox::critical(nullptr, "错误", "与服务器连接失败！");
            return;
        }
    }
    QThread::msleep(1);

    char c_temp[30] = {0};
    tcpc.TcpclientRecv( c_temp, sizeof(c_temp));
    char flag1, flag2, flag3;
    sscanf(c_temp, "%c %c %c", &flag1, &flag2, &flag3);
    if (flag2 == 'N')
    {
        ui->addFriendalineEdit->clear();
        QMessageBox::information(nullptr, "提示", "添加成功！");
    }
    else if (flag3 == 'n')
        QMessageBox::warning(nullptr, "提示", "添加失败，该用户当前已是您的好友！");
    else if (flag3 == 'N')
        QMessageBox::warning(nullptr, "提示", "添加失败，未找到该用户！");
}

void client::addGroup()
{
    auto c_Group = ui->addGrouplineEdit->text();
    if(c_Group.isEmpty())
    {
        QMessageBox::warning(nullptr, "警告", "群聊ID不能为空！");
        return;
    }
    else
    {
        char str[100] = {0};
        sprintf(str, "%c %d %s", 'M',c_Group.toInt(),  this->userName.toUtf8().constData());
        if(tcpc.TcpClientSend( str, strlen(str) + 1) < 0)
        {
            QMessageBox::critical(nullptr, "错误", "与服务器连接失败！");
            return;
        }
    }
    QThread::msleep(1);

    char c_temp[50] = {0};
    tcpc.TcpclientRecv( c_temp, sizeof(c_temp));
    if(*c_temp == 'M')
    {
        char flag1, flag2, flag3;
        sscanf(c_temp, "%c %c %c", &flag1, &flag2, &flag3);
        if (flag3 == 'n')
            QMessageBox::warning(nullptr, "提示", "添加失败，您当前已经在群聊内！");
        else if (flag3 == 'N')
            QMessageBox::warning(nullptr, "提示", "添加失败，未找到该群聊！");
    }
    else if(*c_temp == 'O')
    {
        ui->addGrouplineEdit->clear();
        QMessageBox::information(nullptr, "提示", "添加成功！");
        QString message(c_temp);
        QStringList messageParts = message.split(' ');
        QString GroupName = messageParts[2];
        ui->addNewGrouplabel->setText("您已成功加入群聊[ "+GroupName +" ]!");
        QTimer::singleShot(3000, [=]() {
            ui->addNewGrouplabel->clear();
        });
    }
}

void client::CreateNewGroup()
{
    auto c_Group = ui->newGrouplineEdit->text();
    if(c_Group.isEmpty())
    {
        QMessageBox::warning(nullptr, "警告", "群聊昵称不能为空！");
        return;
    }
    else
    {
        char str[100] = {0};
        sprintf(str, "%c %s %s", 'K',c_Group.toUtf8().constData(),  this->userName.toUtf8().constData());
        if(tcpc.TcpClientSend( str, strlen(str) + 1) < 0)
        {
            QMessageBox::critical(nullptr, "错误", "与服务器连接失败！");
            return;
        }
    }
    QThread::msleep(1);

    char c_temp[2048] = {0};
    memset(c_temp,0,2048);
    if(tcpc.TcpclientRecv(c_temp, sizeof(c_temp)) < 0)
    {
        qDebug()<<"未收到"<<endl;
    }
    if(*c_temp == 'M')
    {
        QMessageBox::warning(nullptr, "提示", "创建失败，改名称已被使用！");
    }
    else if(*c_temp == 'I')
    {
        ui->newGrouplineEdit->clear();
        QMessageBox::information(nullptr, "提示", "创建成功！");
        QString message(c_temp);
        QStringList messageParts = message.split(' ');
        QString GroupName = messageParts[3];
        ui->newNewGrouplabel->setText("您已成功创建群聊[ "+GroupName +" ]!");
        QTimer::singleShot(3000, [=]() {
            ui->newNewGrouplabel->clear();
        });
    }
}

void client::leaveJoinedGroup()
{
    bool confirm = QMessageBox::question(nullptr, "提示", "您确定要执行该操作吗？", QMessageBox::Ok | QMessageBox::Cancel) == QMessageBox::Ok;
    if (confirm)
    {
        // 点击确定按钮继续执行
        QTreeWidgetItem* selectedItem = ui->groupTreeWidget->currentItem();
        if (selectedItem)
        {
            QString temp = selectedItem->text(0);
            QStringList nameAndId = temp.split(" (ID: ");
            int groupId = nameAndId.last().trimmed().chopped(1).toInt(); // 提取最后一个元素并去除首尾的括号和空格
            char str[100] = {0};
            sprintf(str, "%c %s %d", 'V', this->userName.toUtf8().constData(), groupId);
            if(tcpc.TcpClientSend( str, strlen(str) + 1) < 0)
            {
                QMessageBox::critical(nullptr, "错误", "与服务器连接失败！");
                return;
            }
            QThread::msleep(1);

            char c_temp[1024] = {0};
            tcpc.TcpclientRecv( c_temp, sizeof(c_temp));
            if (*c_temp == 'O')
            {
                QMessageBox::information(nullptr, "提示", "已退出该群聊！");
                ShowGroupList();
            }
            else if(*c_temp == 'M')
            {
                QMessageBox::warning(nullptr, "提示", "该群聊已被群主解散！");
                ShowGroupList();
            }
        }
    }
    else
    {
        return;
    }
}

void client::dismissCreatedGroup()
{
    // 在某个函数中弹出一个确认提示框
    bool confirm = QMessageBox::question(nullptr, "提示", "您确定要执行该操作吗？", QMessageBox::Ok | QMessageBox::Cancel) == QMessageBox::Ok;
    if (confirm)
    {
        // 点击确定按钮继续执行
        QTreeWidgetItem* selectedItem = ui->groupTreeWidget->currentItem();
        if (selectedItem)
        {
            QString temp = selectedItem->text(0);
            QStringList nameAndId = temp.split(" (ID: ");
            int groupId = nameAndId.last().trimmed().chopped(1).toInt(); // 提取最后一个元素并去除首尾的括号和空格
            char str[100] = {0};
            sprintf(str, "%c %d %s", 'J', groupId, this->userName.toUtf8().constData());
            if(tcpc.TcpClientSend( str, strlen(str) + 1) < 0)
            {
                QMessageBox::critical(nullptr, "错误", "与服务器连接失败！");
                return;
            }
            QThread::msleep(1);

            char c_temp[1024] = {0};
            tcpc.TcpclientRecv( c_temp, sizeof(c_temp));
            if (*c_temp == 'I')
            {
                QMessageBox::information(nullptr, "提示", "已解散该群聊！");
                ShowGroupList();
            }
        }
    }
    else
    {
        return;
    }
}

void client::sendMessageToCreatedGroup()
{
    QTreeWidgetItem* selectedItem = ui->groupTreeWidget->currentItem();;
    if (selectedItem)
    {
        if (selectedItem->parent() == nullptr)
        {
            return;
        }
        QString temp = selectedItem->text(0);
        QStringList nameAndId = temp.split("  (ID: ");
        QString groupName = nameAndId[0]; // 第一个部分为群聊名称
        int groupId = nameAndId[1].remove(")").toInt(); // 第二个部分为群聊ID，需要去除末尾的 ")"
        foreach (GroupBox* groupBox, groupBoxList)
        {
            if (groupBox->g_getGroupID() == groupId)
            {
                // 如果已经打开，则将聊天框关闭
                groupBox->close();
                groupBoxList.removeAll(groupBox);
                break;
            }
        }
        GroupBox* groupBox = new GroupBox();
        groupBoxList.append(groupBox);
        // 获取根节点的文本内容
        QString rootText = selectedItem->parent()->text(0);
        // 根据根节点的不同，添加不同的操作
        if (rootText == "我创建的群聊")
        {
            groupBox->g_setMyGroupFalg(3);
        }
        else if (rootText == "我管理的群聊")
        {
            groupBox->g_setMyGroupFalg(2);
        }
        else if (rootText == "我加入的群聊")
        {
            groupBox->g_setMyGroupFalg(1);
        }
        groupBox->g_setGroupID(groupId);
        groupBox->g_setGroupName(groupName);
        groupBox->g_setFromName(this->userName);
        groupBox->g_setGroupMessageLable();
        groupBox->g_lookGroupMember();
        groupBox->g_historyMseeage();
        groupBox->show();
    }
}

void client::LookNewMessage()
{
    ui->list_stackedWidget->setCurrentIndex(0);

    char c_Friends[50] = {0};
    sprintf(c_Friends, "%c %s", 'X', this->userName.toUtf8().constData());
    if(tcpc.TcpClientSend( c_Friends, strlen(c_Friends) + 1) < 0)
    {
        QMessageBox::critical(nullptr, "错误", "与服务器连接失败！");
        return;
    }
    QThread::msleep(1);

    ui->newMessage_listWidget->clear();
    char c_temp[2048] = {0};
    while(1)
    {
        memset(c_temp,0,2048);
        if(tcpc.TcpclientRecv(c_temp, sizeof(c_temp)) < 0)
        {
            qDebug()<<"未收到"<<endl;
            break;
        }
        if(*c_temp == 'M')
        {
            QString message(c_temp);
            QStringList messageParts = message.split(' ');
            QString num = messageParts[1];
            ui->friend_label->setText("好友消息列表  (当前有"+num+"条未读消息)");
            return;
        }
        else if(*c_temp == 'X')
        {
            // 解析服务端发来的消息
            QString message(c_temp);
            QStringList messageParts = message.split(' ');
            if (messageParts.size() < 3) {
                // 错误处理，数组长度不足
                continue; // 跳过当前循环，继续下一次循环
            }
            QString fromName = messageParts[1];
            QString messageText = messageParts.mid(3).join(' ');
            QListWidgetItem *item = new QListWidgetItem;
            item->setText("「 "+fromName+"」给你发来：  "+messageText);
            ui->newMessage_listWidget->addItem(item);
        }
    }
}

void client::LookNewGroupMessage()
{
    char str[50] = {0};
    sprintf(str, "%c %s", 'Z', this->userName.toUtf8().constData());
    if(tcpc.TcpClientSend( str, strlen(str) + 1) < 0)
    {
        QMessageBox::critical(nullptr, "错误", "与服务器连接失败！");
        return;
    }
    QThread::msleep(1);

    ui->newgroupMessage_listWidget->clear();
    char c_temp[2048] = {0};
    while(1)
    {
        memset(c_temp,0,2048);
        if(tcpc.TcpclientRecv(c_temp, sizeof(c_temp)) < 0)
        {
            qDebug()<<"未收到"<<endl;
            break;
        }
        if(*c_temp == 'm')
        {
            QString message(c_temp);
            QStringList messageParts = message.split(' ');
            QString num = messageParts[1];
            ui->group_label->setText("群聊消息列表  (当前有"+num+"条未读消息)");
            return;
        }
        else if(*c_temp == 'Z')
        {
            // 解析服务端发来的消息
            char temp[2048] = {0};
            strcat(temp, c_temp);
            char *flag = strtok(temp, " ");
            char *content = strtok(NULL, "\0");
            QString message(content);

            QListWidgetItem *item = new QListWidgetItem();
            item->setText(message);
            ui->newgroupMessage_listWidget->addItem(item);
        }
    }
}

void client::showGroupContextMenu(const QPoint &pos)
{
    QTreeWidgetItem* item = ui->groupTreeWidget->itemAt(pos);
    if (item )
    {
        if (item->parent() == nullptr)
        {
            QMenu menu(this);
            QAction* action1 = menu.addAction("添加群聊");
            QAction* action2 = menu.addAction("创建群聊");
            // 连接相应的槽函数进行处理
            connect(action1, &QAction::triggered, ui->groups_stackedWidget, [=]()
            {
                ui->addNewGrouplabel->clear();
                ui->groups_stackedWidget->setCurrentIndex(1);
            });
            connect(action2, &QAction::triggered, ui->groups_stackedWidget, [=]()
            {
                ui->newNewGrouplabel->clear();
                ui->groups_stackedWidget->setCurrentIndex(2);
            });
            // 显示菜单
            menu.exec(ui->groupTreeWidget->mapToGlobal(pos));
        }
        else
        {
            // 获取根节点的文本内容
            QString rootText = item->parent()->text(0);
            // 创建自定义菜单
            QMenu menu(this);

            // 根据根节点的不同，添加不同的操作
            if (rootText == "我创建的群聊")
            {
                QAction* action1 = menu.addAction("给群聊发消息");
                QAction* action2 = menu.addAction("解散群聊");
                // 连接相应的槽函数进行处理
                connect(action1, &QAction::triggered, this, &client::sendMessageToCreatedGroup);
                connect(action2, &QAction::triggered, this, &client::dismissCreatedGroup);
            }
            else
            {
                QAction* action1 = menu.addAction("给群聊发消息");
                QAction* action2 = menu.addAction("退出群聊");
                // 连接相应的槽函数进行处理
                connect(action1, &QAction::triggered, this, &client::sendMessageToCreatedGroup);
                connect(action2, &QAction::triggered, this, &client::leaveJoinedGroup);
            }
            // 显示菜单
            menu.exec(ui->groupTreeWidget->mapToGlobal(pos));
        }
    }
}

void client::InitQss(const QString &qsspath)
{
    QFile file(qsspath);
    if(file.open(QIODevice::ReadOnly) == false)
    {
        qDebug()<<"open qss file fail!\n";
        return;
    }
    QString QssContent = file.readAll();
    //qApp 全局应用指针
    // setStyleSheet 设置全局样式表
    qApp->setStyleSheet(QssContent);
}

bool client::Uploadfile()
{
    filePath.clear();
    // 打开文件选择对话框，并获取选择的文件路径
    filePath = QFileDialog::getOpenFileName(nullptr, "选择文件", "", "所有文件 (*.*)");
    if (filePath.isEmpty())
    {
        return false;
    }

//    QFileInfo fileInfo(filePath);
//    QString fileName = fileInfo.fileName();
//    char str[100] = {0};
//    sprintf(str,"%c %s",'<',fileName.toUtf8().constData());
//    tcpc.TcpClientSend(str, strlen(str) + 1);
//    QThread::msleep(20);
//    char c_temp[10] = {0};
//    tcpc.TcpclientRecv(c_temp, sizeof(c_temp));
//    if(*c_temp == '1')
//    {
//        char flag1, flag2, flag3;
//        sscanf(c_temp, "%c %c %c", &flag1, &flag2, &flag3);
//        qDebug()<<c_temp<<endl;
//        if(flag3 == 'N')
//        {
//            QMessageBox::warning(this, "警告", "文件上传失败，当前已有重名文件！");
//            filePath.clear();
//            return false;
//        }
//    }

    return true;
}

bool client::eventFilter(QObject *object, QEvent *event)
{
    if (event->type() == QEvent::MouseButtonPress && object == ui->label_sign)
    {
        startEditing();
        return true;
    }

    return QObject::eventFilter(object, event);
}

void client::downLoad()
{

}

void client::showMusic()
{
    if(isMusicWidgetOpen == 0)
    {
        isMusicWidgetOpen = 1;
        wm =new Widgetmusic();
        wm->SetUserName(this->userName);
        wm->SetUserLabel();
        wm->show();
    }
    else
    {
        wm->show();
    }
}

void client::mousePressEvent(QMouseEvent *event)
{
    // 记录鼠标点击的位置
    if (event->button() == Qt::LeftButton )
    {
        m_dragStartPosition = event->globalPos() - frameGeometry().topLeft();
    }
}

void client::mouseMoveEvent(QMouseEvent *event)
{
    // 实现窗口的拖动效果
    if (event->buttons() & Qt::LeftButton &&
            !ui->closeButton->underMouse() &&
            !ui->smallButton->underMouse() &&
            !ui->colorButton->underMouse())
    {
        // 判断当前鼠标位置是否在指定范围内
        if (event->pos().x() >= 0 && event->pos().x() <= 450 &&
                event->pos().y() >= 0 && event->pos().y() <= 120)
        {
            move(event->globalPos() - m_dragStartPosition);
        }
    }
}

void client::mouseReleaseEvent(QMouseEvent *event)
{
    // 清空鼠标点击的位置
    if (event->button() == Qt::LeftButton)
    {
        m_dragStartPosition = QPoint();
    }
}

client::~client()
{
    delete ui;
}

