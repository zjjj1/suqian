#ifndef CLIENT_H
#define CLIENT_H

#include <QWidget>
#include <QMouseEvent>
#include <QPushButton>
#include "SysTheamHard.h"
#include "chatbox.h"
#include "groupbox.h"
#include "widgetmusic.h"
#include "movetothread.h"

QT_BEGIN_NAMESPACE
namespace Ui { class client; }
QT_END_NAMESPACE

class client : public QWidget
{
    Q_OBJECT


private slots:
    // 关闭窗口
    void CloseWindow();

    // 最小化窗口
    void MiniWindow();

    // 设置个性签名
    void startEditing();

    // 显示个性签名
    void stopEditing();

    // 显示好友列表页面
    void ShowFriendList();

    // 显示群聊列表页面
    void ShowGroupList();

    // 显示好友列表页面的上下文菜单
    void showFriendContextMenu(const QPoint &pos);

    // 查看好友状态（在线/离线）
    void viewFriendStatus();

    // 向好友发送消息
    void sendFriendMessage();

    // 删除好友
    void deleteFriend();

    // 添加好友
    void addFriend();

    // 添加群聊
    void addGroup();

    // 创建新群聊
    void CreateNewGroup();

    // 退出已加入的群聊
    void leaveJoinedGroup();

    // 解散已创建的群聊
    void dismissCreatedGroup();

    // 向已创建的群聊发送消息
    void sendMessageToCreatedGroup();

    // 查看新消息
    void LookNewMessage();

    // 查看新群聊消息
    void LookNewGroupMessage();

    // 显示群聊列表页面的上下文菜单
    void showGroupContextMenu(const QPoint &pos);

    // 初始化QSS（Qt样式表）
    void InitQss(const QString &qsspath);

    // 下载
    void downLoad();

    // 打开音乐界面
    void showMusic();



public:
    // 构造函数
    client(QWidget *parent = nullptr);
    // 析构函数
    ~client();
    // 鼠标按下事件
    void mousePressEvent(QMouseEvent *event) override;
    // 鼠标移动事件
    void mouseMoveEvent(QMouseEvent *event) override;
    // 鼠标释放事件
    void mouseReleaseEvent(QMouseEvent *event) override;
    // 上传是否有重名
    bool Uploadfile();

    void setWindow();
    void setButton();
    void setEdit();
    void setQMap();
    void LoginSignup();
    void setFriend();
    void setGroup();
    void setMessage();
    void setUpLoad();
    void setdownLoad();

protected:
    bool eventFilter(QObject* object, QEvent* event) override;

signals:
    void startUpload();

private:
    Ui::client *ui;                 // UI界面对象指针
    QPoint m_dragStartPosition;     // 鼠标拖拽起始位置
    QString userName;               // 用户名
    QList<ChatBox*> chatBoxList;    // 聊天框对象列表
    QList<GroupBox*> groupBoxList;  // 群聊框对象列表
    QString filePath;               // 上传文件路径
    Widgetmusic *wm;
    int isMusicWidgetOpen;
    int isdownListShow;

};
#endif // CLIENT_H
