#ifndef CHATBOX_H
#define CHATBOX_H

#include <QWidget>
#include <QMouseEvent>
#include "SysTheamHard.h"
#include <QTimer>


namespace Ui {
class ChatBox;
}

class ChatBox : public QWidget
{
    Q_OBJECT



public:
    explicit ChatBox(QWidget *parent = nullptr);
    ~ChatBox();
    void mousePressEvent(QMouseEvent *event)override;
    void mouseMoveEvent(QMouseEvent *event)override;
    void mouseReleaseEvent(QMouseEvent *event)override;
    void c_setFromName(QString fromName);
    void c_setToName(QString toName);
    void c_setMessageLable();
    void c_historyMseeage();
    QString c_getToName();



private slots:
    void c_CloseWindow();
    void c_MiniWindow();
    void c_sendMessage();

private:
    Ui::ChatBox *ui;
    QPoint m_dragStartPosition;
    QString fromName;
    QString toName;
    QTimer timer;

    std::map<QString,QString> AllMessageList;

};

#endif // CHATBOX_H
