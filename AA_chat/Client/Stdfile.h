#ifndef __STDFILE_H_
#define __STDFILE_H_
#include "DoubleLinklist.h"
#include <stddef.h>


//文件是否存在
int IsFileExist(const char *FilePath);
//读取整个文件
char *LoadFormFile(const char *FilePath);
//一行一行读取整个文件
DLlist *GetLineFormFile(const char *FilePath);
//在文件里写，覆盖保存
void WriteToFile(const char *filePath, void *ptr, size_t size);
//在文件里一行一行写，覆盖保存
void WriteLineToFile(const char *filePath, DLlist *list);
//复制文件
void CopyFile(const char *SourcePath,const char *targetPath);
//在文件里追加写
void AppendToFile(const char *FilePath, void *ptr, size_t size);

#endif