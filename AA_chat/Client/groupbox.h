﻿#ifndef GROUPBOX_H
#define GROUPBOX_H

#include <QWidget>
#include <QMouseEvent>
#include "SysTheamHard.h"
#include <QTimer>

namespace Ui {
class GroupBox;
}

class GroupBox : public QWidget
{
    Q_OBJECT

public:
    explicit GroupBox(QWidget *parent = nullptr);
    ~GroupBox();
    void mousePressEvent(QMouseEvent *event)override;
    void mouseMoveEvent(QMouseEvent *event)override;
    void mouseReleaseEvent(QMouseEvent *event)override;
    void g_setFromName(QString fromName);
    void g_setGroupName(QString groupName);
    void g_setGroupID(int ID);
    void g_setMyGroupFalg(int Falg);
    void g_setGroupMessageLable();
    void g_lookGroupMember();
    void g_historyMseeage();
    QString g_getGroupName();
    int g_getGroupID();



private slots:
    void g_CloseWindow();
    void g_MiniWindow();
    void g_sendMessage();
    void g_showGroupfriendsContextMenu(const QPoint &pos);


private:
    Ui::GroupBox *ui;
    QPoint m_dragStartPosition;
    QTimer timer;  // 刷新聊天记录
    QTimer timer1;  // 刷新群成员
    QString fromName;
    QString groupName;
    int groupID;
    int myGroupFalg;
    std::map<QString,QString> AllMessageList;

};

#endif // GROUPBOX_H
