#ifndef MOVETOTHREAD_H
#define MOVETOTHREAD_H

#include <QObject>
#include <QThread>
#include <QDebug>
#include <QMessageBox>
#include <QApplication>
#include <QFileDialog>
#include "SysTheamHard.h"

class movetothread : public QObject
{
    Q_OBJECT
public:
    explicit movetothread(QObject *parent = nullptr);
    void mUploadfile(QString filePath);
    void setDownloadFileName(QString FileName);
    void mDownloadFile();

signals:
    // 显示进度
    void m_showUploadFileLable(long size,long Num);
    void m_showDownloadFileLable(long size,long Num);

private:
    QString FileName;    // 文件下载路径
};

#endif // MOVETOTHREAD_H
