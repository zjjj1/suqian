#include "groupbox.h"
#include "ui_groupbox.h"
#include <QMessageBox>
#include <QMenu>
#include <QThread>
#include <QtDebug>
#include <fcntl.h>

extern TcpClient tcpc;

GroupBox::GroupBox(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::GroupBox)
{
    ui->setupUi(this);

    setWindowIcon(QIcon(":/image/message.png"));
    setFixedSize(730,480);
    this->move(550,200);

    // 隐藏状态栏（标题栏）
    setWindowFlag(Qt::FramelessWindowHint);

    timer.start(1500);
    timer1.start(3000);

    ui->Close_Button->setIcon(QIcon(":/image/close.png"));
    connect(ui->Close_Button, &QPushButton::clicked, this, &GroupBox::g_CloseWindow);
    ui->Close_Button->setIconSize(ui->Close_Button->size());
    connect(ui->close_Button, &QPushButton::clicked, this, &GroupBox::g_CloseWindow);
    ui->mini_Button->setIcon(QIcon(":/image/small.png"));
    connect(ui->mini_Button, &QPushButton::clicked, this, &GroupBox::g_MiniWindow);
    ui->mini_Button->setIconSize(ui->mini_Button->size());

    QPixmap memberGroupMap(":/image/member.png");
    ui->memberLabel->setPixmap(memberGroupMap.scaled(ui->memberLabel->size(),Qt::KeepAspectRatio));

    // 创建三个根节点
    QTreeWidgetItem* flag3 = new QTreeWidgetItem(ui->groupsMemberWidget);
    flag3->setText(0, "群主");
    QTreeWidgetItem* flag2 = new QTreeWidgetItem(ui->groupsMemberWidget);
    flag2->setText(0, "管理");
    QTreeWidgetItem* flag1 = new QTreeWidgetItem(ui->groupsMemberWidget);
    flag1->setText(0, "成员");
    // 将根节点添加到 groupsMemberWidget 中
    ui->groupsMemberWidget->addTopLevelItem(flag3);
    ui->groupsMemberWidget->addTopLevelItem(flag2);
    ui->groupsMemberWidget->addTopLevelItem(flag1);
    // 设置qtreewidget为展开状态
    ui->groupsMemberWidget->expandAll();

    // 发送消息
    connect(ui->sendMessage_Button, &QPushButton::clicked, [this](){
        emit g_sendMessage();
    });

    // 刷新消息
    connect(&timer,&QTimer::timeout,[this]()
    {
        g_historyMseeage();
    });

    // 刷新群成员
    connect(&timer1,&QTimer::timeout,[this]()
    {
        g_lookGroupMember();
    });

    ui->groupsMemberWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->groupsMemberWidget, &QTreeWidget::customContextMenuRequested, this, &GroupBox::g_showGroupfriendsContextMenu);
}

GroupBox::~GroupBox()
{
    delete ui;
}

void GroupBox::mousePressEvent(QMouseEvent *event)
{
    // 记录鼠标点击的位置
    if (event->button() == Qt::LeftButton )
    {
        m_dragStartPosition = event->globalPos() - frameGeometry().topLeft();
    }
}

void GroupBox::mouseMoveEvent(QMouseEvent *event)
{
    // 实现窗口的拖动效果
    if (event->buttons() & Qt::LeftButton &&
            !ui->Close_Button->underMouse() &&
            !ui->mini_Button->underMouse())
    {
        // 判断当前鼠标位置是否在指定范围内
        if (event->pos().x() >= 0 && event->pos().x() <= 730 &&
                event->pos().y() >= 0 && event->pos().y() <= 60)
        {
            move(event->globalPos() - m_dragStartPosition);
        }
    }
}

void GroupBox::mouseReleaseEvent(QMouseEvent *event)
{
    // 清空鼠标点击的位置
    if (event->button() == Qt::LeftButton)
    {
        m_dragStartPosition = QPoint();
    }
}

void GroupBox::g_setFromName(QString fromName)
{
    this->fromName = fromName;
}

void GroupBox::g_setGroupName(QString groupName)
{
    this->groupName = groupName;
}

void GroupBox::g_setGroupID(int ID)
{
    this->groupID = ID;
}

void GroupBox::g_setMyGroupFalg(int Falg)
{
    this->myGroupFalg = Falg;
}

void GroupBox::g_setGroupMessageLable()
{
    ui->Message_label->setText(this->groupName);
}

QString GroupBox::g_getGroupName()
{
    return this->groupName;
}

int GroupBox::g_getGroupID()
{
    return this->groupID;
}

void GroupBox::g_CloseWindow()
{
    timer.stop();
    timer1.stop();
    close();
}

void GroupBox::g_MiniWindow()
{
    showMinimized();
}

void GroupBox::g_sendMessage()
{
    QString message = ui->SendMessage_Edit->toPlainText();
    if(message.isEmpty())
    {
        QMessageBox::warning(nullptr, "警告", "不能发送空消息！");
        return;
    }
    char str[2048] = {0};
    sprintf(str, "%c %d %s %s", 'U', this->groupID, this->fromName.toUtf8().constData(), message.toUtf8().constData());
    if(tcpc.TcpClientSend(str, strlen(str) + 1) < 0)
    {
        QMessageBox::critical(nullptr, "错误", "与服务器连接失败！");
        return;
    }
    QThread::msleep(10);

    char c_temp[10] = {0};
    tcpc.TcpclientRecv(c_temp, sizeof(c_temp));
    char flag1, flag2, flag3;
    sscanf(c_temp, "%c %c %c", &flag1, &flag2, &flag3);
    if(*c_temp == 'M')
    {
        if (flag2 == '/')
        {
            QMessageBox::warning(nullptr, "提示", "消息发送失败，该群聊当前已经被群主解散！");
            QApplication::activeWindow()->close();
        }
        else
        {
            if (flag3 == 'Y')
            {
                ui->SendMessage_Edit->clear();
                g_historyMseeage();
            }
            else
                QMessageBox::warning(nullptr, "提示", "消息发送失败，您当前已经被禁言！");
        }
    }
}

void GroupBox::g_showGroupfriendsContextMenu(const QPoint &pos)
{
    QTreeWidgetItem* item = ui->groupsMemberWidget->itemAt(pos);
    if (item)
    {
        if(item->text(0) != this->fromName)
        {
            if (item->parent() != nullptr)
            {
                // 获取根节点的文本内容
                QString rootText = item->parent()->text(0);
                // 创建自定义菜单
                QMenu menu(this);

                // 根据根节点的不同，添加不同的操作
                if (rootText == "管理")
                {
                    QAction* action1 = menu.addAction("设为群成员");
                    QAction* action2 = menu.addAction("设置禁言/解除禁言");
                    QAction* action3 = menu.addAction("踢出群聊");
                    QAction* action4 = menu.addAction("转让群主");
                    // 连接相应的槽函数进行处理
//                    connect(action1, &QAction::triggered, this, &GroupBox::sendMessageToCreatedGroup);
//                    connect(action2, &QAction::triggered, this, &GroupBox::dismissCreatedGroup);
                }
                else if (rootText == "成员")
                {
                    QAction* action1 = menu.addAction("设为群管理");
                    QAction* action2 = menu.addAction("设置禁言/解除禁言");
                    QAction* action3 = menu.addAction("踢出群聊");
                    QAction* action4 = menu.addAction("转让群主");
                    // 连接相应的槽函数进行处理
//                    connect(action1, &QAction::triggered, this, &GroupBox::sendMessageToCreatedGroup);
//                    connect(action2, &QAction::triggered, this, &GroupBox::leaveJoinedGroup);
                }
                // 显示菜单
                menu.exec(ui->groupsMemberWidget->mapToGlobal(pos));
            }
        }
    }
}

void GroupBox::g_lookGroupMember()
{
    QTreeWidgetItem* flag3 = ui->groupsMemberWidget->topLevelItem(0);
    flag3->takeChildren();
    QTreeWidgetItem* flag2 = ui->groupsMemberWidget->topLevelItem(1);
    flag2->takeChildren();
    QTreeWidgetItem* flag1 = ui->groupsMemberWidget->topLevelItem(2);
    flag1->takeChildren();

    char c_Group[50] = {0};
    sprintf(c_Group, "%c %d %s", 'S', this->groupID,this->fromName.toUtf8().constData());
    if(tcpc.TcpClientSend(c_Group, strlen(c_Group) + 1) < 0)
    {
        QMessageBox::critical(nullptr, "错误", "与服务器连接失败！");
        return;
    }

    QThread::msleep(2);

    char c_temp[1024] = {0};
    while(1)
    {
        memset(c_temp,0,1024);
        if(tcpc.TcpclientRecv(c_temp, sizeof(c_temp)) < 0)
        {
            qDebug()<<"未收到"<<endl;
            break;
        }
        if(*c_temp == 'M')
        {
            QMessageBox::warning(nullptr, "提示", "该群聊当前已经被群主解散！");
            QApplication::activeWindow()->close();
            return;
        }
        else if(*c_temp == 'm')
        {
            QMessageBox::warning(nullptr, "提示", "您当前已经不在群内！");
            QApplication::activeWindow()->close();
            return;
        }
        if(*c_temp == 's')
        {
            break;
        }
        else if(*c_temp == 'S')
        {
            // 解析服务端发来的消息
            QString message(c_temp);
            QStringList infoList = message.split(" ");

            QString messageType = infoList[0];
            int perms = infoList[1].toInt();
            int count = infoList[2].toInt();

            for (int i = 0; i < count; i++)
            {
                int gidIndex = 3 + i;
                QString gname = infoList[gidIndex];

                // 创建新的群聊项
                QTreeWidgetItem* item = new QTreeWidgetItem();

                // 设置群聊名称和ID
                item->setText(0, gname);

                // 根据权限级别添加到不同的列表
                switch (perms) {
                case 3:
                    flag3->addChild(item);
                    break;
                case 2:
                    flag2->addChild(item);
                    break;
                case 1:
                    flag1->addChild(item);
                    break;
                default:
                    break;
                }
            }
        }
    }
}

void GroupBox::g_historyMseeage()
{
    char c_Group[10] = {0};
    sprintf(c_Group, "%c %d %s", 'H', this->groupID,this->fromName.toUtf8().constData());
    if(tcpc.TcpClientSend(c_Group, strlen(c_Group) + 1) < 0)
    {
        QMessageBox::critical(nullptr, "错误", "与服务器连接失败！");
        return;
    }
    ui->allMessage_listWidget->clear();
    QThread::msleep(1);

    char c_temp[2048] = {0};
    while(1)
    {
        memset(c_temp,0,2048);
        if(tcpc.TcpclientRecv(c_temp, sizeof(c_temp)) < 0)
        {
            qDebug()<<"未收到"<<endl;
            break;
        }
        // qDebug()<<c_temp<<endl;
        if(*c_temp == 'M')
        {
            QMessageBox::warning(nullptr, "提示", "该群聊当前已经被群主解散！");
            QApplication::activeWindow()->close();
            return;
        }
        else if(*c_temp == 'm')
        {
            QMessageBox::warning(nullptr, "提示", "您当前已经不在群内！");
            QApplication::activeWindow()->close();
            return;
        }
        else if(*c_temp == 'h')
        {
            if(AllMessageList.size() == 0)
            {
                QListWidgetItem *item = new QListWidgetItem;
                item->setText(QString("未查询到历史聊天"));
                ui->allMessage_listWidget->insertItem(0,item);
            }
            return;
        }
        else if(*c_temp == 'H')
        {
            // 解析服务端发来的消息
            QString message(c_temp);
            QStringList messageParts = message.split(' ');
            QString messageType = messageParts[0];
            QString fromName = messageParts[1];
            QString messageText = messageParts.mid(2).join(' ');
            QListWidgetItem *item = new QListWidgetItem;
            if( fromName == this->fromName)
            {
                item->setText("「 "+messageText+" 」<——"+fromName);
                item->setTextAlignment(Qt::AlignRight);
            }
            else
            {
                item->setText(fromName+"——>「 "+messageText+" 」");
                item->setTextAlignment(Qt::AlignLeft);
            }
            ui->allMessage_listWidget->addItem(item);
            AllMessageList.insert(std::make_pair(fromName,messageText));
            ui->allMessage_listWidget->scrollToBottom();
        }
    }
}
