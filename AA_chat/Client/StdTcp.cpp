#include <stdio.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include "StdTcp.h"


TcpClient::TcpClient(const char *Serverip, short Serverport)
{
    this->sock = socket(AF_INET, SOCK_STREAM, 0);
    if (this->sock < 0)
    {
        perror("InitTcpClient sock");
        close(this->sock);
    }
    struct sockaddr_in addr;
    //协议族
    addr.sin_family = AF_INET;
    //端口号
    addr.sin_port = htons(Serverport);
    //IP地址
    addr.sin_addr.s_addr = inet_addr(Serverip);

    if (connect(this->sock, (struct sockaddr*)&addr, sizeof(addr)) != 0)
    {
        perror("InitTcpClient connect");
    }
}

TcpClient::~TcpClient()
{
    close(this->sock);
}

int TcpClient::TcpClientSend(void* ptr, size_t len)
{
    if (send(this->sock, ptr, len, 0) < 0)
    {
        perror("TcpClientSend send");
        return - 1;
    }
    return 0;
}

int TcpClient::TcpclientRecv(void* ptr, size_t len)
{
    if (recv(this->sock, ptr, len, 0) <= 0)
    {
        perror("TcpClientRecv recv");
        return -1;
    }
    return 0;
}
