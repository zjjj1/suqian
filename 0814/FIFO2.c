#include "SystemHead.h"

#define FIFO1 "MyFIFO1"
#define FIFO2 "MyFIFO2"

int main()
{
    mkfifo(FIFO1, S_IRUSR | S_IWUSR);
    mkfifo(FIFO2, S_IRUSR | S_IWUSR);
    int fd_first = open(FIFO1, O_RDONLY);
    if (fd_first < 0)
    {
        perror("open:");
        return -1;
    }
    int fd_sencend = open(FIFO2, O_WRONLY);
    if (fd_sencend < 0)
    {
        perror("open:");
        return -1;
    }
    while (1)
    {
        char temp[100] = {0};
        read(fd_first, temp, sizeof(temp));
        printf("从管道1读: %s\n", temp);
        
        memset(temp, 0, 100);
        printf("给管道2写:——>");
        scanf("%[^\n]", temp);
        while (getchar() != '\n')
            ;
        write(fd_sencend, temp, strlen(temp));
    }

    close(fd_first);
    close(fd_sencend);

    return 0;
}