#include "SystemHead.h"

#define FIFO1 "MyFIFO1"
#define FIFO2 "MyFIFO2"

int main()
{
    mkfifo(FIFO1, S_IRUSR | S_IWUSR);
    mkfifo(FIFO2, S_IRUSR | S_IWUSR);
    int fd_frist = open(FIFO1, O_WRONLY);
    if (fd_frist < 0)
    {
        perror("open:");
        return -1;
    }
    int fd_sencond = open(FIFO2, O_RDONLY);
    if (fd_sencond < 0)
    {
        perror("open:");
        return -1;
    }
    while (1)
    {
        char temp[100] = {0};
        printf("给管道1写:——>");
        scanf("%[^\n]", temp);
        while (getchar() != '\n')
            ;
        write(fd_frist, temp, strlen(temp));

        memset(temp,0,100);
        read(fd_sencond,temp,sizeof(temp));
        printf("从管道2读: %s\n",temp);
    }

    close(fd_frist);
    close(fd_sencond);

    return 0;
}