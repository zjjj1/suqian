#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
//FIFO
#include <sys/stat.h>
#include <fcntl.h>
//MessageQueue
#include <sys/ipc.h>
#include <sys/msg.h>
