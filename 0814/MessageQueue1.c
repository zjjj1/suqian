#include "SystemHead.h"

struct Message
{
    long type; // message type
    char content[100];
};
typedef struct Message MSG;

int main()
{
    key_t key = ftok("./", 10);
    if (key < 0)
    {
        perror("ftok:");
        return -1;
    }

    int msgid = msgget(key, IPC_CREAT | 0777);
    if (msgid < 0)
    {
        perror("msgid:");
        return -1;
    }

    MSG msg;
    msg.type = 1;
    strcpy(msg.content, "helloworld");
    if (msgsnd(msgid, &msg, sizeof(MSG), 0) == -1)
    {
        perror("msgsnd:");
        return -1;
    }

    return 0;
}