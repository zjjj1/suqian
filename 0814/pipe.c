#include "SystemHead.h"

int main()
{
    int fd_first[2];
    int fd_second[2];
    if (pipe(fd_first) == -1)
    {
        perror("pipe:");
        return -1;
    }
    if (pipe(fd_second) == -1)
    {
        perror("pipe:");
        return -1;
    }

    pid_t pid = fork();
    if (pid < 0)
    {
        perror("fork:");
        return -1;
    }

    if (pid == 0) // 子进程
    {
        while (1)
        {
            char Buff[100] = {0};
            read(fd_first[0], Buff, sizeof(Buff));
            printf("孩子读到父亲写的: %s!\n", Buff);

            memset(Buff, 0, 100);
            printf("孩子写给父亲:\n");
            scanf("%[^\n]", Buff);
            while (getchar() != '\n')
                ;
            write(fd_second[1], Buff, strlen(Buff));
        }
    }
    else // 父进程
    {
        while (1)
        {
            char buff[100] = {0};
            
            printf("父亲写给孩子:\n");
            scanf("%[^\n]", buff);
            while (getchar() != '\n')
                ;
            write(fd_first[1], buff, strlen(buff));

            memset(buff, 0, 100);
            read(fd_second[0], buff, sizeof(buff));
            printf("父亲读到孩子写的: %s!\n", buff);
        }
    }

    return 0;
}