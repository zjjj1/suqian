#include "StdFIFO.h"
#include "SystemHead.h"

int main()
{
    FIFO *f=InitFIFO("textFIFO");
    pid_t pid = fork();
    if(pid==0)
    {
        OpenFIFO(f,WriteOnly);
        WriteToFIFO(f,"hello",5);
    }
    else{
        OpenFIFO(f,ReadOnly);
        char temp[10]={0};
        ReadFormFIFO(f,temp,5);
        printf("%s\n",temp);
    }
    ClearFIFO(f);
    return 0;
}