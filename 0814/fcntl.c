#include "SystemHead.h"

int main()
{
    int fd_first[2];
    if (pipe(fd_first) == -1)
    {
        perror("pipe:");
        return -1;
    }
    pid_t pid = fork();
    if (pid < 0)
    {
        perror("fork:");
        return -1;
    }

    if (pid == 0) // 子进程
    {
        int fd_stdin= fileno(stdin);//间接获取stdin的文件描述符
        fcntl(fd_stdin, F_SETFL, O_NONBLOCK);
        while (1)
        {
            char temp[100] = {0};
            scanf("%[^\n]", temp);
            while (getchar() != '\n')
                ;

            printf("孩子\n");
            sleep(2);
        }
    }
    else // 父进程
    {
        fcntl(fd_first[0], F_SETFL, O_NONBLOCK);
        while (1)
        {
            char buff[100] = {0};
            printf("父亲\n");
            read(fd_first[0], buff, sizeof(buff));
            printf("父亲读到孩子写的: %s!\n", buff);
            sleep(2);
        }
    }

    return 0;
}