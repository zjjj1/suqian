#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
// FIFO
#include <sys/stat.h>
#include <fcntl.h>
// MessageQueue
#include <sys/ipc.h>
#include <sys/msg.h>
#include "StdMsgQueue.h"

struct MessageQueue
{
    key_t key;
    int msgid;
};

MsgQueue *InitMsgQueue(const char *__pathname, int __proj_id)
{
    MsgQueue *q = (MsgQueue *)malloc(sizeof(MsgQueue));
    if (q == NULL)
    {
        printf("初始化消息队列出错!\n");
        return NULL;
    }
    q->key = ftok("./", 10);
    if (q->key < 0)
    {
        perror("ftok:");
        return -1;
    }

    q->msgid = msgget(q->key, IPC_CREAT | 0777);
    if (q->msgid < 0)
    {
        perror("msgid:");
        free(q);
        return -1;
    }
    return q;
}

void MessageSend(MsgQueue *q, void *msg, size_t size)
{
    if (msgsnd(q->msgid, &msg, size, 0) == -1)
    {
        perror("msgid:");
        return;
    }
}

void MessageRCV(MsgQueue *q, void *msg, size_t size, int type)
{
    if (msgrcv(q->msgid, &msg, size, type, IPC_NOWAIT) == -1)
    {
        perror("msgrcv:");
        return;
    }
}

void ClearMsgQueue(MsgQueue *q)
{
    msgctl(q->msgid, IPC_RMID, NULL);
}
