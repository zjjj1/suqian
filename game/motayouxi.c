#include <stdio.h>
#include<stdlib.h>
#include<time.h>
#include"mystring.h"
#include"dongtaishuzu.h"


#define Map_Size 9
#define true 1
#define false 0

struct Player
{
    int HP;
    int Max_HP;
    int attack;
    int defense;
    int crit;
    int experience;
    int level;
    int x;
    int y;
};

struct Monster
{
    MyString name;
    int HP;
    int attack;
    int experience;
    int x;
    int y;
};

int GetRandNumber(int max)
{
    return rand() % max;
}

struct Treasure
{
    MyString name;
    int value;
    int x;
    int y;
};

void Initplayer(struct Player* player)
{
    player->HP = 100;
    player->Max_HP = 150;
    player->attack = 10;
    player->defense=5;
    player->crit=0;
    player->level=1;
    player->experience=0;
    player->x = 0;
    player->y = 0;
}

struct Monster * CreateMonster(const char* name,int HP,int attack,int experience)
{
    struct Monster *monster=(struct Monster *)malloc(sizeof(struct Monster));
    if(monster == NULL)
    {
        printf("Creat monster error!\n");
        return NULL;
    }
    
    monster->HP = HP;
    monster->attack = attack;
    monster->experience = experience;
    Initialize(&monster->name,name);
    monster->x = GetRandNumber(Map_Size);
    monster->y = GetRandNumber(Map_Size);
    return monster;
}

struct Treasure * CreateTreasure(const char* name,int value)
{
    struct Treasure *Treasure=(struct Treasure *)malloc(sizeof(struct Treasure));
    if(Treasure == NULL)
    {
        printf("Creat Treasure error!\n");
        return NULL;
    }    
    
    Initialize(&Treasure->name,name);
    Treasure->value = value;

    Treasure->x = GetRandNumber(Map_Size);
    Treasure->y = GetRandNumber(Map_Size);
    return Treasure;
}

void InitMonsters(DTSZ *array)
{
    if(AInty(array)==false)
    {
        return;
    }

    InsertArray(array,CreateMonster("巨龙",100,20,80));
    InsertArray(array,CreateMonster("食人魔",50,10,30));
    InsertArray(array,CreateMonster("食人魔",50,10,30));
    InsertArray(array,CreateMonster("食人魔",50,10,30));
    InsertArray(array,CreateMonster("食人魔",50,10,30));
    InsertArray(array,CreateMonster("食人魔",50,10,30));
    InsertArray(array,CreateMonster("哥布林",15,5,10));
    InsertArray(array,CreateMonster("哥布林",15,5,10));
    InsertArray(array,CreateMonster("哥布林",15,5,10));
    InsertArray(array,CreateMonster("哥布林",15,5,10));
    InsertArray(array,CreateMonster("哥布林",15,5,10));
    InsertArray(array,CreateMonster("哥布林",15,5,10));
    InsertArray(array,CreateMonster("哥布林",15,5,10));
    InsertArray(array,CreateMonster("哥布林",15,5,10));
    InsertArray(array,CreateMonster("哥布林",15,5,10));
    InsertArray(array,CreateMonster("哥布林",15,5,10));
}

void InitTreasures(DTSZ *array)
{
    if(AInty(array)==false)
    {
        return;
    }

    InsertArray(array,CreateTreasure("大宝箱",5));
    InsertArray(array,CreateTreasure("大宝箱",5));
    InsertArray(array,CreateTreasure("宝箱",2));
    InsertArray(array,CreateTreasure("宝箱",2));
    InsertArray(array,CreateTreasure("宝箱",2));
    InsertArray(array,CreateTreasure("宝箱",2));
    InsertArray(array,CreateTreasure("宝箱",2));
    InsertArray(array,CreateTreasure("小宝箱",1));
    InsertArray(array,CreateTreasure("小宝箱",1));
    InsertArray(array,CreateTreasure("小宝箱",1));
    InsertArray(array,CreateTreasure("小宝箱",1));
    InsertArray(array,CreateTreasure("小宝箱",1));
    InsertArray(array,CreateTreasure("小宝箱",1));
    InsertArray(array,CreateTreasure("小宝箱",1));
    InsertArray(array,CreateTreasure("小宝箱",1));
    
}

void Drug(struct Player *player,struct Treasure* treasure)
{
    int big=0,ac=0,ad=0;
    
    player->HP+=treasure->value;
    if(player->HP>=150) player->HP=150;
}

void Bag(struct Player *player)
{
    int back=0;
    printf("\n");
    printf("您目前拥有以下物品：\n");
    printf("----------------------------------------------------\n");
    // treasure->name.
    printf("\n\n\n\n\n");
    printf("输入'l'返回\n");
    printf("------------------------------------------\n");
    while(back==0)
    {   char choice;
        scanf(" %c",&choice);
        switch (choice)
       {   case 'l':
                back=1;break;
            default:
                break;}
    }
}

void Attribute(struct Player *player)
{
    int back=0;
    printf("\n");
    printf("-----------------------------------------\n");
    printf("                个人属性                  \n");
    printf("-----------------------------------------\n");
    printf("当前血量:%d%%\n",player->HP);
    printf("血量上限:%d%%\n",player->Max_HP);
    printf("当前攻击力%d\n",player->attack);
    printf("当前防御力%d\n",player->defense);
    printf("当前暴击率%d\n",player->crit);
    printf("当前经验值%d%%\n",player->experience);
    printf("当前等级%d\n",player->level);
    printf("\n\n\n\n");
    printf("输入'l'返回\n");
    printf("------------------------------------------\n");
    while(back==0)
    {   char choice;
        scanf(" %c",&choice);
        switch (choice)
       {   case 'l':
                back=1;break;
            default:
                break;}
    }
}

void Book()
{
    int back=0;
    printf("\n");
    printf("-----------------------------------------\n");
    printf("                手册大全                 \n");
    printf("-----------------------------------------\n");
    printf("一本解决你所有的烦心事\n");
    printf("***1、怪物手册***\n");
    printf("龙肉:血量回满(击败巨龙获得)\n");
    printf("龙心:攻击力增加10%%(击败巨龙概率获得\n");
	printf("龙骨:防御力增加20%%(击败巨龙概率获得)\n");
	printf("哥布林的眼珠:攻击力增加1(击败哥布林概率获得)\n");
	printf("食人魔的头发:防御力增加1(击败食人魔概率获得)\n");
    printf("***2、宝箱手册***\n");
    printf("绷带:血量回复10; 急救包:血量回复70; 医疗箱:血量回复100;\n");
    printf("止痛药:经验增加30; 能量饮料:经验增加15; 肾上腺素:经验增加60;\n");
    printf("不可描述之物:听说商人喜欢这玩意...\n");
    printf("\n");
    printf("输入'l'返回\n");
    printf("------------------------------------------\n");
    while(back==0)
    {   char choice;
        scanf(" %c",&choice);
        switch (choice)
       {   case 'l':
                back=1;break;
            default:
                break;}
    }
}

void MakeMove(struct Player *player,char symbol)
{
    switch(symbol)
    {
        case'w':player->y--;break;
        case's':player->y++;break;
        case'a':player->x--;break;
        case'd':player->x++;break;
        // case'b':Bag(player);break;
        case'v':Attribute(player);break;
        case'm':Book();break;
        default:break;
    }

    if(player->x<0) player->x = 0;
    if(player->x>=Map_Size) player->x = Map_Size-1;
    if(player->y<0) player->y = 0;
    if(player->y>=Map_Size) player->y = Map_Size-1;
}

int Battle(struct Player *player,struct Monster* monster)
{
    int count=0;
    system("clear");
    printf("您遭遇了  %s  血量:|%d| 攻击力：|%d|\n",monster->name.string,monster->HP,monster->attack);
    printf("***************************************************************\n");
    printf("*                          开始战斗！                         *\n");
    printf("***************************************************************\n");
    while(player->HP>0&&monster->HP>0)
    {
        int temp=monster->HP;
        printf("您当前的血量是 %d%% \n",player->HP);
        printf("请选择您要执行的行为:  1、浅刀一下.  2、全力一刀!  3、打不过,快跑!!\n");
        char choice;
        scanf(" %c",&choice);
        switch (choice)
        {
            case '1':
                monster->HP-=player->attack;             
                printf("\n你对 %s 造成了 %d 的普通伤害\n",monster->name.string,player->attack);               
                count++;

                if(monster->HP<=0)  break;                             
                else             
                {   printf("当前怪兽血量剩余%d%%\n",monster->HP);
                    player->HP-=monster->attack;    
                    printf(" %s 对你造成了 %d 的普通伤害\n",monster->name.string,monster->attack);
                    
                    break;  }

            case '2':
                if(GetRandNumber(2) == 1)                
                {   monster->HP-=player->attack*2;
                    count++;
                    printf("\n这一刀开天辟地!!!\n你对 %s 造成了 %d 的巨额伤害\n",monster->name.string,player->attack*2);                   

                    if(monster->HP<=0) break; 
                    else
                    {   printf("当前怪兽血量剩余%d%%\n",monster->HP);
                        player->HP-=monster->attack;
                        printf(" %s 对你造成了 %d 的普通伤害\n",monster->name.string,monster->attack);  
                       }} 

                else              
                {   printf("\n这一刀软弱无力。。。\n");
                    player->HP-=monster->attack;
                    printf(" %s 对你造成了 %d 的巨额伤害\n",monster->name.string,monster->attack); 
                     }                           
                break;

            case '3':           
                if(GetRandNumber(2) == 1)
                {   printf("\n留得青山在,不怕没柴烧!!\n");
                    count=0;     
                    return true;   } 

                else               
                {   printf("\n就你还想跑? 站着挨打!!\n");
					printf(" %s 对你造成了 %d 的巨额伤害\n",monster->name.string,monster->attack);                    
                    player->HP-=monster->attack;   
                    printf("当前怪兽血量剩余%d\n",monster->HP); }               
                break;

            default:
                break;
        }
        if(monster->HP<=0)
        {monster->HP=0;
        printf("当前怪兽血量剩余%d\n",monster->HP);  } 
        printf("\n=======================================================\n");

        if(player->HP<=0)
        {   printf("\n你被 %s 击败了!",monster->name.string);
            player->HP=0;
            return false; }        
        if(monster->HP<=0)                
        {   monster->HP=0;
            if(count==1)
            {   player->HP+=temp/2;if(player->HP>=150) player->HP=150;
                printf("\n神之一刀!斩掉 %s 狗头!!\n你当前还有%d%%血\n",monster->name.string,player->HP);count=0;  }   
            else
            {   printf("\n恭喜你! 你击败了 %s !\n你当前还有%d%%血\n",monster->name.string,player->HP); count=0;  }
            player->experience += monster->experience;
            printf("你获得了%d点经验\n", monster->experience);
            if(player->experience>=100)
            {
                player->level++;
                player->experience-=100;
                player->attack+=player->attack/5;
                player->crit+=5;
                player->Max_HP+=player->Max_HP/10;
                player->HP+=20;
                if(player->level==2)
                printf("恭喜您!升级了呢,属性提升了一大波,目前等级%d,您真厉害!!!\n",player->level);
                else
                printf("恭喜您!又升级了呢,属性又提升了一大波,目前等级%d,您真厉害!!!\n",player->level);
            }                    
            return true;    
        }
    }
}

void Initboard(char (*p)[Map_Size])
{
    for(int i=0;i<Map_Size;i++)
    {
        for(int j=0;j<Map_Size;j++)
        {
            p[i][j] = '-';
        }
    }
}

void PrintMap(char (*p)[Map_Size],struct Player *player)
{
    Initboard(p);

    p[player->y][player->x] ='I';

    printf("      ");
    for(int i=0;i<Map_Size;i++)
    {
      printf(" %4d ",i+1);  
    }
    printf("\n");
    
    for(int i=0;i<Map_Size;i++)
    {
        printf(" %4d ",i+1); 
        for(int j=0;j<Map_Size;j++)
        {
        printf(" %4c ",p[i][j]);
        }
         printf("\n");
       
    }
}

int main()
{
    srand(time(NULL));

    printf("*****************************************************************\n");
    printf("*                      欢迎来到魔塔世界！                       *\n");
    printf("*****************************************************************\n");

    char board[Map_Size][Map_Size];
    Initboard(board);

    struct Player player;
    Initplayer(&player);

    DTSZ monsters={NULL,20,0};
    InitMonsters(&monsters);

    DTSZ treasures={NULL,20,0};
    InitTreasures(&treasures);

    PrintMap(board,&player);

    while(1)
    {
        PrintMap(board,&player);
        printf("\n你当前所在的位置是:<%d,%d>\n",player.x+1,player.y+1);

        for(int i = 0;i < monsters.len;i++)
        {
            struct Monster *monster=(struct Monster *)monsters.dp[i];
            if(monster->HP > 0 && monster->x == player.x && monster->y == player.y )
            {
                if(Battle(&player,monster)==false)
                {
                    printf(" GG啦!!!\n");
                    break;
                }
                PrintMap(board,&player);
            }
        }

        for(int i=0; i<treasures.len; i++){
            struct Treasure *treasure = (struct Treasure *)treasures.dp[i];
            if(treasure->x == player.x && treasure->y == player.y && treasure->value != 0)
            {
                Drug(&player,treasure);
                printf("你获得了%s,价值是%d\n您当前的血量是 %d%%. \n",treasure->name.string,treasure->value,player.HP);
                treasure->value = 0;
            }
        }

        if(player.HP == 0)
            break;

        printf("请选择你要进行的操作:(移动:输入w,a,s,d)(打开背包:输入b)(查看属性:输入v)(查看手册:输入m)\n");
        char choice;
        scanf(" %c",&choice);      
        MakeMove(&player,choice);
    }

    for(int i=0;i<monsters.len;i++)
    {
        struct Monster *monster =(struct Monster *)monsters.dp[i];
        free(monster->name.string);
        free(monster);
    }

    for(int i=0;i<treasures.len;i++)
    {
        struct Treasure *treasure  =(struct Treasure *)treasures.dp[i];
        free(treasure->name.string);
        free(treasure);
    }

    free(monsters.dp);
    free(treasures.dp);

    return 0;
}