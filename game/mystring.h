#ifndef __MYSTRING_H__
#define __MYSTRING_H__


#define IM(obj,str) MyString obj;Initialize(&obj,str);
typedef struct String MyString;

struct String
{
    char* string;
    int size;

    void (*print)(MyString*obj);
    int (*IsEqual)(MyString *obj1,MyString *obj2);
    int (*IsContains)(MyString *dest,MyString *src);
    int (*stringSize)(MyString *obj);
    void (*RMString)(MyString *dest,const char *str);
    void (*ISString)(MyString *obj,const char *str,int index);
};


void Initialize (MyString *obj,const char *str);
void FreeMYString(MyString *obj);


#endif