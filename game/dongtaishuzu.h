#ifndef __DTSZ_H__
#define __DTSZ_H__

#define ElType void*

struct SZ
{
    ElType * dp;//dong tai shu zu zhi zheng
    int size;//dui shang sheng qing de dan wei kong jian da ge shu
    int len;//len<=size;yuan su de ge shu
}; 
typedef struct SZ DTSZ;

int AInty(DTSZ *array);
void FreeArray(DTSZ *array);
int InsertArray(DTSZ *array,ElType element);

#endif