#include <stdio.h>

// int add(int a,int b)
// {
//     return a+b;
// }

// int sub(int a,int b)
// {
//     return a-b;
// }

// void Func( int (*ptr)(int,int))
// {
//     printf("%d\n",ptr(1,2));
// }

void Write_to_Client1(const char*s)
{
    printf("Write_to_Client1:%s\n",s);
}

void Write_to_Client2(const char*str)
{
    printf("Write_to_Client2:%s\n",str);
}

void Func( void (*ptr)(const char*str))
{
   const char*s="hello!";
   ptr(s);
}

int main()
{
    Func(Write_to_Client1);
    Func(Write_to_Client2);
    return 0;
}