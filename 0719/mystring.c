#include "mystring.h"
#include<stdio.h>
#include<string.h>

void Print(MyString *obj);
int IsEqual (MyString *obj1,MyString *obj2);
int IsContains(MyString *dest,MyString *src);
int Size (MyString *obj);
void RMString(MyString *dest,const char *str);
void ISString(MyString *dest,const char *str,int index);

void Initialize(MyString *obj, const char *str)
{
    strcpy(obj->string,str);
    obj->size=strlen(str);

    // func Init
    obj->print=Print;
    obj->IsContains=IsContains;
    obj->IsEqual=IsEqual;
    obj->stringSize=Size;
    obj->ISString=ISString;
    obj->RMString=RMString;

}

void Print(MyString *obj)
{
    printf("%s\n",obj->string);
}

int IsEqual(MyString *obj1, MyString *obj2)
{
    if(strcmp(obj1->string,obj2->string)==0)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int IsContains(MyString *dest, MyString *src)
{
    char *str=strstr(dest->string,src->string);
    if(str==NULL)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

int Size(MyString *obj)
{
    return obj->size;
}

void RMString(MyString *dest, const char *str)
{
    char* RMstr=strstr(dest->string,str);
    if(RMstr==0)
    {
        return;
    }
    else
    {
        char*destination=RMstr+strlen(str);
        while(*destination!='\0')
        {
            *RMstr=*destination;
            RMstr++;
            destination++;
        }
        *RMstr='\0';
    }
}

void ISString(MyString *dest, const char *str, int index)
{
    if(index<0||index>dest->size)
    {
        return;
    }
    
    char new_str[Max]={0};
    strncpy(new_str,dest->string,index);
    strncpy(new_str+index,str,strlen(str));
    strcpy(new_str+index+strlen(str),dest->string+index);

    strcpy(dest->string,new_str);
    dest->size=strlen(dest->string);
}
