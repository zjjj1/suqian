#include<stdio.h>
#define ArrayLen(a) sizeof(a)/sizeof(a[0])

int IsSmaller(int a,int b)
{
    if(a<b)
        return 1;
    else
        return 0;
}

int IsBigger(int a,int b)
{
    if(a<b)
        return 0;
    else
        return 1;
}

void BulleSort(int *a,int len,int (*Rule)(int,int))
{
    for(int i=0;i<len-1;i++)
    {
        int flag=0;
        for(int j=0;j<len-i-1;j++)
        {
            if(Rule(a[j],a[j+1]))
            {
                int temp=a[j];
                a[j]=a[j+1];
                a[j+1]=temp;
                flag=1;
            }
        }
        if(flag==0)
        {
            break;
        }
    }
}

void BulleSort2(int *a,int len)
{
    for(int i=0;i<len-1;i++)
    {
        int flag=0;
        for(int j=1;j<len-i;j++)
        {
            if(a[j]>a[j-1])
            {
                int temp=a[j];
                a[j]=a[j-1];
                a[j-1]=temp;
                flag=1;
            }
        }
        if(flag==0)
        {
            break;
        }
    }
}

void InsertSort(int *a,int len)
{
    for(int i=1;i<len;i++)
    {
        int temp=a[i];
        int j=0;
        for( j=i;j>0;j--)
        {
            if(a[j-1]>temp)
            {
                a[j]=a[j-1];
                
            }
            else
            {
                break;
            }
        }
        a[j]=temp;
    }
}

void ChooseSort(int *a,int len)
{
    for(int i=0;i<len;i++)
    {
        int min =i;
        int Minum=a[min];
        for(int j=i+1;j<len;j++)
        {
            if(a[j]<a[min])
            {
                Minum=a[j];
                min=j;
            }
        }
        int temp=a[i];
        a[i]=Minum;
        a[min]=temp;
    }
}

void ChooseSort2(int *a,int len)
{
    int left=0;
    int right=len-1;
    while(left<right)
    {
        int min=left;
        int max=right;
        for(int i=left;i<=right;i++)
        {
            if(a[i]<a[min])
            min=i;
            if(a[i]>a[max])
            max=i;
        }
        int temp=a[left];
        a[left]=a[min];
        a[min]=temp;

        if(max==left)
       { 
        max=min;
        }
        
    
        temp=a[right];
        a[right]=a[max];
        a[max]=temp;
       

        left++;
        right--;

    }

}

void FastSort(int *a,int start,int end)
{
    int temp=a[start];
    int left=start;
    int right=end;
    while(left<right)
    {
        while(left<right && temp<a[right])
        {
            right--;
        }
        if(left<right)
        {
        a[left]=a[right];
        left++;
        }
        while(right>left&&temp>a[left])
        {
            left++;
        }
        if(left<right)
        {
        a[right]=a[left];
        right--;
        }
        
        a[left]=temp;
    }
    
    FastSort(a,start,left-1);
    FastSort(a,right-1,end);
}

int main()
{
    int a[]={11,8,8,3,6,5,5,4,7};
    // BulleSort(a,ArrayLen(a),IsBigger);
    // BulleSort2(a,ArrayLen(a));   
    InsertSort(a,ArrayLen(a));
    // ChooseSort(a,ArrayLen(a));
    // ChooseSort2( a,ArrayLen(a));
    // FastSort(a,0,ArrayLen(a)-1);

    for(int i=0;i<ArrayLen(a);i++)
    {
        printf("%d ",a[i]);
    }
    printf("\n");
    return 0;
}