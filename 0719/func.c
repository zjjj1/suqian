#include <stdio.h>

int MyStrLen(char *s)
{
    int count =0;
    while(*s!='\0')
    {
        count++;
        s++;
    }
    return count;
}

int MyStrCmp(char *s1,char *s2)
{
    if(MyStrLen(s1)!=MyStrLen(s2))
    return 0;
    while(*s1!='\0'&&*s2!='\0')
    {
        if(*s1!=*s2)
        {
            return 0;
        }
        s1++;
        s2++;
    }
    return 1;   
}

void MyStrCpy(char *dest,char *src)
{
    while(*src!='\0')
    {
        *dest=*src;
        dest++;
        src++;
        *dest='\0';
    }
}

int main()
{
    char s[]="hello";
    printf("%d\n",MyStrLen(s));
    // MyStrCpy()

    char dest[20];
    
    return 0;
}