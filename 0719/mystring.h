#ifndef __MYSTRING_H__
#define __MYSTRING_H__
#define Max 1024

#define IM(obj,str) MyString obj;IsContains(&obj,str);
typedef struct String MyString;

struct String
{
    char string[Max];
    int size;

    void (*print)(MyString*obj);
    int (*IsEqual)(MyString *obj1,MyString *obj2);
    int (*IsContains)(MyString *dest,MyString *src);
    int (*stringSize)(MyString *obj);
    void (*RMString)(MyString *dest,const char *str);
    void (*ISString)(MyString *dest,const char *str,int index);
};


void Initialize (MyString *obj,const char *str);


#endif