#include"math.h"
main()
{
    double e=0.1,b=0.5,c,d;
    long int i;
    for(i=6; ;i*=2){
        d=1.0-sqrt(1.0-b*b);
        b=0.5*sqrt(b*b+d*d);
        if (2*i*b-i*e<1e-15)break;
        e=b;
    }
    printf("pai=%.20lf\n",2*i*b);
    printf("The number of edges of required polygon: %ld\n",i);
}