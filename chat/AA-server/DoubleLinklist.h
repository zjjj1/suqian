#ifndef __DOUBLELINKLIST_H__
#define __DOUBLELINKLIST_H__

#define ElementType void*

struct Node
{
    ElementType data;
    struct Node *next;
    struct Node *prev;  
};

struct DoubleLinklist
{
    struct Node *head;
    struct Node *tail;
    int len;
};

typedef struct Node node;
typedef struct DoubleLinklist DLlist;

int InitDLlist(DLlist *list);
void DLInsertTail(DLlist *list,ElementType element);
void DLInsertHead(DLlist *list,ElementType element);
void DLRemoveByIndex(DLlist *list,int index);
void DLRemoveByElement(DLlist *list,ElementType element);
int FindFirstByElement(DLlist *list,ElementType element);
void DLTravel(DLlist *list1, void (*func)(ElementType,DLlist*),DLlist*list2);
void FreeDLlist(DLlist *list, void (*func)(ElementType));
int GetDLlistLen(DLlist *list);

#endif