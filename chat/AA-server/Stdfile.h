#ifndef __STDFILE_H_
#define __STDFILE_H_
#include "DoubleLinklist.h"
#include <stddef.h>

int IsFileExist(const char *FilePath);
char *LoadFormFile(const char *FilePath);
DLlist *GetLineFormFile(const char *FilePath);
void WriteToFile(const char *filePath, void *ptr, size_t size);
void WriteLineToFile(const char *filePath, DLlist *list);
void CopyFile(const char *SourcePath,const char *targetPath);
void AppendToFile(const char *FilePath, void *ptr, size_t size);

#endif