#ifndef __LOGIN_H__
#define __LOGIN_H__

#include "StdTcp.h"
#include "StdSqlite.h"
#include "DoubleLinklist.h"



struct Client
{
    int sock;
    char name[30];
};
typedef struct Client Cli;
//创建在线用户结构体
Cli *creatClient(int sock, const char *name);
//释放用户节点
void FreeClient(Cli *c);
//释放链表中用户节点
void FreeCli(void * data);
//服务端接收登录
void Login_Server(char *S_ALL, int sock, SQL *sql,DLlist *UserList);
//服务端接收注册
void Register_Server(char *S_ALL, int sock, SQL *sql);



#endif
