#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include "ThreadPool.h"
#include "DoubleLinklist.h"
#include "LinkQueue.h"
#include "StdThread.h"

#define true 1
#define false 0
#define Sleep_time 3

struct Task
{
    void *(*function)(void *);
    void *arg;
};
typedef struct Task task;

task *CreatTask(void *(*function)(void *), void *arg)
{
    task *t = (task *)malloc(sizeof(task));
    if (t == NULL)
    {
        printf("task malloc error!\n");
        return NULL;
    }

    t->function = function;
    t->arg = arg;
    return t;
}

void FreeTsask(task *t)
{
    free(t);
}

struct ThreadPool
{
    DLlist threads;    // 线程链表
    LQueue task_queue; // 任务等待队列

    int max_task_queue_num; // 最大等待数量
    int min_thrd_num;       // 最小线程数
    int max_thrd_num;       // 最大线程数
    int busy_thrd_num;      // 在工作的线程数
    int exit_thrd_num;      // 需要删除的线程数

    pthread_mutex_t pool_mutex;      // 给等待的任务上锁
    pthread_mutex_t busy_thrd_mutex; // 给在工作的线程上锁
    pthread_cond_t queue_not_empty;  // 任务队列不为空，激发条件变量
    pthread_cond_t queue_not_full;   // 队列不满的情况

    Thread *admin_thread; // 管理者线程
    int shutdown;
};
typedef struct ThreadPool ThreadP;

void *Thread_worker(void *arg)
{
    ThreadP *p = (ThreadP *)arg;
    while (1)
    {
        pthread_mutex_lock(&p->pool_mutex);
        while (IsQEmpty(&p->task_queue) == true && p->shutdown == false)
        {
            pthread_cond_wait(&p->queue_not_empty, &p->pool_mutex);
            if (p->exit_thrd_num > 0)
            {
                p->exit_thrd_num--;
                node *TravePoint = p->threads.head;
                while (TravePoint != NULL)
                {
                    Thread *t = (Thread *)TravePoint->data;
                    // pthread_self(); // 返回当前线程的线程号
                    if (GetThreadId(t) == pthread_self())
                    {
                        DLRemoveByElement(&p->threads, t);
                        break;
                    }
                    TravePoint = TravePoint->next;
                }

                pthread_mutex_unlock(&p->pool_mutex);
                pthread_exit(NULL);
            }
        }
        if (p->shutdown == true)
        {
            pthread_mutex_unlock(&p->pool_mutex);
            pthread_exit(NULL);
        }
        task *tk = (task *)(*QPop(&p->task_queue));
        pthread_mutex_unlock(&p->pool_mutex);
        pthread_cond_broadcast(&p->queue_not_full);

        pthread_mutex_lock(&p->busy_thrd_mutex);
        p->busy_thrd_num++;
        pthread_mutex_unlock(&p->busy_thrd_mutex);

        tk->function(tk->arg);

        pthread_mutex_lock(&p->busy_thrd_mutex);
        p->busy_thrd_num--;
        pthread_mutex_unlock(&p->busy_thrd_mutex);

        FreeTsask(tk);
    }
}

void *thread_manager(void *arg)
{
    ThreadP *p = (ThreadP *)arg;
    while (p->shutdown != true)
    {
        sleep(Sleep_time);
        pthread_mutex_lock(&p->pool_mutex);

        // 加线程
        int queueLen = GetQueueLen(&p->task_queue);
        int thread_num = GetDLlistLen(&p->threads);
        if (p->busy_thrd_num < queueLen && thread_num < p->max_thrd_num)
        {
            int add = ((queueLen / 2) > (p->max_thrd_num - thread_num) ? (p->max_thrd_num - thread_num) : (queueLen / 2));
            for (int i = 0; i < add; i++)
            {
                Thread *t = InitThread(Thread_worker, p);
                DLInsertTail(&p->threads, t);
            }
        }
        // 减线程
        thread_num = GetDLlistLen(&p->threads);
        if (thread_num > p->busy_thrd_num * 2 && thread_num > p->min_thrd_num)
        {
            int minus_thrd_num = (thread_num - p->busy_thrd_num) / 2;
            int minus = (minus_thrd_num > (thread_num - p->min_thrd_num) ? (thread_num - p->min_thrd_num) : minus_thrd_num);
            p->exit_thrd_num = minus;
            pthread_mutex_unlock(&p->pool_mutex);
            for (int i = 0; i < minus; i++)
            {
                pthread_cond_broadcast(&p->queue_not_empty);
            }
            continue;
        }
        pthread_mutex_unlock(&p->pool_mutex); // 如果既不加，也不需要减，直接解锁
    }
    pthread_exit(NULL);
}

ThreadP *InitThreadPool(int max_task_queue_num, int min_thrd_num, int max_thrd_num)
{
    ThreadP *p = (ThreadP *)malloc(sizeof(ThreadP));
    if (p == NULL)
    {
        printf("InitThreadPool malloc error!\n");
        return NULL;
    }

    InitDLlist(&p->threads);
    InitLQueue(&p->task_queue);
    p->max_task_queue_num = max_task_queue_num;
    p->min_thrd_num = min_thrd_num;
    p->max_thrd_num = max_thrd_num;
    p->busy_thrd_num = 0;
    p->exit_thrd_num = 0;

    pthread_mutex_init(&p->pool_mutex, NULL);
    pthread_mutex_init(&p->busy_thrd_mutex, NULL);
    pthread_cond_init(&p->queue_not_empty, NULL);
    pthread_cond_init(&p->queue_not_full, NULL);
    p->shutdown = false;
    for (int i = 0; i < p->max_thrd_num; i++)
    {
        Thread *t = InitThread(Thread_worker, p);
        DLInsertTail(&p->threads, t);
    }

    p->admin_thread = InitThread(thread_manager, p);

    return p;
}

void ThreadP_Addtask(ThreadP *p, void *(*func)(void *), void *arg)
{
    pthread_mutex_lock(&p->pool_mutex);
    while (GetQueueLen(&p->task_queue) == p->max_task_queue_num)
    {
        pthread_cond_wait(&p->queue_not_full, &p->pool_mutex);
    }
    if (p->shutdown == true)
    {
        pthread_mutex_unlock(&p->pool_mutex);
        return;
    }

    QPush(&p->task_queue, CreatTask(func, arg));
    pthread_cond_broadcast(&p->queue_not_empty);
    pthread_mutex_unlock(&p->pool_mutex);
}

void FreeL(void *arg)
{
    Thread *t = (Thread *)arg;
    JoinThread(t);
    free(t);
}

void FreeQ(void *arg)
{
    task *t = (task *)arg;//弹出来，销毁
    free(t);
}


void DestoryThreadPool(ThreadP *p)
{
    if (p == NULL)
        return;

    p->shutdown = true;
    JoinThread(p->admin_thread);
    free(p->admin_thread);
    int len = GetDLlistLen(&p->threads);
    for (int i = 0; i < len; i++)
    {
        pthread_cond_broadcast(&p->queue_not_empty);
    }

    FreeDLlist(&p->threads, FreeL);

    while (IsQEmpty(&p->task_queue) != true)
    {
        task *t = (task *)(*QPop(&p->task_queue));
        free(t);
    }
    
    FreeQueue(&p->task_queue,FreeQ);
    pthread_mutex_destroy(&p->pool_mutex);
    pthread_mutex_destroy(&p->busy_thrd_mutex);

    pthread_cond_destroy(&p->queue_not_empty);
    pthread_cond_destroy(&p->queue_not_full);
}
