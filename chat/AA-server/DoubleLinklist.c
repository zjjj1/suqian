#include <stdio.h>
#include <stdlib.h>
#include "DoubleLinklist.h"

int InitDLlist(DLlist *list)
{
    list->head = NULL;
    list->tail = NULL;
    list->len = 0;
    return 0;
}

node *CreatNode(ElementType elemen)
{
    node *newNode = (node *)malloc(sizeof(node));
    if (newNode == NULL)
    {
        printf("CreatNode malloc error!\n");
        return NULL;
    }
    newNode->data = elemen;
    newNode->next = NULL;
    newNode->prev = NULL;
    return newNode;
}

void DLInsertTail(DLlist *list, ElementType element)
{
    node *newNode = CreatNode(element);
    if (newNode == NULL)
    {
        printf("InsertTail malloc error!\n");
        return;
    }
    if (list->len == 0)
    {
        list->head = newNode;
        list->tail = newNode;
    }
    else
    {
        list->tail->next = newNode;
        newNode->prev = list->tail;
        list->tail = newNode;
    }
    list->len++;
}

void DLInsertHead(DLlist *list, ElementType element)
{
    node *newNode = CreatNode(element);
    if (newNode == NULL)
    {
        printf("InsertHead malloc error!\n");
        return;
    }
    if (list->len == 0)
    {
        list->head = newNode;
        list->tail = newNode;
    }
    else
    {
        list->head->prev = newNode;
        newNode->next = list->head;
        list->head = newNode;
    }
    list->len++;
}

void DLRemoveByIndex(DLlist *list, int index)
{
    if (index < 0 || index >= list->len)
    {
        printf("RemoveByIndex invalid please!\n");
        return;
    }
    if (index == 0)
    {
        if (list->len==1)
        {
            free(list->head);
            list->head=NULL;
            list->tail=NULL;
            list->len=0;
            return;
        }
        node *freeNode = list->head;
        list->head = list->head->next;
        list->head->prev = NULL;
        free(freeNode);
        list->len--;
        return;
    }
    if (index == list->len - 1)
    {
        node *freeNode = list->tail;
        list->tail = list->tail->prev;
        list->tail->next = NULL;
        free(freeNode);
        list->len--;
        return;
    }
    node *TravelPoint = list->head;
    while (index > 0)
    {
        TravelPoint = TravelPoint->next;
        index--;
    }
    node *PrevNode = TravelPoint->prev;
    node *NextNode = TravelPoint->next;
    PrevNode->next = TravelPoint->next;
    NextNode->prev = TravelPoint->prev;
    free(TravelPoint);
    list->len--;
}

void DLRemoveByElement(DLlist *list, ElementType element)
{
    int index = FindFirstByElement(list, element);
    while (index != -1)
    {
        DLRemoveByIndex(list, index);
        index = FindFirstByElement(list, element);
    }
}

int FindFirstByElement(DLlist *list, ElementType element)
{
    int count = 0;
    node *TravelPoint = list->head;
    while (TravelPoint != NULL)
    {
        if (TravelPoint->data == element)
        {
            return count;
        }
        count++;
        TravelPoint = TravelPoint->next;
    }
    return -1;
}

void DLTravel(DLlist *list1, void (*func)(ElementType,DLlist*),DLlist*list2)
{
    node *TravelPoint = list1->head;
    while (TravelPoint != NULL)
    {
        if (func != NULL)
        {
            func(TravelPoint->data,list2);
        }
        TravelPoint = TravelPoint->next;
    }
    printf("\n");
}


void FreeDLlist(DLlist *list, void (*func)(ElementType))
{
    while (list->head != NULL)
    {
        node *freeNode = list->head;
        list->head = list->head->next;
        func(freeNode->data);
        free(freeNode);
    }
    list->head = NULL;
    list->tail = NULL;
    list->len = 0;
}

int GetDLlistLen(DLlist *list)
{
    return list->len;
}
