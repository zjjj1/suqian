#ifndef __SERVER_H__
#define __SERVER_H__

#include "StdTcp.h"
#include "StdSqlite.h"
#include "DoubleLinklist.h"

struct UserMessage
{
    int flag;
    char FromName[30];
    char ToName[30];
    char Message[1024];
};
typedef struct UserMessage UM;

// 创建用户消息结构体
UM *creatUserMessage(int flag, const char *FormName, const char *ToName, const char *Message);

// 创建总用户表
void InitTable(SQL *sql_data);

// 初始化
void InitAll(SQL *sql_data, DLlist *s_UserList, DLlist *s_MessageList);

// 服务端接收用户发消息请求
void Server_SendMessage(const char *S_ALL, int sock, SQL *sql, DLlist *UserList, DLlist *MessageList);

// 给在线用户发提示消息
int SendOnline(DLlist *list, const char *s_toName, const char *FromName, char *flag);

// 服务端接收用户查看好友请求
void Server_SeeFriends(const char *S_ALL, int sock, SQL *sql);

// 服务端判断用户是否在线
void Server_IsOnile(const char *S_ALL, int sock, SQL *sql);

// 服务端处理用户加好友申请
void Server_AddFriend(const char *S_ALL, int sock, SQL *sql, DLlist *UserList, DLlist *MessageList);

// 服务端处理用户删除好友请求
void Server_DelFriend(const char *S_ALL, int sock, SQL *sql, DLlist *UserList);

// 服务器处理用户退出
void Server_ExitUser(const char *S_ALL, int sock, SQL *sql, DLlist *UserList);

// 将链表中离线用户删除
void RemoveUserByList(const char *exit_UserName, DLlist *UserList);

// 查看链表中数据数量
int GetMessageNum(int flag, const char *s_UserName, DLlist *MessageList);

// 服务端处理用户查看新消息
void Server_SeeNewMessage(const char *S_ALL, int sock, SQL *sql, DLlist *MessageList);

// 服务端接收好友查看新加好友
void Server_SeeNewFriend(const char *S_ALL, int sock, SQL *sql, DLlist *MessageList);

// 服务端处理同意加好友申请
void Server_AgreeAddFriend(const char *S_ALL, int sock, SQL *sql, DLlist *UserList);

// 服务端接收查看与好友的聊天记录
void Server_LookAllMessage(const char *S_ALL, int sock, SQL *sql);

// 服务端查找群聊列表
void Server_SeeGroups(const char *S_ALL, int sock, SQL *sql);

// 服务端查看群聊成员
void Server_SeeGroupFriends(const char *S_ALL, int sock, SQL *sql);

// 服务端查看用户群权限
void Server_UserPermissions(const char *S_ALL, int sock, SQL *sql);

// 获取用户群中权限
int GetUserPermissions(const char *Username, int GroupID, SQL *sql);

// 查找群名
int GetGroupName(char *name, int GroupID, SQL *sql);

// 给在线用户发提示消息2
int SendOnline2(DLlist *list, const char *s_toName, const char *FromName, char *flag, char *GroupName);

// 服务端拉好友入群
void Server_InviteFriend(const char *S_ALL, int sock, SQL *sql, DLlist *UserList, DLlist *MessageList);

// 服务端接收成员禁言
void Server_MemberSilence(const char *S_ALL, int sock, SQL *sql, DLlist *UserList, DLlist *MessageList);

// 服务端踢出群成员
void Server_KickPeople(const char *S_ALL, int sock, SQL *sql, DLlist *UserList, DLlist *MessageList);

// 服务端设置群管理
void Server_AssignGroupManagement(const char *S_ALL, int sock, SQL *sql, DLlist *UserList, DLlist *MessageList);

// 服务端转让群主
void Server_AssignGroupLeader(const char *S_ALL, int sock, SQL *sql, DLlist *UserList, DLlist *MessageList);

// 服务端解散群聊
void Server_RemoveGroup(const char *S_ALL, int sock, SQL *sql, DLlist *UserList, DLlist *MessageList);

// 服务端搜索群聊加入
void Server_SearchGroup(const char *S_ALL, int sock, SQL *sql, DLlist *UserList, DLlist *MessageList);

// 获取新的群号
int GetNewGroupID(SQL *sql);

// 服务端创建群聊
void Server_CreateGroup(const char *S_ALL, int sock, SQL *sql);

// 服务端群发消息
void Server_SendGroupMessage(const char *S_ALL, int sock, SQL *sql, DLlist *UserList, DLlist *MessageList);

// 服务端退出群聊
void Server_QuitGroup(const char *S_ALL, int sock, SQL *sql, DLlist *UserList, DLlist *MessageList);

// 服务端查看群聊天记录
void Server_See_GroupMessage(const char *S_ALL, int sock, SQL *sql);

// 服务端处理用户查看群聊消息
void Server_LookGroupMessage(const char *S_ALL, int sock, SQL *sql, DLlist *MessageList);

// 获取用户的VIP
int UserISVIP(char *name, SQL *sql);

// 服务端处理用户是否为VIP
void Server_VIPUser(const char *S_ALL, int sock, SQL *sql);

// 服务端处理用户开通VIP
void Server_OPEN_VIPUser(const char *S_ALL, int sock, SQL *sql);

// 服务端接收VIP用户群发消息请求
void Server_Any_ALLSendMessage(const char *S_ALL, int sock, SQL *sql, DLlist *UserList);

// 服务端接收VIP用户发消息请求
void Server_Any_SendMessage(const char *S_ALL, int sock, SQL *sql, DLlist *UserList);

// VIP给在线用户发提示消息
int VIPSendOnline(DLlist *list, const char *s_toName, const char *content, int flag);

// 服务端查看群文件
void Server_See_GroupFile(const char *S_ALL, int sock, SQL *sql);

// 服务端上传群文件
void Server_UploadGroupFile(const char *S_ALL, int sock, SQL *sql);

// 服务端下载备份
void Server_DownloadArchive(const char *S_ALL, int sock, SQL *sql);

// 线程函数
void Server_FuncThread(int sock,SQL *sql_data,DLlist* s_MessageList,DLlist *s_UserList);


#endif