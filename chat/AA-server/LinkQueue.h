#ifndef __LINKQUEUE_H__
#define __LINKQUEUE_H__
#include "DoubleLinklist.h"

struct LinkQueue
{
    DLlist queue;
    ElementType FrontData;
};

typedef struct LinkQueue LQueue;

int InitLQueue(LQueue *lq);
void QPush(LQueue *lq, ElementType element);
ElementType *QPop(LQueue *lq);
int GetQueueLen(LQueue *lq);
int IsQEmpty(LQueue *lq);
node *GetFront(LQueue *lq);
void FreeQueue(LQueue *lq, void (*func)(ElementType));

#endif