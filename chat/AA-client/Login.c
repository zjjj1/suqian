#include "SysTheamHard.h"

void Login(char *name, TcpC *tcpc)
{
    srand(time(NULL));
    system("clear");
    printf("***********************************\n");
    printf("*        \033[1;37m欢迎来到\033[1;3;37mAA随便聊聊\033[1;37m!\033[0m        *\n");

    char SignIn_flag = false;
    char UserName[30] = {0};
    char PassWd[30] = {0};
    while (SignIn_flag == false)
    {
        printf("***********************************\n");
        printf("*                                 *\n");
        printf("*                                 *\n");
        printf("*        \033[1;37m 1、用户登录   \033[0m          *\n");
        printf("*        \033[1;37m 2、用户注册   \033[0m          *\n");
        printf("*        \033[1;37m 3、退出登录   \033[0m          *\n");
        printf("*                                 *\n");
        printf("***********************************\n");
        printf("\n请输入您的选择!\n");
        printf("———————————————————————————————————\n");
        int choice;
        scanf("%d", &choice);
        switch (choice)
        {
        case 1:
            printf("请输入用户名\n——————>>");
            scanf("%s", UserName);
            printf("请输入密码\n——————>>");
            scanf("%s", PassWd);
            char s_signin[100] = {0};
            sprintf(s_signin, "%c %s %s", 'A', UserName, PassWd);
            TcpClientSend(tcpc, s_signin, strlen(s_signin) + 1);
            sleep(1);
            char temp[10] = {0};
            TcpclientRecv(tcpc, temp, sizeof(temp));
            if (*temp == 'A')
                SignIn_flag = true;
            else if (*temp == '1')
                printf("该账户已经被登录！\n");
            else
                printf("用户名或密码错误！请重新输入！\n");
            break;
        case 2:
            Register(tcpc);
            getchar();
            break;
        case 3:
            ClearTcpClient(tcpc);
            exit(0);
        default:
            getchar();
            printf("\033[3;37m————>请重新输入!\033[0m \n");
            break;
        }
    }
    sleep(1);
    memset(name, 0, 30);
    strcat(name, UserName);
    printf("***********************************\n");
    printf("\033[1;37m————>登录成功!<————  \033[0;37m   欢迎用户 @%s ! \033[0m  \n", UserName);
    printf("***********************************\n");
}

void Register(TcpC *tcpc)
{
    printf("***********************************\n");
    printf("*                                 *\n");
    printf("*        \033[1;37m 2、用户注册   \033[0m          *\n");
    printf("*                                 *\n");
    printf("***********************************\n");
    char new_UserName[50] = {0};
    char new_PassWd[50] = {0};

a_username:
    printf("请输入用户名\n——————>>");
    scanf("%s", new_UserName);
    if (strlen(new_UserName) >= 20)
    {
        memset(new_UserName, 0, 50);
        printf("用户名过长,请重新输入!(最大不超过20个字符)\n");
        goto a_username;
    }

b_passwd:
    printf("请输入密码\n——————>>");
    scanf("%s", new_PassWd);
    if (strlen(new_PassWd) >= 20)
    {
        memset(new_PassWd, 0, 50);
        printf("密码过长,请重新输入!(最大不超过20个字符)\n");
        goto b_passwd;
    }
    char s_register[150] = {0};
    sprintf(s_register, "%c %s %s", 'B', new_UserName, new_PassWd);
    TcpClientSend(tcpc, s_register, strlen(s_register) + 1);
    sleep(1);
    char temp[10] = {0};
    TcpclientRecv(tcpc, temp, sizeof(temp));
    if (*temp == 'B')
    {
        printf("***********************************\n");
        printf("\033[1;37m————>注册成功!<————  \033[0m   \n");
        printf("***********************************\n");
        char s_name[20] = "";
        strcat(s_name, "AA-client/Save/");
        strcat(s_name, new_UserName);
        CreateDir(s_name);
    }
    else
        printf("用户名已被注册！请重新输入！\n");
    sleep(1);
}
