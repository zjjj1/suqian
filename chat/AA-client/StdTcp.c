#include <stdio.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <unistd.h>
#include "StdTcp.h"

struct TcpServer
{
    int sock;
};

struct TcpClient
{
    int sock;
};

TcpS *InitTcpServer(const char *ip, short int port)
{
    TcpS *s = (TcpS *)malloc(sizeof(TcpS));
    if (s == NULL)
    {
        printf("InitTcpServer malloc error!\n");
        return NULL;
    }

    s->sock = socket(AF_INET, SOCK_STREAM, 0);
    if (s->sock < 0)
    {
        perror("socket");
        free(s);
        return NULL;
    }
    int on = 1;
    if (setsockopt(s->sock, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on)) < 0)//端口后可以重复使用
    {
        perror("setsockopt");
        free(s);
        return NULL;
    }

    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = inet_addr(ip);
    if (bind(s->sock, (struct sockaddr *)&addr, sizeof(addr)) < 0)
    {
        perror("bind");
        free(s);
        return NULL;
    }
    if (listen(s->sock, 10) != 0)
    {
        perror("listen");
        return NULL;
    }

    return s;
}

int TcpServerAccept(TcpS *s)
{
    int acceptSock = 0;
    struct sockaddr_in addr;
    socklen_t len;
    if ((acceptSock = accept(s->sock, (struct sockaddr *)&addr, &len)) < 0)
    {
        perror("Accept");
        return -1;
    }
    return acceptSock;  
}

void TcpServerSend(int ClientSock, void *ptr, size_t size)
{
    if (send(ClientSock, ptr, size, 0) < 0)
    {
        perror("send");
    }
}

void TcpServerRecv(int ClientSock, void *ptr, size_t size)
{
    if (recv(ClientSock, ptr, size, 0) < 0)
    {
        perror("recv");
    }
}

void ClearTcpServer(TcpS *s)
{
    close(s->sock);
    free(s);
}



TcpC *InitTcpClient(const char *Serverip, short int Serverport)
{
    TcpC *c = (TcpC *)malloc(sizeof(TcpC));
    if (c == NULL)
    {
        printf("InitTcpClient malloc error");
        return NULL;
    }

    c->sock = socket(AF_INET, SOCK_STREAM, 0);
    if (c->sock < 0)
    {
        perror("socket");
        free(c);
        return NULL;
    }

    struct sockaddr_in Serveraddr;
    Serveraddr.sin_family = AF_INET;
    Serveraddr.sin_port = htons(Serverport);
    Serveraddr.sin_addr.s_addr = inet_addr(Serverip);
    if (connect(c->sock, (struct sockaddr *)&Serveraddr, sizeof(Serveraddr)) < 0)
    {
        perror("connect");
        free(c);
        return NULL;
    }

    return c;
}

void TcpClientSend(TcpC *c, void *ptr, size_t size)
{
    if (send(c->sock, ptr, size, 0) < 0)
    {
        perror("send");
    }
}

void TcpclientRecv(TcpC *c, void *ptr, size_t size)
{
    if (recv(c->sock, ptr, size, 0) < 0)
    {
        perror("recv");
    }
}

void ClearTcpClient(TcpC *c)
{
    close(c->sock);
    free(c);
}
