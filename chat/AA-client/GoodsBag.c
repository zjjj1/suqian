#include "SysTheamHard.h"

void Bag(struct Player *player)
{
    printf("*************************\033[1;3;37m背包\033[0m***************************\n\n");
    printf("\033[1;33mQ、龙肉 * %d         W、龙心 * %d           E、龙骨 * %d\n", player->GoodsNum[7], player->GoodsNum[8], player->GoodsNum[9]);
    printf("\033[1;32mZ、包菜 * %d        X、 虾虾皮 * %d\n", player->GoodsNum[10], player->GoodsNum[11]);
    printf("\033[1;31mA、绷带 * %d         S、急救包 * %d          D、医疗箱 * %d\n", player->GoodsNum[0], player->GoodsNum[2], player->GoodsNum[4]);
    printf("\033[1;34mF、能量饮料 * %d       G、止痛药 * %d       H、肾上腺素 * %d\n", player->GoodsNum[1], player->GoodsNum[3], player->GoodsNum[5]);
    printf("\033[1;35mJ、不可描述之物 * %d\n", player->GoodsNum[6]);
    printf("\n\n");

    while (1)
    {
        printf("\033[1;37m请输入要使用的物品 :\033[0;37m     返回:输入'\033[1;37ml\033[0m';\n");
        printf("-------------------------------------------------------\n");
        char choice;
        scanf(" %c", &choice);
        while ((getchar()) != '\n')
            ;
        switch (choice)
        {
        case 'q':
            if (player->GoodsNum[7] == 0)
                printf("您没有此物品！\n");
            else
            {
                player->GoodsNum[7]--;
                player->HP = player->Max_HP;
                printf("物品使用成功,血量已回满\n");
            }
            break;
        case 'w':
            if (player->GoodsNum[8] == 0)
                printf("您没有此物品！\n");
            else
            {
                player->GoodsNum[8]--;
                player->attack += player->attack / 3;
                printf("物品使用成功,攻击力增加33%%\n");
            }
            break;
        case 'e':
            if (player->GoodsNum[9] == 0)

                printf("您没有此物品！\n");
            else
            {
                player->GoodsNum[9]--;
                player->defense += player->defense / 3;
                printf("物品使用成功,防御力增加33%%\n");
            }
            break;
        case 'z':
            if (player->GoodsNum[10] == 0)

                printf("您没有此物品！\n");
            else
            {
                player->GoodsNum[10]--;
                player->attack++;
                printf("物品使用成功,攻击力+1\n");
            }
            break;
        case 'x':
            if (player->GoodsNum[11] == 0)
                printf("您没有此物品！\n");
            else
            {
                player->GoodsNum[11]--;
                player->defense++;
                printf("物品使用成功,防御力+1\n");
            }
            break;
        case 'a':
            if (player->GoodsNum[0] == 0)

                printf("您没有此物品！\n");
            else
            {
                player->GoodsNum[0]--;
                player->HP += 30;
                if (player->HP > player->Max_HP)
                    player->HP = player->Max_HP;
                printf("物品使用成功,血量恢复30,当前血量 %d\n", player->HP);
            }
            break;
        case 's':
            if (player->GoodsNum[2] == 0)
                printf("您没有此物品！\n");
            else
            {
                player->GoodsNum[2]--;
                player->HP += 70;
                if (player->HP > player->Max_HP)
                    player->HP = player->Max_HP;
                printf("物品使用成功,血量恢复70,当前血量 %d\n", player->HP);
            }
            break;
        case 'd':
            if (player->GoodsNum[4] == 0)
                printf("您没有此物品！\n");
            else
            {
                player->GoodsNum[4]--;
                player->HP += 100;
                if (player->HP > player->Max_HP)
                    player->HP = player->Max_HP;
                printf("物品使用成功,血量恢复100,当前血量 %d\n", player->HP);
            }
            break;
        case 'f':
            if (player->GoodsNum[1] == 0)
                printf("您没有此物品！\n");
            else
            {
                player->GoodsNum[1]--;
                player->experience += 5;
                printf("物品使用成功,经验值增加5\n");
                LevelUp(player);
            }
            break;
        case 'g':
            if (player->GoodsNum[3] == 0)
                printf("您没有此物品！\n");

            else
            {
                player->GoodsNum[3]--;
                player->experience += 10;
                printf("物品使用成功,经验值增加10\n");
                LevelUp(player);
            }
            break;
        case 'h':
            if (player->GoodsNum[5] == 0)
                printf("您没有此物品！\n");
            else
            {
                player->GoodsNum[5]--;
                player->experience += 30;
                printf("物品使用成功,经验值增加30\n");
                LevelUp(player);
            }
            break;
        case 'j':
            if (player->GoodsNum[6] == 0)
                printf("您没有此物品！\n");
            else
                printf("此物品不可使用哦~\n");
            break;
        case 'l':
            return;
        default:
            printf("\033[3;37m————>请重新输入!\033[0m \n");
            break;
        }
        printf("\n*************************\033[1;3;37m背包\033[0m***************************\n\n");
        printf("\033[1;33mQ、龙肉 * %d         W、龙心 * %d           E、龙骨 * %d\n", player->GoodsNum[7], player->GoodsNum[8], player->GoodsNum[9]);
        printf("\033[1;32mZ、包菜 * %d        X、 虾虾皮 * %d\n", player->GoodsNum[10], player->GoodsNum[11]);
        printf("\033[1;31mA、绷带 * %d         S、急救包 * %d          D、医疗箱 * %d\n", player->GoodsNum[0], player->GoodsNum[2], player->GoodsNum[4]);
        printf("\033[1;34mF、能量饮料 * %d       G、止痛药 * %d       H、肾上腺素 * %d\n", player->GoodsNum[1], player->GoodsNum[3], player->GoodsNum[5]);
        printf("\033[1;35mJ、不可描述之物 * %d\033[0m\n\n", player->GoodsNum[6]);
    }
}

void OpenBox(struct Player *player, struct Treasure *treasure)
{
    if (treasure->value == 1)
    {
        printf("远处金光一闪，你走过去一看，发现了一个小盒子!\n");
        if (GetRandNumber(3) == 1)
        {
            player->GoodsNum[1]++;
            printf("你获得了%s!\n", player->GoodsName[1]);
        }
        else
        {
            player->GoodsNum[0]++;
            printf("你获得了%s!", player->GoodsName[0]);
        }
        int ml = GetRandNumber(200) + 400;
        player->money += ml;
        printf("你获得了%d摩拉!当前摩拉剩余%d.\n", ml, player->money);
    }

    if (treasure->value == 3)
    {
        printf("你艳福不浅，偶遇了一个富婆!\n");
        for (int i = 0; i < 2; i++)
        {
            int num = GetRandNumber(2) + 2;
            player->GoodsNum[num]++;
            printf("你获得了%s\n!", player->GoodsName[num]);
        }
        for (int i = 0; i < 2; i++)
        {
            int num = GetRandNumber(2);
            player->GoodsNum[num]++;
            printf("你获得了%s\n!", player->GoodsName[num]);
        }
        int ml = GetRandNumber(200) + 600;
        player->money += ml;
        printf("你获得了%d摩拉!当前摩拉剩余%d.\n", ml, player->money);
    }

    if (treasure->value == 5)
    {
        printf("你不幸坠下了山崖,却因祸得福，发现了藏在山洞里的宝藏!\n");
        player->GoodsNum[4]++;
        player->GoodsNum[5]++;
        printf("你获得了%s!\n", player->GoodsName[4]);
        printf("你获得了%s!\n", player->GoodsName[5]);
        for (int i = 0; i < 3; i++)
        {
            int num = GetRandNumber(4);
            player->GoodsNum[num]++;
            printf("你获得了%s!\n", player->GoodsName[num]);
        }
        int ml = GetRandNumber(200) + 900;
        player->money += ml;
        printf("你获得了%d摩拉!当前摩拉剩余%d.\n", ml, player->money);
    }
    if (GetRandNumber(6) == 1)
    {
        player->GoodsNum[6]++;
        printf("你在野外探索时，额外获得了%s!\n", player->GoodsName[6]);
    }
    treasure->value = 0;
}
