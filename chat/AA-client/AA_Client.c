#include "SysTheamHard.h"

// 给好友发消息
void SendMessage(const char *UserName, TcpC *tcpc)
{
    char toName[30] = {0};
    char content[1024] = {0};
    printf("你想发给谁：\n");
    scanf("%s", toName);
    while (getchar() != '\n')
        ;
    if (strcmp(UserName, toName) == 0)
    {
        printf("不能给自己发送消息！！！\n");
        return;
    }
    printf("你要发送的内容：\n");
    scanf("%[^\n]", content);
    while (getchar() != '\n')
        ;
    char c_signin[2048] = {0};
    sprintf(c_signin, "%c %s %s %s", 'C', UserName, toName, content);
    TcpClientSend(tcpc, c_signin, strlen(c_signin) + 1);
}

// 请求查看好友列表
void See_Friends(const char *UserName, TcpC *tcpc)
{
    char c_Friends[50] = {0};
    sprintf(c_Friends, "%c %s", 'F', UserName);
    TcpClientSend(tcpc, c_Friends, strlen(c_Friends) + 1);
}

// 请求查看群聊列表
void See_Groups(const char *UserName, TcpC *tcpc)
{
    char c_Friends[50] = {0};
    sprintf(c_Friends, "%c %s", 'G', UserName);
    TcpClientSend(tcpc, c_Friends, strlen(c_Friends) + 1);
}

// 请求查看好友是否在线
void Friends_isOnline(const char *UserName, TcpC *tcpc)
{
    char c_Friends[30] = {0};
    printf("请输入您要查看的好友：\n");
    scanf("%s", c_Friends);
    while (getchar() != '\n')
        ;
    if (strcmp(UserName, c_Friends) == 0)
    {
        printf("您当前肯定是在线状态啦！！！\n");
        return;
    }
    char str[100] = {0};
    sprintf(str, "%c %s %s", 'I', UserName, c_Friends);
    TcpClientSend(tcpc, str, strlen(str) + 1);
}

// 添加好友请求
void AddFriends(const char *UserName, TcpC *tcpc)
{
    char c_Friends[30] = {0};
    printf("请输入您要添加的好友：\n");
    scanf("%s", c_Friends);
    while (getchar() != '\n')
        ;
    if (strcmp(UserName, c_Friends) == 0)
    {
        printf("不能添加自己为好友！！！\n");
        return;
    }
    else
    {
        char str[100] = {0};
        sprintf(str, "%c %s %s", '+', UserName, c_Friends);
        TcpClientSend(tcpc, str, strlen(str) + 1);
    }
}

// 删除好友请求
void DeleteFriends(const char *UserName, TcpC *tcpc)
{
    char c_Friends[30] = {0};
    printf("请输入您要删除的好友：\n");
    scanf("%s", c_Friends);
    while (getchar() != '\n')
        ;
    if (strcmp(UserName, c_Friends) == 0)
    {
        printf("自己怎么能把自己删除掉呢！！！\n");
        return;
    }
    int flag = 0;
    printf("确定删除该好友？(确认：输入'1')\n");
    scanf("%d", &flag);
    while (getchar() != '\n');
    if (flag == 1)
    {
        char str[100] = {0};
        sprintf(str, "%c %s %s", '-', UserName, c_Friends);
        TcpClientSend(tcpc, str, strlen(str) + 1);
    }
}

// 好友系统
void Friends(const char *UserName, TcpC *tcpc)
{
    printf("正在查看好友信息....\n");
    sleep(1);
    See_Friends(UserName, tcpc);
    sleep(1);
    while (1)
    {
        printf("***********************************\n");
        printf("*                                 *\n");
        printf("*     \033[1;37m 1、给好友发送消息   \033[0m       *\n");
        printf("*     \033[1;37m 2、查看好友是否在线   \033[0m     *\n");
        printf("*     \033[1;37m 3、添加好友   \033[0m             *\n");
        printf("*     \033[1;37m 4、删除好友   \033[0m             *\n");
        printf("*     \033[1;37m 5、查看当前好友   \033[0m         *\n");
        printf("*     \033[1;37m 6、返回   \033[0m                 *\n");
        printf("*                                 *\n");
        printf("***********************************\n");
        printf("\n请输入您的选择!\n");
        printf("———————————————————————————————————\n");
        int choice;
        scanf("%d", &choice);
        switch (choice)
        {
        case 1:
            SendMessage(UserName, tcpc);
            sleep(1);
            break;
        case 2:
            Friends_isOnline(UserName, tcpc);
            sleep(1);
            break;
        case 3:
            AddFriends(UserName, tcpc);
            sleep(1);
            break;
        case 4:
            DeleteFriends(UserName, tcpc);
            sleep(1);
            break;
        case 5:
            See_Friends(UserName, tcpc);
            sleep(1);
            break;
        case 6:
            sleep(1);
            return;
        default:
            while (getchar() != '\n');
            printf("\033[3;37m————>请重新输入!\033[0m \n");
            break;
        }
    }
}

// 查看新消息
void NewMessage(const char *UserName, TcpC *tcpc)
{
    char c_Friends[50] = {0};
    sprintf(c_Friends, "%c %s", 'X', UserName);
    TcpClientSend(tcpc, c_Friends, strlen(c_Friends) + 1);
}

// 查看好友申请
void NewFriend(const char *UserName, TcpC *tcpc, int *flag, char *fromName)
{
    char c_Friends[100] = {0};
    sprintf(c_Friends, "%c %s", 'N', UserName);
    TcpClientSend(tcpc, c_Friends, strlen(c_Friends) + 1);
    sleep(1);
    if (*flag == true)
    {
        *flag = false;
        printf("——>(同意：输入'1';拒绝：输入'2')<——\n");
        while (1)
        {
            int choice;
            scanf("%d", &choice);
            while ((getchar()) != '\n')
                ;
            switch (choice)
            {
            case 1:
                memset(c_Friends, 0, 100);
                sprintf(c_Friends, "%c %s %s", 'n', UserName, fromName);
                TcpClientSend(tcpc, c_Friends, strlen(c_Friends) + 1);
                memset(fromName, 0, 30);
                return;
            case 2:
                printf("您已拒绝用户 %s 的好友申请！\n", fromName);
                memset(fromName, 0, 30);
                return;
            default:
                printf("\033[3;37m————>请重新输入!\033[0m \n");
                break;
            }
        }
    }
}

// 查看聊天记录
void Look_AllMessage(const char *UserName, TcpC *tcpc)
{
    char c_Friends[30] = {0};
    printf("请输入您要查看的用户：\n");
    scanf("%s", c_Friends);
    while (getchar() != '\n')
        ;
    if (strcmp(UserName, c_Friends) == 0)
    {
        printf("无法查看自己与自己的聊天记录！！！\n");
        return;
    }
    char str[100] = {0};
    sprintf(str, "%c %s %s", 'L', UserName, c_Friends);
    TcpClientSend(tcpc, str, strlen(str) + 1);
    printf("\n");
}

// 查看群聊消息
void Look_GroupMessage(const char *UserName, TcpC *tcpc)
{
    char str[50] = {0};
    sprintf(str, "%c %s", 'Z', UserName);
    TcpClientSend(tcpc, str, strlen(str) + 1);
    printf("\n");
}

// 消息系统
void MessageList(const char *UserName, TcpC *tcpc, int *flag, char *fromName)
{
    printf("正在打开消息列表....\n");
    sleep(1);
    while (1)
    {
        printf("***********************************\n");
        printf("*   \033[1;37m 1、查看新消息   \033[0m             *\n");
        printf("*   \033[1;37m 2、处理好友申请   \033[0m           *\n");
        printf("*   \033[1;37m 3、查看聊天记录   \033[0m           *\n");
        printf("*   \033[1;37m 4、查看群聊消息   \033[0m           *\n");
        printf("*   \033[1;37m 5、返回   \033[0m                   *\n");
        printf("***********************************\n");
        printf("请输入您的选择!\n");
        printf("———————————————————————————————————\n");
        int choice;
        scanf("%d", &choice);
        switch (choice)
        {
        case 1:
            NewMessage(UserName, tcpc);
            sleep(1);
            break;
        case 2:
            NewFriend(UserName, tcpc, flag, fromName);
            sleep(1);
            break;
        case 3:
            Look_AllMessage(UserName, tcpc);
            sleep(1);
            break;
        case 4:
            Look_GroupMessage(UserName, tcpc);
            sleep(1);
            break;
        case 5:
            sleep(1);
            return;
        default:
            while (getchar() != '\n');
            printf("\033[3;37m————>请重新输入!\033[0m \n");
            break;
        }
    }
}

// 请求查看群权限
int GroupPermissions(const char *UserName, TcpC *tcpc)
{
    char id_s[5] = {0};
    printf("请输入您要进行管理的群ID:\n");
    scanf("%s", id_s);
    char c_groupFriends[50] = {0};
    sprintf(c_groupFriends, "%c %s %s ", 'Q', UserName, id_s);
    TcpClientSend(tcpc, c_groupFriends, strlen(c_groupFriends) + 1);
    int id = atoi(id_s);
    return id;
}

// 退出用户
void Exit_Client(const char *UserName, TcpC *tcpc, Thread *t)
{
    printf("正在退出用户...\n");

    char str[50] = {0};
    sprintf(str, "%c %s", '*', UserName);
    TcpClientSend(tcpc, str, strlen(str) + 1);
    sleep(1);
    ClearThread(t);
    ClearTcpClient(tcpc);
    printf("用户退出成功！\n");
}

// 群发消息
void SendGroupMessage(int GroupID, const char *UserName, TcpC *tcpc)
{
    char content[1024] = {0};
    printf("你要发送的内容：\n");
    while (getchar() != '\n')
        ;
    scanf("%[^\n]", content);
    char c_signin[2048] = {0};
    sprintf(c_signin, "%c %d %s %s", 'U', GroupID, UserName, content);
    TcpClientSend(tcpc, c_signin, strlen(c_signin) + 1);
}

// 查看群消息
void See_GroupMessage(int GroupID, TcpC *tcpc)
{
    char c_signin[10] = {0};
    sprintf(c_signin, "%c %d", 'H', GroupID);
    TcpClientSend(tcpc, c_signin, strlen(c_signin) + 1);
}

// 查看群成员
void See_Groupfriends(int GroupID, TcpC *tcpc)
{
    char c_Group[10] = {0};
    sprintf(c_Group, "%c %d", 'S', GroupID);
    TcpClientSend(tcpc, c_Group, strlen(c_Group) + 1);
}

// 拉好友入群
void InviteFriend(const char *UserName, TcpC *tcpc, int GroupID)
{
    char c_Friends[30] = {0};
    printf("请输入您想要邀请的好友：\n");
    scanf("%s", c_Friends);
    while (getchar() != '\n')
        ;
    if (strcmp(UserName, c_Friends) == 0)
    {
        printf("不能邀请自己！！！\n");
        return;
    }
    else
    {
        char str[100] = {0};
        sprintf(str, "%c %s %s %d", 'Y', UserName, c_Friends, GroupID);
        TcpClientSend(tcpc, str, strlen(str) + 1);
    }
}

// 成员禁言
void MemberSilence(const char *UserName, TcpC *tcpc, int GroupID, int Per)
{
    char c_Friends[30] = {0};
    printf("请输入成员名字：\n");
    scanf("%s", c_Friends);
    while (getchar() != '\n')
        ;
    if (strcmp(UserName, c_Friends) == 0)
    {
        printf("不能输入自己的名字！！！\n");
        return;
    }
    else
    {
        char str[100] = {0};
        sprintf(str, "%c %s %d %d", 'D', c_Friends, GroupID, Per);
        TcpClientSend(tcpc, str, strlen(str) + 1);
    }
}

// 踢出群成员
void KickPeople(const char *UserName, TcpC *tcpc, int GroupID, int Per)
{
    char c_Friends[30] = {0};
    printf("请输入要踢出成员名字：\n");
    scanf("%s", c_Friends);
    while (getchar() != '\n')
        ;
    if (strcmp(UserName, c_Friends) == 0)
    {
        printf("不能将自己踢出群！！！\n");
        return;
    }
    else
    {
        int flag = 0;
        printf("确定踢出该群员？(确认：输入'1')\n");
        scanf("%d", &flag);
        while (getchar() != '\n');
        if (flag == 1)
        {
            char str[100] = {0};
            sprintf(str, "%c %s %d %d", 'T', c_Friends, GroupID, Per);
            TcpClientSend(tcpc, str, strlen(str) + 1);
        }
    }
}

// 设置群管理
void AssignGroupManagement(const char *UserName, TcpC *tcpc, int GroupID)
{
    int flag = 0;
    printf("您正在操作：设置群管理；(确认：输入'1')\n");
    scanf("%d", &flag);
    while (getchar() != '\n');
    if (flag == 1)
    {
        char c_Friends[30] = {0};
        printf("您想设置谁为管理：\n");
        scanf("%s", c_Friends);
        while (getchar() != '\n')
            ;
        if (strcmp(UserName, c_Friends) == 0)
        {
            printf("不能将自己设为管理！！！\n");
            return;
        }
        else
        {
            char str[100] = {0};
            sprintf(str, "%c %s %d", 'E', c_Friends, GroupID);
            TcpClientSend(tcpc, str, strlen(str) + 1);
        }
    }
}

// 转让群主
void AssignGroupLeader(const char *UserName, TcpC *tcpc, int GroupID)
{
    int flag = 0;
    printf("您正在操作：转让群主；(确认：输入'1')\n");
    scanf("%d", &flag);
    while (getchar() != '\n');
    if (flag == 1)
    {
        char c_Friends[30] = {0};
        printf("您想将群主转让给：\n");
        scanf("%s", c_Friends);
        while (getchar() != '\n')
            ;
        if (strcmp(UserName, c_Friends) == 0)
        {
            printf("您已经是本群群主了！！！\n");
            return;
        }
        else
        {
            int num = 0;
            do
            {
                printf("您想将自己设置为：\n");
                printf("(管理员：输入'2')(普通成员：输入'1')\n");
                scanf("%d", &num);
            } while (num != 1 && num != 2);
            char str[100] = {0};
            sprintf(str, "%c %s %s %d %d", 'R', UserName, c_Friends, GroupID, num);
            TcpClientSend(tcpc, str, strlen(str) + 1);
        }
    }
}

// 解散群聊
void RemoveGroup(TcpC *tcpc, int GroupID)
{
    int flag = 0;
    printf("您正在操作：解散群聊；(确认：输入'1')\n");
    scanf("%d", &flag);
    while (getchar() != '\n');
    if (flag == 1)
    {
        char str[50] = {0};
        sprintf(str, "%c %d", 'J', GroupID);
        TcpClientSend(tcpc, str, strlen(str) + 1);
    }
}

// 退出群聊
void QuitGroup(const char *UserName, TcpC *tcpc, int GroupID)
{
    int flag = 0;
    printf("您正在操作退出群聊；(确认：输入'1')\n");
    scanf("%d", &flag);
    while (getchar() != '\n');
    if (flag == 1)
    {
        char str[50] = {0};
        sprintf(str, "%c %s %d", 'V', UserName, GroupID);
        TcpClientSend(tcpc, str, strlen(str) + 1);
    }
}

// 上传群文件
void UploadGroupFile(TcpC *tcpc)
{
    while (1)
    {
        char temp_filePath[100] = {0};
        char content[1024] = {0};
        printf("请输入需要上传的文件路径(输入'l'退出):\n");
        while (getchar() != '\n')
            ;
        scanf("%s", temp_filePath);
        if (strcmp(temp_filePath, "l") == 0)
            return;
        FILE *file = fopen(temp_filePath, "r");
        if (file == NULL)
        {
            printf("输入的文件路径有误!\n");
            continue;
        }
        fseek(file, 0, SEEK_END);
        long size = ftell(file);
        if (size >= 1024)
        {
            printf("文件超出1024字节,无法上传！\n");
            continue;
        }
        fseek(file, 0, SEEK_SET);
        fread(content, size, 1, file);
        fclose(file);
        // 获取文件名
        char name[50] = {0};
        strcpy(name, basename(temp_filePath));

        char str[2048] = {0};
        sprintf(str, "%c %s %s", '.', name, content);
        TcpClientSend(tcpc, str, strlen(str) + 1);
        sleep(1);
    }
}

// 查看群文件
void See_GroupFile(TcpC *tcpc)
{
    char c_Group[10] = {0};
    sprintf(c_Group, "%c", '?');
    TcpClientSend(tcpc, c_Group, strlen(c_Group) + 1);
    sleep(1);

    char name[30] = {0};
    while (1)
    {
        memset(name, 0, 30);
        printf("请输入需要下载的文件名称;(输入'l'退出):\n");
        while (getchar() != '\n')
            ;
        scanf("%s", name);
        if (strcmp(name, "l") == 0)
            return;
        char str[50] = {0};
        sprintf(str, "%c %s", '!', name);
        TcpClientSend(tcpc, str, strlen(str) + 1);
        sleep(1);
    }
}

// 群聊管理系统
void ManageGroup(const char *UserName, TcpC *tcpc, int *flag)
{
    int GroupID = GroupPermissions(UserName, tcpc);
    sleep(1);

    int Per = *flag;
    *flag = false;
    if (Per == -1)
    {
        printf("您不是该群成员！\n\n");
        sleep(1);
        return;
    }
    if (Per == 3)
    {
        printf("您是本群群主，拥有以下权限：\n");
        while (1)
        {
            printf("***********************************\n");
            printf("*   \033[1;37m 1、群发消息   \033[0m               *\n");
            printf("*   \033[1;37m 2、查看群消息   \033[0m             *\n");
            printf("*   \033[1;37m 3、上传群文件   \033[0m             *\n");
            printf("*   \033[1;37m 4、查看群文件   \033[0m             *\n");
            printf("*   \033[1;37m 5、查看群成员   \033[0m             *\n");
            printf("*   \033[1;37m 6、拉好友入群   \033[0m             *\n");
            printf("*   \033[1;37m 7、设置成员禁言    \033[0m          *\n");
            printf("*   \033[1;37m 8、踢出群成员   \033[0m             *\n");
            printf("*   \033[1;37m 9、设置群管理  \033[0m              *\n");
            printf("*   \033[1;37m 10、转让群主  \033[0m               *\n");
            printf("*   \033[1;37m 11、解散群聊   \033[0m              *\n");
            printf("*   \033[1;37m 0、返回   \033[0m                   *\n");
            printf("***********************************\n");
            printf("\n请输入您的选择!\n");
            printf("———————————————————————————————————\n");
            int choice;
            scanf("%d", &choice);
            switch (choice)
            {
            case 1:
                SendGroupMessage(GroupID, UserName, tcpc);
                sleep(1);
                break;
            case 2:
                See_GroupMessage(GroupID, tcpc);
                sleep(1);
                break;
            case 3:
                UploadGroupFile(tcpc);
                sleep(1);
                break;
            case 4:
                See_GroupFile(tcpc);
                sleep(1);
                break;
            case 5:
                See_Groupfriends(GroupID, tcpc);
                sleep(1);
                break;
            case 6:
                InviteFriend(UserName, tcpc, GroupID);
                sleep(1);
                break;
            case 7:
                MemberSilence(UserName, tcpc, GroupID, Per);
                sleep(1);
                break;
            case 8:
                KickPeople(UserName, tcpc, GroupID, Per);
                sleep(1);
                break;
            case 9:
                AssignGroupManagement(UserName, tcpc, GroupID);
                sleep(1);
                break;
            case 10:
                AssignGroupLeader(UserName, tcpc, GroupID);
                sleep(1);
                return;
            case 11:
                RemoveGroup(tcpc, GroupID);
                sleep(1);
                return;
            case 0:
                sleep(1);
                return;
            default:
                while (getchar() != '\n');
                printf("\033[3;37m————>请重新输入!\033[0m \n");
                break;
            }
        }
    }
    if (Per == 2)
    {
        printf("您是本群管理，拥有以下权限：\n");
        while (1)
        {
            printf("***********************************\n");
            printf("*   \033[1;37m 1、群发消息   \033[0m               *\n");
            printf("*   \033[1;37m 2、查看群消息   \033[0m             *\n");
            printf("*   \033[1;37m 3、上传群文件   \033[0m             *\n");
            printf("*   \033[1;37m 4、查看群文件   \033[0m             *\n");
            printf("*   \033[1;37m 5、查看群成员   \033[0m             *\n");
            printf("*   \033[1;37m 6、拉好友入群   \033[0m             *\n");
            printf("*   \033[1;37m 7、设置成员禁言    \033[0m          *\n");
            printf("*   \033[1;37m 8、踢出群成员   \033[0m             *\n");
            printf("*   \033[1;37m 9、退出群聊   \033[0m               *\n");
            printf("*   \033[1;37m 0、返回   \033[0m                   *\n");
            printf("***********************************\n");
            printf("\n请输入您的选择!\n");
            printf("———————————————————————————————————\n");
            int choice;
            scanf("%d", &choice);
            switch (choice)
            {
            case 1:
                SendGroupMessage(GroupID, UserName, tcpc);
                sleep(1);
                break;
            case 2:
                See_GroupMessage(GroupID, tcpc);
                sleep(1);
                break;
            case 3:
                UploadGroupFile(tcpc);
                sleep(1);
                break;
            case 4:
                See_GroupFile(tcpc);
                sleep(1);
                break;
            case 5:
                See_Groupfriends(GroupID, tcpc);
                sleep(1);
                break;
            case 6:
                InviteFriend(UserName, tcpc, GroupID);
                sleep(1);
                break;
            case 7:
                MemberSilence(UserName, tcpc, GroupID, Per);
                sleep(1);
                break;
            case 8:
                KickPeople(UserName, tcpc, GroupID, Per);
                sleep(1);
                break;
            case 9:
                QuitGroup(UserName, tcpc, GroupID);
                sleep(1);
                return;
            case 0:
                sleep(1);
                return;
            default:
                while (getchar() != '\n');
                printf("\033[3;37m————>请重新输入!\033[0m \n");
                break;
            }
        }
    }
    if (Per == 1)
    {
        printf("您是本群成员，拥有以下权限：\n");
        while (1)
        {
            printf("***********************************\n");
            printf("*   \033[1;37m 1、群发消息   \033[0m               *\n");
            printf("*   \033[1;37m 2、查看群消息   \033[0m             *\n");
            printf("*   \033[1;37m 3、上传群文件   \033[0m             *\n");
            printf("*   \033[1;37m 4、查看群文件   \033[0m             *\n");
            printf("*   \033[1;37m 5、查看群成员   \033[0m             *\n");
            printf("*   \033[1;37m 6、拉好友入群   \033[0m             *\n");
            printf("*   \033[1;37m 7、退出群聊   \033[0m               *\n");
            printf("*   \033[1;37m 0、返回   \033[0m                   *\n");
            printf("***********************************\n");
            printf("\n请输入您的选择!\n");
            printf("———————————————————————————————————\n");
            int choice;
            scanf("%d", &choice);
            switch (choice)
            {
            case 1:
                SendGroupMessage(GroupID, UserName, tcpc);
                sleep(1);
                break;
            case 2:
                See_GroupMessage(GroupID, tcpc);
                sleep(1);
                break;
            case 3:
                UploadGroupFile(tcpc);
                sleep(1);
                break;
            case 4:
                See_GroupFile(tcpc);
                sleep(1);
                break;
            case 5:
                See_Groupfriends(GroupID, tcpc);
                sleep(1);
                break;
            case 6:
                InviteFriend(UserName, tcpc, GroupID);
                sleep(1);
                break;
            case 7:
                QuitGroup(UserName, tcpc, GroupID);
                sleep(1);
                return;
            case 0:
                sleep(1);
                return;
            default:
                while (getchar() != '\n');
                printf("\033[3;37m————>请重新输入!\033[0m \n");
                break;
            }
        }
    }
    if (Per == 0)
    {
        printf("您已经被禁言，只能拥有以下权限：\n");
        while (1)
        {
            printf("***********************************\n");
            printf("*   \033[1;37m 1、群发消息(已被禁言)   \033[0m     *\n");
            printf("*   \033[1;37m 2、查看群消息   \033[0m             *\n");
            printf("*   \033[1;37m 3、上传群文件   \033[0m             *\n");
            printf("*   \033[1;37m 4、查看群文件   \033[0m             *\n");
            printf("*   \033[1;37m 5、查看群成员   \033[0m             *\n");
            printf("*   \033[1;37m 6、拉好友入群   \033[0m             *\n");
            printf("*   \033[1;37m 7、退出群聊   \033[0m               *\n");
            printf("*   \033[1;37m 0、返回   \033[0m                   *\n");
            printf("***********************************\n");
            printf("\n请输入您的选择!\n");
            printf("———————————————————————————————————\n");
            int choice;
            scanf("%d", &choice);
            switch (choice)
            {
            case 1:
                SendGroupMessage(GroupID, UserName, tcpc);
                sleep(1);
                break;
            case 2:
                See_GroupMessage(GroupID, tcpc);
                sleep(1);
                break;
            case 3:
                UploadGroupFile(tcpc);
                sleep(1);
                break;
            case 4:
                See_GroupFile(tcpc);
                sleep(1);
                break;
            case 5:
                See_Groupfriends(GroupID, tcpc);
                sleep(1);
                break;
            case 6:
                InviteFriend(UserName, tcpc, GroupID);
                sleep(1);
                break;
            case 7:
                QuitGroup(UserName, tcpc, GroupID);
                sleep(1);
                return;
            case 0:
                sleep(1);
                return;
            default:
                while (getchar() != '\n');
                printf("\033[3;37m————>请重新输入!\033[0m \n");
                break;
            }
        }
    }
}

// 搜索群聊加入
void SearchGroup(const char *UserName, TcpC *tcpc)
{
    char GroupID[5] = {0};
    printf("请输入您想加入的群聊ID:\n");
    scanf("%s", GroupID);
    char str[50] = {0};
    sprintf(str, "%c %s %s", 'M', GroupID, UserName);
    TcpClientSend(tcpc, str, strlen(str) + 1);
}

// 创建群聊
void CreateGroup(const char *UserName, TcpC *tcpc)
{
    char c_Group[30] = {0};
a_Groupname:
    printf("请输入您想创建的群聊名称：\n");
    scanf("%s", c_Group);
    if (strlen(c_Group) >= 27)
    {
        memset(c_Group, 0, 30);
        printf("群名过长,请重新输入!(最大不超过27个字符)\n");
        goto a_Groupname;
    }
    char str[100] = {0};
    sprintf(str, "%c %s %s", 'K', c_Group, UserName);
    TcpClientSend(tcpc, str, strlen(str) + 1);
}

// 群聊系统
void Groups(const char *UserName, TcpC *tcpc, int *flag)
{
    printf("正在查看群聊信息....\n");
    sleep(1);
    See_Groups(UserName, tcpc);
    sleep(1);
    while (1)
    {
        printf("***********************************\n");
        printf("*                                 *\n");
        printf("*     \033[1;37m 1、对群进行管理   \033[0m         *\n");
        printf("*     \033[1;37m 2、搜索群聊加入   \033[0m         *\n");
        printf("*     \033[1;37m 3、创建群聊   \033[0m             *\n");
        printf("*     \033[1;37m 4、显示群聊列表   \033[0m         *\n");
        printf("*     \033[1;37m 5、返回   \033[0m                 *\n");
        printf("*                                 *\n");
        printf("***********************************\n");
        printf("\n请输入您的选择!\n");
        printf("———————————————————————————————————\n");
        int choice;
        scanf("%d", &choice);
        switch (choice)
        {
        case 1:
            ManageGroup(UserName, tcpc, flag);
            sleep(1);
            break;
        case 2:
            SearchGroup(UserName, tcpc);
            sleep(1);
            break;
        case 3:
            CreateGroup(UserName, tcpc);
            sleep(1);
            break;
        case 4:
            See_Groups(UserName, tcpc);
            sleep(1);
            break;
        case 5:
            sleep(1);
            return;
        default:
            while (getchar() != '\n');
            printf("\033[3;37m————>请重新输入!\033[0m \n");
            break;
        }
    }
}

// VIP给用户发消息
void Any_SendMessage(const char *UserName, TcpC *tcpc)
{
    char toName[30] = {0};
    char content[1024] = {0};
    printf("您想发给谁：\n");
    scanf("%s", toName);
    while (getchar() != '\n')
        ;
    printf("您要发送的内容：\n");
    scanf("%[^\n]", content);
    while (getchar() != '\n')
        ;
    char c_signin[2048] = {0};
    sprintf(c_signin, "%c %s %s %s", 'c', UserName, toName, content);
    TcpClientSend(tcpc, c_signin, strlen(c_signin) + 1);
}

// VIP给用户群发消息
void Any_ALLSendMessage(const char *UserName, TcpC *tcpc)
{
    char content[1024] = {0};
    while (getchar() != '\n')
        ;
    printf("你要发送的内容：\n");
    scanf("%[^\n]", content);
    while (getchar() != '\n')
        ;
    char c_signin[2048] = {0};
    sprintf(c_signin, "%c %s %s", 's', UserName, content);
    TcpClientSend(tcpc, c_signin, strlen(c_signin) + 1);
}

// VIP系统
void VIPUser(const char *UserName, TcpC *tcpc, int *flag)
{
    char c_s[50] = {0};
    sprintf(c_s, "%c %s", 'W', UserName);
    TcpClientSend(tcpc, c_s, strlen(c_s) + 1);
    sleep(1);
    if (*flag == true)
    {
        *flag = false;
        printf("您是尊贵的VIP用户!尊享以下特权!!!\n");
        while (1)
        {
            printf("***********************************\n");
            printf("*                                 *\n");
            printf("*  \033[1;37m 1、给任意在线用户私发消息   \033[0m  *\n");
            printf("*  \033[1;37m 2、给所有在线用户发送消息   \033[0m  *\n");
            printf("*  \033[1;37m 3、返回   \033[0m                    *\n");
            printf("*                                 *\n");
            printf("***********************************\n");
            printf("\n请输入您的选择!\n");
            printf("———————————————————————————————————\n");
            int choice;
            scanf("%d", &choice);
            switch (choice)
            {
            case 1:
                Any_SendMessage(UserName, tcpc);
                sleep(1);
                break;
            case 2:
                Any_ALLSendMessage(UserName, tcpc);
                sleep(1);
                break;
            case 3:
                sleep(1);
                return;
            default:
                while (getchar() != '\n');
                printf("\033[3;37m————>请重新输入!\033[0m \n");
                break;
            }
        }
    }
    if (*flag == false)
    {
        printf("您当前还不是VIP用户!\n");
        printf("原价999一天的VIP,现在只需要9.9,即可永久带回家！！！!\n");
        printf("花费9.9,开通VIP(输入'1')  拒绝(输入'2')\n");
        int VIP = 0;
        scanf("%d", &VIP);
        while (getchar() != '\n');
        if (VIP == 1)
        {
            memset(c_s, 0, 50);
            sprintf(c_s, "%c %s", 'w', UserName);
            TcpClientSend(tcpc, c_s, strlen(c_s) + 1);
        }
        sleep(1);
        return;
    }
}

// 单机版
void OnlyOne(const char *UserName, TcpC *tcpc, int *flag)
{
    // 创建棋盘并初始化
    char board[Map_Size][Map_Size];
    Initboard(board);

    // 创建玩家，血包，野怪，宝箱
    struct Player player;
    InitPlayerGoods(&player);
    DTSZ bloodpacks = {NULL, 20, 0};
    DTSZ monsters = {NULL, 20, 0};
    DTSZ treasures = {NULL, 20, 0};

    while (1)
    {
        // 读档系统
        if (ReadSave(UserName, tcpc, flag, &player, &bloodpacks, &monsters, &treasures) == false)
            return;

        // 游戏系统
        PLAY(board, &player, &bloodpacks, &monsters, &treasures, UserName);

        // 释放数组
        FREE(&bloodpacks, &monsters, &treasures);
    }
}

// 你的世界
void MyTowe(const char *UserName, TcpC *tcpc, int *flag)
{
    printf("正在进入你的世界....\n");
    sleep(1);
    while (1)
    {
        printf("***********************************\n");
        printf("*                                 *\n");
        printf("*     \033[1;37m 1、自己玩   \033[0m               *\n");
        printf("*     \033[1;37m 2、与好友一起玩   \033[0m         *\n");
        printf("*     \033[1;37m 3、返回       \033[0m             *\n");
        printf("*                                 *\n");
        printf("***********************************\n");
        printf("\n请输入您的选择!\n");
        printf("———————————————————————————————————\n");
        int choice;
        scanf("%d", &choice);
        switch (choice)
        {
        case 1:
            OnlyOne(UserName, tcpc, flag);
            break;
        case 2:
            printf("\n——>此功能尚在开发中.....(敬请期待！）\n\n");
            sleep(1);
            break;
        case 3:
            sleep(1);
            return;
        default:
            while (getchar() != '\n');
            printf("\033[3;37m————>请重新输入!\033[0m \n");
            break;
        }
    }
}

// 游戏系统
void PlayGame(const char *UserName, TcpC *tcpc, int *flag)
{
    printf("正在打开游戏列表....\n");
    sleep(1);
    while (1)
    {
        printf("***********************************\n");
        printf("*                                 *\n");
        printf("*     \033[1;37m 1、你的世界   \033[0m             *\n");
        printf("*     \033[1;37m 2、返回       \033[0m             *\n");
        printf("*                                 *\n");
        printf("***********************************\n");
        printf("\n请输入您的选择!\n");
        printf("———————————————————————————————————\n");
        int choice;
        scanf("%d", &choice);
        switch (choice)
        {
        case 1:
            MyTowe(UserName, tcpc, flag);
            break;
        case 2:
            sleep(1);
            return;
        default:
            while (getchar() != '\n');
            printf("\033[3;37m————>请重新输入!\033[0m \n");
            break;
        }
    }
}

// AA聊天系统
void AA_Chat(const char *UserName, TcpC *tcpc, Thread *t, int *flag, char *fromName)
{
    while (1)
    {
        printf("***********************************\n");
        printf("*                                 *\n");
        printf("*     \033[1;37m 0、VIP特权   \033[0m              *\n");
        printf("*     \033[1;37m 1、消息列表   \033[0m             *\n");
        printf("*     \033[1;37m 2、好友列表   \033[0m             *\n");
        printf("*     \033[1;37m 3、群聊列表   \033[0m             *\n");
        printf("*     \033[1;37m 4、游戏列表   \033[0m             *\n");
        printf("*     \033[1;37m 5、用户退出   \033[0m             *\n");
        printf("*                                 *\n");
        printf("***********************************\n");
        printf("\n请输入您的选择!\n");
        printf("———————————————————————————————————\n");
        int choice;
        scanf("%d", &choice);
        switch (choice)
        {
        case 0:
            VIPUser(UserName, tcpc, flag);
            break;
        case 1:
            MessageList(UserName, tcpc, flag, fromName);
            break;
        case 2:
            Friends(UserName, tcpc);
            break;
        case 3:
            Groups(UserName, tcpc, flag);
            break;
        case 4:
            PlayGame(UserName, tcpc, flag);
            break;
        case 5:
            Exit_Client(UserName, tcpc, t);
            exit(0);
        default:
            while (getchar() != '\n');
            printf("\033[3;37m————>请重新输入!\033[0m \n");
            break;
        }
    }
}

// 在线用户来消息提醒
void Client_RecvMessage(const char *c_temp)
{
    char flag1, flag2;
    char C_fromName[30] = {0};
    sscanf(c_temp, "%c %c %s", &flag1, &flag2, C_fromName);
    if (flag2 == 'C')
        printf("——>(您的好友: %s ,给您发来一条消息，请注意查看！)<——\n", C_fromName);
    if (flag2 == '-')
        printf("——>(用户 %s 已将您从好友列表删除！)<——\n", C_fromName);
    if (flag2 == '+')
        printf("——>(用户 %s 给您发来好友申请！)<——\n", C_fromName);
    if (flag2 == 'N')
        printf("——>(您与用户 %s 成功成为好友！)<——\n", C_fromName);
    if (flag2 == 'M')
        printf("——>(您已成功加入群聊[ %s ]!)<——\n", C_fromName);
    if (flag2 == 'V')
        printf("——>(您已成功退出群聊[ %s ]!)<——\n", C_fromName);
}

// 输出好友列表
void Client_SeeFriends(const char *c_temp)
{
    char temp[1024] = {0};
    strcat(temp, c_temp);
    char *flag = strtok(temp, " ");
    int num = atoi(strtok(NULL, " "));
    if (num == 0)
        printf("您当前没有好友哦，快去添加好友吧！");
    else
    {
        printf("您当前有 %d 个好友，分别为：\n", num);
        char temp2[1024];
        strcpy(temp2, strtok(NULL, "\0"));
        char *t1 = temp2;
        char *a[] = {0};
        for (int i = 0; i < num; i++)
        {
            a[i] = strtok(t1, " ");
            t1 += strlen(a[i]) + 1;
            printf("——> %s\n", a[i]);
        }
    }
    printf("\n");
    return;
}

// 用户接收消息
void ReceiveMessages(const char *c_temp)
{
    char flag1, flag2;
    char *flag3;
    sscanf(c_temp, "%c %c %s", &flag1, &flag2, flag3);
    if (flag2 == '+')
    {
        if (*flag3 == 'n')
            printf("添加好友失败，该用户当前已是您的好友！\n");
        else if (*flag3 == 'N')
            printf("添加好友失败，未找到该用户！\n");
        else
            printf("已向该用户发送请求！\n");
        return;
    }
    if (flag2 == '-')
    {
        if (*flag3 == 'N')
            printf("删除好友失败，该用户当前已经不是您的好友！\n");
        else
            printf("已删除该用户！\n");
        return;
    }
    if (flag2 == 'I')
    {
        if (*flag3 == 'N')
            printf("查看用户失败，该用户不是您的好友！\n");
        else if (*flag3 == 'Y')
            printf("该用户当前状态：在线！\n");
        else
            printf("该用户当前状态：离线！\n");
        return;
    }
    if (flag2 == 'C')
    {
        if (*flag3 == 'Y')
            printf("消息发送成功！\n");
        else if (*flag3 == 'N')
            printf("消息发送失败，该用户不是您的好友！\n");
        else
            printf("消息发送失败，您不是该用户的好友！\n");
        return;
    }
    if (flag2 == 'c')
    {
        if (*flag3 == 'Y')
            printf("消息发送成功！\n");
        else
            printf("消息发送失败，您用户当前未在线！\n");
        return;
    }
    if (flag2 == 'Y')
    {
        if (*flag3 == 'N')
            printf("该用户当前不是您的好友！\n");
        else if (*flag3 == 'n')
            printf("该好友当前已经在群聊内！\n");
        else
            printf("成功将好友添加至群聊内！\n");
        return;
    }
    if (flag2 == 'T')
    {
        if (*flag3 == 'n')
            printf("该用户当前已经不是群聊成员！\n");
        else if (*flag3 == 'N')
            printf("您没有权限踢出该用户！\n");
        else
            printf("成功将该用户踢出群聊！\n");
        return;
    }
    if (flag2 == 'D')
    {
        if (*flag3 == 'n')
            printf("该用户当前已经不是群聊成员！\n");
        else if (*flag3 == 'N')
            printf("您没有权限禁言该用户！\n");
        else if (*flag3 == 'V')
            printf("对方为至尊VIP用户!您没有权限禁言该用户！\n");
        else if (*flag3 == 'Y')
            printf("成功将该用户禁言！\n");
        else
            printf("成功将该用户禁言解除！\n");
        return;
        return;
    }
    if (flag2 == 'E')
    {
        if (*flag3 == 'N')
            printf("该用户当前已经不是群聊成员！\n");
        if (*flag3 == 'y')
            printf("成功解除该用户管理员！\n");
        else
            printf("成功将该用户设置为管理员！\n");
        return;
    }
    if (flag2 == 'R')
    {
        if (*flag3 == 'N')
            printf("该用户当前已经不是群聊成员！\n");
        else
            printf("成功将该用户设置为新的群主！\n");
    }
    if (flag2 == 'M')
    {
        if (*flag3 == 'N')
            printf("未找到该群聊！\n");
        else
            printf("您已经在该群聊内！\n");
        return;
    }
    if (flag2 == 'U')
    {
        if (*flag3 == 'Y')
            printf("消息发送成功！\n");
        else if (*flag3 == 'N')
            printf("消息发送失败！您已被该群聊禁言！！\n");
        return;
    }
    if (flag2 == '.')
    {
        if (*flag3 == 'Y')
            printf("文件上传成功！！！\n");
        else if (*flag3 == 'N')
            printf("文件上传失败（有相同的文件名）！！！\n");
        return;
    }
    if (flag2 == 'W')
    {
        if (*flag3 == 'Y')
            printf("您已成功开通VIP!\n");
        return;
    }
    if (flag2 == '/')
    {
        printf("该群聊当前已被群主解散！\n");
        return;
    }
    if (flag2 == '?')
    {
        if (*flag3 == 'N')
            printf("当前没有上传任何文件！\n");
        else if (*flag3 == 'n')
            printf("文件打开失败！\n");
        return;
    }
    if (flag2 == '!')
    {
        if (*flag3 == 'N')
            printf("不存在该文件，请输入正确的文件名称！\n");
        else if (*flag3 == 'Y')
            printf("文件下载成功！\n");
        return;
    }
    if (flag2 == ':')
    {
        if (*flag3 == 'a')
            printf("玩家数据上传成功！\n");
        else if (*flag3 == 'b')
            printf("宝箱数据上传成功！\n");
        else if (*flag3 == 'c')
            printf("野怪数据上传成功！\n");
        else
            printf("血包数据上传成功！\n");
        return;
    }
    if (flag2 == ';')
    {
        if (*flag3 == 'n')
            printf("当前云端没有存档！\n");
        else if (*flag3 == 'a')
            printf("玩家数据下载成功！\n");
        else if (*flag3 == 'b')
            printf("宝箱数据下载成功！\n");
        else if (*flag3 == 'c')
            printf("野怪数据下载成功！\n");
        else if (*flag3 == 'd')
            printf("血包数据下载成功！\n");
        return;
    }
    if (flag2 == 'H')
    {
        printf("———————————————————————————————————\n");
        printf("群聊当前共有 %s 条聊天记录！\n", flag3);
        printf("———————————————————————————————————\n");
        return;
    }
    if (flag2 == 'Z')
    {
        printf("———————————————————————————————————\n");
        printf("当前共有 %s 条群聊消息！\n", flag3);
        printf("———————————————————————————————————\n");
        return;
    }
    if (flag2 == 'X')
    {
        printf("———————————————————————————————————\n");
        printf("您当前共有 %s 条未读消息！\n", flag3);
        printf("———————————————————————————————————\n");
        return;
    }
    if (flag2 == 'L')
    {
        printf("———————————————————————————————————\n");
        printf("您与该好友当前共有 %s 条聊天记录！\n", flag3);
        printf("———————————————————————————————————\n");
        return;
    }
    if (flag2 == 'N')
    {
        printf("———————————————————————————————————\n");
        printf("您当前共有 %s 条好友申请需要处理！\n", flag3);
        printf("———————————————————————————————————\n");
        return;
    }
}

// 处理用户新消息
void Client_LookMessage(const char *c_temp)
{
    char temp[2048] = {0};
    strcat(temp, c_temp);
    char *flag = strtok(temp, " ");
    char *c_FromName = strtok(NULL, " ");
    char *c_toName = strtok(NULL, " ");
    char *content = strtok(NULL, "\0");
    printf("———————————————————————————————————\n");
    printf(" \033[37m好友[ %s ]给您发来：\n", c_FromName);
    printf("———>\033[1;37m  %s\033[0m\n", content);
}

// 处理用户与好友的聊天记录
void Client_LookAllMessage(const char *c_temp)
{
    char temp[2048] = {0};
    strcat(temp, c_temp);
    char *flag = strtok(temp, " ");
    char *c_FromName = strtok(NULL, " ");
    char *c_toName = strtok(NULL, " ");
    char *content = strtok(NULL, "\0");
    printf("[ %s ] ——> [ %s ] : \n", c_FromName, c_toName);
    printf("———> %s\n", content);
    printf("———————————————————————————————————\n");
}

// 处理用户新加好友
void Client_NewFriend(const char *c_temp, int *flag, char *fromName)
{
    char flag1;
    int num = 0;
    memset(fromName, 0, 30);
    sscanf(c_temp, "%c %d %s", &flag1, &num, fromName);
    printf("———————————————————————————————————\n");
    printf("您当前共有 %d 条好友申请需要处理！\n", num);
    printf("———————————————————————————————————\n");
    printf("用户 %s 请求添加您为好友！\n", fromName);
    printf("+++++++++++++++++++++++++++++++++++\n");
    *flag = true;
}

// 显示群列表
void Client_SeeGroups(const char *c_temp)
{
    char temp[1024] = {0};
    strcat(temp, c_temp);
    char *flag = strtok(temp, " ");
    char *flag1 = strtok(NULL, " ");
    int num = atoi(strtok(NULL, " "));
    if (*flag1 == '3')
    {
        printf("我创建的群聊：\n");
        if (num != 0)
        {
            char temp2[1024];
            strcpy(temp2, strtok(NULL, "\0"));
            char *t1 = temp2;
            char *a[] = {0};
            for (int i = 0; i < num; i++)
            {
                a[i * 2] = strtok(t1, " ");
                t1 += strlen(a[i * 2]) + 1;
                a[i * 2 + 1] = strtok(t1, " ");
                t1 += strlen(a[i * 2 + 1]) + 1;
                printf("——>群聊ID %s 群聊名称 %s\n", a[i * 2], a[i * 2 + 1]);
            }
        }
        else
            printf("\n");
    }
    if (*flag1 == '2')
    {
        printf("我管理的群聊：\n");
        if (num != 0)
        {
            char temp2[1024];
            strcpy(temp2, strtok(NULL, "\0"));
            char *t1 = temp2;
            char *a[] = {0};
            for (int i = 0; i < num; i++)
            {
                a[i * 2] = strtok(t1, " ");
                t1 += strlen(a[i * 2]) + 1;
                a[i * 2 + 1] = strtok(t1, " ");
                t1 += strlen(a[i * 2 + 1]) + 1;
                printf("——>群聊ID %s 群聊名称 %s\n", a[i * 2], a[i * 2 + 1]);
            }
        }
        else
            printf("\n");
    }
    if (*flag1 == '1')
    {
        printf("我加入的群聊：\n");
        if (num != 0)
        {
            char temp2[1024];
            strcpy(temp2, strtok(NULL, "\0"));
            char *t1 = temp2;
            char *a[] = {0};
            for (int i = 0; i < num; i++)
            {
                a[i * 2] = strtok(t1, " ");
                t1 += strlen(a[i * 2]) + 1;
                a[i * 2 + 1] = strtok(t1, " ");
                t1 += strlen(a[i * 2 + 1]) + 1;
                printf("——>群聊ID %s 群聊名称 %s\n", a[i * 2], a[i * 2 + 1]);
            }
        }
        else
            printf("\n");
        printf("\n");
    }
    return;
}

// 查看权限
void Client_UserPermissions(const char *c_temp, int *flag)
{
    char flag1;
    int num = 0;
    sscanf(c_temp, "%c %d", &flag1, &num);
    *flag = num;
}

// 显示群聊成员
void Client_SeeGroupFriends(const char *c_temp)
{
    char temp[1024] = {0};
    strcat(temp, c_temp);
    char *flag = strtok(temp, " ");
    char *flag1 = strtok(NULL, " ");
    int num = atoi(strtok(NULL, " "));
    if (*flag1 == '3')
    {
        printf("尊贵的群主大人：\n");
        if (num != 0)
        {
            char temp2[1024];
            strcpy(temp2, strtok(NULL, "\0"));
            char *t1 = temp2;
            char *a[] = {0};
            for (int i = 0; i < num; i++)
            {
                a[i] = strtok(t1, " ");
                t1 += strlen(a[i]) + 1;
                printf("——> [ %s ]\n", a[i]);
            }
        }
    }
    if (*flag1 == '2')
    {
        printf("本群管理：\n");
        if (num != 0)
        {
            char temp2[1024];
            strcpy(temp2, strtok(NULL, "\0"));
            char *t1 = temp2;
            char *a[] = {0};
            for (int i = 0; i < num; i++)
            {
                a[i] = strtok(t1, " ");
                t1 += strlen(a[i]) + 1;
                printf("——> [ %s ]\n", a[i]);
            }
        }
        else
            printf("\n");
    }
    if (*flag1 == '1')
    {
        printf("普通成员：\n");
        if (num != 0)
        {
            char temp2[1024];
            strcpy(temp2, strtok(NULL, "\0"));
            char *t1 = temp2;
            char *a[] = {0};
            for (int i = 0; i < num; i++)
            {
                a[i] = strtok(t1, " ");
                t1 += strlen(a[i]) + 1;
                printf("——> [ %s ]\n", a[i]);
            }
        }
        else
            printf("\n");
        printf("\n");
    }
    return;
}

// 在线用户来消息提醒
void Client_RecvMessage2(const char *c_temp)
{
    char flag1, flag2;
    char C_fromName[30] = {0};
    char GroupName[30] = {0};
    sscanf(c_temp, "%c %c %s %s", &flag1, &flag2, C_fromName, GroupName);
    if (flag2 == 'Y')
        printf("——>(您的好友: %s ,将您拉入[ %s ]群聊内！)<——\n", C_fromName, GroupName);
    if (flag2 == 'T')
        printf("——>(更高级管理员已将您踢出[ %s ]群聊！)<——\n", GroupName);
    if (flag2 == 'D')
    {
        if (*C_fromName == 'Y')
            printf("——>(群聊[ %s ]更高级管理员已将您禁言！)<——\n", GroupName);
        else
            printf("——>(群聊[ %s ]更高级管理员已将您禁言解除！)<——\n", GroupName);
    }
    if (flag2 == 'E')
    {
        if (*C_fromName == 'Y')
            printf("——>(您已被群聊[ %s ]的群主设置为管理员！)<——\n", GroupName);
        else
            printf("——>(您已被群聊[ %s ]的群主解除管理员！)<——\n", GroupName);
    }
    if (flag2 == 'R')
        printf("——>(您已成为群聊[ %s ]新的群主！)<——\n", GroupName);
    if (flag2 == 'J')
        printf("——>(群聊[ %s ]已被解散！)<——\n", GroupName);
    if (flag2 == 'M')
        printf("——>(用户[ %s ]加入群聊[ %s ]! )<——\n", C_fromName, GroupName);
    if (flag2 == 'K')
        printf("——>(您已创建群聊[ %s ],群聊ID[ %s ]! )<——\n", GroupName, C_fromName);
    if (flag2 == 'U')
        printf("——>(用户[ %s ]在群聊[ %s ]发送了一条消息! )<——\n", C_fromName, GroupName);
    if (flag2 == 'V')
        printf("——>(用户[ %s ]已经退出群聊[ %s ]! )<——\n", C_fromName, GroupName);
}

// 处理群聊天记录
void Client_GroupMessage(const char *c_temp)
{
    char temp[2048] = {0};
    strcat(temp, c_temp);
    char *flag = strtok(temp, " ");
    char *c_FromName = strtok(NULL, " ");
    char *content = strtok(NULL, "\0");
    printf("—>[ %s ] : \n", c_FromName);
    printf("—————> %s\n", content);
    printf("———————————————————————————————————\n");
}

// 处理群聊消息
void Client_LoolGroupMessage(const char *c_temp)
{
    char temp[2048] = {0};
    strcat(temp, c_temp);
    char *flag = strtok(temp, " ");
    char *content = strtok(NULL, "\0");
    printf("———————————————————————————————————\n");
    printf("\033[1;37m%s\033[0m\n", content);
}

// 处理VIP用户消息
void Client_VIPMessage(const char *c_temp)
{
    char temp[2048] = {0};
    strcat(temp, c_temp);
    char *flag = strtok(temp, " ");
    char *c_FromName = strtok(NULL, " ");
    char *c_toName = strtok(NULL, " ");
    char *content = strtok(NULL, "\0");
    if (strcmp(c_FromName, c_toName) == 0)
    {
        printf("*—*—*—*—*—*—*—*—*—*—*—*—*—*—*—*—*—*\n");
        printf(" \033[37m您给你自己发送了:\n");
        printf("———>\033[1;37m  %s\033[0m\n", content);
        printf("———————————————————————————————————\n");
    }
    else
    {
        printf("*—*—*—*—*—*—*—*—*—*—*—*—*—*—*—*—*—*\n");
        printf(" \033[37m至尊VIP用户[ %s ]给您发来：\n", c_FromName);
        printf("———>\033[1;37m  %s\033[0m\n", content);
        printf("———————————————————————————————————\n");
    }
}

// 处理VIP用户群发消息
void Client_VIPALLMessage(const char *c_temp)
{
    char temp[2048] = {0};
    strcat(temp, c_temp);
    char *flag = strtok(temp, " ");
    char *c_FromName = strtok(NULL, " ");
    char *content = strtok(NULL, "\0");
    printf("*—*—*—*—*—*—*—*—*—*—*—*—*—*—*—*—*—*\n");
    printf(" \033[37m至尊VIP用户[ %s ]给您发来：\n", c_FromName);
    printf("———>\033[1;37m  %s\033[0m\n", content);
    printf("———————————————————————————————————\n");
}

// 客户端查看群文件
void Client_See_GroupFile(const char *c_temp)
{
    char flag1;
    char Name[280] = {0};
    sscanf(c_temp, "%c %s", &flag1, Name);
    printf("———>\033[1;37m文件名称:  %s\033[0m\n", Name);
    printf("———————————————————————————————————\n");
}

// 客户端下载群文件
void Client_DownGroupFile(const char *c_temp)
{
    char temp[2048] = {0};
    strcat(temp, c_temp);
    char *flag = strtok(temp, " ");
    char *Filename = strtok(NULL, " ");
    char *content = strtok(NULL, "\0");

    char temp_Filename[100] = {0};
    sprintf(temp_Filename, "/home/lvguanzhong/桌面/chat/AA-client/File/%s", Filename);
    FILE *file = fopen(temp_Filename, "r");
    if (file != NULL)
    {
        printf("下载失败！（有相同的文件名）！！\n");
        fclose(file);
        return;
    }

    file = fopen(temp_Filename, "w");
    fwrite(content, strlen(content), 1, file);
    fclose(file);
    printf("成功下载文件 %s !\n", Filename);
}

// 客户端下载备份
void Client_DownloadArchive(const char *c_temp)
{
    char temp[2048] = {0};
    strcat(temp, c_temp);
    char *flag = strtok(temp, " ");
    char *Filename = strtok(NULL, " ");
    char *Username = strtok(NULL, " ");
    char *content = strtok(NULL, "\0");

    char temp_Filename[100] = {0};
    sprintf(temp_Filename, "AA-client/Save/%s/%s", Username, Filename);
    FILE *file = fopen(temp_Filename, "w");
    fwrite(content, strlen(content), 1, file);
    fclose(file);

    if (strcmp(Filename, "player.txt") == 0)
        printf("——> 玩家数据下载成功！\n");
    else if (strcmp(Filename, "treasure.txt") == 0)
        printf("——> 宝箱数据下载成功！\n");
    else if (strcmp(Filename, "monsters.txt") == 0)
        printf("——> 野怪数据下载成功！\n");
    else
        printf("——> 血包数据下载成功！\n");
}

// 客户端线程函数
void Client_FuncThread(TcpC *tcpc, int *flag_New, char *C_NewName)
{
    while (1)
    {
        char c_temp[2050] = {0};
        TcpclientRecv(tcpc, c_temp, sizeof(c_temp));
        switch (*c_temp)
        {
        case 'c':
            Client_VIPMessage(c_temp);
            break;
        case 's':
            Client_VIPALLMessage(c_temp);
            break;
        case 'F':
            Client_SeeFriends(c_temp);
            break;
        case 'G':
            Client_SeeGroups(c_temp);
            break;
        case 'H':
            Client_GroupMessage(c_temp);
            break;
        case 'I':
            Client_RecvMessage2(c_temp);
            break;
        case 'L':
            Client_LookAllMessage(c_temp);
            break;
        case 'M':
            ReceiveMessages(c_temp);
            break;
        case 'N':
            Client_NewFriend(c_temp, flag_New, C_NewName);
            break;
        case 'O':
            Client_RecvMessage(c_temp);
            break;
        case 'Q':
            Client_UserPermissions(c_temp, flag_New);
            break;
        case 'S':
            Client_SeeGroupFriends(c_temp);
            break;
        case 'X':
            Client_LookMessage(c_temp);
            break;
        case 'Z':
            Client_LoolGroupMessage(c_temp);
            break;
        case '?':
            Client_See_GroupFile(c_temp);
            break;
        case '!':
            Client_DownGroupFile(c_temp);
            break;
        case ';':
            Client_DownloadArchive(c_temp);
            break;
        case '*':
            return;
        default:
            break;
        }
    }
}
