#ifndef __STDTCP_H__
#define __STDTCP_H__

#include <stddef.h>

struct TcpServer;
typedef struct TcpServer TcpS;

TcpS *InitTcpServer(const char *ip, short int port);
int TcpServerAccept(TcpS *s);
void TcpServerSend(int ClientSock, void *ptr, size_t size);
void TcpServerRecv(int ClientSock, void *ptr, size_t size);
void ClearTcpServer(TcpS *s);

struct TcpClient;
typedef struct TcpClient TcpC;

TcpC *InitTcpClient(const char *Serverip, short int Serverport);
void TcpClientSend(TcpC*c, void *ptr, size_t size);
void TcpclientRecv(TcpC*c, void *ptr, size_t size);
void ClearTcpClient(TcpC *c);



#endif