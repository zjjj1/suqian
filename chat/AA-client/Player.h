#ifndef __PLAYER_H__
#define __PLAYER_H__

#include "Content.h"


// 手册大全
void Book();
/* 个人属性 */
void Attribute(struct Player *player);
//玩家升级
void LevelUp(struct Player *player);
// 吃血包
void Drug(struct Player *player);
// 吃金币
void Gold(struct Player *player);
/* 商店 */
void Shop(struct Player *player);
/* 商人 */
int Merchant(struct Player *player);
// 卖物品
void Sell(struct Player *player);



#endif