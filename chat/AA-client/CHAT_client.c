#include "SysTheamHard.h"

int flag_New = false;
char C_NewName[30] = {0};

void *Thread_Cli_handler(void *arg)
{
    TcpC *tcpc = (TcpC *)arg;
    Client_FuncThread(tcpc,&flag_New,C_NewName);
}

int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        printf("重新输入！\n");
        return -1;
    }
    // 绑定服务端
    TcpC *tcpc = InitTcpClient(argv[1], atoi(argv[2]));
    if (tcpc == NULL)
    {
        printf("tcpc is NULL!\n");
        ClearTcpClient(tcpc);
        return -1;
    }
    // 保存用户名
    char UserName[30] = {0};
    // 登录系统
    Login(UserName, tcpc);
    // 创建线程接收服务端发送数据
    Thread *t = InitThread(Thread_Cli_handler, tcpc);
    // AA聊天系统
    AA_Chat(UserName, tcpc, t, &flag_New, C_NewName);

    return 0;
}