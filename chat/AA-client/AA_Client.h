#ifndef __CLIENT_H__
#define __CLIENT_H__

#include "StdTcp.h"
#include "StdThread.h"

// 给好友发消息
void SendMessage(const char *UserName, TcpC *tcpc);

// 请求查看好友列表
void See_Friends(const char *UserName, TcpC *tcpc);

// 请求查看群聊列表
void See_Groups(const char *UserName, TcpC *tcpc);

// 请求查看好友是否在线
void Friends_isOnline(const char *UserName, TcpC *tcpc);

// 添加好友请求
void AddFriends(const char *UserName, TcpC *tcpc);

// 删除好友请求
void DeleteFriends(const char *UserName, TcpC *tcpc);

// 查看新消息
void NewMessage(const char *UserName, TcpC *tcpc);

// 查看好友申请
void NewFriend(const char *UserName, TcpC *tcpc, int *flag, char *fromName);

// 消息系统
void MessageList(const char *UserName, TcpC *tcpc, int *flag, char *fromName);

// 查看聊天记录
void Look_AllMessage(const char *UserName, TcpC *tcpc);

// 好友系统
void Friends(const char *UserName, TcpC *tcpc);

// 请求查看群权限
int GroupPermissions(const char *UserName, TcpC *tcpc);

// 退出用户
void Exit_Client(const char *UserName, TcpC *tcpc, Thread *t);

// 群发消息
void SendGroupMessage(int GroupID, const char *UserName, TcpC *tcpc);

// 查看群消息
void See_GroupMessage(int GroupID, TcpC *tcpc);

// 查看群成员
void See_Groupfriends(int GroupID, TcpC *tcpc);

// 拉好友入群
void InviteFriend(const char *UserName, TcpC *tcpc, int GroupID);

// 成员禁言
void MemberSilence(const char *UserName, TcpC *tcpc, int GroupID, int Per);

// 踢出群成员
void KickPeople(const char *UserName, TcpC *tcpc, int GroupID, int Per);

// 设置群管理
void AssignGroupManagement(const char *UserName, TcpC *tcpc, int GroupID);

// 转让群主
void AssignGroupLeader(const char *UserName, TcpC *tcpc, int GroupID);

// 解散群聊
void RemoveGroup(TcpC *tcpc, int GroupID);

// 退出群聊
void QuitGroup(const char *UserName, TcpC *tcpc, int GroupID);

// 上传群文件
void UploadGroupFile( TcpC *tcpc);

// 查看群文件
void See_GroupFile( TcpC *tcpc);

// 群聊管理系统
void ManageGroup(const char *UserName, TcpC *tcpc, int *flag);

// 搜索群聊加入
void SearchGroup(const char *UserName, TcpC *tcpc);

// 创建群聊
void CreateGroup(const char *UserName, TcpC *tcpc);

// 群聊系统
void Groups(const char *UserName, TcpC *tcpc, int *flag);

// VIP给用户发消息
void Any_SendMessage(const char *UserName, TcpC *tcpc);

// VIP给用户群发消息
void Any_ALLSendMessage(const char *UserName, TcpC *tcpc);

// VIP系统
void VIPUser(const char *UserName, TcpC *tcpc, int *flag);

// 单机版
void OnlyOne(const char *UserName, TcpC *tcpc, int *flag);

// 你的世界
void MyTowe(const char *UserName, TcpC *tcpc, int *flag);

// 游戏系统
void PlayGame(const char *UserName, TcpC *tcpc, int *flag);

// AA聊天系统
void AA_Chat(const char *UserName, TcpC *tcpc, Thread *t, int *flag, char *fromName);



// 在线用户来消息提醒
void Client_RecvMessage(const char *c_temp);

// 输出好友列表
void Client_SeeFriends(const char *c_temp);

// 用户接收消息
void ReceiveMessages(const char *c_temp);

// 处理用户新消息
void Client_LookMessage(const char *c_temp);

//处理用户与好友的聊天记录
void Client_LookAllMessage(const char *c_temp);

//处理用户新加好友
void Client_NewFriend(const char *c_temp, int *flag, char *fromName);

// 显示群列表
void Client_SeeGroups(const char *c_temp);

// 查看群权限
void Client_UserPermissions(const char *c_temp, int *flag);

//显示群聊成员
void Client_SeeGroupFriends(const char *c_temp);

// 在线用户来消息提醒
void Client_RecvMessage2(const char *c_temp);

// 处理群聊天记录
void Client_GroupMessage(const char *c_temp);

// 处理群聊消息
void Client_LoolGroupMessage(const char *c_temp);

// 处理VIP用户消息
void Client_VIPMessage(const char *c_temp);

// 处理VIP用户群发消息
void Client_VIPALLMessage(const char *c_temp);

// 客户端查看群文件
void Client_See_GroupFile(const char *c_temp);

// 客户端下载群文件
void Client_DownGroupFile(const char *c_temp);

// 客户端下载备份
void Client_DownloadArchive(const char *c_temp);

// 客户端线程函数
void Client_FuncThread(TcpC *tcpc, int *flag_New, char *C_NewName);



#endif