#ifndef __STDTHREAD_H__
#define __STDTHREAD_H__

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

struct StdThread;
typedef struct StdThread Thread;

// 初始化线程
Thread *InitThread(void *(func)(void *), void *arg);
// 合并线程
void *JoinThread(Thread *t);
// 分离线程
void DetachThread(Thread *t);
// 取消线程
void CancelThread(Thread *t);
// 获取线程号
unsigned long int GetThreadId(Thread *t);
// 清理线程
void ClearThread(Thread *t);

#endif