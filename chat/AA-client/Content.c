#include "SysTheamHard.h"
#include "Content.h"

void Initplayer(struct Player *player)
{
    int goods[Goods_size] = {0};
    Createplayer(player, 100, 150, 10, 5, 0, 0, 1, 0, 0, 0, 0, goods);
}

void InitBloodPacks(DTSZ *array)
{
    if (AInty(array) == false)
    {
        return;
    }
    for (int i = 0; i < Map_Size * Map_Size / 10; i++)
    {
        InsertArray(array, CreateBloodPacks(1, 0));
    }
    for (int i = 0; i < Map_Size * Map_Size / 10; i++)
    {
        InsertArray(array, CreateBloodPacks(0, 1));
    }
}

void InitMonsters(DTSZ *array)
{
    if (AInty(array) == false)
    {
        return;
    }
    InsertArray(array, CreateMonster("风暴龙王", 100, 25, 40));
    InsertArray(array, CreateMonster("皮皮虾", 50, 15, 15));
    InsertArray(array, CreateMonster("皮皮虾", 50, 15, 15));
    InsertArray(array, CreateMonster("皮皮虾", 50, 15, 15));
    InsertArray(array, CreateMonster("皮皮虾", 50, 15, 15));
    InsertArray(array, CreateMonster("皮皮虾", 50, 15, 15));
    InsertArray(array, CreateMonster("丘丘人", 20, 10, 5));
    InsertArray(array, CreateMonster("丘丘人", 20, 10, 5));
    InsertArray(array, CreateMonster("丘丘人", 20, 10, 5));
    InsertArray(array, CreateMonster("丘丘人", 20, 10, 5));
    InsertArray(array, CreateMonster("丘丘人", 20, 10, 5));
    InsertArray(array, CreateMonster("丘丘人", 20, 10, 5));
    InsertArray(array, CreateMonster("丘丘人", 20, 10, 5));
    InsertArray(array, CreateMonster("丘丘人", 20, 10, 5));
    InsertArray(array, CreateMonster("丘丘人", 20, 10, 5));
    InsertArray(array, CreateMonster("丘丘人", 20, 10, 5));
}

void InitTreasures(DTSZ *array)
{
    if (AInty(array) == false)
    {

        return;
    }

    InsertArray(array, CreateTreasure(5));
    InsertArray(array, CreateTreasure(3));
    InsertArray(array, CreateTreasure(3));
    InsertArray(array, CreateTreasure(3));
    InsertArray(array, CreateTreasure(1));
    InsertArray(array, CreateTreasure(1));
    InsertArray(array, CreateTreasure(1));
    InsertArray(array, CreateTreasure(1));
    InsertArray(array, CreateTreasure(1));
    InsertArray(array, CreateTreasure(1));
    InsertArray(array, CreateTreasure(1));
    InsertArray(array, CreateTreasure(1));
}

void InitPlayerGoods(struct Player *player)
{
    strcpy(player->GoodsName[7], "龙肉");
    strcpy(player->GoodsName[8], "龙心");
    strcpy(player->GoodsName[9], "龙骨");
    strcpy(player->GoodsName[10], "包菜");
    strcpy(player->GoodsName[11], "虾虾皮");
    strcpy(player->GoodsName[0], "绷带");
    strcpy(player->GoodsName[2], "急救包");
    strcpy(player->GoodsName[4], "医疗箱");
    strcpy(player->GoodsName[1], "能量饮料");
    strcpy(player->GoodsName[3], "止痛药");
    strcpy(player->GoodsName[5], "肾上腺素");
    strcpy(player->GoodsName[6], "不可描述之物");
}

void Createplayer(struct Player *player, int HP, int Max_HP, int attack, int defense, int crit, int experience, int level, int x, int y, int money, int shovel, int a[Goods_size])
{
    player->HP = HP;
    player->Max_HP = Max_HP;
    player->attack = attack;
    player->defense = defense;
    player->crit = crit;
    player->level = level;
    player->experience = experience;
    player->x = x;
    player->y = y;
    player->money = money;
    player->shovel = shovel;
    for (int i = 0; i < Goods_size; i++)
        player->GoodsNum[i] = a[i];
}

struct BloodPacks *CreateBloodPacks(int blood, int gold)
{
    struct BloodPacks *BloodPacks = (struct BloodPacks *)malloc(sizeof(struct BloodPacks));
    if (BloodPacks == NULL)
    {
        printf("Creat BloodPacks error!\n");
        return NULL;
    }

    BloodPacks->blood = blood;
    BloodPacks->gold = gold;
    if (gold == 1)
    {
        do
        {
            BloodPacks->x = GetRandNumber(Map_Size);
            BloodPacks->y = GetRandNumber(Map_Size);
        } while ((BloodPacks->x == 0 && BloodPacks->y == 0) ||
                 (BloodPacks->x == Map_Size - 1 && BloodPacks->y == 0) ||
                 (BloodPacks->x == Map_Size - 1 && BloodPacks->y == Map_Size - 1));
    }
    do
    {
        BloodPacks->x = GetRandNumber(Map_Size);
        BloodPacks->y = GetRandNumber(Map_Size);
    } while ((BloodPacks->x == 0 && BloodPacks->y == 0) ||
             (BloodPacks->x == Map_Size - 1 && BloodPacks->y == 0));
    return BloodPacks;
}

struct Monster *CreateMonster(const char *name, int HP, int attack, int experience)
{
    struct Monster *monster = (struct Monster *)malloc(sizeof(struct Monster));
    if (monster == NULL)
    {
        printf("Creat monster error!\n");
        return NULL;
    }
    monster->level = 1;
    monster->HP = HP;
    monster->attack = attack;
    monster->experience = experience;
    Initialize(&monster->name, name);
    do
    {
        monster->x = GetRandNumber(Map_Size);
        monster->y = GetRandNumber(Map_Size);
    } while ((monster->x == 0 && monster->y == 0) ||
             (monster->x == Map_Size - 1 && monster->y == 0) ||
             (monster->x == Map_Size - 1 && monster->y == Map_Size - 1));
    if (monster->experience == 40)
    {
        monster->x = Map_Size - 1;
        monster->y = Map_Size - 1;
    }
    return monster;
}

struct Treasure *CreateTreasure(int value)
{
    struct Treasure *Treasure = (struct Treasure *)malloc(sizeof(struct Treasure));
    if (Treasure == NULL)
    {
        printf("Creat Treasure error!\n");
        return NULL;
    }

    Treasure->value = value;
    do
    {
        Treasure->x = GetRandNumber(Map_Size);
        Treasure->y = GetRandNumber(Map_Size);
    } while ((Treasure->x == 0 && Treasure->y == 0) ||
             (Treasure->x == Map_Size - 1 && Treasure->y == 0) ||
             (Treasure->x == Map_Size - 1 && Treasure->y == Map_Size - 1));
    if (Treasure->value == 5)
    {
        Treasure->x = Map_Size - 1;
        Treasure->y = Map_Size - 1;
    }

    return Treasure;
}

void InitAll(DTSZ *bloodpacks, DTSZ *monsters, DTSZ *treasures)
{
    InitBloodPacks(bloodpacks);
    InitMonsters(monsters);
    InitTreasures(treasures);
}

void Initboard(char (*p)[Map_Size])
{
    for (int i = 0; i < Map_Size; i++)
    {
        for (int j = 0; j < Map_Size; j++)
        {
            p[i][j] = '-';
        }
    }
}

void PrintMap(char (*p)[Map_Size], struct Player *player, const char *name)
{
    Initboard(p);
    p[Map_Size - 1][Map_Size - 1] = '!';
    p[0][Map_Size - 1] = '$';
    p[player->y][player->x] = 'I';

    printf("      ");
    for (int i = 0; i < Map_Size; i++)
    {
        printf(" \033[37m%4d\033[0m  ", i + 1);
    }
    printf("\n");

    for (int i = 0; i < Map_Size; i++)
    {
        printf(" \033[37m%4d\033[0m ", i + 1);
        for (int j = 0; j < Map_Size; j++)
        {
            printf(" \033[37m%4c\033[0m  ", p[i][j]);
        }
        printf("\n");
    }
    printf("\n\033[1;37m用户@\033[1;3;33m %s \033[0;1;37m,你当前所在的位置是:<%d,%d>\033[0m\n", name, player->x + 1, player->y + 1);
}

int GetRandNumber(int max)
{
    return rand() % max;
}

void FREE(DTSZ *bloodpacks, DTSZ *monsters, DTSZ *treasures)
{
    for (int i = 0; i < bloodpacks->len; i++)
    {
        free((struct BloodPacks *)bloodpacks->dp[i]);
    }
    free(bloodpacks->dp);
    bloodpacks->len = 0;
    for (int i = 0; i < monsters->len; i++)
    {
        struct Monster *monster = (struct Monster *)monsters->dp[i];
        free(monster->name.string);
        free(monster);
    }
    free(monsters->dp);
    monsters->len = 0;
    for (int i = 0; i < treasures->len; i++)
    {
        free((struct Treasure *)treasures->dp[i]);
    }
    free(treasures->dp);
    treasures->len = 0;
}

int Qite()
{
    printf("\033[37m是否确定退出?   (再次输入'p'确定退出）（建议保存后在退出)\033[0m\n");
    while (1)
    {
        char choice;
        scanf("%c", &choice);
        while ((getchar()) != '\n')
            ;
        if (choice == 'p')
            return true;
        return false;
    }
}

void PLAY(char (*board)[Map_Size], struct Player *player, DTSZ *bloodpacks, DTSZ *monsters, DTSZ *treasures, const char *UserName)
{
    while (1)
    {
        for (int i = 0; i < monsters->len; i++)
        {
            struct Monster *monster = (struct Monster *)monsters->dp[i];
            if (monster->HP > 0 && monster->x == player->x && monster->y == player->y && player->HP > 0)
            {
                Battle(player, monster);
                break;
            }
        }

        for (int i = 0; i < bloodpacks->len; i++)
        {
            struct BloodPacks *bloodpack = (struct BloodPacks *)bloodpacks->dp[i];
            if (bloodpack->x == player->x && bloodpack->y == player->y && bloodpack->blood != 0)
            {
                Drug(player);
                bloodpack->blood = 0;
            }
        }

        if (player->HP == 0)
            return;

        PrintMap(board, player, UserName);

        for (int i = 0; i < treasures->len; i++)
        {
            struct Treasure *treasure = (struct Treasure *)treasures->dp[i];
            if (treasure->x == player->x && treasure->y == player->y && treasure->value != 0)
            {
                OpenBox(player, treasure);
            }
        }

        for (int i = 0; i < bloodpacks->len; i++)
        {
            struct BloodPacks *bloodpack = (struct BloodPacks *)bloodpacks->dp[i];
            if (bloodpack->x == player->x && bloodpack->y == player->y && bloodpack->gold != 0)
            {
                Gold(player);
                bloodpack->gold = 0;
            }
        }

        if (player->x == Map_Size - 1 && player->y == 0)
        {
            if (Merchant(player) == true)
                PrintMap(board, player, UserName);
        }

        if (player->shovel == false)
        {
            printf("\033[1;37m请选择你要进行的操作:\033[0;37m(移动:输入\033[1;37mw,a,s,d\033[0m)(打开背包:输入\033[1;37mb\033[0m)(查看手册:输入\033[1;37mm\033[0m)(查看属性:输入\033[1;37mv\033[0m)(存档:输入\033[1;37mr\033[0m)(退出游戏:输入\033[1;37mp\033[0m)\033[0m\n");
            char choice;
            scanf("%c", &choice);
            while ((getchar()) != '\n')
                ;
            if (choice == 'p')
                if (Qite() == true)
                    return;
            MakeMove(player, choice, bloodpacks, monsters, treasures, UserName);
        }
        else
        {
            printf("\033[1;37m请选择你要进行的操作:\033[0;37m(移动:输入\033[1;37mw,a,s,d\033[0m)(打开背包:输入\033[1;37mb\033[0m)(查看手册:输入\033[1;37mm\033[0m)(查看属性:输入\033[1;37mv\033[0m)(存档:输入\033[1;37mr\033[0m)(退出游戏:输入\033[1;37mp\033[0m)\033[0m\n");
            printf("\033[1;33m输入'f'使用 铲子 进入下一层\033[0m\n");
            char choice;
            scanf("%c", &choice);
            while ((getchar()) != '\n')
                ;

            if (choice == 'p')
                if (Qite() == true)
                    return;
            if (choice == 'f')
                Next(player, bloodpacks, monsters, treasures);
            else
                MakeMove(player, choice, bloodpacks, monsters, treasures, UserName);
        }
    }
}

void Next(struct Player *player, DTSZ *bloodpacks, DTSZ *monsters, DTSZ *treasures)
{
    player->shovel = false;
    player->x = 0;
    player->y = 0;
    printf("\033[1;3;37m正在使用钻石铲挖向下一层......\033[0m\n");
    sleep(2);
    struct Monster *mon = (struct Monster *)monsters->dp[0];
    int floor = mon->level + 1;
    FREE(bloodpacks, monsters, treasures);
    InitAll(bloodpacks, monsters, treasures);
    for (int i = 0; i < monsters->len; i++)
    {
        struct Monster *m = (struct Monster *)monsters->dp[i];
        m->level = floor;
        for (int i = 1; i < floor; i++)
        {
            m->HP += m->HP / 2;
            m->attack += m->attack / 2;
        }
    }
    printf("\033[1;3;37m消耗一把钻石铲,终于挖到了下一层!当前层数——> \033[0;1;37m%d \033[0m!\n\n", floor);
}

void MakeMove(struct Player *player, char symbol, DTSZ *bloodpacks, DTSZ *monsters, DTSZ *treasures, const char *name)
{
    switch (symbol)
    {
    case 'w':
        player->y--;
        break;
    case 's':
        player->y++;
        break;
    case 'a':
        player->x--;
        break;
    case 'd':
        player->x++;
        break;
    case 'b':
        Bag(player);
        break;
    case 'm':
        Book();
        break;
    case 'v':
        Attribute(player);
        break;
    case 'r':
        SaveArchive(name, player, bloodpacks, monsters, treasures);
        break;
    default:
        printf("\033[3;37m————>请重新输入!\033[0m \n");
        break;
    }

    if (player->x < 0)
        player->x = 0;
    if (player->x >= Map_Size)
        player->x = Map_Size - 1;
    if (player->y < 0)
        player->y = 0;
    if (player->y >= Map_Size)
        player->y = Map_Size - 1;
}

void freechar(ElementType value)
{
    free((char *)value);
}
