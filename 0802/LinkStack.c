#include "LinkStack.h"
#include <stdio.h>
#define true 1
#define false 0

int InitStack(lstack *s)
{

    return InitDLlist(&s->stack);
}

void Push(lstack *s, ElementType element)
{
    InsertTail(&s->stack,element);
}

ElementType *Pop(lstack *s)
{
    if(s->stack.len == 0)
    {
        printf("the stack is empty");
        return NULL;
    }
    s->TopElement= s->stack.tail->data;
    RemoveByIndex(&s->stack,s->stack.len-1);
    return &s->TopElement;
}

node *GetTop(lstack *s)
{
    return s->stack.tail;
}

int IsEmpty(lstack *s)
{
    if(s->stack.len == 0)
    {
        return true;
    }
    return false;
}

void StackTravle(lstack *s)
{
    Travel(&s->stack);
}
