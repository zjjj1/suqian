#ifndef __LCCQueue_H__
#define __LCCQueue_H__

#include "DoubleCircleLinklist.h"

struct LinkCircleQueue
{
    DCLlist queue;
    ElementType FrontData;
};

typedef struct LinkCircleQueue LCQueue;

int InitLCQueue(LCQueue *lcq);
void LCQPush(LCQueue *lcq,ElementType element);
ElementType *LCQPop(LCQueue *lcq);

#endif
