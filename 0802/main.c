#include<stdio.h>
#include<stdlib.h>
#include"Queue.h"
#define true 1
#define false 0

int main()
{
    LQueue lq;
    InitLQueue(&lq);
    for(int i=0;i<10;i++)
    {
        QPush(&lq,i+1);
    }
    Travel(&lq.queue);
    while(IsQEmpty(&lq)==false)
    {
        printf("%d ",*LQPop(&lq));
    }
    printf("\n");
    return 0;
}