#ifndef __QUEUE_H__
#define __QUEUE_H__
#include "DoubleLinklist.h"

struct LinkQueue
{
    DLlist queue;
    ElementType FrontData;
};

typedef struct LinkQueue LQueue;

int InitLQueue(LQueue *lq);
void QPush(LQueue *lq,ElementType element);
ElementType *LQPop(LQueue *lq);
int IsQEmpty(LQueue *lq);
node *GetFront(LQueue *lq);
void FreeQueue(LQueue *lq);

#endif