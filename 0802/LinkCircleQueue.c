#include "LinkCircleQueue.h"
#include <stdio.h>


int InitLCQueue(LCQueue *lcq)
{ 
    return InitDCLlist(&lcq->queue);
}

void LCQPush(LCQueue *lcq, ElementType element)
{
    InsertTail(&lcq->queue,element);
}

ElementType *LCQPop(LCQueue *lcq)
{
    if(lcq->queue.len == 0)
    {
        printf("Queue is Empty!\n");
        return NULL;
    }

    lcq->FrontData = lcq->queue.head->data;
    RemoveByIndex(&lcq->queue,0);
    return &lcq->FrontData;
}
