#include "Queue.h"
#include <stdio.h>

#define true 1
#define false 0

int InitLQueue(LQueue *lq)
{ 
    return InitDLlist(&lq->queue);
}

void QPush(LQueue *lq,ElementType element)
{
    InsertTail(&lq->queue,element);
}

ElementType *LQPop(LQueue *lq)
{
    if(lq->queue.len == 0)
    {
        printf("Queue is Empty!\n");
        return NULL;
    }

    lq->FrontData = lq->queue.head->data;
    RemoveByIndex(&lq->queue,0);
    return &lq->FrontData;
}

int IsQEmpty(LQueue *lq)
{
    if(lq->queue.len == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

node *GetFront(LQueue *lq)
{
    return lq->queue.head;
}

void FreeQueue(LQueue *lq)
{
    FreeDLlist(&lq->queue);
}
