#include <stdio.h>
#include <stdlib.h>
#include "DoubleLinklist.h"


int InitDLlist(DLlist *list)
{
    list->head = NULL;
    list->tail = NULL;
    list->len = 0;
    return 0;
}

node *CreatNode(ElementType elemen)
{
    node *newNode = (node *)malloc(sizeof(node));
    if(newNode == NULL)
    {
        printf("CreatNode malloc error!\n");
        return NULL;
    }
    newNode->data = elemen;
    newNode->next = NULL;
    newNode->prev = NULL;
    return newNode;
}


void InsertTail(DLlist *list, ElementType element)
{
    node *newNode = CreatNode(element);
    if(newNode == NULL)
    {
        printf("InsertTail malloc error!\n");
        return;
    }
    if(list->len == 0)
    {
        list->head = newNode;
        list->tail = newNode;
    }
    else
    {
        list->tail->next = newNode;
        newNode->prev = list->tail;
        list->tail = newNode;
    }
    list->len++;
}

void InsertHead(DLlist *list, ElementType element)
{
    node *newNode = CreatNode(element);
    if(newNode == NULL)
    {
        printf("InsertHead malloc error!\n");
        return;
    }
    if(list->len == 0)
    {
        list->head = newNode;
        list->tail = newNode;
    }
    else
    {
        list->head->prev = newNode;
        newNode->next = list->head;
        list->head = newNode;
    }
    list->len++;
}

void RemoveByIndex(DLlist *list, int index)
{
    if( index<0 || index>=list->len ) 
    {
        printf("RemoveByIndex invalid please!\n");
        return;
    }
    if(index == 0)
    {
        if(list->len == 1)
        {
            FreeDLlist(list);
            return;
        }
        node *freeNode = list->head;
        list->head = list->head->next;
        list->head->prev = NULL;
        free(freeNode);
        list->len--;
        return;
    }
    if(index == list->len-1)
    {
        node *freeNode = list->tail;
        list->tail = list->tail->prev;
        list->tail->next = NULL;
        free(freeNode);
        list->len--;
        return;
    }
    node *TravelPoint = list->head;
    while(index>0)
    {
        TravelPoint=TravelPoint->next;
        index--;
    }
    node *PrevNode = TravelPoint->prev;
    node *NextNode = TravelPoint->next;
    PrevNode->next = TravelPoint->next;
    NextNode->prev = TravelPoint->prev;
    free(TravelPoint);
    list->len--;
}

void RemoveByElement(DLlist *list, ElementType element)
{
    // node *TravelPoint = list->head;
    // while(TravelPoint != NULL)
    // {
    //     if(TravelPoint->next->data == element)
    //     {
    //         node *Freenode = TravelPoint->next;
    //         TravelPoint->next = Freenode->next;
    //         free(Freenode);
    //         list->len--;
    //     }
    //     else
    //     {
    //         TravelPoint = TravelPoint->next
    //     }
    // }
    int index = FindFirstByElement(list,element);
    while(index != -1)
    {
        RemoveByIndex(list,index);
        index = FindFirstByElement(list,element);
    }
    
}

int FindFirstByElement(DLlist *list, ElementType element)
{
    int count = 0;
    node *TravelPoint = list->head;
     while(TravelPoint != NULL)
    {
        if(TravelPoint->data == element)
        {
            return count;
        }
        count++;
        TravelPoint = TravelPoint->next;
    }
    return -1;
}

void Travel(DLlist *list)
{
    printf("len:  %d\n",list->len);
    printf("next: ");
    node *TravelPoint = list->head;
    while(TravelPoint != NULL)
    {
        printf("%2d ",TravelPoint->data);
        TravelPoint = TravelPoint->next;
    }
    printf("\nprev: ");
    TravelPoint = list->tail;
    while(TravelPoint != NULL)
    {
        printf("%2d ",TravelPoint->data);
        TravelPoint = TravelPoint->prev;
    }
    printf("\n");
}

void FreeDLlist(DLlist *list)
{
    while(list->head != NULL)
    {
        node *freeNode = list->head;
        list->head=list ->head->next;
        free(freeNode);
    }

    list->head = NULL;
    list->tail = NULL;
    list->len = 0;
}
