#ifndef __DOUBLELINKLIST_H__
#define __DOUBLELINKLIST_H__

#define ElementType int

struct Node
{
    ElementType data;
    struct Node *next;
    struct Node *prev;  
};

struct DoubleLinklist
{
    struct Node *head;
    struct Node *tail;
    int len;
};

typedef struct Node node;
typedef struct DoubleLinklist DLlist;

int InitDLlist(DLlist *list);
void InsertTail(DLlist *list,ElementType element);
void InsertHead(DLlist *list,ElementType element);
void RemoveByIndex(DLlist *list,int index);
void RemoveByElement(DLlist *list,ElementType element);
int FindFirstByElement(DLlist *list,ElementType element);
void Travel(DLlist *list);
void FreeDLlist(DLlist *list);

#endif