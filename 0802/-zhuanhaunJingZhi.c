#include<stdio.h>
#include"LinkStack.h"
#define true 1
#define false 0

int main()
{
    lstack stack;
    InitStack(&stack);

    int num = 0;
    printf("please your number:");
    scanf("%d",&num);
    int n = 0;
    printf("please your system number:");
    scanf("%d",&n);
    if(n==16)printf("0x");

    while(num != 0)
    {
        int temp = num % n;
        if(temp <= 9)
        {   temp=temp+'0';  }
        else
        {   temp=temp-10+'A';}
        Push(&stack,temp);
        num = num/n;
    }

    while(IsEmpty(&stack) == false)
    {
        printf("%c",*Pop(&stack));
    }
    printf("\n");
    
    
    return 0;
}