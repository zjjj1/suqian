#include<stdio.h>
#include<stdlib.h>
#include"LinkStack.h"
#define true 1
#define false 0

int IsNuM(char c)
{
    if(c <= '9' && c >= '0')
    {
        return true;
    }
    else
    {
        return false;
    }

}
int IsOper(char c)
{
    switch(c)
    {
        case'+':
        case'-':
        case'*':
        case'/':
        case'(':
        case')':
        case'=':
            return true;
        default:
            return false;
    }
}

int Operate(int prev,int next,char symbol)
{
    switch(symbol)
    {
        case'+':return prev + next;
        case'-':return prev - next;
        case'*':return prev * next;
        case'/':
            if(next == 0)
            {
                printf("devide zero!\n");
                exit(-1);
            }
            return prev / next;
        default:
            break;
    }
}

char Precede(char ch1,char ch2)
{
    char pre[7][7] =
    {
        {'>','>','<','<','<','>','>'},
        {'>','>','<','<','<','>','>'},
        {'>','>','>','>','<','>','>'},
        {'>','>','>','>','<','>','>'},
        {'<','<','<','<','<','=','0'},
        {'>','>','>','>','0','>','>'},
        {'0','0','0','0','0','0','='}
    };

    int i=0,j=0;

    switch(ch1)
    {
        case'+':i=0;break;
        case'-':i=1;break;
        case'*':i=2;break;
        case'/':i=3;break;
        case'(':i=4;break;
        case')':i=5;break;
        case'=':i=6;break;
        default:
            break;
    }
    switch(ch2)
    {
        case'+':j=0;break;
        case'-':j=1;break;
        case'*':j=2;break;
        case'/':j=3;break;
        case'(':j=4;break;
        case')':j=5;break;
        case'=':j=6;break;
        default:
            break;
    }
    return pre[i][j];
}

int IsValid(char *s)
{
    lstack Oper;
    InitStack(&Oper);
    while(*s != '\0')
    {
        if(*s == '('||*s == '['||*s == '{')
        {
            Push(&Oper,*s);
        }
        else if(IsEmpty(&Oper) == true)
        {
            return false;
        }
        else if(*s == ')' && GetTop(&Oper)->data == '('||
                *s == ']' && GetTop(&Oper)->data == '['||
                *s == '}' && GetTop(&Oper)->data == '{')
        {
            Pop(&Oper);
        }
        else
        {
            return false;
        }
        s++;
    }
    return IsEmpty(&Oper);
}

int main()
{
    // lstack Num;
    // InitStack(&Num);
    // lstack Oper;
    // InitStack(&Oper);
    
    // char str[100] = {0};
    // printf("please:\n");
    // scanf("%s",str);
    // int i=0;
    // while(str[i] != '\0')
    // {
    //     if(IsOper(str[i]) == true)
    //     {
    //         if(IsEmpty(&Oper) == true)
    //         {
    //             Push(&Oper,str[i]);
    //             i++;
    //         }
    //         else
    //         {
    //             char symbol = Precede(GetTop(&Oper)->data,str[i]);
    //             switch (symbol)
    //             {
    //                 case'<':
    //                     Push(&Oper,str[i]);
    //                     i++;
    //                     break;
    //                 case'>':
    //                     int next = *Pop(&Num);
    //                     int prev = *Pop(&Num);
    //                     char s = *Pop(&Oper);
    //                     Push(&Num,Operate(prev,next,s));
    //                     break;
    //                 case'=':
    //                     Pop(&Oper);
    //                     i++;
    //                     break;
    //                 default:
    //                     break;
    //             }
    //         }
    //     }
    //     else if(IsNuM(str[i]))
    //     {
    //         int num = str[i] - '0';
    //         Push(&Num,num);
    //         i++;
    //         while(IsNuM(str[i]))
    //         {
    //             int higher = *Pop(&Num);
    //             Push(&Num,higher*10 + str[i] - '0');
    //             i++;
    //         }
    //     }
    //     else if(str[i] == ' ')
    //     {
    //         while(i == ' ')
    //         i++;
    //     }
    // }
    
    // printf("the answer is %d\n",GetTop(&Num)->data);
    // return 0;


    char *s="{(])}";
    printf("%d\n/",IsValid(s));
    return 0;
}