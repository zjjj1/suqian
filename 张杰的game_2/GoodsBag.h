#ifndef __GOODSBAG_H__
#define __GOODSBAG_H__

#include "Content.h"

//使用物品
void UseGoods(struct Player *player);
// 背包
void Bag(struct Player *player);
/* 开箱子 */ 
void OpenBox(struct Player *player, struct Treasure *treasure);

#endif