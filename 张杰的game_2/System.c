#include "SystemHead.h"


void CreateDir(const char *name){
    char temp[100] = {0};
    sprintf(temp, "mkdir %s", name);
    system(temp);
}

void CreateDoc(const char *name){
    char temp[100] = {0};
    sprintf(temp, "touch %s", name);
    system(temp);
}

void DeleteDoc(const char *name){
    char temp[100] = {0};
    sprintf(temp, "rm -f %s", name);
    system(temp);
}

void DeleteDir(const char *name){
    char temp[100] = {0};
    sprintf(temp, "rm -r %s", name);
    system(temp);
}

void Copy(const char *sourceName, const char *targetName){
    char temp[100] = {0};
    sprintf(temp, "cp %s %s", sourceName, targetName);
    system(temp);
}

void GrantPermissions(const char *name, const char *permissions){
    char temp[100] = {0};
    sprintf(temp, "chmod %s %s", permissions, name);
    system(temp);
}
