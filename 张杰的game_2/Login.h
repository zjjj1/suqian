#ifndef __LOGIN_H__
#define __LOGIN_H__

#include "SystemHead.h"

struct User
{
    char UserName[20];
    char Passwd[20];
};
typedef struct User user;

//创建用户结构体
user *CreatUser(const char *name, const char *passwd);
//释放用户结构体
void FreeUser(ElementType use);
//将数据切割并放入链表中
void StrTok(ElementType element, DLlist *UserList);
//登录
void Log(char *name);
//登录匹配
int login(const char *name, const char *passwd, DLlist *UserList);
//注册
int Register(DLlist *UserList);
//打印用户信息
void UsePrint(ElementType element);



#endif
