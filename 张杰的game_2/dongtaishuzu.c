#include "SystemHead.h"

int AInty(DTSZ *array) // dong tai shu zu de chu shi hua
{
    array->dp = (ElType *)malloc(array->size * sizeof(ElType)); // sheng qing dui shang de kongjian
    if (array->dp == NULL)                                      // shi fu sheng qing cheng gong de pan duan
    {
        printf("Init malloc Error!\n"); // sheng qing shiban daying cuowu xingxi
        return false;                   // fanhui false
    }
    return true;
}

void FreeArray(DTSZ *array) // shifang shuzu
{
    if (array->dp != NULL) // panduan shizheng buwei kong ,shifang kongzhizheng huichu cuo
    {
        free(array->dp);
        array->dp = NULL; // fangkong zhihou zhizheng yao zhiweikong
    }
}

int ReallocArray(DTSZ *array) // kuo rong dongtai shu zu
{
    ElType *temp = array->dp;                                       // baocun yuanxian neicun de beifeng zhizheng
    array->dp = (ElType *)malloc(sizeof(ElType) * array->size * 2); // gei dp zhizheng shengqing yikuaixing neicun daxiaowei yaunxian 2bei
    if (array->dp == NULL)                                          // banduan shifou shengqing chenggong
    {
        printf("ReallocArray Error!\n"); // shengqingshi bai dayingcuowu xingxi
        return false;
    }
    for (int i = 0; i < array->len; i++) // bianli yuanxian de shuzu neicun ,jiang meiyige xingyuanxu doukaobei daoxing de neicun zhong
    {
        array->dp[i] = temp[i];
    }
    array->size *= 2;
    free(temp); // shifang yuan xian de neicun koongjian
    return true;
}

int InsertArray(DTSZ *array, ElType element)
{
    if (array->len == array->size) // shuzu yiman
    {
        if (ReallocArray(array) == false) // panduan kuorong shifu shiban
        {
            printf("can not contain more elements!\n"); // kuo rong shiban daying cuo wu xongxi fanhui false
            return false;
        }
    }

    array->dp[array->len] = element; // kuo rong chenggong huozhe shuzumeiman zaishuzu de di len ge weizhi ,fushang womeng yuansu de zhi
    array->len++;                    // jisuan shuzu changdu de jishuqi +1
    return true;
}
