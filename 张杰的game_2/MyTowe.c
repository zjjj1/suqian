#include "SystemHead.h"

int main()
{
    // 创建棋盘并初始化
    char board[Map_Size][Map_Size];
    Initboard(board);
    // 定义循环标志符
    int Start = false;
    // 创建玩家，血包，野怪，宝箱
    struct Player player;
    InitPlayerGoods(&player);
    DTSZ bloodpacks = {NULL, 20, 0};
    DTSZ monsters = {NULL, 20, 0};
    DTSZ treasures = {NULL, 20, 0};
    // 保存用户名
    char UserName[20] = {0};
    while (1)
    {
        while (Start == false)
        { // 登录系统
            Log(UserName);
            // 读档系统
            ReadSave(UserName, &player, &bloodpacks, &monsters, &treasures, &Start);
        }
        // 游戏系统
        PLAY(board, &player, &bloodpacks, &monsters, &treasures, UserName);
        // 重新开始游戏
        GaveAgain(UserName, &player, &bloodpacks, &monsters, &treasures, &Start);
    }
    //释放数组
    FREE(&bloodpacks, &monsters, &treasures);
    return 0;
}