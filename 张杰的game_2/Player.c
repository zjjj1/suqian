#include "SystemHead.h"

void Book()
{
    printf("\n");
    printf("-----------------------------------------\n");
    printf("                \033[1;3;37m手册大全\033[0m                 \n");
    printf("-----------------------------------------\n");
    printf("\033[37m一本解决你所有的烦心事\n");
    printf("***\033[1m\033[37m1、怪物手册\033[0m***\n");
    printf("\033[1;33m龙肉:\033[0m血量回满\033[37m        (击败风暴龙王获得)\n");
    printf("\033[1;33m龙心:\033[0m攻击力增加33%%\033[37m   (击败风暴龙王概率获得)\n");
    printf("\033[1;33m龙骨:\033[0m防御力增加33%%\033[37m   (击败风暴龙王概率获得)\n");
    printf("\033[1;32m包菜:\033[0m攻击力增加1\033[37m     (击败丘丘人概率获得)\n");
    printf("\033[1;32m虾虾皮:\033[0m防御力增加1\033[37m   (击败皮皮虾概率获得)\n");
    printf("***\033[1m\033[37m2、宝箱手册\033[0m***\n");
    printf("\033[1;31m绷带:\033[0m血量回复30;        \033[1;31m急救包:\033[0m血量回复70;    \033[1;31m医疗箱:\033[0m血量回复100;\n");
    printf("\033[1;34m能量饮料:\033[0m经验增加15;    \033[1;34m止痛药:\033[0m经验增加30;    \033[1;34m肾上腺素:\033[0m经验增加60;\n");
    printf("\033[1;35m不可描述之物:\033[0m听说商人喜欢这玩意...\n");
    printf("\n");
    printf("输入'\033[1;37ml\033[0m'返回\n");
    printf("------------------------------------------\n");
    while (1)
    {
        char choice;
        scanf(" %c", &choice);
        while ((getchar()) != '\n')
            ;
        switch (choice)
        {
        case 'l':
            return;
        default:
            printf("\033[3;37m————>请重新输入!\033[0m \n");
            break;
        }
    }
}

void Attribute(struct Player *player)
{
    printf("\n");
    printf("-----------------------------------------\n");
    printf("                个人属性                  \n");
    printf("-----------------------------------------\n");
    printf("当前血量:%d\n", player->HP);
    printf("血量上限:%d\n", player->Max_HP);
    printf("当前攻击力%d\n", player->attack);
    printf("当前防御力%d\n", player->defense);
    printf("当前暴击率%d\n", player->crit);
    printf("当前经验值%d%%\n", player->experience);
    printf("当前等级%d\n", player->level);
    printf("摩拉:%d\n", player->money);
    printf("铲子:%d\n", player->shovel);
    printf("\n\n\n");
    printf("输入'l'返回\n");
    printf("------------------------------------------\n");
    while (1)
    {
        char choice;
        scanf("%c", &choice);
        while ((getchar()) != '\n')
            ;
        switch (choice)
        {
        case 'l':
            return;
        default:
            printf("\033[3;37m————>请重新输入!\033[0m \n");
            break;
        }
    }
}

void LevelUp(struct Player *player)
{
    if (player->experience >= 100)
    {
        player->level++;
        player->experience -= 100;
        player->attack += player->attack / 5;
        player->defense += player->defense / 5;
        player->crit += 10;
        if (player->crit > 100)
            player->crit == 10;
        player->Max_HP += player->Max_HP / 10;
        player->HP += 20;
        if ((player->HP > player->Max_HP))
            player->HP = player->Max_HP;
        if (player->level == 2)
            printf("恭喜您!升级了呢,属性提升了一大波,目前等级%d,您真厉害!!!\n", player->level);
        else
            printf("恭喜您!又升级了呢,属性又提升了一大波,目前等级%d,您真厉害!!!\n", player->level);
    }
}

void Drug(struct Player *player)
{
    player->HP += player->Max_HP / 10;
    if (player->HP > player->Max_HP)
        player->HP = player->Max_HP;
    if (player->HP == player->Max_HP / 10)
        printf("恭喜您,您吃到了血包,起死回生! 恢复了10%%的血量,您当前的血量是 %d. \n", player->HP);
    else
        printf("您吃到了血包,恢复了10%%的血量,您当前的血量是 %d. \n", player->HP);
}

void Gold(struct Player *player)
{
    int num = 0;
    if (GetRandNumber(10) == 9)
    {
        num = GetRandNumber(200) + 800;
        player->money += num;
        printf("恭喜你！捡到了富婆遗留在这里的%d巨款\n当前摩拉余额:%d\n", num, player->money);
    }
    if (GetRandNumber(5) == 4)
    {
        num = GetRandNumber(200) + 600;
        player->money += num;
        printf("恭喜你！捡到了巴巴遗留在这里的%d私房钱\n当前摩拉余额:%d\n", num, player->money);
    }
    if (GetRandNumber(3) == 2)
    {
        num = GetRandNumber(200) + 400;
        player->money += num;
        printf("mm突然给你转了%d摩拉,并嘱咐你好好学习！\n当前摩拉余额:%d\n", num, player->money);
    }
    if (GetRandNumber(4) != 3)
    {
        num = GetRandNumber(100) + 300;
        player->money += num;
        printf("巴巴突然给你转了%d摩拉,并告诫你不要拿去鬼混！\n当前摩拉余额:%d\n", num, player->money);
    }
    else
    {
        num = GetRandNumber(300) + 1;
        player->money += num;
        printf("地上掉了300摩拉,一群丘丘人上去疯抢,你抢到了%d\n当前摩拉余额:%d\n", num, player->money);
    }
    if (GetRandNumber(4) == 1)
    {
        player->GoodsNum[6]++;
        printf("你在野外探索时，额外获得了%s!\n", player->GoodsName[6]);
    }
    return;
}

void Shop(struct Player *player)
{
    printf("-----------------------------------------\n");
    printf("                \033[1;3;37m桀桀\033[0m                 \n");
    printf("-----------------------------------------\n");
    printf("小伙子，我看你骨骼惊奇，是块试药的好苗子！我这里有一些药剂，就便宜买给你了！！！\n");
    printf("\n");
    printf("*************************************\n");
    printf("* \e[31;1m$3000 ——>红色药剂（提升血量上限）\e[0m *\n");
    printf("* \e[34;1m$3000 ——>蓝色药剂（提升攻击力）\e[0m   *\n");
    printf("* \e[33;1m$3000 ——>黄色药剂（提升防御力）\e[0m   *\n");
    printf("* \e[37;1m$3000 ——>白色药剂（提升暴击率）\e[0m   *\n");
    printf("*************************************\n");
    printf("\n\n");

    while (1)
    {
        printf("\e[31;1m红:'1'  \e[34;1m蓝:'2'  \e[33;1m黄:'3'   \e[37;1m白:'4'   \e[37;m出售物品:'c'    退出'l'\033[0m\n");
        printf("——————————————————————————————————————————————————————————\n");
        char choice;
        scanf("%c", &choice);
        while ((getchar()) != '\n')
            ;
        switch (choice)
        {
        case '1':
            if (player->money < 3000)
                printf("小伙子，等有钱再来吧！\n");
            else
            {
                printf("血量上限提升了 %d .", player->Max_HP / 10);
                player->Max_HP += player->Max_HP / 10;
                player->money -= 3000;
                printf("当前血量上限 %d ,剩余摩拉 %d\n", player->Max_HP, player->money);
            }
            break;
        case '2':
            if (player->money < 3000)
                printf("小伙子，等有钱再来吧！\n");
            else
            {
                printf("攻击力提升了 %d .", player->attack / 5);
                player->attack += player->attack / 5;
                player->money -= 3000;
                printf("当前攻击力 %d ,剩余摩拉 %d\n", player->attack, player->money);
            }
            break;
        case '3':
            if (player->money < 3000)
                printf("小伙子，等有钱再来吧！\n");
            else
            {
                printf("防御力提升了 %d .", player->defense / 5);
                player->defense += player->defense / 5;
                player->money -= 3000;
                printf("当前防御力 %d ,剩余摩拉 %d\n", player->defense, player->money);
            }
            break;
        case '4':
            if (player->crit == 100)
                printf("当前暴击率已满，无法继续购买！\n");
            else if (player->money < 3000)
                printf("小伙子，等有钱再来吧！\n");
            else
            {
                printf("暴击率提升了 10 .");
                player->crit += 10;
                if (player->crit > 100)
                    player->crit = 100;
                player->money -= 3000;
                printf("当前暴击率 %d ,剩余摩拉 %d\n", player->crit, player->money);
            }
            break;
        case 'c':
            Sell(player);
            break;
        case 'l':
            return;
        default:
            printf("\033[3;37m————>请重新输入!\033[0m \n");
            break;
        }
    }
}

int Merchant(struct Player *player)
{
    printf("\033[1;37m您遇到了黑心商人,是否与他进行交易？    (y/n)\033[0m\n");
    while (1)
    {
        char choice;
        scanf("%c", &choice);
        while ((getchar()) != '\n')
            ;
        switch (choice)
        {
        case 'y':
            Shop(player);
            return true;
        case 'n':
            return false;
        default:
            printf("\033[3;37m————>请重新输入!\033[0m \n");
            break;
        }
    }
}

void Sell(struct Player *player)
{
    if (player->GoodsNum[6] == 0)
        printf("小伙子，你这里什么都没有啊，莫要消遣老夫！\n");
    else
    {
        printf("小伙子，没想到你竟然会有这好东西！！！\n");
        int ml = 0;
        for (int i = 0; i < player->GoodsNum[6]; i++)
            ml += GetRandNumber(500);
        player->GoodsNum[6] = 0;
        player->money += ml;
        printf("黑心商人以%d的价格,买走了你所有的 %s !\n", ml, player->GoodsName[6]);
    }
    return;
}