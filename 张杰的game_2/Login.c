#include "Login.h"

user *CreatUser(const char *name, const char *passwd)
{
    user *use = (user *)malloc(sizeof(user));
    if (use == NULL)
    {
        printf("malloc error!\n");
        return NULL;
    }
    strcpy(use->UserName, name);
    strcpy(use->Passwd, passwd);
    return use;
}

void FreeUser(ElementType use)
{
    user *ues = (user *)use;
    if (ues != NULL)
    {
        free(ues);
    }
}

void StrTok(ElementType element, DLlist *UserList)
{
    char *str = (char *)element;
    char *userName = strtok(str, "=");
    char *passwd = strtok(NULL, "=");
    user *use = CreatUser(userName, passwd);
    DLInsertTail(UserList, use);
}

void Log(char *name)
{
    DLlist UserList;
    InitDLlist(&UserList);
    if (IsFileExist("UserLogin.txt") == true)
    {
        DLlist *list = GetLineFormFile("UserLogin.txt");
        DLTravel(list, StrTok, &UserList);
        FreeDLlist(list, freechar);
        free(list);
    }

    srand(time(NULL));
    system("clear");
    printf("*****************************************************************\n");
    printf("*                      \033[1;37m欢迎来到\033[1;3;37m你的世界\033[1;37m!\033[0m                        *\n");

    char SignIn_flag = false;
    char UserName[20] = {0};
    char PassWd[20] = {0};
    while (SignIn_flag == false)
    {
        printf("*****************************************************************\n");
        printf("*                                                               *\n");
        printf("*                                                               *\n");
        printf("*                                                               *\n");
        printf("*                    \033[1;37m 1、用户登录   \033[0m                            *\n");
        printf("*                    \033[1;37m 2、用户注册   \033[0m                            *\n");
        printf("*                    \033[1;37m 3、退出游戏   \033[0m                            *\n");
        printf("*                                                               *\n");
        printf("*****************************************************************\n");
        printf("\n请输入您的选择!\n");
        printf("——————————————————————————————————————————————————————————————————————\n");
        char choice;
        scanf("%c", &choice);
        while ((getchar()) != '\n')
            ;
        switch (choice)
        {
        case '1':
            printf("请输入用户名\n——————>>");
            scanf("%s", UserName);
            printf("请输入密码\n——————>>");
            scanf("%s", PassWd);
            if (login(UserName, PassWd, &UserList) == true)
            {
                SignIn_flag = true;
                getchar();
            }
            else
            {
                printf("用户名或密码错误！请重新输入！\n");
                getchar();
            }
            break;
        case '2':
            Register(&UserList);
            getchar();
            break;
        case '3':
            exit(0);
        default:
            printf("\033[3;37m————>请重新输入!\033[0m \n");
            break;
        }
    }
    FreeDLlist(&UserList, FreeUser);
    memset(name, 0, 20);
    strcat(name, UserName);
    printf("*****************************************************************\n");
    printf("\033[1;37m————>登录成功!<————  \033[0;37m   欢迎玩家     @%s ! \033[0m  \n", UserName);
    printf("*****************************************************************\n");
}

int login(const char *name, const char *passwd, DLlist *UserList)
{
    node *TravelPoint = UserList->head;
    while (TravelPoint != NULL)
    {
        user *use = (user *)TravelPoint->data;
        if (strcmp(use->UserName, name) == 0 &&
            strcmp(use->Passwd, passwd) == 0)
        {
            return true;
        }
        TravelPoint = TravelPoint->next;
    }
    return false;
}

int Register(DLlist *UserList)
{
    printf("*****************************************************************\n");
    printf("*                                                               *\n");
    printf("*                    \033[1;37m   用户注册   \033[0m                            *\n");
    printf("*                                                               *\n");
    printf("*****************************************************************\n");
    char new_UserName[50] = {0};
    char new_PassWd[50] = {0};

a_username:
    printf("请输入用户名\n——————>>");
    scanf("%s", new_UserName);
    if (strlen(new_UserName) >= 8)
    {
        memset(new_UserName, 0, 50);
        printf("用户名过长,请重新输入!(最大不超过8个字符)\n");
        goto a_username;
    }
    node *TravelPoint = UserList->head;
    while (TravelPoint != NULL)
    {
        user *use = (user *)TravelPoint->data;
        if (strcmp(use->UserName, new_UserName) == 0)
        {
            memset(new_UserName, 0, 50);
            printf("用户名已存在，请重新输入！\n");
            goto a_username;
        }
        TravelPoint = TravelPoint->next;
    }

b_passwd:
    printf("请输入密码\n——————>>");
    scanf("%s", new_PassWd);
    if (strlen(new_PassWd) >= 8)
    {
        memset(new_PassWd, 0, 50);
        printf("密码过长,请重新输入!(最大不超过8个字符)\n");
        goto b_passwd;
    }

    char s[23] = "";
    strcat(s, new_UserName);
    strcat(s, "=");
    strcat(s, new_PassWd);
    strcat(s, "\n");
    AppendToFile("UserLogin.txt", s, strlen(s));

    user *use = CreatUser(new_UserName, new_PassWd);
    DLInsertTail(UserList, use);
    printf("*****************************************************************\n");
    printf("\033[1;37m————>注册成功!<————  \033[0m   \n");
    printf("*****************************************************************\n");
    char s_name[20] = "";
    strcat(s_name, "./Save/");
    strcat(s_name, new_UserName);
    CreateDir(s_name);
}

void UsePrint(ElementType element)
{
    user *use = (user *)element;
    printf("user : %s   passwd : %s\n", use->UserName, use->Passwd);
}
