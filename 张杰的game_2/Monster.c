#include "SystemHead.h"

void Battle(struct Player *player, struct Monster *monster)
{
    int count = 0;
    system("clear");
    printf("您遭遇了  %s 当前等级：|%d|  血量:|%d| 攻击力：|%d|\n", monster->name.string, monster->level, monster->HP, monster->attack);
    printf("***************************************************************\n");
    printf("*                          开始战斗！                         *\n");
    printf("***************************************************************\n");
    while (player->HP > 0 && monster->HP > 0)
    {
        int temp = monster->HP;
        int harm = monster->attack - player->defense;
        if (harm <= 0)
            harm = 0;
        printf("您当前的血量是 %d \n", player->HP);
        printf("请选择您要执行的行为:  1、浅刀一下.  2、全力一刀!  3、打不过,快跑!!\n");
        char choice;
        scanf("%c", &choice);
        while ((getchar()) != '\n')
            ;
        switch (choice)
        {
        case '1':
            count++;
            if (GetRandNumber(100) < player->crit)
            {
                monster->HP -= player->attack * 2;
                printf("\n\033[31m你对 %s 造成了 %d 点普通伤害\e[0m\n", monster->name.string, player->attack * 2);
            }
            else
            {
                monster->HP -= player->attack;
                printf("\n你对 %s 造成了 %d 点普通伤害\n", monster->name.string, player->attack);
            }

            if (monster->HP <= 0)
                break;
            else
            {
                printf("当前怪兽血量剩余%d\n", monster->HP);
                player->HP -= harm;
                printf(" %s 对你造成了 %d 点普通伤害\n", monster->name.string, harm);

                break;
            }

        case '2':
            if (GetRandNumber(3) == 1)
            {
                count++;
                if (GetRandNumber(100) < player->crit)
                {
                    monster->HP -= player->attack * 6;
                    printf("\n\033[31m这一刀开天辟地!!!\n你对 %s 造成了 %d 点巨额伤害\e[0m\n", monster->name.string, player->attack * 6);
                }
                else
                {
                    monster->HP -= player->attack * 3;

                    printf("\n这一刀开天辟地!!!\n你对 %s 造成了 %d 点巨额伤害\n", monster->name.string, player->attack * 3);
                }

                if (monster->HP <= 0)
                    break;
                else
                {
                    printf("当前怪兽血量剩余%d\n", monster->HP);
                    player->HP -= harm;
                    printf(" %s 对你造成了 %d 点普通伤害\n", monster->name.string, harm);
                }
            }

            else
            {
                printf("\n这一刀软弱无力。。。\n");
                player->HP -= harm;
                printf(" %s 对你造成了 %d 点巨额伤害\n", monster->name.string, harm);
            }
            break;

        case '3':
            if (GetRandNumber(2) == 1)
            {
                printf("当前怪兽血量剩余%d\n", monster->HP);
                printf("恭喜你，逃跑成功！\n留得青山在,不怕没柴烧!!\n");
                count = 0;
                return;
            }

            else
            {
                printf("\n就你还想跑? 站着挨打!!\n");
                printf(" %s 对你造成了 %d 点巨额伤害\n", monster->name.string, harm);
                player->HP -= harm;
                printf("当前怪兽血量剩余%d\n", monster->HP);
            }
            break;

        default:
            printf("\033[3;37m————>请重新输入!\033[0m \n");
            break;
        }

        printf("\n=======================================================\n");

        if (player->HP <= 0)
        {
            printf("\n你被 %s 击败了!\nGG啦!!!\n", monster->name.string);
            player->HP = 0;
            return;
        }
        if (monster->HP <= 0)
        {
            monster->HP = 0;
            printf("当前怪兽血量剩余%d\n", monster->HP);
            if (count == 1)
            {
                player->HP += temp / 2;
                if (player->HP > player->Max_HP)
                    player->HP = player->Max_HP;
                printf("\n神之一刀!斩掉 %s 狗头!!\n你当前还有%d血\n", monster->name.string, player->HP);
                count = 0;
            }
            else
            {
                printf("\n恭喜你! 你击败了 %s !\n你当前还有%d血\n", monster->name.string, player->HP);
                count = 0;
            }
            player->experience += monster->experience;
            printf("你获得了%d点经验,当前经验值%d\n", monster->experience, player->experience);
            LevelUp(player);
            MonsterGoods(monster->experience, player);
            return;
        }
    }
}

void MonsterGoods(int monster_experience, struct Player *player)
{
    if (monster_experience == 10)
    {
        if (GetRandNumber(2) == 1)
        {
            player->GoodsNum[10]++;
            printf("恭喜你，你在丘丘人身上找到了一个 %s !\n", player->GoodsName[10]);
        }
        int ml = GetRandNumber(200) + 400;
        player->money += ml;
        printf("你将丘丘人卖给了旅行者，获得了%d摩拉!当前摩拉剩余%d.\n", ml, player->money);
    }
    if (monster_experience == 30)
    {
        int ml = GetRandNumber(200) + 600;
        player->money += ml;
        printf("你在皮皮虾身上找到了%d摩拉!当前摩拉剩余%d.\n", ml, player->money);
        if (GetRandNumber(2) == 1)
        {
            player->GoodsNum[11]++;
            printf("你将皮皮虾肉吃掉后，意外获得了 %s !\n", player->GoodsName[11]);
        }
    }
    if (monster_experience == 70)
    {
        player->shovel=true;
        printf("恭喜你!你击杀了BOSS风暴龙王!!!\n你在龙穴里找到一把可以通往异世界的 钻石铲 !!!! !\n");
        player->GoodsNum[7]++;
        printf("你把巨龙初步解剖，获得了 %s !\n", player->GoodsName[7]);
        if (GetRandNumber(2) == 1)
        {
            player->GoodsNum[8]++;
            printf("你把巨龙再次解剖，获得了 %s !\n", player->GoodsName[8]);
        }
        else
        {
            player->GoodsNum[9]++;
            printf("你把巨龙再次解剖，获得了 %s !\n", player->GoodsName[9]);
        }
        int ml = GetRandNumber(200) + 800;
        player->money += ml;
        printf("你在龙穴找到了%d摩拉!当前摩拉剩余%d.\n", ml, player->money);
    }
    if (GetRandNumber(4) == 1)
    {
        player->GoodsNum[6]++;
        printf("你在野外探索时，额外获得了%s!\n", player->GoodsName[6]);
    }
    return;
}