#ifndef __ARCHIVE_H__
#define __ARCHIVE_H__

#include "Content.h"

// 读取存档
int ReadArchive(const char *name, struct Player *player, DTSZ *bloodpacks, DTSZ *monsters, DTSZ *treasures);
// 读取玩家
void ReadPlayer(const char *name, struct Player *player);
// 读取血包
void ReadBloodPacks(const char *name, DTSZ *bloodpacks);
// 读取野怪
void ReadMonster(const char *name, DTSZ *monsters);
// 读取宝箱
void ReadTreasure(const char *name, DTSZ *treasures);
// 保存存档
void SaveArchive(const char *name, struct Player *player, DTSZ *bloodpacks, DTSZ *monsters, DTSZ *treasures);
// 保存玩家
void SavePlayer(const char *FilePath, struct Player *player);
// 保存血包
void SaveBloodPacks(const char *FilePath, DTSZ *bloodpacks);
// 保存野怪
void SaveMonster(const char *FilePath, DTSZ *monsters);
// 保存宝箱
void SaveTreasure(const char *FilePath, DTSZ *treasures);
//读档存档
void ReadSave(const char *UserName, struct Player *player, DTSZ *bloodpacks, DTSZ *monsters, DTSZ *treasures, int *Start);



#endif