#include "SystemHead.h"
#include "Archive.h"

int ReadArchive(const char *name, struct Player *player, DTSZ *bloodpacks, DTSZ *monsters, DTSZ *treasures)
{
    char s[35] = "";
    strcat(s, "./Save/");
    strcat(s, name);
    strcat(s, "/player.txt");
    if (IsFileExist(s) == false)
    {
        printf("\033[1;37m您当前没有存档!\033[0m\n");
        return false;
    }
    if (AInty(bloodpacks) == false)
    {

        printf("初始化血包数组失败！\n");
        return false;
    }
    if (AInty(monsters) == false)
    {
        printf("初始化怪物数组失败！\n");
        return false;
    }
    if (AInty(treasures) == false)
    {
        printf("初始化宝箱数组失败！\n");
        return false;
    }
    ReadPlayer(name, player);
    sleep(1);
    printf("\033[1;37m玩家数据加载成功...\033[0m\n");
    ReadBloodPacks(name, bloodpacks);
    sleep(1);
    printf("\033[1;37m血包数据加载成功...\033[0m\n");
    ReadMonster(name, monsters);
    sleep(1);
    printf("\033[1;37m野怪数据加载成功...\033[0m\n");
    ReadTreasure(name, treasures);
    sleep(1);
    printf("\033[1;37m宝箱数据加载成功..\033[0m.\n");
    return true;
}

void ReadPlayer(const char *name, struct Player *player)
{
    int pl_HP, pl_Max_HP, pl_attack, pl_defense, pl_crit, pl_experience, pl_level, pl_x, pl_y, pl_money, pl_shovel;
    int goods[Goods_size] = {0};
    char s[35] = "";
    strcat(s, "./Save/");
    strcat(s, name);
    strcat(s, "/player.txt");

    DLlist *list = GetLineFormFile(s);
    node *T = list->head;
    char *str = (char *)T->data;
    goods[0] = atoi(strtok(str, ","));
    for (int i = 1; i < Goods_size; i++)
    {
        goods[i] = atoi(strtok(NULL, ","));
    }
    T = T->next;
    char *str_1 = (char *)T->data;
    pl_HP = atoi(strtok(str_1, ","));
    pl_Max_HP = atoi(strtok(NULL, ","));
    pl_attack = atoi(strtok(NULL, ","));
    pl_defense = atoi(strtok(NULL, ","));
    pl_crit = atoi(strtok(NULL, ","));
    pl_experience = atoi(strtok(NULL, ","));
    pl_level = atoi(strtok(NULL, ","));
    pl_x = atoi(strtok(NULL, ","));
    pl_y = atoi(strtok(NULL, ","));
    pl_money = atoi(strtok(NULL, ","));
    pl_shovel = atoi(strtok(NULL, ","));
    FreeDLlist(list, freechar);
    free(list);
    Createplayer(player, pl_HP, pl_Max_HP, pl_attack, pl_defense, pl_crit, pl_experience, pl_level, pl_x, pl_y, pl_money, pl_shovel, goods);
}

struct BloodPacks *ARCBloodPacks(int blood, int gold, int x, int y)
{
    struct BloodPacks *BloodPacks = (struct BloodPacks *)malloc(sizeof(struct BloodPacks));
    if (BloodPacks == NULL)
    {
        printf("Creat BloodPacks error!\n");
        return NULL;
    }

    BloodPacks->blood = blood;
    BloodPacks->gold = gold;
    BloodPacks->x = x;
    BloodPacks->y = y;
    return BloodPacks;
}

struct Monster *ARCMonster(const char *moname, int HP, int attack, int experience, int level, int x, int y)
{
    struct Monster *monster = (struct Monster *)malloc(sizeof(struct Monster));
    if (monster == NULL)
    {
        printf("Creat monster error!\n");
        return NULL;
    }
    monster->HP = HP;
    monster->attack = attack;
    monster->experience = experience;
    monster->level = level;
    monster->x = x;
    monster->y = y;
    Initialize(&monster->name, moname);

    return monster;
}

struct Treasure *ARCTreasure(int value, int x, int y)
{
    struct Treasure *Treasure = (struct Treasure *)malloc(sizeof(struct Treasure));
    if (Treasure == NULL)
    {
        printf("Creat Treasure error!\n");
        return NULL;
    }

    Treasure->value = value;
    Treasure->x = x;
    Treasure->y = y;
    return Treasure;
}

void ReadBloodPacks(const char *name, DTSZ *bloodpacks)
{
    char s[35] = "";
    strcat(s, "./Save/");
    strcat(s, name);
    strcat(s, "/bloodpacks.txt");
    DLlist *list = GetLineFormFile(s);
    node *T = list->head;
    while (T != NULL)
    {
        int bp_blood = 0, bp_gold = 0, bp_x = 0, bp_y = 0;
        char *str = (char *)T->data;
        bp_blood = atoi(strtok(str, ","));
        bp_gold = atoi(strtok(NULL, ","));
        bp_x = atoi(strtok(NULL, ","));
        bp_y = atoi(strtok(NULL, ","));
        InsertArray(bloodpacks, ARCBloodPacks(bp_blood, bp_gold, bp_x, bp_y));
        T = T->next;
    }
    FreeDLlist(list, freechar);
    free(list);
}

void ReadMonster(const char *name, DTSZ *monsters)
{
    char s[35] = "";
    strcat(s, "./Save/");
    strcat(s, name);
    strcat(s, "/monsters.txt");
    DLlist *list = GetLineFormFile(s);
    node *T = list->head;
    while (T != NULL)
    {
        int mo_HP = 0, mo_attack = 0, mo_experience = 0, mo_level = 0, mo_x = 0, mo_y = 0;
        char *str = (char *)T->data;
        char *mo_Name = strtok(str, ",");
        mo_HP = atoi(strtok(NULL, ","));
        mo_attack = atoi(strtok(NULL, ","));
        mo_experience = atoi(strtok(NULL, ","));
        mo_level = atoi(strtok(NULL, ","));
        mo_x = atoi(strtok(NULL, ","));
        mo_y = atoi(strtok(NULL, ","));
        InsertArray(monsters, ARCMonster(mo_Name, mo_HP, mo_attack, mo_experience, mo_level, mo_x, mo_y));
        T = T->next;
    }
    FreeDLlist(list, freechar);
    free(list);
}

void ReadTreasure(const char *name, DTSZ *treasures)
{
    char s[35] = "";
    strcat(s, "./Save/");
    strcat(s, name);
    strcat(s, "/treasure.txt");
    DLlist *list = GetLineFormFile(s);
    node *T = list->head;
    while (T != NULL)
    {
        int tr_value = 0, tr_x = 0, tr_y = 0;
        char *str = (char *)T->data;
        tr_value = atoi(strtok(str, ","));
        tr_x = atoi(strtok(NULL, ","));
        tr_y = atoi(strtok(NULL, ","));
        InsertArray(treasures, ARCTreasure(tr_value, tr_x, tr_y));
        T = T->next;
    }
    FreeDLlist(list, freechar);
    free(list);
}

void SaveArchive(const char *name, struct Player *player, DTSZ *bloodpacks, DTSZ *monsters, DTSZ *treasures)
{
    printf("正在保存存档！\n");
    char s[20] = "";
    strcat(s, "./Save/");
    strcat(s, name);
    char s1[35] = "";
    strcat(s1, s);
    strcat(s1, "/player.txt");
    SavePlayer(s1, player);

    char s2[35] = "";
    strcat(s2, s);
    strcat(s2, "/bloodpacks.txt");
    SaveBloodPacks(s2, bloodpacks);

    char s3[35] = "";
    strcat(s3, s);
    strcat(s3, "/monsters.txt");
    SaveMonster(s3, monsters);

    char s4[35] = "";
    strcat(s4, s);
    strcat(s4, "/treasure.txt");
    SaveTreasure(s4, treasures);
    sleep(1);
    printf("存档保存成功！\n");
}

void SavePlayer(const char *FilePath, struct Player *player)
{
    FILE *file = fopen(FilePath, "w");
    char str[10] = {0};
    sprintf(str, "%d", player->GoodsNum[0]);
    fputs(str, file);
    for (int i = 1; i < Goods_size; i++)
    {
        char str_1[10] = {0};
        sprintf(str_1, ",%d", player->GoodsNum[i]);
        fputs(str_1, file);
    }
    char str_2[100] = {0};
    sprintf(str_2, "\n%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d", player->HP, player->Max_HP, player->attack, player->defense, player->crit, player->experience, player->level, player->x, player->y, player->money, player->shovel);
    fputs(str_2, file);
    fclose(file);
}

void SaveBloodPacks(const char *FilePath, DTSZ *bloodpacks)
{
    FILE *file = fopen(FilePath, "w");
    for (int i = 0; i < bloodpacks->len; i++)
    {
        struct BloodPacks *bloodpack = (struct BloodPacks *)bloodpacks->dp[i];
        char str[100] = {0};
        sprintf(str, "%d,%d,%d,%d\n", bloodpack->blood, bloodpack->gold, bloodpack->x, bloodpack->y);
        fputs(str, file);
    }
    fclose(file);
}

void SaveMonster(const char *FilePath, DTSZ *monsters)
{
    FILE *file = fopen(FilePath, "w");
    for (int i = 0; i < monsters->len; i++)
    {
        struct Monster *monster = (struct Monster *)monsters->dp[i];
        char str[100] = {0};
        sprintf(str, "%s,%d,%d,%d,%d,%d,%d\n", monster->name.string, monster->HP, monster->attack, monster->experience, monster->level, monster->x, monster->y);
        fputs(str, file);
    }
    fclose(file);
}

void SaveTreasure(const char *FilePath, DTSZ *treasures)
{
    FILE *file = fopen(FilePath, "w");
    for (int i = 0; i < treasures->len; i++)
    {
        struct Treasure *treasure = (struct Treasure *)treasures->dp[i];
        char str[100] = {0};
        sprintf(str, "%d,%d,%d\n", treasure->value, treasure->x, treasure->y);
        fputs(str, file);
    }
    fclose(file);
}

void ReadSave(const char *UserName, struct Player *player, DTSZ *bloodpacks, DTSZ *monsters, DTSZ *treasures, int *Start)
{
    int start = false;
    while (start == false)
    {

        printf("*****************************************************************\n");
        printf("*                                                               *\n");
        printf("*                                                               *\n");
        printf("*                                                               *\n");
        printf("*                    \033[1;37m 1、开始游戏   \033[0m                            *\n");
        printf("*                    \033[1;37m 2、加载存档   \033[0m                            *\n");
        printf("*                    \033[1;37m 3、返回登录   \033[0m                            *\n");
        printf("*                    \033[1;37m 4、退出游戏   \033[0m                            *\n");
        printf("*                                                               *\n");
        printf("*****************************************************************\n");
        printf("\n\033[37m请输入您的选择!\033[0m \n");
        printf("——————————————————————————————————————————————————————————————————————\n");
        char choice;
        scanf("%c", &choice);
        while ((getchar()) != '\n')
            ;
        switch (choice)
        {
        case '1':
            Initplayer(player);
            InitAll(bloodpacks, monsters, treasures);
            start = true;
            *Start = true;
            break;
        case '2':
            if (ReadArchive(UserName, player, bloodpacks, monsters, treasures) == true)
            {
                start = true;
                *Start = true;
            }
            break;
        case '3':
            *Start = false;
            return;
        case '4':
            exit(0);
        default:
                     printf("\033[3;37m————>请重新输入!\033[0m \n");
            break;
        }
    }
}