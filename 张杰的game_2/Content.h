#ifndef __CONTENT_H__
#define __CONTENT_H__

#include "mystring.h"
#include "dongtaishuzu.h"
#define Goods_size 12
#define Map_Size 9

// 玩家
struct Player
{
    int HP;                         // 血量
    int Max_HP;                     // 最大血量
    int attack;                     // 攻击力
    int defense;                    // 防御力
    int crit;                       // 暴击率
    int experience;                 // 经验值
    int level;                      // 等级
    int x;                          // x坐标
    int y;                          // y坐标
    int money;                      // 摩拉
    int GoodsNum[Goods_size];       // 拥有物品数量
    char GoodsName[Goods_size][20]; // 物品名字
    int shovel;                     // 钥匙
};
// 血包
struct BloodPacks
{
    int blood;
    int gold;
    int x;
    int y;
};
// 野怪
struct Monster
{
    MyString name;
    int HP;
    int attack;
    int experience;
    int level;
    int x;
    int y;
};
// 宝箱
struct Treasure
{
    int value;
    int x;
    int y;
};
// 初始化玩家
void Initplayer(struct Player *player);
// 初始化玩家物品
void InitPlayerGoods(struct Player *player);
// 初始化血包
void InitBloodPacks(DTSZ *array);
// 初始化野怪
void InitMonsters(DTSZ *array);
// 初始化宝箱
void InitTreasures(DTSZ *array);
//初始化集合
void InitAll(DTSZ *bloodpacks, DTSZ *monsters, DTSZ *treasures);
// 创建玩家
void Createplayer(struct Player *player, int HP, int Max_HP, int attack, int defense, int crit, int experience, int level, int x, int y, int money, int shovel, int a[Goods_size]);
/* 创建血包 */
struct BloodPacks *CreateBloodPacks(int blood, int gold);
// 创建野怪
struct Monster *CreateMonster(const char *name, int HP, int attack, int experience);
// 创建宝箱
struct Treasure *CreateTreasure(int value);
/* 初始化地图 */
void Initboard(char (*p)[Map_Size]);
/* 打印地图 */
void PrintMap(char (*p)[Map_Size], struct Player *player, const char *name);
// 随机数
int GetRandNumber(int max);
// 释放
void FREE(DTSZ *bloodpacks, DTSZ *monsters, DTSZ *treasures);
/* 是否退出 */
int Qite();
/* 游戏 */
void PLAY(char (*board)[Map_Size], struct Player *player, DTSZ *bloodpacks, DTSZ *monsters, DTSZ *treasures, const char *UserName);
//下一层
void Next(struct Player *player, DTSZ *bloodpacks, DTSZ *monsters, DTSZ *treasures);
// 重新开始
void GaveAgain(const char *UserName, struct Player *player, DTSZ *bloodpacks, DTSZ *monsters, DTSZ *treasures, int *Start);
// 玩家操作
void MakeMove(struct Player *player, char symbol, DTSZ *bloodpacks, DTSZ *monsters, DTSZ *treasures, const char *name);

#endif